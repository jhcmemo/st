package pipimy.nearby;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ProductListSorter {

	// Hot
	public static ArrayList<ProductObject> getSortByHot(ArrayList<ProductObject> iList) {
		Collections.sort(iList, new Comparator<ProductObject>() {

			@Override
			public int compare(ProductObject lhs, ProductObject rhs) {
				return rhs.getFavorite() - lhs.getFavorite();
			}
		});
		return iList;
	}

	// Time
	public static ArrayList<ProductObject> getSortByTime(ArrayList<ProductObject> iList) {

		Collections.sort(iList, new Comparator<ProductObject>() {

			@Override
			public int compare(ProductObject lhs, ProductObject rhs) {
				return rhs.getTime().compareTo(lhs.getTime());
			}
		});
		return iList;
	}

	// Price
	public static ArrayList<ProductObject> getSortByPrice_L(ArrayList<ProductObject> iList) {

		Collections.sort(iList, new Comparator<ProductObject>() {

			@Override
			public int compare(ProductObject lhs, ProductObject rhs) {
				return (int) ((Double.valueOf(lhs.getPrice()) - Double.valueOf(rhs.getPrice())));
			}
		});
		return iList;
	}

	// Price
	public static ArrayList<ProductObject> getSortByPrice_H(ArrayList<ProductObject> iList) {

		Collections.sort(iList, new Comparator<ProductObject>() {

			@Override
			public int compare(ProductObject lhs, ProductObject rhs) {
				return (int) ((Double.valueOf(rhs.getPrice()) - Double.valueOf(lhs.getPrice())));
			}
		});
		return iList;
	}

	// Distance
	public static ArrayList<ProductObject> getSortByDistance(ArrayList<ProductObject> iList) {
		Collections.sort(iList, new Comparator<ProductObject>() {

			@Override
			public int compare(ProductObject lhs, ProductObject rhs) {
				return (int) (lhs.getDistance() * 100000 - rhs.getDistance() * 100000);
			}
		});
		return iList;
	}
}
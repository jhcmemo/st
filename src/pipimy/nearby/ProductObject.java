package pipimy.nearby;

import java.io.Serializable;

import pipimy.post.FoursquareObject;

public class ProductObject  implements Serializable{
	private String productID;
	private String ID;
	private String contact;
	private String title;
	private String type;
	private String price;
	private String des;
	private double lat;
	private double lng;
	private String time;
	private int favorite;
	private String CC;
	private int delete;
	private int picNumber;
	private int isBuyer;
	private int delivery;
	private int payment;
	private String brand;

	private int newold;
	private String sno;

	private FoursquareObject f1;
	private FoursquareObject f2;
	private FoursquareObject f3;
	private int order;
	private String productPicUrl;
	private String sellerPicUrl;
	private String stock="";
	private String hide;
	private String parentID;
	
	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}
	
	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getHide() {
		return hide;
	}

	public void setHide(String hide) {
		this.hide = hide;
	}

	public void setSellerPicUrl(String url) {
		this.sellerPicUrl = url;
	}
	
	public String getSellerPicUrl(){
		return this.sellerPicUrl;
	}
	
	public void setProductPicUrl(String url){
		this.productPicUrl = url;
	}
	
	public String getProductPicUrl(){
		return productPicUrl;
	}

	public int getIsBuyer() {
		return isBuyer;
	}

	public void setIsBuyer(int isBuyer) {
		this.isBuyer = isBuyer;
	}

	public int getDelete() {
		return delete;
	}

	public void setDelete(int delete) {
		this.delete = delete;
	}

	public String getCC() {
		return CC;
	}

	public void setCC(String cC) {
		CC = cC;
	}

	public int getPicNumber() {
		return picNumber;
	}

	public void setPicNumber(int picNumber) {
		this.picNumber = picNumber;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	private float distance;

	public ProductObject() {

	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getFavorite() {
		return favorite;
	}

	public void setFavorite(int favorite) {
		this.favorite = favorite;
	}

	public int getDelivery() {
		return delivery;
	}

	public void setDelivery(int delivery) {
		this.delivery = delivery;
	}
	
	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;
	}
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public int getNewOld() {
		return newold;
	}

	public void setNewOld(int newold) {
		this.newold = newold;
	}
	
	
	
	public String getSNO() {
		return sno;
	}

	public void setSNO(String sno) {
		this.sno = sno;
	}
	
	
	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}
	
	

	public FoursquareObject getF1() {
		return f1;
	}

	public void setF1(FoursquareObject f1) {
		this.f1 = f1;
	}

	public FoursquareObject getF2() {
		return f2;
	}

	public void setF2(FoursquareObject f2) {
		this.f2 = f2;
	}

	public FoursquareObject getF3() {
		return f3;
	}

	public void setF3(FoursquareObject f3) {
		this.f3 = f3;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
}

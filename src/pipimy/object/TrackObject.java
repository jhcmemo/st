package pipimy.object;

public class TrackObject {
	private String mMemberID;
	private String mTime;
	private int [] pic = new int[3];
	private String isTrackingMsg;
	//private int pic2;
	//private int pic3;

	public String getIsTrackingMsg() {
		return isTrackingMsg;
	}

	public void setIsTrackingMsg(String isTrackingMsg) {
		this.isTrackingMsg = isTrackingMsg;
	}

	public String getMemberID() {
		return mMemberID;
	}

	public void setMemberID(String memberID) {
		this.mMemberID = memberID;
	}
	
	public String getTime() {
		return this.mTime;
	}
	
	public void setTime(String time) {
		mTime = time;
	}
	
	
	/*get product pic**/
	public void setProduct_pic(int[] pic) {
		this.pic = pic;
	}
	/*public void setProduct_pic2(int pic2) {
		this.pic2 = pic2;
	}
	public void setProduct_pic3(int pic3) {
		this.pic3 = pic3;
	}*/
	
	public int [] getPic() {
		return this.pic;
	}
	/*public int getPic2() {
		return this.pic2;
	}
	public int getPic3() {
		return this.pic3;
	}*/
	
	
}

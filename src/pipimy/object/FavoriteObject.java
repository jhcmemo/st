package pipimy.object;

import java.io.Serializable;

public class FavoriteObject implements Serializable{

	private String productID;
	private String productTitle;
	private String memberID;
	private String contact;
	private String time;
	private String type;
	private String price;
	private String stock;
	private String des;
	
	
	private String brand;
	private int newold;
	private String sno;

	private int payment;
	private int delievery;
	private double lat;
	private double lng;
	private float distance;

	
	private int picNumber;


	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	
	public void setPicNumber(int picNumber) {
		this.picNumber = picNumber;
	}
	public int  getpicNumber() {
		return picNumber;
	}
	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getPrice() {
		return price;
	}
	

	public void setPrice(String price) {
		this.price = price;
	}
	
	
	public float getDistance() {
		return distance;
	}
	

	public void setDistance(float distance) {
		this.distance = distance;
	}
	

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;
	}
	
	public int getDelievery() {
		return delievery;
	}

	public void setDelievery(int delievery) {
		this.delievery = delievery;
	}
	
	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	
	
	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock ;
	}
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public int getNewOld() {
		return newold;
	}

	public void setNewOld(int newold) {
		this.newold = newold;
	}
	
	
	
	public String getSNO() {
		return sno;
	}

	public void setSNO(String sno) {
		this.sno = sno;
	}
}

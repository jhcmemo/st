package pipimy.object;

public class SomeoneRatingObject {

	private String memberID;
	private int count;
	private double average;
	private String dealTimes;
	private String registerTime;
	private int openTimes;
	private String lastOpenTime;

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public String getDealTimes() {
		return dealTimes;
	}

	public void setDealTimes(String dealTimes) {
		this.dealTimes = dealTimes;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}

	public int getOpenTimes() {
		return openTimes;
	}

	public void setOpenTimes(int openTimes) {
		this.openTimes = openTimes;
	}

	public String getLastOpenTime() {
		return lastOpenTime;
	}

	public void setLastOpenTime(String lastOpenTime) {
		this.lastOpenTime = lastOpenTime;
	}

}

package pipimy.object;

public class MyDealObject {

	private String userIDFrom;
	private String userIDTo;
	private String productId;
	private String productTitle;
	private String productPrice;
	private String time;
	private int ratingScore;
	private String ratingComment;
	private int isBuyer;

	public int getIsBuyer() {
		return isBuyer;
	}

	public void setIsBuyer(int isBuyer) {
		this.isBuyer = isBuyer;
	}

	public String getUserIDFrom() {
		return userIDFrom;
	}

	public void setUserIDFrom(String userIDFrom) {
		this.userIDFrom = userIDFrom;
	}

	public String getUserIDTo() {
		return userIDTo;
	}

	public void setUserIDTo(String userIDTo) {
		this.userIDTo = userIDTo;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}

	public String getRatingComment() {
		return ratingComment;
	}

	public void setRatingComment(String ratingComment) {
		this.ratingComment = ratingComment;
	}

}

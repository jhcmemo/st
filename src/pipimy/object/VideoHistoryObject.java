package pipimy.object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

public class VideoHistoryObject {
	public static final String VIDEO_LIST = "videoList";
	public static final String PrefrenceTag = "Preference";
	
	private String fileName;
	private String createTime;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public static void addVideoHistory(Activity activity,VideoHistoryObject historyObject) {
		List<VideoHistoryObject> videoList = getVideoHistory(activity);
		if (videoList == null)
			videoList = new ArrayList<VideoHistoryObject>();

		boolean isSame = false;
		for (int i = 0; i < videoList.size(); i++) {
			if (videoList.get(i).getFileName()
					.equals(historyObject.getFileName())) {
				isSame = true;
			}
		}
		if (!isSame) {
			videoList.add(historyObject);
			saveVideoHistory(activity, videoList);
		}
	}
	
	public static ArrayList<VideoHistoryObject> getVideoHistory(Activity activity) {
		SharedPreferences settings;
		List<VideoHistoryObject> videoList;

		settings = activity.getSharedPreferences(PrefrenceTag, 0);

		if (settings.contains(VIDEO_LIST)) {
			String jsonVideoList = settings.getString(VIDEO_LIST, null);
			Gson gson = new Gson();
			VideoHistoryObject[] VideoHistoryItems = gson.fromJson(
					jsonVideoList, VideoHistoryObject[].class);

			videoList = Arrays.asList(VideoHistoryItems);
			videoList = new ArrayList<VideoHistoryObject>(videoList);
		} else
			return new ArrayList<VideoHistoryObject>();

		return (ArrayList<VideoHistoryObject>) videoList;
	}
	
	public static void saveVideoHistory(Activity activity,
			List<VideoHistoryObject> videoList) {

		SharedPreferences settings;
		Editor editor;

		settings = activity.getSharedPreferences(PrefrenceTag, 0);
		editor = settings.edit();

		Gson gson = new Gson();
		String jsonVideoList = gson.toJson(videoList);

		editor.putString(VIDEO_LIST, jsonVideoList);

		editor.commit();
	}
	
	public static void removeVideoHistory(Activity activity, int count) {
		ArrayList<VideoHistoryObject> videoList = getVideoHistory(activity);
		if (videoList != null) {

			if (count <= videoList.size()) {
				for (int i = 0; i < count; i++) {
					videoList.remove(0);
				}
			}
			saveVideoHistory(activity, videoList);
		}
	}
}

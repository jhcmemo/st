package pipimy.object;

public class ProductRatingDesObject {

	private int count;
	private double average;
	private String dealTimes;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public String getDealTimes() {
		return dealTimes;
	}

	public void setDealTimes(String dealTimes) {
		this.dealTimes = dealTimes;
	}

}

package pipimy.object;

import android.os.Parcel;
import android.os.Parcelable;

public class CartItemsObject implements Parcelable{

	String productID;
	String productSellerID;
	String productTitle;
	String cashType;
	String deliveryType;
	String productDes;
	String productType;
	String productPrice;
	String postTime;
	int favoriteCount;
	String productPicUrl;
	String displayType;
	String productStock;
	int numberToBuy;
	boolean isChecked;
	String isHide;
	
	

	

	public CartItemsObject(){
		
	}
	
	CartItemsObject(Parcel p) {
		productID = p.readString();
		productSellerID = p.readString();
		productTitle = p.readString();
		cashType = p.readString();
		deliveryType = p.readString();
		productDes = p.readString();
		productType = p.readString();
		productPrice = p.readString();
		postTime = p.readString();
		productPicUrl = p.readString();
		displayType = p.readString();
		productStock = p.readString();
		favoriteCount = p.readInt();
		numberToBuy = p.readInt();
	}
	


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		// TODO Auto-generated method stub
		dest.writeString(productID);
		dest.writeString(productSellerID);
		dest.writeString(productTitle);
		dest.writeString(cashType);
		dest.writeString(deliveryType);
		dest.writeString(productDes);
		dest.writeString(productType);
		dest.writeString(productPrice);
		dest.writeString(postTime);
		dest.writeString(productPicUrl);
		dest.writeString(displayType);
		dest.writeString(productStock);
		dest.writeInt(favoriteCount);
		dest.writeInt(numberToBuy);
	}
	
	public static final Parcelable.Creator<CartItemsObject> CREATOR = 
			new Parcelable.Creator<CartItemsObject>() {
		public CartItemsObject createFromParcel(Parcel p) {
			return new CartItemsObject(p);
		}

		@Override
		public CartItemsObject[] newArray(int size) {
			// TODO Auto-generated method stub
			return new CartItemsObject[size];
		}
	};
	
	public String getIsHide() {
		return isHide;
	}

	public void setIsHide(String isHide) {
		this.isHide = isHide;
	}
	
	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	
	public int getNumberToBuy() {
		return numberToBuy;
	}

	public void setNumberToBuy(int numberToBuy) {
		this.numberToBuy = numberToBuy;
	}

	public void setDeliveryType(String type) {
		this.deliveryType = type;
	}
	
	public String getDeliveryType(){
		return this.deliveryType;
	}
	
	public void setProductStock(String stock) {
		this.productStock = stock;
	}
	
	public String getProductStock(){
		return this.productStock;
	}
	
	public void setDisplayType(String type) {
		this.displayType = type;
	}
	
	public String displayType(){
		return displayType;
	}
	
	public void setProductPicUrl(String url) {
		this.productPicUrl = url;
	}
	
	public String getProductPicUrl(){
		return productPicUrl;
	}
	
	public void setFavoriteCount(int count) {
		this.favoriteCount = count;
	}
	
	public int getFavoriteCount(){
		return favoriteCount;
	}
	
	public void setPostTime(String time) {
		this.postTime = time;
	}
	
	public String getPostTime(){
		return postTime;
	}
	
	public void setProductPrice(String price) {
		this.productPrice = price;
	}
	
	public String getPrice(){
		return productPrice;
	}
	
	public void setProductType(String type) {
		this.productType = type;
	}
	
	public String getProductType(){
		return productType;
	}
	
	public void setProductDes(String des) {
		this.productDes = des;
	}
	
	public String getDes(){
		return productDes;
	}
	
	public void setCashType(String type){
		this.cashType = type;
	}
	
	public String getCashType(){
		return cashType;
	}
	
	public void setProductTitle(String title) {
		this.productTitle = title;
	}
	
	public String getTitle(){
		return productTitle;
	}
	
	public void setSellerID(String sellerid) {
		this.productSellerID = sellerid;
	}
	
	public String getSellerID(){
		return productSellerID;
	}
	
	public void setProductID(String productid) {
		this.productID = productid;
	}
	
	public String getProductID(){
		return productID;
	}

}

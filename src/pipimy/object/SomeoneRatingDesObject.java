package pipimy.object;

public class SomeoneRatingDesObject {

	private String userIdFrom;
	private int score;
	private String comment;
	private String ratingTime;
	private String dealTime;

	public String getUserIdFrom() {
		return userIdFrom;
	}

	public void setUserIdFrom(String userIdFrom) {
		this.userIdFrom = userIdFrom;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getRatingTime() {
		return ratingTime;
	}

	public void setRatingTime(String ratingTime) {
		this.ratingTime = ratingTime;
	}

	public String getDealTime() {
		return dealTime;
	}

	public void setDealTime(String dealTime) {
		this.dealTime = dealTime;
	}

}

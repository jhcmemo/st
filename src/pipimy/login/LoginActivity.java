package pipimy.login;

import pipimy.service.R;
import pipimy.others.FragmentManagerUtil;
//import pipimy.others.LocationInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends FragmentActivity
{
	// for ActionBar
	public TextView actionBar_titleTextView;
	public Button actionBar_backButton;
	
	public String user_mobileNumber;
	public String user_password;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		actionBar_titleTextView = (TextView) findViewById(R.id.Login_ActionBar_title);
		actionBar_backButton = (Button) findViewById(R.id.Login_ActionBar_back);
		
		actionBar_titleTextView.setText(getResources().getString(R.string.login_login));
		actionBar_backButton.setOnClickListener(backButtonClickListener);
		FragmentManagerUtil.init(savedInstanceState, getSupportFragmentManager(), R.id.LoginActivity_frameLayout, new LoginFragment());
	
	}
	
	/*public void login_callBack(){
		finish();
		
	}*/
	
	private OnClickListener backButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
				FragmentManagerUtil.popBackStackFragment(getSupportFragmentManager());
			} else {
				finish();
			}
		}
	};
}

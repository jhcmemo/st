package pipimy.login;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import pipimy.others.*;
import pipimy.service.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CertificationFragment extends Fragment {

	private LoginActivity activity;
	private Button submitButton;
	private EditText inputEditText;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_certification, container, false);
		findViews(rootView);
		init();
		return rootView;
	}

	/** =============================================================== */
	/** ============================ init ============================= */
	/** =============================================================== */

	private void findViews(View rootView) {
		submitButton = (Button) rootView.findViewById(R.id.Certification_submitButton);
		inputEditText = (EditText) rootView.findViewById(R.id.Certification_inputEditText);
		TextView hintTextView = (TextView) rootView.findViewById(R.id.Certification_hintTextView);
		hintTextView.setText(getString(R.string.certification_hint) + ": " + PreferenceUtil.getString(activity, Constant.USER_MOBILE_NUMBER));
		submitButton.setOnClickListener(submitClickListener);
	}

	private void init() {
		activity = (LoginActivity) getActivity();
	}

	private OnClickListener submitClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String EmailCode = Util.getEditText(inputEditText);
			if (EmailCode.equals("")) {
				Toast.makeText(activity, activity.getString(R.string.certification_title), Toast.LENGTH_SHORT).show();
			} else {
				HTTP_checkEmailCode(EmailCode);
			}
		}
	};

	/** =============================================================== */
	/** ======================== HTTP Command ========================= */
	/** =============================================================== */

	private void HTTP_checkEmailCode(String emailCode) {
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("Email", PreferenceUtil.getString(activity, Constant.USER_MOBILE_NUMBER)));
		params.add(new BasicNameValuePair("EmailCode", emailCode));
		///////activity.startHttpPost(GlobalVariable.CHECK_EMAIL_CODE, true, null, activity.getString(R.string.loading),
				//params, null, null);
	}
}

package pipimy.login;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;

import pipimy.main.ProductFragment;
import pipimy.others.AES;
import pipimy.others.Constant;
import pipimy.others.FragmentManagerUtil;
import pipimy.others.Http;
import pipimy.others.LibBase64;
import pipimy.others.Util;
import pipimy.service.R;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AllPayFragment extends Fragment implements Runnable {

	int pipi_id = 0;
	String cell_phone = "";

	WebView wv_allpay;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		pipi_id = getArguments().getInt("PIPI_ID");
		cell_phone = getArguments().getString("CellPhone");

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//setHasOptionsMenu(true);

		return inflater.inflate(R.layout.fragment_allpay, container, false);

	}

	@SuppressLint("SetJavaScriptEnabled")
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Util.showProgressDialog(getActivity(), "Loading...", "Please wait...");

		wv_allpay = (WebView) view.findViewById(R.id.wv_allpay);
		wv_allpay.getSettings().setJavaScriptEnabled(true);
		// wv_allpay.getSettings().setAllowContentAccess(true);
		// wv_allpay.getSettings().setAllowFileAccess(true);
		// wv_allpay.getSettings().setAppCacheEnabled(true);
		// wv_allpay.getSettings().setBlockNetworkImage(true);
		//wv_allpay.getSettings().setBlockNetworkLoads(true);
		// wv_allpay.getSettings().setSaveFormData(true);
		// wv_allpay.getSettings().setDomStorageEnabled(true);
		
		  wv_allpay.setWebViewClient(new WebViewClient()
		    {
		       

		        @Override
		        public void onPageFinished(WebView view, String url)
		        {
		            // TODO Auto-generated method stub
		            super.onPageFinished(view, url);

					Util.dismissProgressDialog();
           Log.d("allpay","finish !");

		        }
		        @Override
		        public boolean shouldOverrideUrlLoading(WebView view, String url) {
		           // Here put your code
		         //     Log.d("allpay", "url: "+url);
                           if(url.substring(12, 16).equals("Bind")){
                        	  String code = url.substring(url.indexOf(":")+8, url.length());
         		             String result=AES.DecryptAESToString(code);
                        	//  Log.d("allpay", "catch code:"+code);
         		           //   Log.d("allpay", "catch code dec:"+AES.DecryptAESToString(code));

      						int ResultCode;
							try {
								ResultCode = new JSONObject(result).getInt("ResultCode");
							
      						if(ResultCode==1){
      							Bundle b =new Bundle();
      							Message m =new Message();
      						   b.putBoolean("State", true);
  							    m.setData(b);
  							  handler_state.sendMessage(m);
	
      						}else{
      							Bundle b =new Bundle();
      							Message m =new Message();
      						    b.putBoolean("State", false);
  							    m.setData(b);
  							  handler_state.sendMessage(m);
      						}
      						
							} 

      						catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
                           }
		           // return true; //Indicates WebView to NOT load the url;
		              return false; //Allow WebView to load url
		        }

		     
		    }); 

		
		 String data=""; try { JSONObject jsonObject = new JSONObject();
		 jsonObject.put("AppCode", 123); 
		 jsonObject.put("ID", pipi_id);
		 jsonObject.put("CellPhone", cell_phone); 
		 jsonObject.put("TimeStamp",(System.currentTimeMillis()/ 1000L));
		  
		  
		  Log.d("allpay","json :"+ jsonObject.toString());
		 
		  data=AES.EncryptAESToHex(jsonObject.toString().trim()); } catch
		  (JSONException e) { // TODO Auto-generated catch block
		  e.printStackTrace(); }
		 
 
		 String postData = "PlatformID=1084329&PlatformData="+data.trim();
		 byte[] post_data = EncodingUtils.getBytes(postData, "BASE64");
		wv_allpay.postUrl(Constant.AllPayURL,post_data);
		 Log.d("allpay","postData :"+postData);
		
		//new Thread(this).start();

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.gc();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();

		Drawable colorDrawable = new ColorDrawable(getResources().getColor(
				R.color.style_red));
		Drawable bottomDrawable = getResources().getDrawable(
				R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable,
				bottomDrawable });
		getActivity().getActionBar().setBackgroundDrawable(ld);
		getActivity().getActionBar().setTitle(
				getString(R.string.register_verify));
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			FragmentManagerUtil.popBackStackFragment(getFragmentManager());
			// FragmentManagerUtil.replaceFragment(getFragmentManager(), new
			// LoginFragment(), R.id.LoginActivity_frameLayout);
		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	public void run() {

		
	}

	Handler handler_state = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			boolean state = msg.getData().getBoolean("State");
  
			if(state){
				
				Toast.makeText(getActivity(), getResources().getString(R.string.verify_success), 2000).show();
			
			//	FragmentManagerUtil.removeFragment(getFragmentManager(), AllPayFragment.this);
						
				
				FragmentManagerUtil.addToBackStackFragment(getFragmentManager(),
						new LoginFragment(), R.id.LoginActivity_frameLayout);
			}else{
				
				Toast.makeText(getActivity(), getResources().getString(R.string.verify_failed), 2000).show();

			}
		

		}
	};

}

package pipimy.login;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.DialogListUtil.OnAgeClickListener;
import pipimy.others.FragmentManagerUtil;
import pipimy.others.Http;
import pipimy.others.LibBase64;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.service.R;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterFragment extends Fragment {

	// Views
//	private RelativeLayout maleButton;
	//private RelativeLayout femaleButton;

	//private EditText ageEditText;

	private View registerButton;

	private EditText IDEditText;
	private EditText phoneEditText;
	private EditText passwordEditText;
	private EditText repeatPasswordEditText;

	// Date
	private LoginActivity activity;
    private  TextView tv_check_id;
	// Params
	private String user_ID = "";
	private String user_phone = "";
	private String user_Pwd = "";
	private String user_rpPwd = "";
	//private String user_Sex = "";
	//private String user_Age = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_register, container, false);
		findViews(rootView);
		init();
		
		setHasOptionsMenu(true);
		return rootView;
	}

	/** =============================================================== */
	/** ============================ init ============================= */
	/** =============================================================== */

	private void findViews(View rootView) {
	//	maleButton = (RelativeLayout) rootView.findViewById(R.id.Register_maleButton);
	//	femaleButton = (RelativeLayout) rootView.findViewById(R.id.Register_femaleButton);
		registerButton = (View) rootView.findViewById(R.id.Register_registerLayout);
		tv_check_id    = (TextView) rootView.findViewById(R.id.tv_check_id);
		IDEditText = (EditText) rootView.findViewById(R.id.Register_IDEditText);
		phoneEditText = (EditText) rootView.findViewById(R.id.Register_PhoneEditText);
		passwordEditText = (EditText) rootView.findViewById(R.id.Register_PasswordEditText);
		repeatPasswordEditText = (EditText) rootView.findViewById(R.id.Register_RepeatPasswordEditText);
		
		
		//ageEditText = (EditText) rootView.findViewById(R.id.Register_ageEditText);

	//	maleButton.setOnClickListener(sexClickListener);
	//	femaleButton.setOnClickListener(sexClickListener);
		IDEditText.addTextChangedListener(check_id);
		registerButton.setOnClickListener(registerOnClickListener);
	 //    ageEditText.setOnClickListener(ageOnClickListener);
	}

	private void init() {
		SetNowCity();
		
		activity = (LoginActivity) getActivity();
		activity.actionBar_titleTextView.setText(activity.getString(R.string.register_register));

		/*----- facebook registered -----*/

		// get Email
		if (activity.getIntent().getStringExtra("user_phone") != null) {
			user_phone = activity.getIntent().getStringExtra("user_phone");
			//emailEditText.setText(user_phone);
			phoneEditText.setEnabled(false);
		}

		// get gender
		/*if (activity.getIntent().getStringExtra("user_gender") != null) {
			user_Sex = activity.getIntent().getStringExtra("user_gender");
			if (user_Sex.equals("male")) {
			//	user_Sex = "3";
			//	maleButton.setBackgroundResource(R.drawable.btn_on_register);
			//	femaleButton.setBackgroundResource(R.drawable.btn_off_register);

			} else if (user_Sex.equals("female")) {
				//maleButton.setBackgroundResource(R.drawable.btn_off_register);
			//	femaleButton.setBackgroundResource(R.drawable.btn_on_register);
			//	user_Sex = "4";
			}
			// maleButton.setClickable(false);
			// femaleButton.setClickable(false);
			//maleButton.setVisibility(View.GONE);
		//	femaleButton.setVisibility(View.GONE);
		}*/

		// get userID
		if (activity.getIntent().getStringExtra("user_id") != null) {
			user_Pwd = activity.getIntent().getStringExtra("user_id");
			passwordEditText.setText(user_Pwd);
			repeatPasswordEditText.setText(user_Pwd);
			// passwordEditText.setEnabled(false);
			// repeatPasswordEditText.setEnabled(false);
			passwordEditText.setVisibility(View.GONE);
			repeatPasswordEditText.setVisibility(View.GONE);
		}

	}

	/** =============================================================== */
	/** =========================== Actionbar ========================= */
	/** =============================================================== */

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		super.onCreateOptionsMenu(menu, inflater);

		//Drawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.style_red));
	//	Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		//LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
	//	getActivity().getActionBar().setBackgroundDrawable(ld);
		getActivity().getActionBar().setTitle(getString(R.string.register_register));
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
		//	FragmentManagerUtil.popBackStackFragment(getFragmentManager());
			getActivity().finish();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/** =============================================================== */
	/** ======================= UI even Callback ====================== */
	/** =============================================================== */


	/*private OnClickListener ageOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			DialogListUtil.showAgeDialog(activity, ageClickListener);
		}
	};*/

	/*private OnAgeClickListener ageClickListener = new OnAgeClickListener() {

		@Override
		public void onAgeClick(String age) {
			user_Age = age;
			ageEditText.setText(age);
		}
	};*/
	
	
	private void SetNowCity(){	
		
		new Thread() {
			@Override
			public void run() {
			
				Http.setLocation(Double.valueOf(PreferenceUtil.getString(
						getActivity(),
						Constant.USER_LAT)), Double
						.valueOf(PreferenceUtil.getString(
								getActivity(),
								Constant.USER_LNG)));


			}

		}.start();
		
		
		
		
	}
	

	private TextWatcher check_id = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
            if( Util.getEditText(IDEditText).equals("")){
            	tv_check_id.setText("");
            }
            else{
            	
    			CheckID();
	
            }
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
		}

	};
	private OnClickListener registerOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if (checkParams()) {
				
				Util.showProgressDialog(activity, "Regist...", "Please wait...");

				HTTP_register();
				
				
			}
			
			
			
		}
	};

	/*private OnClickListener sexClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if (v.getId() == maleButton.getId()) {
				maleButton.setBackgroundResource(R.drawable.btn_on_register);
				femaleButton.setBackgroundResource(R.drawable.btn_off_register);
				user_Sex = "1";
			} else {
				maleButton.setBackgroundResource(R.drawable.btn_off_register);
				femaleButton.setBackgroundResource(R.drawable.btn_on_register);
				user_Sex = "2";
			}
		}
	};*/

	/** =============================================================== */
	/** ======================== HTTP Command ========================= */
	/** =============================================================== */

	private void CheckID(){
		
		
		new Thread() {			
			@Override
			public void run() {
try{ 
		String[] params = { "ID"};
				
		String[] data = {LibBase64.encodeString( Util.getEditText(IDEditText))};
				
				String result = Http.post(Constant.CHECK_ID, params,
						data, getActivity()); 
			Log.d("regg","reg result:"+result);
		
				
				if(result.contains("success")){
					
					Message msg = new Message();
					msg.arg1 = 1;				  
					handler_checkID.sendMessage(msg);
					
			  	}
				else if(result.contains("sameID")){
					Message msg = new Message();
					msg.arg1 = 0;				  
					handler_checkID.sendMessage(msg);
				}else{
					
				}
				
}catch(UnsupportedEncodingException e){


}	
						
				
			}

		}.start();
	
		
	}
	
	
	
	private void HTTP_register() {

		new Thread() {			
				@Override
				public void run() {
try{ 
			String[] params = { "ID","Mobile","Pwd","City"};
					
			String[] data = {LibBase64.encodeString(user_ID), LibBase64.encodeString(user_phone)
					,LibBase64.encodeString(user_Pwd),"Taipei"};
					Log.d("regg","userID: "+LibBase64.encodeString(user_ID));
					Log.d("regg","userPHONE: "+LibBase64.encodeString(user_phone));
					Log.d("regg","userPWD: "+LibBase64.encodeString(user_Pwd));
					//Log.d("regg","CITY: "+Constant.NowCity);
Log.d("regg","Constant.REGISTER_URL :"+Constant.REGISTER_URL);
					String result = Http.post(Constant.REGISTER_URL, params,
							data, getActivity()); 
				Log.d("regg","reg result:"+result);
				Bundle b =new Bundle();
				Message m =new Message();
					
					if(result.contains("success")){
						
						
						try {
						int PIPI_ID= new JSONObject(result).getInt("pipimy_id");
						    b.putString("State", "success");
							b.putInt("pipi_id", PIPI_ID);
							 m.setData(b);
							handler_allpay.sendMessage(m);
						
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							
							  b.putString("State", "same_id");
								 m.setData(b);

								handler_allpay.sendMessage(m);	
							e.printStackTrace();
							
						}
					
							}
					
					else if(result.contains("empty")){
						b.putString("State", "empty");
                         //	b.putInt("pipi_id", PIPI_ID);
						 m.setData(b);

						handler_allpay.sendMessage(m);
						
					}
					else if(result.contains("IDFormatWrong")){
						
						
					    b.putString("State", "format_wrong");
						 m.setData(b);

						handler_allpay.sendMessage(m);
						
					}
					
					
					else{
						Log.d("regg","註冊 ELSE");
						
						
						b.putString("State", "else");
						//	b.putInt("pipi_id", PIPI_ID);
							 m.setData(b);

							handler_allpay.sendMessage(m);
					  
						
						
					}
					
}catch(UnsupportedEncodingException e){
	
	
}	
							
					
				}

			}.start();
		
		
		
		
		///////activity.startHttpPost(Constant.REGISTER_URL, true, null, activity.getString(R.string.loading), params,
				//null, null);
	}

	private boolean checkParams() {
		user_ID = Util.getEditText(IDEditText);
		user_phone = Util.getEditText(phoneEditText);
		user_Pwd = Util.getEditText(passwordEditText);
		user_rpPwd = Util.getEditText(repeatPasswordEditText);

		if (user_ID.equals("") ) {
			Toast.makeText(activity, activity.getString(R.string.register_checkID), Toast.LENGTH_SHORT).show();
		
		} 
		else if (user_ID.length()<=3 ) {
			Toast.makeText(activity, activity.getString(R.string.register_checkID2), Toast.LENGTH_SHORT).show();
	
		}
       else if (user_ID.length()>12 ) {
			Toast.makeText(activity, activity.getString(R.string.register_checkID3), Toast.LENGTH_SHORT).show();

		}
		else if (user_phone.equals("") ) {
			Toast.makeText(activity, activity.getString(R.string.register_checkPhone), Toast.LENGTH_SHORT).show();
		} else if (user_Pwd.equals("")) {
			Toast.makeText(activity, activity.getString(R.string.register_checkPwd), Toast.LENGTH_SHORT).show();
		} else if (user_rpPwd.equals("")) {
			Toast.makeText(activity, activity.getString(R.string.register_checkRpPwd), Toast.LENGTH_SHORT).show();
		} else if (!user_Pwd.equals(user_rpPwd)) {
			Toast.makeText(activity, activity.getString(R.string.register_checkPwdMatch), Toast.LENGTH_SHORT).show();
		} 
		
		/*else if (user_Sex.equals("")) {
			Toast.makeText(activity, activity.getString(R.string.register_checkSex), Toast.LENGTH_SHORT).show();
		}*/
		
		/*else if (user_Age.equals("")) {
			Toast.makeText(activity, activity.getString(R.string.register_checkAge), Toast.LENGTH_SHORT).show();
		} 
		*/
		
		else {
			return true;
		}
		return false;
	}
	Handler handler_checkID = new Handler() {
		@Override
		public void handleMessage(Message msg) {
	
			switch (msg.arg1) {
			case 0:
				
				tv_check_id.setText(getString(R.string.register_same_id));
				tv_check_id.setTextColor(Color.parseColor("#ffea7a7a"));
				
				break;
				
            case 1:
				
            	tv_check_id.setText(getString(R.string.register_different_id));
    			tv_check_id.setTextColor(Color.parseColor("#ff40cca7"));
            	
				break;
				
				
			}
		
		}
			
			
	};
	
	Handler handler_allpay = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Log.d("regist","收到all pay");
			Util.dismissProgressDialog();
			
			String state =msg.getData().getString("State");
				
			if(state.equals("success")){
				int pipi_id =msg.getData().getInt("pipi_id");

			AllPayFragment fragment = new AllPayFragment();
			FragmentTransaction ft = getFragmentManager().beginTransaction();	    
			ft.replace(R.id.LoginActivity_frameLayout, fragment);
			//ft.addToBackStack(null);
	        Bundle bundle = new Bundle();
	        bundle.putInt("PIPI_ID", pipi_id);
	        bundle.putString("CellPhone", user_phone);

	        fragment.setArguments(bundle);
	        ft.commit();
			}
			else if(state.equals("same_id")){
				
				/*int pipi_id =msg.getData().getInt("pipi_id");
				Toast.makeText(getActivity(), "Have same ID !", 2000).show();
				Log.d("regg","pipi_id :"+pipi_id);
				*/
				
				Toast.makeText(getActivity(), getString(R.string.register_same_id), 2000).show();
	
				
			}
          else if(state.equals("format_wrong")){
				
				/*int pipi_id =msg.getData().getInt("pipi_id");
				Toast.makeText(getActivity(), "Have same ID !", 2000).show();
				Log.d("regg","pipi_id :"+pipi_id);
				*/
				
				Toast.makeText(getActivity(), getString(R.string.format_wrong), 2000).show();
	
				
			}
          else{
        	  
			Toast.makeText(getActivity(),getString(R.string.toast_failed), 2000).show();

          }
			
			
		}
	};
}

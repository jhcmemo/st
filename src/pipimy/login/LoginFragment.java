package pipimy.login;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.GCMRegistrar;

import pipimy.service.R;
import pipimy.setting.activity.MyDealActivity;
import pipimy.main.SettingFragment;
import pipimy.others.Constant;
import pipimy.others.Cookie;
import pipimy.others.FragmentManagerUtil;
import pipimy.others.Http;
import pipimy.others.LibBase64;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginFragment extends Fragment {

	private View loginView;
	private View registerView;
	// private TextView forgetPWDTextView;
	private TextView feedbackTextView;
	private EditText userIDEditText;
	private EditText passwordEditText;
	private String mAndroidToken;
	
	private Handler scanHandler = new Handler();
	private static LoginActivity activity;
	private static String loginResult;
	private final static int UPDATE_ANDROID_TOKEN = 10001;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login, container,
				false);
		activity = (LoginActivity) getActivity();
		activity.actionBar_titleTextView.setText(activity
				.getString(R.string.login_login));
		mAndroidToken = GCMRegistrar.getRegistrationId(activity);

		loginView = (View) rootView.findViewById(R.id.Login_loginLayou);
		registerView = (View) rootView.findViewById(R.id.Login_registerLayou);
		// forgetPWDTextView = (TextView)
		// rootView.findViewById(R.id.Login_forgetTextView);
		feedbackTextView = (TextView) rootView
				.findViewById(R.id.Login_feedbackdTextView);
		userIDEditText = (EditText) rootView
				.findViewById(R.id.Login_EmailorIDEditText);
		passwordEditText = (EditText) rootView
				.findViewById(R.id.Login_passwordEditText);

		loginView.setOnClickListener(new ButtonListenerLogin());
		registerView.setOnClickListener(registerClickListener);
		feedbackTextView.setOnClickListener(feedbackClickListener);

		if (PreferenceUtil.getString(activity, Constant.USER_MOBILE_NUMBER)
				.length() > 0) {
			userIDEditText.setText(PreferenceUtil.getString(activity,
					Constant.USER_ID));
		}

		setHasOptionsMenu(true);
		
		prepareToLogin();
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/** =============================================================== */
	/** =========================== Actionbar ========================= */
	/** =============================================================== */

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();

		//Drawable colorDrawable = new ColorDrawable(getResources().getColor(
		//		R.color.style_red));
		//Drawable bottomDrawable = getResources().getDrawable(
		//		R.drawable.actionbar_bottom);
	//	LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable,
		//		bottomDrawable });
		//getActivity().getActionBar().setBackgroundDrawable(ld);
		getActivity().getActionBar().setTitle(getString(R.string.login_login));
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			activity.finish();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/** =============================================================== */
	/** =========================== Login ============================= */
	/** =============================================================== */
	
	private void prepareToLogin() {
		Util.showProgressDialog(getActivity(), "", "");
		scanHandler.postDelayed(gcmRegistered, 0);
	}
	
	private Runnable gcmRegistered = new Runnable() {

		@Override
		public void run() {
			if(Constant.isGCMRegistered) {
				Util.dismissProgressDialog();
				scanHandler.removeCallbacks(gcmRegistered);
			} else {
				scanHandler.postDelayed(gcmRegistered, 300);
			}
		}
		
	};

	class ButtonListenerLogin implements OnClickListener, Runnable {
		@Override
		public void onClick(View v) {

			if (Util.getEditText(userIDEditText).length() <= 0) {
				//Log.d("logingg","尚未輸入ID");
				Util.showOKAlertDialog(activity, activity.getString(R.string.login_check_id),
						activity.getString(R.string.plz_enter_id));
			} else if (Util.getEditText(passwordEditText).length() <= 0) {
			//	Log.d("logingg","尚未輸入密碼");

				Util.showOKAlertDialog(activity, activity.getString(R.string.login_check_pwd),
						activity.getString(R.string.plz_enter_pwd));
			} else {
				Util.showProgressDialog(activity, activity.getString(R.string.loging), activity.getString(R.string.post_plz_wait));

				new Thread(this).start();
			}
		}

		@Override
		public void run() {
			try {
				String[] parameter = new String[3];
				String[] data = new String[3];
				parameter[0] = "ID";
				data[0] = LibBase64.encodeString(Util
						.getEditText(userIDEditText));
				parameter[1] = "Pwd";
				data[1] = LibBase64.encodeString(Util
						.getEditText(passwordEditText));
				parameter[2] = "Token";
				data[2] = PreferenceUtil.getString(activity,
						Constant.USER_TOKEN);
				// Log.e("=======", PreferenceUtil.getString(activity,
				// Constant.USER_TOKEN));

				loginResult = Http.post(Constant.LOGIN_URL, parameter, data,
						activity);

				handler.sendEmptyMessage(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void HTTP_updateAndroidToken() {

			new Thread() {
				@Override
				public void run() {
					String[] params = { "Token" };
					String[] data = { mAndroidToken };
					String result = Http.post(Constant.UPDATE_ANDROID_TOKEN,
							params, data, getActivity());

					Log.d("login", "result: " + result);

					Message msg = new Message();
					msg.arg1 = UPDATE_ANDROID_TOKEN;
					msg.obj = result;

					handler.sendMessage(msg);
				}
			}.start();

		}

		private Handler handler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				if (msg.arg1 == UPDATE_ANDROID_TOKEN) {
					String result = msg.obj.toString();
					if (result.contains("success")) {
						PreferenceUtil.SavePreferences(activity,
								Constant.GCM_TOKEN, mAndroidToken);

					} else {
						Log.d("deal", "failed");
					}
				} else {
					if (loginResult.contains("loginError")) {
						// Log.e("=======", "login unsuccessfully");
						Util.showOKAlertDialog(activity, "", "Wrong password!");
					} else if (loginResult.contains("specialWords")) {
						// Log.e("=======", "input special words: " +
						// loginResult);
						Util.showOKAlertDialog(activity, "", "Have inviald word");
					} else if (loginResult.contains("emailCodeError")) {
						certificate();
					} else if (loginResult.contains("success")) {
						// SettingFragment.replace=true;
						// getActivity().finish();
						Log.e("=======", "login successfully");
						try {
							JSONObject jsonObject = new JSONObject(loginResult);
							Log.e("=======",
									"ID successfully  :"
											+ jsonObject.getString("ID"));

							PreferenceUtil.setString(activity,
									Constant.USER_MOBILE_NUMBER,
									jsonObject.getString("mobile"));
							PreferenceUtil.setString(activity,
									Constant.USER_ID,
									jsonObject.getString("ID"));
							PreferenceUtil.setString(activity,Constant.USER_TOKEN, PreferenceUtil
											.getString(activity,
													Constant.USER_TOKEN));
							PreferenceUtil.setBoolean(activity,
									Constant.USER_LOGIN, true);

							HTTP_updateAndroidToken();

						} catch (JSONException e) {
							e.printStackTrace();
						}
						// Constant.LOGIN_SUCCESS=true;
						// activity.login_callBack();
						Intent intent = new Intent();
						activity.setResult(2, intent);
						activity.finish();

					}

					else {
						Log.e("=======", loginResult);
					}

					Util.dismissProgressDialog();
				}

				return true;
			}
		});
	}

	private OnClickListener registerClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			FragmentManagerUtil.addToBackStackFragment(getFragmentManager(),
					new RegisterFragment(), R.id.LoginActivity_frameLayout);
		}
	};

	private void certificate() {
		FragmentManagerUtil.addToBackStackFragment(getFragmentManager(),
				new CertificationFragment(), R.id.LoginActivity_frameLayout);
	}

	private OnClickListener feedbackClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("message/rfc822");
			i.putExtra(Intent.EXTRA_EMAIL,
					new String[] { "pipimy.service@gmail.com" });
			// i.putExtra(Intent.EXTRA_SUBJECT, "Email Subject");
			// i.putExtra(Intent.EXTRA_TEXT, "Email Content");
			try {
				startActivity(Intent.createChooser(i, "Send mail"));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(getActivity(),
						"There are no email clients installed.",
						Toast.LENGTH_SHORT).show();
			}
		}
	};
}

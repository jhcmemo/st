package pipimy.product.activity;

import java.util.ArrayList;

import org.json.JSONException;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import pipimy.chat.ChatUtil;
import pipimy.main.ApplicationController;
import pipimy.main.ChatFragment;
import pipimy.main.MainActivity;
import pipimy.main.MyProductActivity;
import pipimy.main.ProductFragment;
import pipimy.main.SettingFragment;
import pipimy.main.ShoppingFragment;
import pipimy.nearby.ProductListSorter;
import pipimy.nearby.ProductObject;
import pipimy.object.FavoriteObject;
import pipimy.object.TrackObject;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.PIPI_Tool;
import pipimy.others.PreferenceProxy;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.service.R;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FavorTracing_Activity extends FragmentActivity implements Runnable {

	// private GridView gridview;
	private CommonAdapter<FavoriteObject> adapter;
	// private Button btn_change;
	private TextView tv_nodata, tv_product, tv_store;
	ImageView iv_underline_product, iv_underline_store;

	ProductObject productObj = new ProductObject();
	private ArrayList<TrackObject> track_list;
	ActionBar actionBar;
	MFragmentPagerAdapter mFragmentPagerAdapter;
	private Menu mMenu;
	private ArrayList<FavoriteObject> favor_list ;

	private boolean isSell;
	ImageLoader mImageLoader;
	int position = 0;
	private String product_id = "";
	private ViewPager mViewPager;

	boolean product_view = true;
	public boolean find_item = false;

    public  int fav_size=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favor);
		getActionBar().setTitle(getString(R.string.favor_title));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		favor_list = PIPI_Tool.getFavorites(this,favor_list,true);	
		fav_size=favor_list.size();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		// gridview = (GridView) findViewById(R.id.FavorActivity_GridView);
		tv_nodata = (TextView) findViewById(R.id.tv_nodata);
		tv_product = (TextView) findViewById(R.id.tv_product);
		tv_store = (TextView) findViewById(R.id.tv_store);
		tv_product.setOnClickListener(tab_click);
		tv_store.setOnClickListener(tab_click);
		iv_underline_product = (ImageView) findViewById(R.id.iv_underline_product);
		iv_underline_store = (ImageView) findViewById(R.id.iv_underline_store);
		// btn_change=(Button) findViewById(R.id.btn_change);
		// btn_change.setOnClickListener(change);
		// init_data();
		actionBar = getActionBar();
		actionBar.setIcon(R.drawable.no_icon);
		// init_data();
		initViewPager();

		// new Thread(this).start();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// adapter.notifyDataSetChanged();
	}
	@Override
	public void onDestroy() {
		super.onDestroy();

		System.gc();

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();

			break;

		case R.id.action_play_all:
			Intent intent = new Intent();
			intent.setClass(FavorTracing_Activity.this,
					PlayVideo_Activity.class);
			intent.setAction("all_play");
			startActivity(intent);

			break;

		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.favor, menu);
		this.mMenu = menu;

		if(favor_list.size()==0){
			MenuItem item = menu.findItem(R.id.action_play_all);
			item.setVisible(false);
			}
		find_item=true;
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void run() {

		String[] params = { "ProductID" };
		String[] data = { product_id };

		String result = Http
				.post(Constant.GET_PRODUCT_VIEW, params, data, this);

		Log.d("favor", "result :" + result);
		if (result.contains("true")) {
			Intent it = new Intent();
			it = new Intent(FavorTracing_Activity.this,
					Product_Detail_Activity.class);
			it.putExtra("product_object", productObj);
			it.putExtra("edit", false);
			startActivity(it);

		} else {
			handler_NOproduct.sendMessage(handler_NOproduct.obtainMessage());

		}

	}


	private void initViewPager() {
		mFragmentPagerAdapter = new MFragmentPagerAdapter(
				getSupportFragmentManager());
		mViewPager = (ViewPager) findViewById(R.id.favor_pager);
		mViewPager.setAdapter(mFragmentPagerAdapter);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {

						if (position == 0) {
							if(fav_size==0){
							MenuItem item = mMenu.findItem(R.id.action_play_all);
							item.setVisible(false);
							}
							else{
								MenuItem item = mMenu.findItem(R.id.action_play_all);
								item.setVisible(true);
								
							}
							tv_product.setTextColor(Color
									.parseColor("#FF514F4E"));
							tv_store.setTextColor(Color.parseColor("#FFC9C9CA"));
							iv_underline_product.setVisibility(View.VISIBLE);
							iv_underline_store.setVisibility(View.GONE);
 
						} else if (position == 1) {
							MenuItem item = mMenu.findItem(R.id.action_play_all);
							item.setVisible(false);
							
							tv_product.setTextColor(Color
									.parseColor("#FFC9C9CA"));
							tv_store.setTextColor(Color.parseColor("#FF514F4E"));
							iv_underline_product.setVisibility(View.GONE);
							iv_underline_store.setVisibility(View.VISIBLE);

						}

					}
				});

	}


public void fragment_product_callBack(){
	MenuItem item = mMenu.findItem(R.id.action_play_all);
	item.setVisible(false);
	
}

	/**
	 * A FragmentPagerAdapter that returns a fragment
	 */
	class MFragmentPagerAdapter extends FragmentPagerAdapter {

		public MFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;

			switch (position) {
			case 0:
				fragment = new favor_fragment_product();
				break;
			case 1:
				fragment = new favor_fragment_store();
				break;

			}

			return fragment;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
		}

	}

	private OnClickListener tab_click = new OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.tv_product:

				tv_product.setTextColor(Color.parseColor("#FF514F4E"));
				tv_store.setTextColor(Color.parseColor("#FFC9C9CA"));
				iv_underline_product.setVisibility(View.VISIBLE);
				iv_underline_store.setVisibility(View.GONE);
				mViewPager.setCurrentItem(0);
				break;

			case R.id.tv_store:

				tv_product.setTextColor(Color.parseColor("#FFC9C9CA"));
				tv_store.setTextColor(Color.parseColor("#FF514F4E"));
				iv_underline_product.setVisibility(View.GONE);
				iv_underline_store.setVisibility(View.VISIBLE);
				mViewPager.setCurrentItem(1);

				break;

			}

		}
	};

	

	private Handler handler_NOproduct = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			Toast.makeText(FavorTracing_Activity.this, "No product!", 2000)
					.show();

		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*----- login -----*/
		if (resultCode == 3) {
			setResult(3);
			finish();
		}

		/*----- get someone product Sell-----*/
		else if (resultCode == 4) {
			setResult(4);
			finish();
		}

		/*----- get someone product Buy-----*/
		else if (resultCode == 7) {
			setResult(7);
			finish();
		}

		/*----- chat start -----*/
		else if (resultCode == 5) {
			setResult(5);
			finish();
		}
	}

	public void refreshList() {
		adapter.notifyDataSetChanged();
	}

	

}

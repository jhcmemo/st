package pipimy.product.activity;

import java.util.ArrayList;

import org.json.JSONException;

import pipimy.main.MyProductActivity;
import pipimy.object.FavoriteObject;
import pipimy.object.TrackObject;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.Http;
import pipimy.others.IGenericDialogUtil;
import pipimy.others.JSONParserTool;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class favor_fragment_store extends Fragment implements Runnable {
	GridView gridview_product;
	TextView tv_nodata;
	private CommonAdapter<FavoriteObject> adapter;
	private String product_id = "";
	ImageLoader mImageLoader;
	private ArrayList<TrackObject> track_list;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		Get_Track_List();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.favor_fragment_product, container,
				false);
	}

	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		tv_nodata = (TextView) view.findViewById(R.id.tv_nodata);
		gridview_product = (GridView) view.findViewById(R.id.favor_gridview);
		gridview_product.setNumColumns(1);
		gridview_product.setVerticalSpacing(10);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Log.e("onActivityCreated","Chat Fragment");
	}

	@Override
	public void onResume() {
		super.onResume();
		// Log.e("onResume","Chat Fragment");
	}

	@Override
	public void onPause() {
		super.onPause();
		// Log.e("onPause","Chat Fragment");
	}

	@Override
	public void run() {

		// Constant.PicServerURL + product_object.getProductID()
		// + "-pic1.jpg", mImageLoader);
	}
	
	protected void changeLocalDataStatus(String id) {
		for(TrackObject obj: track_list) {
			if(obj.getMemberID().equals(id)) {
				if(obj.getIsTrackingMsg().equals("1")) obj.setIsTrackingMsg("2");
				else obj.setIsTrackingMsg("1");
			}
		}
		
		initGridView();
	}
	
	protected void switchTrackingStatus(final String isTrackingMsg, final String memberID) {
		String msg = "";
		if(isTrackingMsg.equals("1")) {
			msg = getResources().getString(R.string.dialog_str_msg_track_msg_open);
		} else if(isTrackingMsg.equals("2")) {
			msg = getResources().getString(R.string.dialog_str_msg_track_msg_close);
		}
		
		String title = getResources().getString(R.string.dialog_str_title_get_push_msg);
		
		DialogUtil.pushGeneralDialog(getActivity(), title, msg, "ok", "cancel", 
				new IGenericDialogUtil.IGenericBtnClickListener() {
			@Override
			public void PositiveMethod(DialogInterface dialog, int id) {
				setMsgTrigger(isTrackingMsg, memberID);
			}
			
			@Override
			public void NegativeMethod(DialogInterface dialog, int id) {
			}
		});
	}
	
	private void Http_delete_trace(final String track_id) {
		new Thread() {
			@Override
			public void run() {

				String[] params = { "TracingID" };
				String[] data = { track_id };
				String result = Http.post(Constant.POST_DELETE_TRACE, params,
						data, getActivity());
				Log.d("favor", "track remove result:" + result);

			}

		}.start();

	}

	protected void setMsgTrigger(final String isTrackingMsg, final String memberID) {
		new Thread(){
			@Override
			public void run() {
				//Log.e("Favor store", "setMsgTrigger isPush = "+isTrackingMsg + " trace id ="+memberID);
				String[] params = {"TracingID"};
				String[] data = {memberID};
				String result = Http.post(Constant.POST_MSG_TRIGGER, params, data, getActivity());
				//Log.e("Favor store", "POST_MSG_TRIGGER res = "+result);
				
				Message msg = new Message();
				if(result.contains("success")) {
					msg.arg1 = Constant.CODE_PUSH_TRIGGER;
					msg.obj = memberID;
				}
				handler_setgridview.sendMessage(msg);
			}
		}.start();
	}

	private void Get_Track_List() {

		new Thread() {
			@Override
			public void run() {

				String[] params = { "" };
				String[] data = { "" };

				String result = Http.get(Constant.GET_TRACK_LIST, params, data,
						getActivity());
				Log.d("favor", "track result:" + result);

				try {
					track_list = JSONParserTool.getTraceList(result);

					Bundle b = new Bundle();
					Message m = new Message();
					b.putBoolean("Product_View", false);
					m.setData(b);
					handler_setgridview.sendMessage(m);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}.start();

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initGridView() {

		if (track_list.size() == 0) {
			gridview_product.setVisibility(View.GONE);
			tv_nodata.setVisibility(View.VISIBLE);

		} else {
			adapter = new CommonAdapter(getActivity(), track_list,
					R.layout.favor_store_item) {
				@Override
				public void setViewData(CommonViewHolder commonViewHolder,
						View currentView, Object item) {

					final TrackObject trackObject = (TrackObject) item;

					RelativeLayout rl_favor_store_outside = (RelativeLayout) commonViewHolder
							.get(commonViewHolder, currentView,
									R.id.rl_favor_store_outside);
					rl_favor_store_outside.setTag(trackObject);
					rl_favor_store_outside.setOnClickListener(item_click);
					
					int res[] = { R.id.favor_member_product_pic1,
							R.id.favor_member_product_pic2,
							R.id.favor_member_product_pic3 };
					int pic[] = trackObject.getPic();
					for (int i = 0; i < pic.length; i++) {
						final NetworkImageView favor_pic = (NetworkImageView) commonViewHolder
								.get(commonViewHolder, currentView, res[i]);
						final String fileName = pic[i] + "-pic1.jpg";
						Log.d("filename", "store pic fileName :" + fileName);

						favor_pic.setImageUrl(Constant.PicServerURL + fileName,
								mImageLoader);

					}

					NetworkImageView favor_member_icon = (NetworkImageView) commonViewHolder
							.get(commonViewHolder, currentView,
									R.id.favor_member_icon);
					favor_member_icon.setImageUrl(Constant.STORE_IMAGE_URL
							+ trackObject.getMemberID() + ".jpg",
							mImageLoader);

					TextView tv_member_id = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_member_id);
					tv_member_id.setText(trackObject.getMemberID());

					// PreferenceUtil.getString(FavorTracing_Activity.this,
					// Constant.USER_ID)
					TextView trace_time = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_trace_time);
					trace_time.setText(trackObject.getTime());

					Button track_delete = (Button) commonViewHolder.get(
							commonViewHolder, currentView,
							R.id.favor_store_trace_delete);
					track_delete.setTag(trackObject);
					track_delete.setOnClickListener(item_click);
					
					//added by Henry
					//display and set the tracking msg status
					TextView txtTrackingMsg = (TextView)commonViewHolder.get(
							commonViewHolder, currentView, R.id.txt_receive_push);
					txtTrackingMsg.setTag(trackObject);
					txtTrackingMsg.setOnClickListener(item_click);
					String isTrackMsg = trackObject.getIsTrackingMsg();
					if(isTrackMsg.equals("2")) {
						txtTrackingMsg.setTextColor(0xFFEA7A7A);
					} else {
						txtTrackingMsg.setTextColor(0xFFDDDDDD);
					}
				}

			};

			gridview_product.setAdapter(adapter);
		}
	}

	private OnClickListener item_click = new OnClickListener() {

		@Override
		public void onClick(View v) {
			FavoriteObject favoriteObject;
			TrackObject trackObject;
			int likePosition = 0;
			int tracePosition = 0;
			switch (v.getId()) {
			case R.id.txt_receive_push:{
				trackObject = (TrackObject) v.getTag();
				switchTrackingStatus(trackObject.getIsTrackingMsg(), trackObject.getMemberID());
				break;
			}

			case R.id.rl_favor_store_outside:
				trackObject = (TrackObject) v.getTag();

				Intent it = new Intent();
				it = new Intent(getActivity(), MyProductActivity.class);
				it.putExtra("memberID", trackObject.getMemberID());
				startActivity(it);

				break;

			case R.id.favor_store_trace_delete:

				trackObject = (TrackObject) v.getTag();

				Log.d("favor",
						"trackObject.getMemberID() :"
								+ trackObject.getMemberID());
				for (int i = 0; i < track_list.size(); i++) {
					if (track_list.get(i).getMemberID()
							.equals(trackObject.getMemberID())) {
						track_list.remove(i);
						break;
					}
				}
				initGridView();
				Http_delete_trace(trackObject.getMemberID());

				break;

			}

		}
	};

	private Handler handler_setgridview = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if(msg.arg1 == Constant.CODE_PUSH_TRIGGER) {
				String id = (String)msg.obj.toString();
				changeLocalDataStatus(id);
			} else {
				initGridView();
			}
			

		}
	};

}

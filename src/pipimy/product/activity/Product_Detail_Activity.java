package pipimy.product.activity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;

import pipimy.login.LoginActivity;
import pipimy.main.ApplicationController;
import pipimy.main.MyProductActivity;
import pipimy.nearby.ProductObject;
import pipimy.object.FavoriteObject;
import pipimy.object.ProductRatingDesObject;
import pipimy.object.ProductRatingObject;
import pipimy.object.VideoHistoryObject;
import pipimy.others.CircleNetwork_ImageView;
import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.DialogListUtil.OnChatClickListener;
import pipimy.others.DialogListUtil.OnReportSubmitClickListener;
import pipimy.others.DialogListUtil.OnTypeClickListener;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.PIPI_Tool;
import pipimy.others.PicViewPagerAdapter;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import pipimy.third.TextureVideoView;
import pipimy.third.TextureVideoView.MediaPlayerListener;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.Video;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class Product_Detail_Activity extends Activity {

	final static String TAG = "Product_Detail_Activity";

	ViewPager ProductDetail_video_previewImageView;
	ArrayList<ProductObject> productList = new ArrayList<ProductObject>();
	private ProductRatingDesObject ratingDescription;
	private ArrayList<FavoriteObject> favorList;

	private ArrayList<ProductRatingObject> ratingList;
	ImageLoader mImageLoader;

	private ProgressDialog progressDialog;

	private static String videoPath;
	final static int GET_STORE_INFO = 10001;
	final static int GET_SOMEONE_PRODUCT = 10002;
	private int product_type;
	private String types[];

	private ImageView[] imageViews;
	private ImageView imageview, detail_report;
	private ViewGroup group;
	private TextureVideoView mTextureVideoView;
	private ProgressBar loadingBar;
	private Button playButton;
	private NetworkImageView preview;
	private boolean isGetNewLocation = false;
	private boolean isActionSell;

	HashMap<String, String> storeInfo = new HashMap<String, String>();

	TextView titleTextView, brandTextView,ed_titleTextView, ed_memberID, ed_timeTextView,
			ed_locationTextView, ed_typeTextView_edit;
	TextView IDTextView;
	TextView priceTextView;
	TextView TypeTextView;
	TextView TimeTextView;
	TextView DisTextView;
	TextView tv_empty;
	TextView Product_stock;
	TextView tv_delivery;
	TextView tv_payment;

	EditText ed_priceEditText, ed_desEditText;

	Button favorButton, deleteButton, uploadButton, btn_cart;
	TextView desTextView,stateTextView,snoTextView;
	CircleNetwork_ImageView img;
	
	CircleNetwork_ImageView ed_img;

	TextView dealTypeTextView;
	TextView loc1;
	TextView loc2;
	TextView loc3;

	String id = "";
	String productID;

	View loc1View;
	View loc2View;
	View loc3View;

	TextView averageTextView;
	TextView countTextView;
	//TextView dealTimesTextView;
	RatingBar avgRatingBar;
	LinearLayout parentView, lv_product_detail, lv_trade, lv_detail_edit,
			ed_typeLayout, lv_Edit_button, lv_rating, lv_trade_way,
			lv_product_description;

	ProductObject product_object;
	RelativeLayout rl_video, rl_contact, layout1;

	private DisplayMetrics metrics;

	int picNumber = 0;
	private boolean mIsPlaying = false;

	private boolean mIsFirstTimeIn = true;

	private PicViewPagerAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_detail);

		mImageLoader = VolleySingleton.getInstance().getImageLoader();

		id = PreferenceUtil.getString(Product_Detail_Activity.this,
				Constant.USER_ID);

		favorList = PIPI_Tool.getFavorites(Product_Detail_Activity.this,
				favorList, true);

		product_object = (ProductObject) getIntent().getExtras()
				.getSerializable("product_object");
		if (product_object.getHide() != null) {
			if (product_object.getHide().equals("4")) productID = product_object.getParentID();
			else productID = product_object.getProductID();
		} else {
			productID = product_object.getProductID();
		}

		/** layout */
		rl_video = (RelativeLayout) findViewById(R.id.rl_video);
		layout1 = (RelativeLayout) findViewById(R.id.layout1);
		lv_product_description = (LinearLayout) findViewById(R.id.lv_product_description);
		// rl_contact = (RelativeLayout) findViewById(R.id.rl_contact);
		lv_product_detail = (LinearLayout) findViewById(R.id.lv_product_detail);
		lv_rating = (LinearLayout) findViewById(R.id.lv_rating);
		// lv_trade = (LinearLayout) findViewById(R.id.lv_trade);
		lv_detail_edit = (LinearLayout) findViewById(R.id.lv_detail_edit);
		lv_Edit_button = (LinearLayout) findViewById(R.id.lv_Edit_button);

		/*** ViewPager group ****/
		// ProductDetail_pic_viewPager = (ViewPager)
		// findViewById(R.id.ProductDetail_pic_viewPager);
		group = (ViewGroup) findViewById(R.id.viewGroup);
		/*** Video ****/
		mTextureVideoView = (TextureVideoView) findViewById(R.id.ProductDetail_video_surfaceView);
		loadingBar = (ProgressBar) findViewById(R.id.ProductDetail_video_loadingProgressBar);
		playButton = (Button) findViewById(R.id.ProductDetail_video_playButton);
		ProductDetail_video_previewImageView = (ViewPager) findViewById(R.id.ProductDetail_video_previewImageView);

		/** member */
		detail_report = (ImageView) findViewById(R.id.detail_report);
		detail_report.setOnClickListener(report_click);
		/*** Content ****/
		Product_stock = (TextView) findViewById(R.id.Product_stock);
		titleTextView = (TextView) findViewById(R.id.ProductDetail_content_title);
		brandTextView = (TextView) findViewById(R.id.ProductDetail_content_brand);
		IDTextView = (TextView) findViewById(R.id.ProductDetail_content_ID);
		priceTextView = (TextView) findViewById(R.id.ProductDetail_content_price);
		TypeTextView = (TextView) findViewById(R.id.ProductDetail_content_type);
		TimeTextView = (TextView) findViewById(R.id.ProductDetail_content_time);
		DisTextView = (TextView) findViewById(R.id.ProductDetail_content_distance);
		favorButton = (Button) findViewById(R.id.ProductDetail_content_favorButton);
		desTextView = (TextView) findViewById(R.id.ProductDetail_content_des);
		stateTextView =(TextView) findViewById(R.id.ProductDetail_content_state);		
		snoTextView=(TextView) findViewById(R.id.ProductDetail_content_sno);
		
		tv_empty = (TextView) findViewById(R.id.tv_empty);
		tv_delivery = (TextView) findViewById(R.id.tv_delivery);
		tv_payment = (TextView) findViewById(R.id.tv_payment);
		img = (CircleNetwork_ImageView) findViewById(R.id.member_img);
		btn_cart = (Button) findViewById(R.id.btn_cart);
		favorButton = (Button) findViewById(R.id.ProductDetail_content_favorButton);
		// productDetail_reportTextView = (TextView)
		// findViewById(R.id.productDetail_reportTextView);
		// productDetail_reportTextView.setOnClickListener(Chat);
		btn_cart.setOnClickListener(Cart);

		/*** Edit Content ****/
		lv_trade_way = (LinearLayout) findViewById(R.id.lv_trade_way);
		ed_titleTextView = (TextView) findViewById(R.id.EditMyProduct_titleTextView);
		ed_img = (CircleNetwork_ImageView) findViewById(R.id.ed_member_img);
		ed_priceEditText = (EditText) findViewById(R.id.EditMyProduct_priceEditText);
		ed_desEditText = (EditText) findViewById(R.id.EditMyProduct_desEditText);
		ed_memberID = (TextView) findViewById(R.id.ed_ProductDetail_content_ID);
		ed_typeLayout = (LinearLayout) findViewById(R.id.EditMyProduct_typeButton);
		ed_timeTextView = (TextView) findViewById(R.id.EditMyProduct_content_time);
		ed_locationTextView = (TextView) findViewById(R.id.EditMyProduct_content_distance);
		ed_typeTextView_edit = (TextView) findViewById(R.id.EditMyProduct_content_type);
		deleteButton = (Button) findViewById(R.id.EditMyProduct_deleteButton);
		uploadButton = (Button) findViewById(R.id.EditMyProduct_updateButton);

		/*** Trade ****/
		// dealTypeTextView = (TextView)
		// findViewById(R.id.ProductDetail_dealType);
		// loc1 = (TextView) findViewById(R.id.ProductDetail_dealType_loc1);
		// loc2 = (TextView) findViewById(R.id.ProductDetail_dealType_loc2);
		// loc3 = (TextView) findViewById(R.id.ProductDetail_dealType_loc3);
		// loc1View = (View) findViewById(R.id.ProductDetail_dealType_loc1View);
		// loc2View = (View) findViewById(R.id.ProductDetail_dealType_loc2View);
		// loc3View = (View) findViewById(R.id.ProductDetail_dealType_loc3View);

		/*** Rating ****/
		averageTextView = (TextView) findViewById(R.id.ProductDetail_rating_averageTextView);
		countTextView = (TextView) findViewById(R.id.ProductDetail_rating_countTextView);
		//dealTimesTextView = (TextView) findViewById(R.id.ProductDetail_rating_dealTimesTextView);
		avgRatingBar = (RatingBar) findViewById(R.id.avg_ratingBar);
		parentView = (LinearLayout) findViewById(R.id.ProductDetail_rating_ListLayout);

		init();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.clear();
		if (PreferenceUtil.getBoolean(Product_Detail_Activity.this,
				Constant.USER_LOGIN)
				&& getIntent().getBooleanExtra("edit", false)
				&& id.equals(product_object.getID())) {

			// getMenuInflater().inflate(R.menu.relocated, menu);
		} else {
			if (PreferenceUtil.getBoolean(Product_Detail_Activity.this,
					Constant.USER_LOGIN) && id.equals(product_object.getID())) {
				btn_cart.setVisibility(View.INVISIBLE);
			} else {
				getMenuInflater().inflate(R.menu.detail, menu);

			}
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:
			this.finish();
			break;

		case R.id.action_detail_chat:
			if (product_object.getHide() != null) {
				if (!product_object.getHide().equals("4")) {
					startChat();
				}
			} else {
				startChat();
			}
			break;

		case R.id.action_relocated:
			isGetNewLocation = true;
			// this.finish();
			break;
		}
		return true;
	}

	private void init() {

		types = getResources().getStringArray(R.array.types);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle(
				getResources().getString(R.string.chat_product_data));

		getActionBar().setIcon(R.drawable.no_icon);

		if (PreferenceUtil.getBoolean(Product_Detail_Activity.this,
				Constant.USER_LOGIN)
				&& getIntent().getBooleanExtra("edit", false)
				&& id.equals(product_object.getID())) {
			isActionSell = true;

			Get_rating();
			// rl_video.setVisibility(View.GONE);
			// rl_contact.setVisibility(View.GONE);
			layout1.setVisibility(View.GONE);
			lv_trade_way.setVisibility(View.GONE);
			lv_product_detail.setVisibility(View.GONE);
			lv_product_description.setVisibility(View.GONE);
			tv_empty.setVisibility(View.GONE);
			// lv_trade.setVisibility(View.GONE);
			lv_detail_edit.setVisibility(View.VISIBLE);
			lv_Edit_button.setVisibility(View.VISIBLE);
			// initPic();
			favorButton.setVisibility(View.GONE);
			btn_cart.setVisibility(View.GONE);
 
			initVideo();
			initEditContent();

		} else {

			if (PreferenceUtil.getBoolean(Product_Detail_Activity.this,
					Constant.USER_LOGIN) && id.equals(product_object.getID())) {

				// rl_contact.setVisibility(View.GONE);
				favorButton.setVisibility(View.GONE);
				Get_rating();
				// initPic();
				initVideo();
				initContent();
				initTrade();
				Log.d("detail", "1");

			} else {
				isActionSell = true;
				Get_rating();
				// initPic();
				initVideo();
				initContent();
				initTrade();
				Log.d("detail", "2");

			}

		}

		// ProductDetail_pic_viewPager
	}

	private void Get_rating() {
		new Thread() {
			@Override
			public void run() {
				ratingList = new ArrayList<ProductRatingObject>();
				ratingDescription = new ProductRatingDesObject();

				String[] params = { "ProductID" };
				String[] data = { product_object.getProductID() };

				String result = Http.post(Constant.GET_PRODUCT_RATING, params,
						data, Product_Detail_Activity.this);

				try {
					JSONParserTool.getProductRating(result, ratingDescription,
							ratingList);
					Get_rating_success.sendMessage(Get_rating_success
							.obtainMessage());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}.start();

	}

	/*
	 * private void initPic() {
	 * 
	 * metrics = new DisplayMetrics(); productList =
	 * ApplicationController.nearProductList;
	 * 
	 * // picNumber= getIntent().getIntExtra("picNumber",0);
	 * 
	 * 
	 * metrics = new DisplayMetrics();
	 * //productList=ApplicationController.nearProductList;
	 * 
	 * 
	 * // picNumber= getIntent().getIntExtra("picNumber",0);
	 * 
	 * imageViews = new ImageView[product_object.getPicNumber()];
	 * 
	 * Log.d("detail","product_object.getPicNumber() :"+product_object.getPicNumber
	 * ()); // ?嚙踝蕭謕蕭豲嚙踝蕭嚙�???嚙踝蕭謕蕭��蕭嚙踐�蕭豲嚙踝蕭嚙�??????雓蕭嚙踝蕭嚙�??????
	 * for (int i = 0; i < product_object.getPicNumber(); i++) { imageview = new
	 * ImageView(Product_Detail_Activity.this); //
	 * ??嚙踝蕭謕蕭豲�蕭嚙踐�蕭��蕭?????????mageview?????
	 * imageview.setLayoutParams(new LayoutParams(30, 30));// ?
	 * imageview.setPadding(20, 0, 20, 0); imageViews[i] = imageview;
	 * 
	 * if (i == 0) {
	 * imageViews[i].setBackgroundResource(R.drawable.circle_selected); } else {
	 * imageViews[i] .setBackgroundResource(R.drawable.circle_unselected); } //
	 * group.addView(imageViews[i]);
	 * 
	 * }
	 * 
	 * adapter = new PicViewPagerAdapter(Product_Detail_Activity.this,
	 * product_object.getProductID(), product_object.getPicNumber());
	 * 
	 * ProductDetail_pic_viewPager.setAdapter(adapter);
	 * ProductDetail_pic_viewPager.setOnTouchListener(viewPager_touch);
	 * ProductDetail_pic_viewPager.setOnPageChangeListener(change_page);
	 * 
	 * }
	 */

	private void initVideo() {

		metrics = new DisplayMetrics();
		productList = ApplicationController.nearProductList;

		metrics = new DisplayMetrics();

		imageViews = new ImageView[product_object.getPicNumber()];

		Log.d("detail",
				"product_object.getPicNumber() :"
						+ product_object.getPicNumber());
		// ?嚙踝蕭謕蕭豲嚙踝蕭嚙�???嚙踝蕭謕蕭��蕭嚙踐�蕭豲嚙踝蕭嚙�??????雓蕭嚙踝蕭嚙�??????
		for (int i = 0; i < product_object.getPicNumber(); i++) {
			imageview = new ImageView(Product_Detail_Activity.this);
			// ??嚙踝蕭謕蕭豲�蕭嚙踐�蕭��蕭?????????mageview?????
			// imageview.setLeft(35);

			imageview.setLayoutParams(new LayoutParams(50, 50));// ?
			// ViewGroup.MarginLayoutParams params =
			// (ViewGroup.MarginLayoutParams )imageview.getLayoutParams();
			// params.setMargins(0, 0, 10, 0);
			// imageview.setPadding(30, 0, 30, 0);
			// imageview.setLayoutParams(new ViewGroup.LayoutParams(
			// ViewGroup.LayoutParams.WRAP_CONTENT,
			// ViewGroup.LayoutParams.WRAP_CONTENT));

			imageViews[i] = imageview;

			if (i == 0) {
				imageViews[i].setBackgroundResource(R.drawable.details_dot);
			} else {
				imageViews[i]
						.setBackgroundResource(R.drawable.details_dot_grey);
			}
			group.addView(imageViews[i]);

		}

		adapter = new PicViewPagerAdapter(Product_Detail_Activity.this,
				productID, product_object.getPicNumber());

		ProductDetail_video_previewImageView.setAdapter(adapter);
		ProductDetail_video_previewImageView
				.setOnPageChangeListener(change_page);

		// mTextureVideoView.setOnClickListener(playOnClickListener);
		playButton.setOnClickListener(playOnClickListener);
		mTextureVideoView.setListener(mMediaPlayerListener);

		String path = getFilesDir() + "/" + productID + "-video.mp4";
		File videoFile = new File(path);
		if (videoFile.exists()) {
			loadingBar.setVisibility(View.GONE);
			playButton.setVisibility(View.VISIBLE);
			videoPath = videoFile.getAbsolutePath();
			// getFirstFrame(path, preview);

		} else {
			if (!startDownloadFile(Constant.VedioServerURL + productID
					+ "-video.mp4", productID + "-video.mp4", false, null, null)) {
				Toast.makeText(this, getString(R.string.device_memory_few),
						Toast.LENGTH_LONG).show();
			}
		}

	}

	private void initContent() {
		int And_value[] = { 1, 2, 4, 8, 16 };
		String payment = "";
		String delivery = "";
		String payment_type[] = getResources()
				.getStringArray(R.array.cash_typs);
		String delivery_type[] = getResources().getStringArray(
				R.array.delivery_type);

		titleTextView.setText(product_object.getTitle());
		brandTextView.setText(product_object.getBrand());
		if (product_object.getID().equals("null")) {
			IDTextView.setText(product_object.getContact());
		} else {
			IDTextView.setText(product_object.getID());

		}

		Log.d("detail", "& : " + (19 & 8));

		for (int i = 0; i < And_value.length; i++) {

			if ((product_object.getPayment() & And_value[i]) != 0) {
				switch (And_value[i]) {
				case 1:
					payment = payment + "," + payment_type[0];
					break;
				case 2:
					payment = payment + "," + payment_type[1];
					break;

				case 4:

					payment = payment + "," + payment_type[2];

					break;

				case 8:

					payment = payment + "," + payment_type[3];

					break;

				case 16:

					payment = payment + "," + payment_type[4];
					break;

				}

			} else {
				payment = payment + "";
			}
			if ((product_object.getDelivery() & And_value[i]) != 0) {
				switch (And_value[i]) {
				case 1:
					delivery = delivery + "," + delivery_type[0];
					break;
				case 2:
					delivery = delivery + "," + delivery_type[1];
					break;

				case 4:

					delivery = delivery + "," + delivery_type[2];

					break;

				case 8:

					delivery = delivery + "," + delivery_type[3];

					break;

				case 16:

					delivery = delivery + "," + delivery_type[4];
					break;

				}

			}

			else {
				delivery = delivery + "";
			}
		}
		// if(payment.substring(0, 1).equals(",")){
		if (payment.length() != 0) {
			payment = payment.substring(1, payment.length());

		}
		if (delivery.length() != 0) {

			delivery = delivery.substring(1, delivery.length());

		}

		// }
		// if(delivery.substring(0, 1).equals(",")){

		// }

		tv_payment.setText(payment);
		tv_delivery.setText(delivery);

		Product_stock.setText(getString(R.string.stock)
				+ product_object.getStock() + getString(R.string.stock_count));
		priceTextView.setText("$" + product_object.getPrice());
		TypeTextView
				.setText(getResources().getStringArray(R.array.types)[Integer
						.valueOf(product_object.getType())]);
		TimeTextView.setText(product_object.getTime().substring(5, 10)
				.replace("-", "/"));
		DisTextView.setText(product_object.getDistance() + "");

		Log.d("detail", "dis :" + product_object.getDistance() + "");
		// favorButton
		favorButton.setOnClickListener(favor_click);
		// favorList = Constant.favorList_seller;
		/*----- favor -----*/
		boolean isLove = false;
		for (int i = 0; i < favorList.size(); i++) {
			if (favorList.get(i).getProductID()
					.equals(product_object.getProductID())) {
				isLove = true;
				break;
			}
		}
		if (isLove) {
			favorButton.setBackgroundResource(R.drawable.cell_product_love);
			favorButton.setTag("true");
		} else {
			favorButton.setBackgroundResource(R.drawable.cell_product_unlove);
			favorButton.setTag("false");
		}
		
		
		
		if(product_object.getSNO()==null||product_object.getSNO().equals("")){
			snoTextView.setVisibility(View.GONE);
		}
		else{
			
			snoTextView.setText(getString(R.string.product_sno)+" "+product_object.getSNO());
			
		}		
		
		
		
		switch(product_object.getNewOld()){
		case 1:
			stateTextView.setText(getString(R.string.product_state)+" "+getString(R.string.All_new));
			break;
			
        case 2:
			stateTextView.setText(getString(R.string.product_state)+" "+getString(R.string.second_never_use));

			break;
		
        case 3:
			stateTextView.setText(getString(R.string.product_state)+" "+getString(R.string.second_use));

			break;
        case 4:
			stateTextView.setText(getString(R.string.product_state)+" "+getString(R.string.second_else));

		break;
		}
		
		if (product_object.getDes() == null
				|| product_object.getDes().trim().equals("")) {

			desTextView.setText(getString(R.string.product_des)+" "+getString(R.string.no_data));

		} else {
			desTextView.setText(getString(R.string.product_des)+" "+product_object.getDes());
		}

		DisTextView.setText(String.format("%.2f",
				(product_object.getDistance() / 1000)) + "km");

		img.setImageUrl(Constant.STORE_IMAGE_URL + product_object.getID() + ".jpg",
				mImageLoader);

		// img.setOnClickListener(clickID);

	}

	private void initEditContent() {
		
		ed_img.setImageUrl(Constant.STORE_IMAGE_URL + product_object.getID() + ".jpg", mImageLoader);

		ed_memberID.setText(product_object.getID());
		ed_titleTextView.setText(product_object.getTitle());
		ed_timeTextView.setText(product_object.getTime().substring(5, 10)
				.replace("-", "/"));
		ed_locationTextView.setText(String.format("%.2f",
				(product_object.getDistance() / 1000))
				+ "km");
		ed_typeTextView_edit.setText(types[Integer.valueOf(product_object
				.getType())]);
		ed_priceEditText.setText("" + product_object.getPrice());
		ed_desEditText.setText(product_object.getDes());
		product_type = Integer.valueOf(product_object.getType());
		ed_typeLayout.setOnClickListener(typeOnClickListener);
		deleteButton.setOnClickListener(deleteOnClickListener);
		uploadButton.setOnClickListener(reNewOnClickListener);

	}

	private void initTrade() {

		// loc1View.setVisibility(View.GONE);
		// loc2View.setVisibility(View.GONE);
		// loc3View.setVisibility(View.GONE);

		// if (product_object.getDelivery().equals("1")) {
		// dealTypeTextView.setText(getString(R.string.delivering_post));
		// }

		// else {

		/** 嚙踐�蕭嚙踝�蕭��蕭�嚙踐�蕭�頨急嚙� **/
		/*
		 * // face if (product_object.getDelivery().equals("2")) {
		 * dealTypeTextView.setText(getString(R.string.delivering_hand)); } //
		 * post/face else if (product_object.getDelivery().equals("3")) {
		 * dealTypeTextView.setText(getString(R.string.delivering_post) + "/" +
		 * getString(R.string.delivering_hand)); }
		 * 
		 * if (product_object.getF1() != null) {
		 * loc1.setText(product_object.getF1().getName());
		 * loc1View.setVisibility(View.VISIBLE); } if (product_object.getF2() !=
		 * null) { loc2.setText(product_object.getF2().getName());
		 * loc2View.setVisibility(View.VISIBLE); } if (product_object.getF3() !=
		 * null) { loc3.setText(product_object.getF3().getName());
		 * loc3View.setVisibility(View.VISIBLE); }
		 */

		// }

	}

	private void initRating() {

		// mMemberPicUrl = Constant.STORE_IMAGE_URL + mMemberID + ".jpg";

		String rating = String.format("%.1f", ratingDescription.getAverage());

		Log.d("detail", "rating :" + rating);
		averageTextView.setText(rating);
		countTextView.setText("(" + ratingDescription.getCount() + ")");
	//	dealTimesTextView.setText("(" + ratingDescription.getDealTimes() + ")");
		avgRatingBar.setRating(Float.valueOf(rating));

		String url_member = "";

		for (int i = 0; i < ratingList.size(); i++) {
			View childView = LayoutInflater.from(this).inflate(
					R.layout.product_detail_buyer, null);
			TextView userIDFromTextView = (TextView) childView
					.findViewById(R.id.ProductRating_cell_userIDFromTextView);
			RatingBar ratingBar = (RatingBar) childView
					.findViewById(R.id.ProductRating_cell_ratingBar);
			TextView commentTextView = (TextView) childView
					.findViewById(R.id.ProductRating_cell_commentTextView);
			TextView ratingTime = (TextView) childView
					.findViewById(R.id.ProductRating_cell_RatingTimeTextView);
			NetworkImageView iv_buyer = (NetworkImageView) childView
					.findViewById(R.id.iv_buyer);

			url_member = Constant.STORE_IMAGE_URL
					+ ratingList.get(i).getUserIdFrom() + ".jpg";

			iv_buyer.setImageUrl(url_member, mImageLoader);

			userIDFromTextView.setText(ratingList.get(i).getUserIdFrom());
			ratingBar.setRating(ratingList.get(i).getScore());
			commentTextView.setText(ratingList.get(i).getComment());
			ratingTime.setText(ratingList.get(i).getRatingTime()
					.substring(0, 10).replace("-", "/"));

			// mTempID = ratingList.get(i).getUserIdFrom();
			parentView.addView(childView);
		}

	}

	private void getFirstFrame(String path, NetworkImageView preview) {
		// surfaceView Can not be reused after set Bitmap with preview ,
		// so it should be add an ImageView to do it.
		Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path,
				Video.Thumbnails.FULL_SCREEN_KIND);

		if (bitmap == null) {
			preview.setImageUrl(
					Constant.PicServerURL + productID + "-pic1.jpg",
					mImageLoader);

			Log.d("detail", "bitmap null!");
		} else {
			Log.d("detail", "bitmap !null!");
			preview.setImageUrl(
					Constant.PicServerURL + productID + "-pic1.jpg",
					mImageLoader);

			// ImageLoader.getInstance().displayImage(GlobalVariable.PicServerURL
			// + productId + "-pic1.jpg",preview,
			// options);
			// preview.setImageBitmap(bitmap);
		}
	}

	/** Download file to SDcard ***/
	public boolean startDownloadFile(String targetDownloadURL, String fileName,
			boolean showProgressDialog, String promptTitle, String promptMessage) {

		if (targetDownloadURL == null || targetDownloadURL.length() == 0
				|| fileName == null || fileName.length() == 0) {
			return false;
		}

		if (!checkFreeSpace()) {
			return false;
		}

		File file = new File(getFilesDir() + "/" + fileName);
		try {

			file.createNewFile();
			VideoHistoryObject historyObject = new VideoHistoryObject();
			historyObject.setFileName(fileName);
			historyObject.setCreateTime(Util.getTime());
			VideoHistoryObject.addVideoHistory(this, historyObject);
			Constant.videoHistry = VideoHistoryObject.getVideoHistory(this);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DownloadFileThread thread = new DownloadFileThread(targetDownloadURL,
				file);
		thread.setName("DownloadFileBackground");
		thread.start();

		// Log.d(TAG, "Start Download File form URL: " + targetDownloadURL);

		/*
		 * if (showProgressDialog == true) {
		 * this.displayProgressDialog(promptTitle, promptMessage); }
		 */

		return true;
	}

	/**
	 * Download file from target URL
	 **/
	class DownloadFileThread extends Thread {
		private String targetdownloadURLString;
		private File targetPath;

		DownloadFileThread(String targetdownloadURLString, File targetPath) {
			this.targetdownloadURLString = targetdownloadURLString;
			this.targetPath = targetPath;
		}

		public void run() {
			Http.startDownloadFile(targetdownloadURLString, targetPath);
			Bundle b = new Bundle();

			if (Http.isDownloadSucceed) {
				b.putBoolean("State", true);
				b.putString("File_name", targetPath.toString());

				Message mg = new Message();
				mg.setData(b);

				handler_download_SuccessOrFailed.sendMessage(mg);
				// DowonloadSucceed downloadSucceed = new
				// DowonloadSucceed(targetURL, fileName.toString());
				// this.runOnUiThread(downloadSucceed);

			} else {
				b.putBoolean("State", false);
				b.putString("File_name", targetPath.toString());

				Message mg = new Message();
				mg.setData(b);
				handler_download_SuccessOrFailed.sendMessage(mg);

				// DowonloadFailed downloadfailed = new
				// DowonloadFailed(targetURL, fileName.toString(),
				// serverResponse);
				// this.runOnUiThread(downloadfailed);
			}

		}
	}

	/** Check Free Space **/

	private boolean checkFreeSpace() {
		String fileDirPath = getFilesDir().getAbsolutePath() + "/";
		File filesDir = new File(fileDirPath);
		long usableSpace = filesDir.getUsableSpace();
		int size = Constant.videoHistry.size();
		// if memory not enough, clear All
		if (usableSpace < Constant.MIN_FREE_SPACE) {
			for (int i = 0; i < size; i++) {
				File file = new File(fileDirPath
						+ Constant.videoHistry.get(0).getFileName());
				if (file.exists()) {
					file.delete();
				}
			}
			VideoHistoryObject.removeVideoHistory(this, size);
			Constant.videoHistry = VideoHistoryObject.getVideoHistory(this);

			// if still not enough...do not download...
			if (filesDir.getUsableSpace() < Constant.MIN_FREE_SPACE) {
				return false;
			}
		}

		// if save 100 data,remove 50
		else if (size >= 100) {
			for (int i = 0; i < 50; i++) {
				File file = new File(Constant.videoHistry.get(0).getFileName());
				if (file.exists()) {
					file.delete();
				}
			}
			VideoHistoryObject.removeVideoHistory(this, 50);
			Constant.videoHistry = VideoHistoryObject.getVideoHistory(this);
		}

		return true;
	}

	private OnTouchListener viewPager_touch = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			v.getParent().requestDisallowInterceptTouchEvent(true);
			return false;

		}
	};

	private OnPageChangeListener change_page = new OnPageChangeListener() {

		@Override
		public void onPageSelected(int position) {
			for (int i = 0; i < imageViews.length; i++) {
				imageViews[position]
						.setBackgroundResource(R.drawable.details_dot);
				// ?嚙踝蕭謕蕭豲嚙踝蕭嚙�??嚙踝蕭謕蕭��蕭��蕭謕蕭豲蕭嚙�????????????嚙踝蕭謕蕭嚙�?嚙踝蕭謕蕭豲嚙踝蕭嚙�?嚙踝蕭謕蕭豲嚙踝蕭嚙�?????嚙踝蕭謕蕭��蕭嚙踐�蕭豲嚙踝蕭嚙�??????雓蕭嚙踝蕭嚙�????嚙踝蕭謕蕭豲?頩�蕭???嚙踝蕭謕蕭豲���ㄜ嚙踝蕭嚙�???????嚙踝蕭謕蕭豲嚙踝蕭嚙�??????
				if (position != i) {
					imageViews[i]
							.setBackgroundResource(R.drawable.details_dot_grey);
				}
			}

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// ProductDetail_pic_viewPager.getParent()
			// .requestDisallowInterceptTouchEvent(true);
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

	};

	// added by Henry
	private void addToShoppingCar(final String productID) {
		// TODO Auto-generated method stub
		Log.e(TAG, "addToCart BEGIN");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "ProductID" };
				String[] data = { productID };
				String result = Http.post(Constant.ADD_TO_CART, params, data,
						Product_Detail_Activity.this);
				Bundle b = new Bundle();
				Message m = new Message();

				if (result.contains("success")) {
					ApplicationController.isCartChanged = true;
					b.putString("State", "add_success");
					m.setData(b);
					http_toast.sendMessage(m);
				} else {
					b.putString("State", "failed");
					m.setData(b);
					http_toast.sendMessage(m);

				}
			}
		}.start();
	}

	private void HTTP_getMyProductUpload(final String price, final String des) {

		new Thread() {
			@Override
			public void run() {

				String[] params = { "ProductID", "Title", "Type", "Price",
						"CC", "Lat", "Lng", "Des", "MemberID" };
				String[] data = { product_object.getProductID(),
						product_object.getTitle(), product_type + "", price,
						product_object.getCC(), product_object.getLat() + "",
						product_object.getLng() + "", des, id };

				String result = Http.post(Constant.GET_MY_PRODUCT_UPDATE,
						params, data, Product_Detail_Activity.this);

				Bundle b = new Bundle();
				Message m = new Message();

				if (result.contains("success")) {

					b.putString("State", "renew");
					m.setData(b);

					http_toast.sendMessage(m);

				} else {
					b.putString("State", "failed");
					m.setData(b);
					http_toast.sendMessage(m);

				}

			}

		}.start();
	}

	private void HTTP_getMyProductDelete() {

		new Thread() {
			@Override
			public void run() {

				String[] params = { "ProductID" };
				String[] data = { product_object.getProductID() };

				String result = Http.post(Constant.GET_MY_PRODUCT_DELETE,
						params, data, Product_Detail_Activity.this);

				Bundle b = new Bundle();
				Message m = new Message();

				if (result.contains("success")) {

					b.putString("State", "delete");
					m.setData(b);

					http_toast.sendMessage(m);

				} else {
					b.putString("State", "failed");
					m.setData(b);
					http_toast.sendMessage(m);

				}

			}

		}.start();

	}

	private void HTTP_startChatContent(final String report) {

		new Thread() {
			@Override
			public void run() {

				//String[] params = { "UserIDTo", "ProductID", "ProductTitle",
				//		"Content", "SendTime" };
				/*String[] data = { product_object.getID(),
						product_object.getProductID(),
						product_object.getTitle(), report, Util.getTime() };*/
				String[] params = { "UserIDTo", "ProductID", "ProductTitle",
						"Content" };
				String[] data = { product_object.getID(),
						product_object.getProductID(),
						product_object.getTitle(), report};

				String result = Http.post(Constant.START_CHAT_CONTENT, params,
						data, Product_Detail_Activity.this);
Log.d("chat","chat result:"+result );
				Bundle b = new Bundle();
				Message m = new Message();

				if (result.contains("success")) {

					b.putString("State", "chat");
					m.setData(b);

					http_toast.sendMessage(m);

				} else {
					b.putString("State", "failed");
					m.setData(b);
					http_toast.sendMessage(m);

				}

			}

		}.start();

	}

	private void HTTP_reportProduct(final String reason) {

		new Thread() {
			@Override
			public void run() {

				String[] params = { "ProductID", "Reason", "ReportTime" };
				String[] data = { product_object.getProductID(), reason,
						Util.getTime() };

				String result = Http.post(Constant.START_CHAT_CONTENT, params,
						data, Product_Detail_Activity.this);

				Bundle b = new Bundle();
				Message m = new Message();

				if (result.contains("success")) {

					/*
					 * b.putString("State", "chat"); m.setData(b);
					 * 
					 * http_toast.sendMessage(m);
					 */
				}

			}

		}.start();

	}

	private void HTTP_favoriteProduct(final String productID) {

		new Thread() {
			@Override
			public void run() {

				String[] params = { "ProductID", "AddTime" };
				String[] data = { productID, Util.getTime() };

				String result = Http.post(Constant.FAVORITE_PRODUCT, params,
						data, Product_Detail_Activity.this);
				Log.d("result", "fav result:" + result);
			}

		}.start();

	}

	private void startChat() {

		// if login, start chat
		if (PreferenceUtil.getBoolean(Product_Detail_Activity.this,
				Constant.USER_LOGIN)) {
			DialogListUtil.showChatDialog(Product_Detail_Activity.this,
					new OnChatClickListener() {
						@Override
						public void onChatClick(String report) {

							HTTP_startChatContent(report);

						}
					});
		}
		// if not login, goto login.
		else {

			Intent it = new Intent(Product_Detail_Activity.this,
					LoginActivity.class);
			startActivity(it);

		}
	}

	private OnClickListener report_click = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if (PreferenceUtil.getBoolean(Product_Detail_Activity.this,
					Constant.USER_LOGIN)) {
				DialogListUtil.showReportDialog(Product_Detail_Activity.this,
						submitClickListener);
			} else {
				Intent it = new Intent(Product_Detail_Activity.this,
						LoginActivity.class);
				startActivity(it);
			}

		}
	};

	private OnClickListener favor_click = new OnClickListener() {

		@Override
		public void onClick(View v) {
			int lovePosition = 0;

			/*----- remove love -----*/
			if (v.getTag().toString().equals("true")) {
				v.setTag("false");

				/*----- search favor position -----*/
				for (int i = 0; i < favorList.size(); i++) {
					if (favorList.get(i).getProductID()
							.equals(product_object.getProductID())) {
						lovePosition = i;
						break;
					}
				}
				v.setBackgroundResource(R.drawable.cell_product_unlove);
				PIPI_Tool.removeFavorite(Product_Detail_Activity.this,
						lovePosition, favorList, isActionSell);
			}
			/*----- add love -----*/
			else {
				v.setTag("true");

				v.setBackgroundResource(R.drawable.cell_product_love);
				FavoriteObject favoriteObject = new FavoriteObject();

				favoriteObject.setProductID(product_object.getProductID());
				favoriteObject.setProductTitle(product_object.getTitle());
				favoriteObject.setTime(product_object.getTime());
				favoriteObject.setContact(product_object.getContact());

				
					favoriteObject.setMemberID(product_object.getID());
					favoriteObject.setPicNumber(product_object.getPicNumber());
					favoriteObject.setType(product_object.getType());
					favoriteObject.setPrice(product_object.getPrice());
					favoriteObject.setDistance(product_object.getDistance());
					favoriteObject.setStock(product_object.getStock());
					favoriteObject.setDes(product_object.getDes());
					favoriteObject.setLat(product_object.getLat());
					favoriteObject.setLng(product_object.getLng());
					favoriteObject.setBrand(product_object.getBrand());
					favoriteObject.setNewOld((product_object.getNewOld()));
					favoriteObject.setSNO((product_object.getSNO()));

					
				PIPI_Tool.addFavorite(Product_Detail_Activity.this,
						favoriteObject, favorList, isActionSell);
				if (PreferenceUtil.getBoolean(Product_Detail_Activity.this,
						Constant.USER_LOGIN))

					HTTP_favoriteProduct(favoriteObject.getProductID());

			}

		}
	};
	/*----- reNew product -----*/
	private OnClickListener reNewOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			HTTP_getMyProductUpload(ed_priceEditText.getText().toString()
					.trim(), ed_desEditText.getText().toString().trim());
		}
	};
	/*----- delete product -----*/
	private OnClickListener deleteOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Builder builder = new Builder(Product_Detail_Activity.this);
			builder.setMessage(getString(R.string.alert_delete_product_msg));
			builder.setPositiveButton(getString(R.string.confirm),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							HTTP_getMyProductDelete();
						}
					});
			builder.setNegativeButton(getString(R.string.cancel),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			builder.show();
		}
	};

	/*----- type -----*/
	private OnClickListener typeOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			DialogListUtil.showTypeDialog(Product_Detail_Activity.this, true,
					new OnTypeClickListener() {

						@Override
						public void onTypeClick(int position) {
							product_type = position;
							ed_typeTextView_edit.setText(types[position]);
						}
					});
		}
	};

	private OnClickListener playOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			group.setVisibility(View.GONE);
			favorButton.setVisibility(View.GONE);

			// Constant.getInstance().sendGoogleAnalytic("android_seller_detail_video");
			if (mIsPlaying) {
				mTextureVideoView.pause();
				mIsPlaying = false;
				// playButton.setVisibility(View.VISIBLE);

			} else {
				mIsPlaying = true;
				mTextureVideoView.setVisibility(View.VISIBLE);
				playButton.setVisibility(View.GONE);
				ProductDetail_video_previewImageView.setVisibility(View.GONE);
				try {
					if (mIsFirstTimeIn) {
						mIsFirstTimeIn = false;
						mTextureVideoView.setDataSource(videoPath);

					} else {
						mTextureVideoView.play();
					}

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				}
			}

		}
	};
	private OnClickListener Cart = new OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {

			/*
			 * case R.id.productDetail_reportTextView:
			 * 
			 * 
			 * break;
			 */

			case R.id.btn_cart:
				addToShoppingCar(product_object.getProductID());
				/*
				 * String title = "Alert"; String msg =
				 * "Want to added this product to shopping car?\n" +
				 * product_object.getTitle(); String positiveBtn = "ok"; String
				 * negativeBtn = "cancel";
				 * 
				 * DialogUtil.pushGeneralDialog(Product_Detail_Activity.this,
				 * title, msg, positiveBtn, negativeBtn, new
				 * IGenericDialogUtil.IGenericBtnClickListener() {
				 * 
				 * @Override public void PositiveMethod(DialogInterface dialog,
				 * int id) { // TODO Auto-generated method stub
				 * addToShoppingCar(product_object.getProductID()); }
				 * 
				 * @Override public void NegativeMethod(DialogInterface dialog,
				 * int id) { // TODO Auto-generated method stub
				 * 
				 * } });
				 */

				break;

			}

		}
	};
	private OnReportSubmitClickListener submitClickListener = new OnReportSubmitClickListener() {

		@Override
		public void onReportSubmitClick(String reason) {

			HTTP_reportProduct(reason);
		}
	};
	private MediaPlayerListener mMediaPlayerListener = new MediaPlayerListener() {

		@Override
		public void onVideoPrepared() {
			// TODO Auto-generated method stub
			// preview.setVisibility(View.GONE);
			mTextureVideoView.play();
		}

		@Override
		public void onVideoEnd() {
			// TODO Auto-generated method stub
			mTextureVideoView.stop();
			mTextureVideoView.setVisibility(View.GONE);
			group.setVisibility(View.VISIBLE);

			ProductDetail_video_previewImageView.setVisibility(View.VISIBLE);
			favorButton.setVisibility(View.VISIBLE);

			playButton.setVisibility(View.VISIBLE);
			mIsPlaying = false;
		}
	};

	private Handler handler_download_SuccessOrFailed = new Handler() {
		public void handleMessage(Message msg) {
			boolean state = msg.getData().getBoolean("State");
			// String file_name =msg.getData().getString("File_name");

			if (state) {
				Log.d("state", "download_finish");
				dismissProgressDialog();

				didFinishWithDownloadFile();

			} else {
				dismissProgressDialog();
			}

		}

	};

	private Handler Get_rating_success = new Handler() {

		public void handleMessage(Message msg) {

			initRating();

		}

	};

	private Handler http_toast = new Handler() {

		public void handleMessage(Message msg) {

			if (msg.getData().getString("State").equals("renew")) {

				Toast.makeText(Product_Detail_Activity.this,
						getString(R.string.myproduct_edit_upload_success),
						Toast.LENGTH_LONG).show();

			} else if (msg.getData().getString("State").equals("delete")) {

				Toast.makeText(Product_Detail_Activity.this,
						getString(R.string.myproduct_edit_delete_success),
						Toast.LENGTH_LONG).show();
				MyProductActivity.member_delete = true;
				MyProductActivity.mMemberID = id;
				finish();
			} else if (msg.getData().getString("State").equals("chat")) {

				Toast.makeText(Product_Detail_Activity.this,
						getString(R.string.alert_startChatContentSuccess),
						Toast.LENGTH_SHORT).show();
				setResult(3);
				finish();
			} else if (msg.getData().getString("State").equals("add_success")) {
				Toast.makeText(Product_Detail_Activity.this,
						"add to cart success", Toast.LENGTH_SHORT).show();
			}

			else {
				Toast.makeText(Product_Detail_Activity.this,
						getString(R.string.myproduct_edit_upload_failed),
						Toast.LENGTH_LONG).show();

			}

		}

	};

	/** ProgressDialog methods **/
	private void displayProgressDialog(String title, String message) {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(this, title, message);
		}
	}

	private void dismissProgressDialog() {
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}

	public void didFinishWithDownloadFile() {

		String path = getFilesDir() + "/" + product_object.getProductID()
				+ "-video.mp4";
		File videoFile = new File(path);

		videoPath = videoFile.getAbsolutePath();
		loadingBar.setVisibility(View.GONE);
		favorButton.setVisibility(View.VISIBLE);
		playButton.setVisibility(View.VISIBLE);
		// getFirstFrame(path, preview);

	}

}

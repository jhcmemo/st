package pipimy.product.activity;

import java.util.ArrayList;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import pipimy.main.MainActivity;
import pipimy.main.MyProductActivity;
import pipimy.nearby.ProductObject;
import pipimy.object.FavoriteObject;
import pipimy.object.TrackObject;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.Http;
import pipimy.others.PIPI_Tool;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class favor_fragment_product extends Fragment implements Runnable {
GridView gridview_product;
private ArrayList<FavoriteObject> favor_list ;
private CommonAdapter <FavoriteObject>adapter;
private String product_id="";
ImageLoader mImageLoader;
ProductObject productObj =new ProductObject();	
TextView tv_nodata;
FavorTracing_Activity activity;   


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		favor_list = PIPI_Tool.getFavorites(getActivity(),favor_list,true);	
     	mImageLoader = VolleySingleton.getInstance().getImageLoader();
		activity = ( FavorTracing_Activity) getActivity();

    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.favor_fragment_product, container, false);
	}
	
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		tv_nodata=(TextView) view.findViewById(R.id.tv_nodata);
		gridview_product=(GridView) view.findViewById(R.id.favor_gridview);
		gridview_product.setNumColumns(2);
		gridview_product.setVerticalSpacing(11);
		//initGridview();
		
	}
	@Override  
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState); 
        //Log.e("onActivityCreated","Chat Fragment");
    }  
  
	@Override
	public void onResume() {
        super.onResume();
        
        favor_list = PIPI_Tool.getFavorites(getActivity(),favor_list,true);	
		if(favor_list.size()!=0){
			initGridview() ;     
			activity.fav_size=1;
			Log.e("favor","favor改變資料");
			
		

		}else{		
			activity.fav_size=0;
			gridview_product.setVisibility(View.GONE);
			tv_nodata.setVisibility(View.VISIBLE);
			
			if(activity.find_item){
				activity.fragment_product_callBack();
				  }
			
		}
        
    }
	
	@Override
    public void onPause() {
        super.onPause();
        //Log.e("onPause","Chat Fragment");
    }


	@Override
	public void run() {
		String[] params = { "ProductID" };
		String[] data = {product_id };
				
		String result = Http.post(Constant.GET_PRODUCT_VIEW, params,data, getActivity());
	 
		Log.d("favor","result :"+result);
		if(result.contains("true")){
			    Intent it =new Intent();
			    it =new Intent(getActivity(),Product_Detail_Activity.class);
			    it.putExtra("product_object", productObj);
				it.putExtra("edit", false);		
			    startActivity(it);
				
				
		}
		else{
			handler_NOproduct.sendMessage(handler_NOproduct.obtainMessage());
			
		}
		
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initGridview() {
		
		 
		if(favor_list.size()==0){
			gridview_product.setVisibility(View.GONE);
			tv_nodata.setVisibility(View.VISIBLE);
			
		}
		else{
		
		  adapter = new CommonAdapter(getActivity(),
		    		favor_list, R.layout.favor_product_item) {
					@Override 
					public void setViewData(CommonViewHolder commonViewHolder,
							View currentView, Object item ) {
						
						final FavoriteObject favoriteObject = (FavoriteObject) item;
			
						//RelativeLayout rl_favor=(RelativeLayout) commonViewHolder.get(commonViewHolder, currentView, R.id.rl_favor);
						//rl_favor.setTag(favoriteObject);
						//rl_favor.setOnClickListener(item_click);
						NetworkImageView favor_product_pic = (NetworkImageView) commonViewHolder.get(commonViewHolder, currentView, R.id.favor_product_pic);
						String fileName = favoriteObject.getProductID() + "-pic1.jpg";
						favor_product_pic.setImageUrl(Constant.PicServerURL+fileName,mImageLoader);
						favor_product_pic.setOnClickListener(item_click);
						favor_product_pic.setTag(favoriteObject);
						
						TextView tv_member_name =(TextView)commonViewHolder.get(commonViewHolder, currentView, R.id.tv_member_name);
						tv_member_name.setText(favoriteObject.getMemberID());
						
						//PreferenceUtil.getString(FavorTracing_Activity.this, Constant.USER_ID)
						TextView tv_prdouct_title =(TextView) commonViewHolder.get(commonViewHolder, currentView, R.id.tv_prdouct_title);
						tv_prdouct_title.setText(favoriteObject.getProductTitle());
						
						TextView tv_price =(TextView) commonViewHolder.get(commonViewHolder, currentView, R.id.tv_price);
						tv_price.setText("$"+favoriteObject.getPrice());
						
						
						Button favor_product_delete =(Button)commonViewHolder.get(commonViewHolder, currentView, R.id.favor_product_delete);
						favor_product_delete.setTag( favoriteObject);
						favor_product_delete.setOnClickListener(item_click);
					}
		     
				};
			    
				gridview_product.setAdapter(adapter);
		}
	}
	
	
	private OnClickListener item_click=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			FavoriteObject favoriteObject;
			TrackObject trackObject; 
		     int likePosition=0;
		     int tracePosition=0;
			switch (v.getId()){

			case R.id.favor_product_pic:
				 
				favoriteObject = (FavoriteObject) v.getTag();	
				product_id=favoriteObject.getProductID();
				productObj.setProductID(favoriteObject.getProductID());
				productObj.setID(favoriteObject.getMemberID());
				productObj.setType(favoriteObject.getType());
				productObj.setDistance(favoriteObject.getDistance());
				productObj.setTime(favoriteObject.getTime());
				productObj.setPrice(favoriteObject.getPrice());
				productObj.setPicNumber(favoriteObject.getpicNumber());
				productObj.setPayment(favoriteObject.getPayment());
				productObj.setDelivery(favoriteObject.getDelievery());
				productObj.setTitle(favoriteObject.getProductTitle());
				productObj.setStock(favoriteObject.getStock());
				productObj.setDes(favoriteObject.getDes());
				productObj.setLat(favoriteObject.getLat());
				productObj.setLng(favoriteObject.getLng());
				productObj.setNewOld(favoriteObject.getNewOld());
				productObj.setBrand(favoriteObject.getBrand());
				productObj.setSNO(favoriteObject.getSNO());

				
				

				new Thread(favor_fragment_product.this).start(); 
				break;
			case R.id.favor_product_delete:
				favoriteObject = (FavoriteObject) v.getTag();	

				/*----- search favor position -----*/
				for (int i = 0; i < favor_list.size(); i++) {
					if (favor_list.get(i).getProductID().equals(favoriteObject.getProductID())) {
						likePosition = i;
						break;
					}
				 }
				
				//Log.d("favor","likePosition :"+likePosition);
				PIPI_Tool.removeFavorite(getActivity(), likePosition, favor_list , true);
				favor_list.clear();
				favor_list = PIPI_Tool.getFavorites(getActivity(),favor_list,true);			
				initGridview();
				
				if(favor_list.size()==0){
					activity.fav_size=0;
					activity.fragment_product_callBack();
					}else{
						
				    activity.fav_size=1;

					}
				
				
				break;
				
			
			}
			
		

		}
	};

	
	
	private Handler handler_NOproduct = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			Toast.makeText(getActivity(), "No product!", 2000).show();
			
	 }
	};
	
	
	
	
}

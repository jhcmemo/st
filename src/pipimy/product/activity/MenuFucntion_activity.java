package pipimy.product.activity;

import com.capricorn.ArcMenu;

import pipimy.login.LoginActivity;
import pipimy.main.LunchActivity;
import pipimy.main.MainActivity;
import pipimy.main.ProductFragment;
import pipimy.others.Constant;
import pipimy.others.PreferenceUtil;
import pipimy.service.R;
import pipimy.setting.activity.PostActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MenuFucntion_activity extends Activity {
	ArcMenu arcMenu;
	public static int res = R.drawable.new_menu;
	private static final int[] ITEM_DRAWABLES = { R.drawable.new_menu_near,
			R.drawable.new_menu_price, R.drawable.new_menu_popular,
			R.drawable.new_menu_latest, R.drawable.menu_post,
			R.drawable.menu_heart, R.drawable.menu_play };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_function);

		RelativeLayout lv = (RelativeLayout) findViewById(R.id.lv);
		lv.setOnClickListener(outside_click);
		arcMenu = (ArcMenu) findViewById(R.id.arc_menu);
		arcMenu.set_drawable(res);

		arcMenu.init(this);
		arcMenu.setOnClickListener(menu_click);

		initArcMenu(arcMenu, ITEM_DRAWABLES);
		Handler handler = new Handler();
		handler.postDelayed(launchRunnable, 300);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		System.gc();

	}

	private Runnable launchRunnable = new Runnable() {

		@Override
		public void run() {
			arcMenu.arc_click();

		}
	};

	private void initArcMenu(ArcMenu menu, int[] itemDrawables) {
		final int itemCount = itemDrawables.length;
		for (int i = 0; i < itemCount; i++) {
			ImageView item = new ImageView(this);
			item.setImageResource(itemDrawables[i]);

			final int position = i;
			menu.addItem(item, new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();

					switch (position) {

					case 0:

						arcMenu.set_drawable(R.drawable.new_ch_menu_near);
						res = R.drawable.new_ch_menu_near;
						setResult(1);

						break;

					case 1:
						arcMenu.set_drawable(R.drawable.new_ch_menu_price);
						res = R.drawable.new_ch_menu_price;

						setResult(2);

						break;

					case 2:
						arcMenu.set_drawable(R.drawable.new_ch_menu_popular);
						res = R.drawable.new_ch_menu_popular;

						setResult(5);

						break;

					case 3:
						arcMenu.set_drawable(R.drawable.new_ch_menu_latest);
						res = R.drawable.new_ch_menu_latest;

						setResult(7);

						break;

					case 4:
						if (PreferenceUtil
								.getBoolean(MenuFucntion_activity.this,
										Constant.USER_LOGIN)) {
							intent.setClass(MenuFucntion_activity.this,
									PostActivity.class);
							startActivity(intent);
						} else {
							intent.setClass(MenuFucntion_activity.this,
									LoginActivity.class);
							startActivity(intent);
						}

						ProductFragment.refresh_gridview = true;

						break;

					case 5:
						if (PreferenceUtil
								.getBoolean(MenuFucntion_activity.this,
										Constant.USER_LOGIN)) {
							intent.setClass(MenuFucntion_activity.this,
									FavorTracing_Activity.class);
							startActivity(intent);
						} else {
							intent.setClass(MenuFucntion_activity.this,
									LoginActivity.class);
							startActivity(intent);
						}

						ProductFragment.refresh_gridview = true;

						break;

					case 6:

						intent.setClass(MenuFucntion_activity.this,
								PlayVideo_Activity.class);
						intent.setAction("all_play");
						startActivity(intent);

						break;
					}

					finish();

				}
			});
		}
	}

	private OnClickListener menu_click = new OnClickListener() {

		@Override
		public void onClick(View v) {

			finish();
		}
	};
	private OnClickListener outside_click = new OnClickListener() {

		@Override
		public void onClick(View v) {

			finish();
		}
	};

}

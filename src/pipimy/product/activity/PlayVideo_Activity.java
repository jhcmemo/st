package pipimy.product.activity;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import com.amazonaws.org.apache.http.Consts;
import com.amazonaws.org.apache.http.params.HttpParams;
import com.google.gson.Gson;

import pipimy.main.ApplicationController;
import pipimy.nearby.ProductObject;
import pipimy.object.FavoriteObject;
import pipimy.object.VideoHistoryObject;
import pipimy.others.Constant;
import pipimy.others.Http;
import pipimy.others.PIPI_Tool;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.service.R;
import pipimy.third.TextureVideoView;
import pipimy.third.TextureVideoView.MediaPlayerListener;



import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class PlayVideo_Activity extends Activity {
	
	private View videoLayout;
	private TextView titleTextView;
	//ArrayList<ProductObject> list = new ArrayList<ProductObject>();
	private Dialog ClickVideoDialog;

//	private ArrayList<ProductObject> list;
	private int cur_position = 0;
	private final static String BAD_RESPONSE = "No Response";

	private boolean isPlaying = false;
	private boolean singlePlay = false;
	private int mPosition = 0;
	private final static String NULL_EXCEPTION = "Null Exception";

	// for video
		private SurfaceView surfaceView; 
		private TextureVideoView mTextureVideoView;
		private ProgressBar loadingBar;
		/* Dialogs */
		private ProgressDialog progressDialog;

		RelativeLayout RelativeLayout1;
		private ArrayList<FavoriteObject> favor_list ;

		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_play_video);
		RelativeLayout1=(RelativeLayout) findViewById(R.id.RelativeLayout1);
		loadingBar = (ProgressBar) findViewById(R.id.Play_progressBar);
		videoLayout = (View) findViewById(R.id.Play_videoLayout);
		mTextureVideoView = (TextureVideoView) findViewById(R.id.Play_surfaceView);
		titleTextView = (TextView)findViewById(R.id.Play_titleTextView);
		loadingBar.setVisibility(View.VISIBLE);
		mTextureVideoView.setVisibility(View.INVISIBLE);
		
		    //  Bundle Get_bundle = this.getIntent().getExtras();
		     // list.get(position).getProductId()=Get_bundle.getInt("position",0);
		     
		      init();

		
		
	  }

	@SuppressWarnings("deprecation")
	public void init() {
		//this.list = GlobalVariable.currentSellProductList;
		this.favor_list =  PIPI_Tool.getFavorites(PlayVideo_Activity.this,favor_list,true);	
		
	//	cur_position=  Integer.valueOf(list.get(0).getProductID().trim());
		titleTextView.setText(getString(R.string.nearby_allplay));

		if (getIntent().getAction().equals(Constant.ACTION_SINGLE_PLAY)) {
			  singlePlay = true;
			  mPosition=getIntent().getIntExtra("position", 7);
		     // list.get(position).getProductId()=getIntent().getIntExtra("position",0);

			titleTextView.setText(getString(R.string.nearby_singleplay));
		}
			
		//	Log.d("position","	list.get(1).getProductTitle(); "+	list.get(1).getTitle());
		
			//Log.d("position",list.get(position).getProductId()+"");
			//Log.d("position","list.size: "+list.size());

			
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		RelativeLayout1.setOnClickListener(Play_cancel);
		videoLayout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, metrics.widthPixels));
		mTextureVideoView.setOnClickListener(surfaceClickListener);
		mTextureVideoView.setListener(mMediaPlayerListener);
		
		//if (favor_list.size() > 0) {
			File file ;
			if(singlePlay){
				file = new File(getFilesDir() + "/" + mPosition + "-video.mp4");	
			
				if (file.exists()) {
					playVideo(file.getAbsolutePath());
					Log.d("position","not download");

				} else {
					Log.d("position","download");

					loadingBar.setVisibility(View.VISIBLE);
					mTextureVideoView.setVisibility(View.INVISIBLE);
					HTTP_downLoadVideo(mPosition);
				}
			
			}
			else{
				file = new File(getFilesDir() + "/" + favor_list.get(cur_position).getProductID() + "-video.mp4");	
				if (file.exists()) {
					playVideo(file.getAbsolutePath());
					Log.d("position","not download");

				} else {
					Log.d("position","download");
					loadingBar.setVisibility(View.VISIBLE);
					mTextureVideoView.setVisibility(View.INVISIBLE);
					HTTP_downLoadVideo(Integer.valueOf(favor_list.get(cur_position).getProductID().trim()));
				
			
				}
			}
			
			
		//}
	} 
	private OnClickListener Play_cancel =new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			Log.d("dialog","click outside");
			mTextureVideoView.stop();
			dismissProgressDialog();
			finish();
			
		}
	};

	private OnClickListener surfaceClickListener =new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			mTextureVideoView.pause();
			
			ClickVideoDialog = new Dialog(PlayVideo_Activity.this, R.style.selectorDialog);
			ClickVideoDialog.setContentView(R.layout.proudct_click_video_show_dialog);
			ClickVideoDialog.setCancelable(false);

			//TextView detailTextView = (TextView) ClickVideoDialog.findViewById(R.id.Dialog_play_detail);
			TextView resumeTextView = (TextView) ClickVideoDialog.findViewById(R.id.Dialog_play_resume);
			TextView cancelTextView = (TextView) ClickVideoDialog.findViewById(R.id.Dialog_play_cancel);

			//detailTextView.setOnClickListener(Click_Video);
			resumeTextView.setOnClickListener(Click_Video);
			cancelTextView.setOnClickListener(Click_Video);

			//detailTextView.setTag(0);
			//resumeTextView.setTag(1);
			//cancelTextView.setTag(2);
			ClickVideoDialog.show();
			
		}
	};
	
	
	private OnClickListener Click_Video =new OnClickListener() {
		
		@Override
		public void onClick(View v) {

			switch(v.getId()){
			/*
			case R.id.Dialog_play_detail:
				ClickVideoDialog.dismiss();
				Intent it =new Intent(PlayVideo_Activity.this,Product_Detail_Activity.class);
				
				it.putExtra("productId",getIntent().getStringExtra("productId"));
				it.putExtra("picNumber",getIntent().getIntExtra("picNumber",0));
				it.putExtra("ID", getIntent().getStringExtra("ID"));
				it.putExtra("title", getIntent().getStringExtra("title"));
				it.putExtra("type",  getIntent().getStringExtra("type"));
				it.putExtra("time",  getIntent().getStringExtra("time"));
				it.putExtra("des",getIntent().getStringExtra("des"));
				it.putExtra("price", getIntent().getStringExtra("price"));
				it.putExtra("Lng", getIntent().getDoubleExtra("Lng",0));
				it.putExtra("Lat", getIntent().getDoubleExtra("Lat",0));
				it.putExtra("dis", getIntent().getFloatExtra("dis",0));
				it.putExtra("Delivery",getIntent().getStringExtra("Delivery"));
				
				if (getIntent().getStringExtra("F1_name") != null) {
					it.putExtra("F1_name", getIntent().getStringExtra("F1_name"));
				}
				if (getIntent().getStringExtra("F2_name") != null) {
					getIntent().getStringExtra("F2_name");
				}
				if (getIntent().getStringExtra("F3_name") != null) {
					getIntent().getStringExtra("F3_name");
				}
				
				startActivity(it);
				finish();
				
				break;
			*/
				
			case R.id.Dialog_play_resume:
				ClickVideoDialog.dismiss();
				mTextureVideoView.play();
				
				
				break;
				
			case R.id.Dialog_play_cancel:
				
				
				ClickVideoDialog.dismiss();
				finish();
				break;
			
			
			
			}
			
			
			
			
			
		}
	};
	
	
	  
	private void playVideo(String vedioPath) {

		try {
			
			if(!isPlaying){
			mTextureVideoView.setDataSource(vedioPath);
			}
			// Integer.valueOf(list.get(0).getProductID().trim());

			// check next file is Exists
			if(singlePlay){
			
			}
			
	   else{	
		/*if(isPlaying){
			if (cur_position + 1 < list.size()) {
				File file = new File(getFilesDir() + "/" + list.get(cur_position+1).getProductID() + "-video.mp4");
				if (!file.exists())	
					HTTP_downLoadVideo(cur_position + 1);
	
			}
		}*/
	  
	   }
			
			

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	}
	private void HTTP_downLoadVideo(int position) {
		if (!startDownloadFile(Constant.VedioServerURL + position + "-video.mp4", position  + "-video.mp4", false, null, null)) {
			Toast.makeText(this, getString(R.string.device_memory_few), Toast.LENGTH_LONG).show();
			Log.d("video","video url: "+Constant.VedioServerURL + position + "-video.mp4");
		}

	}
	/** Download file to SDcard ***/
	public boolean startDownloadFile(String targetDownloadURL, String fileName, boolean showProgressDialog,
			String promptTitle, String promptMessage) {

		if (targetDownloadURL == null || targetDownloadURL.length() == 0 || fileName == null || fileName.length() == 0) {
			return false;
		}

		if (!checkFreeSpace()) {
			return false;
		}

		File file = new File(getFilesDir() + "/" + fileName);
		try {

			file.createNewFile();
			VideoHistoryObject historyObject = new VideoHistoryObject();
			historyObject.setFileName(fileName);
			historyObject.setCreateTime(Util.getTime());
		    VideoHistoryObject.addVideoHistory(this, historyObject);
			Constant.videoHistry = VideoHistoryObject.getVideoHistory(this);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DownloadFileThread thread = new DownloadFileThread(targetDownloadURL, file);
		thread.setName("DownloadFileBackground");
		thread.start();
		
	
//		Log.d(TAG, "Start Download File form URL: " + targetDownloadURL);

		if (showProgressDialog == true) {
			this.displayProgressDialog(promptTitle, promptMessage);
		}

		return true;
	}	 		 
	/**
	 * Download file from target URL
	 **/
	class DownloadFileThread extends Thread {
		private String targetdownloadURLString;
		private File targetPath;

		DownloadFileThread(String targetdownloadURLString, File targetPath) {
			this.targetdownloadURLString = targetdownloadURLString;
			this.targetPath = targetPath;
		}

		public void run() {
		Http.startDownloadFile(targetdownloadURLString, targetPath);
		Bundle b =new Bundle();
		
		if (Http.isDownloadSucceed) {			
			b.putBoolean("State", true);
			b.putString("File_name", targetPath.toString());
			
			Message mg =new Message();
			mg.setData(b);
			
			handler_download_SuccessOrFailed.sendMessage(mg);
			//DowonloadSucceed downloadSucceed = new DowonloadSucceed(targetURL, fileName.toString());
			//this.runOnUiThread(downloadSucceed);
 
		} else {
			b.putBoolean("State", false);
			b.putString("File_name", targetPath.toString());
		
			Message mg =new Message();
			mg.setData(b);
			handler_download_SuccessOrFailed.sendMessage(mg);

			//DowonloadFailed downloadfailed = new DowonloadFailed(targetURL, fileName.toString(), serverResponse);
			//this.runOnUiThread(downloadfailed);
		}
		
		  }
	}

	

	/** Check Free Space **/

	private boolean checkFreeSpace() {
		String fileDirPath = getFilesDir().getAbsolutePath() + "/";
		File filesDir = new File(fileDirPath);
		long usableSpace = filesDir.getUsableSpace();
		int size = Constant.videoHistry.size();
		// if memory not enough, clear All
		if (usableSpace <Constant.MIN_FREE_SPACE) {
			for (int i = 0; i < size; i++) {
				File file = new File(fileDirPath + Constant.videoHistry.get(0).getFileName());
				if (file.exists()) { 
					file.delete(); 
				}
			}
			 VideoHistoryObject.removeVideoHistory(this, size);
			Constant.videoHistry = VideoHistoryObject.getVideoHistory(this);

			// if still not enough...do not download...
			if (filesDir.getUsableSpace() < Constant.MIN_FREE_SPACE) {
				return false;
			}
		}

		// if save 100 data,remove 50
		else if (size >= 100) {
			for (int i = 0; i < 50; i++) {
				File file = new File(Constant.videoHistry.get(0).getFileName());
				if (file.exists()) {
					file.delete();
				}
			}
			 VideoHistoryObject.removeVideoHistory(this, 50);
			Constant.videoHistry = VideoHistoryObject.getVideoHistory(this);
		}

		return true;
	}
	


	private MediaPlayerListener mMediaPlayerListener = new MediaPlayerListener(){

		@Override
		public void onVideoPrepared() {
			// TODO Auto-generated method stub
			
		
					
			loadingBar.setVisibility(View.INVISIBLE);
			mTextureVideoView.setVisibility(View.VISIBLE);
			isPlaying = true;
			mTextureVideoView.play();
			
			
		
			
			
			Log.d("mediaplay","準備播放");
		}
		@Override
		public void onVideoEnd() {
			// TODO Auto-generated method stub
			// replay
			isPlaying = false;
			loadingBar.setVisibility(View.VISIBLE);
			mTextureVideoView.setVisibility(View.INVISIBLE);
			
						if (!singlePlay) {
							cur_position++;
							if (cur_position < favor_list.size()) {
								
								
								play(cur_position);
								
								
							} else {
								mTextureVideoView.stop();
								finish();								
							}
						}
						// single play
						else {
							mTextureVideoView.stop();
							finish();
						}
			
		}
		
	};
	
	private void play(int position) {
		loadingBar.setVisibility(View.VISIBLE);
		mTextureVideoView.setVisibility(View.INVISIBLE);
		
	
			if (position < favor_list.size()) {
				File file = new File(getFilesDir() + "/" + favor_list.get(position).getProductID() + "-video.mp4");
				
				if (!file.exists())	
					
					HTTP_downLoadVideo(Integer.valueOf(favor_list.get(position).getProductID()));
				else
					playVideo(file.toString());
				
				
			}else{
				isPlaying = false;

				
			}
	
		
		
		
		/*File file = new File(getFilesDir() + "/" + Integer.valueOf(list.get(position).getProductID().trim()) + "-video.mp4");

		if (file.exists()) {
			playVideo(file.getAbsolutePath());

		} else {
			isPlaying = false;
		}*/
	}
private Handler handler_download_SuccessOrFailed = new Handler(){
	 	
		public void handleMessage (Message msg){
			
			boolean state =msg.getData().getBoolean("State");
			String file_name =msg.getData().getString("File_name");
			
			if(state){
				Log.d("state","download_finish");
				dismissProgressDialog();

				didFinishWithDownloadFile(file_name);

			}
			else{
				dismissProgressDialog();

			}
			

			
		 
		}
		 
		
	};
	
	/** ProgressDialog methods **/
	private void displayProgressDialog(String title, String message) {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(this, title, message);
		}
	}
	private void dismissProgressDialog() {
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}
	public void didFinishWithDownloadFile(String filePath) {
		File file;
		if(singlePlay){
			 file = new File(getFilesDir() + "/" + mPosition + "-video.mp4");
	
		}
		  else{
			 file = new File(getFilesDir() + "/" + favor_list.get(cur_position).getProductID() + "-video.mp4");

		}

		if (file.exists()) {
			playVideo(file.getAbsolutePath());
		}
		
	}


}

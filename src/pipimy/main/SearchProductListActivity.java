package pipimy.main;

import java.util.ArrayList;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;

import pipimy.nearby.ProductListSorter;
import pipimy.nearby.ProductObject;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;

public class SearchProductListActivity extends Activity {

	String TAG = "SearchProductListActivity";
	GridView product_gridview;
	PullToRefreshGridView mPullRefreshGridView;

	ImageLoader mImageLoader;
	RequestQueue queue;
	ArrayList<ProductObject> productList = new ArrayList<ProductObject>();
	String[] types;
	
	final static String searchByKeyWord = "_searchByKeyWord";
	final static String searchByType = "_searchByType";
	String name = "";
	String searchBy = "";
	
	/*
	 * sorting bar layout
	 */
	private View sortBarView;
	RelativeLayout ProductList_sortTimeLayout;
	RelativeLayout ProductList_sortNearLayout;
	RelativeLayout ProductList_sortHotLayout;
	RelativeLayout ProductList_sortPriceLayout;
	
	TextView tv_sort_lastTime;
	TextView tv_sort_location;
	TextView tv_sort_hot;
	TextView tv_sort_price;
	
	private int lastVisibleItem = 0;
	private boolean isSortBarShow = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.fragment_product);
		
		sortBarView = (View)findViewById(R.id.ProductList_sortBar);
		ProductList_sortTimeLayout = (RelativeLayout)findViewById(R.id.ProductList_sortTimeLayout);
		ProductList_sortNearLayout = (RelativeLayout)findViewById(R.id.ProductList_sortNearLayout);
		ProductList_sortHotLayout = (RelativeLayout) findViewById(R.id.ProductList_sortHotLayout);
		ProductList_sortPriceLayout = (RelativeLayout)findViewById(R.id.ProductList_sortPriceLayout);

		tv_sort_lastTime = (TextView)findViewById(R.id.tv_sort_lastTime);
		tv_sort_location = (TextView)findViewById(R.id.tv_sort_location);
		tv_sort_hot = (TextView)findViewById(R.id.tv_sort_hot);
		tv_sort_price = (TextView)findViewById(R.id.tv_sort_price);

		// 嚙踐嚙踐馭嚙質�������雓�頩蕭
		ProductList_sortTimeLayout.setOnClickListener(onclick);
		ProductList_sortNearLayout.setOnClickListener(onclick);
		ProductList_sortHotLayout.setOnClickListener(onclick);
		ProductList_sortPriceLayout.setOnClickListener(onclick);

		mPullRefreshGridView = (PullToRefreshGridView) findViewById(R.id.product_gridview);
		mPullRefreshGridView.setOnScrollListener(scrollListener);
		product_gridview = mPullRefreshGridView.getRefreshableView();
		mPullRefreshGridView
				.setOnRefreshListener(new OnRefreshListener2<GridView>() {

					@Override
					public void onPullDownToRefresh(
							PullToRefreshBase<GridView> refreshView) {

						mPullRefreshGridView.onRefreshComplete();
					}

					@Override
					public void onPullUpToRefresh(
							PullToRefreshBase<GridView> refreshView) {

					}

				});

		init();
		initActionBar();
		setListView();
	}
	
	private void init() {
		// TODO Auto-generated method stub
		queue = VolleySingleton.getInstance().getRequestQueue();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		productList = ApplicationController.searchProductList;
		types = getResources().getStringArray(R.array.types);
		Bundle params = getIntent().getExtras();
		if(params != null) {
			searchBy = params.getString("searchBy");
			if(searchBy.equals(searchByKeyWord))
				name = params.getString("typeOrKeywordName");
			else name = types[Integer.parseInt(params.getString("typeOrKeywordName"))];
			Log.e(TAG, "get bundle search by = "+searchBy+ " name = "+name);
		}
	}

	private void initActionBar() {
		// TODO Auto-generated method stub
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle(name);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setListView() {

		product_gridview.setAdapter(new CommonAdapter(this,
				productList, R.layout.product_gridview_item) {
			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				// TODO Auto-generated method stub
				ProductObject productObj = (ProductObject) item;
				NetworkImageView iv_product_pic = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView, R.id.iv_product_pic);
				iv_product_pic.setScaleType(ImageView.ScaleType.CENTER_CROP);

				NetworkImageView iv_member_icon = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView, R.id.iv_member_icon);

				Button btn_play_video = (Button) commonViewHolder.get(
						commonViewHolder, currentView, R.id.btn_play_video);
				btn_play_video.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
					}
				});

				TextView tv_prdouct_title = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_prdouct_title);

				TextView tv_price = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_price);

				//TextView tv_prdouct_type = (TextView) commonViewHolder.get(
			//			commonViewHolder, currentView, R.id.tv_prdouct_type);

				TextView tv_member_name = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_member_name);

				TextView tv_date = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_date);

				TextView tv_location = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_location);

				// /////////////////////////////////////

				iv_member_icon.setImageUrl(productObj.getSellerPicUrl(), mImageLoader);
				iv_product_pic.setImageUrl(productObj.getProductPicUrl(), mImageLoader);
				tv_prdouct_title.setText(productObj.getTitle());
				tv_date.setText(productObj.getTime());
				//tv_prdouct_type.setText(types[Integer.parseInt(productObj.getType())]);
				tv_price.setText("$" + productObj.getPrice());

				/*----- set ID -----*/
				if (productObj.getID().equals("null")
						&& !productObj.getContact().equals("null")) {
					tv_member_name.setText(productObj.getContact());
					tv_member_name.setTextColor(getResources().getColor(
							R.color.light_grey));
				} else {
					tv_member_name.setText(productObj.getID());
					tv_member_name.setTextColor(getResources().getColor(
							R.color.style_red));
				}

				/*----- set distance -----*/
				if (productObj.getLat() == 0 || productObj.getLng() == 0) {
					tv_location.setText("--km");
				} else {
					float d = productObj.getDistance() / 1000;
					if (d >= 1000) {
						tv_location.setText(">"
										+ (int)(productObj.getDistance() / 1000 / 1000)
										+ "K km");
					} else if (d > 100) {
						tv_location.setText(String.format("%.0f", (productObj.getDistance() / 1000))
								+ "km");
					} else if (d > 10) {
						tv_location.setText(String.format("%.1f", (productObj.getDistance() / 1000))
								+ "km");
					} else {
						tv_location.setText(String.format("%.2f", (productObj.getDistance() / 1000))
								+ "km");
					}

				}

			}
		});
		

	}
	
	/*
	 *  handle sorting bar
	 */
	private OnScrollListener scrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			/*----- up Scroll -----*/
			Log.i("Scroll", "firstVisibleItem:" + firstVisibleItem
					+ ",visibleItemCount:" + visibleItemCount);
			if (firstVisibleItem - lastVisibleItem > 0) {
				if (isSortBarShow) {
					isSortBarShow = false;
					Animation animOut = AnimationUtils.loadAnimation(
							SearchProductListActivity.this, R.anim.push_down_out);
					animOut.setDuration(500);
					sortBarView.setAnimation(animOut);
					sortBarView.startAnimation(animOut);
					animOut.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							sortBarView.setVisibility(View.INVISIBLE);
						}
					});
				}
			}
			/*----- down Scroll -----*/
			else if (firstVisibleItem - lastVisibleItem < 0) {
				if (!isSortBarShow) {
					isSortBarShow = true;
					Animation animIn = AnimationUtils.loadAnimation(
							SearchProductListActivity.this, R.anim.push_up_in);
					sortBarView.setVisibility(View.VISIBLE);
					sortBarView.setAnimation(animIn);
					sortBarView.startAnimation(animIn);
				}
			}
			lastVisibleItem = firstVisibleItem;
		}
	};
	
	// sort bar click listener
	private OnClickListener onclick = new OnClickListener() {

		@SuppressLint("ResourceAsColor")
		@Override
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.ProductList_sortTimeLayout:{
				
				ProductListSorter.getSortByTime(productList);
				setListView();
				
				ProductList_sortTimeLayout
						.setBackgroundResource(R.drawable.bar_sort_on);
				ProductList_sortNearLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortHotLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortPriceLayout.setBackgroundColor(Color.WHITE);

				tv_sort_lastTime.setTextColor(Color.RED);
				tv_sort_location.setTextColor(Color.BLACK);
				tv_sort_hot.setTextColor(Color.BLACK);
				tv_sort_price.setTextColor(Color.BLACK);

				break;
			}
			case R.id.ProductList_sortNearLayout:{

				ProductListSorter.getSortByDistance(productList);
				setListView();
				
				ProductList_sortTimeLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortNearLayout
						.setBackgroundResource(R.drawable.bar_sort_on);
				ProductList_sortHotLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortPriceLayout.setBackgroundColor(Color.WHITE);

				tv_sort_lastTime.setTextColor(Color.RED);
				tv_sort_location.setTextColor(Color.BLACK);
				tv_sort_hot.setTextColor(Color.BLACK);
				tv_sort_price.setTextColor(Color.BLACK);

				break;
			}
			case R.id.ProductList_sortHotLayout:{
				
				ProductListSorter.getSortByHot(productList);
				setListView();

				ProductList_sortTimeLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortNearLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortHotLayout
						.setBackgroundResource(R.drawable.bar_sort_on);
				ProductList_sortPriceLayout.setBackgroundColor(Color.WHITE);

				tv_sort_lastTime.setTextColor(Color.RED);
				tv_sort_location.setTextColor(Color.BLACK);
				tv_sort_price.setTextColor(Color.BLACK);
				tv_sort_price.setTextColor(Color.BLACK);

				break;
			}
			case R.id.ProductList_sortPriceLayout:{
				
				ProductListSorter.getSortByPrice_L(productList);
				setListView();

				ProductList_sortTimeLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortNearLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortHotLayout.setBackgroundColor(Color.WHITE);
				ProductList_sortPriceLayout
						.setBackgroundResource(R.drawable.bar_sort_on);

				tv_sort_lastTime.setTextColor(Color.RED);
				tv_sort_location.setTextColor(Color.BLACK);
				tv_sort_hot.setTextColor(Color.BLACK);
				tv_sort_price.setTextColor(Color.BLACK);

				break;
			}
			}

		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			break;
		}

		return true;
	}
}

package pipimy.main;

import java.util.ArrayList;
import java.util.HashMap;

import pipimy.cart.CVSObject;
import pipimy.chat.ChatContentObject;
import pipimy.chat.ChatIndexObject;
import pipimy.nearby.ProductObject;
import pipimy.object.CartItemsObject;
import pipimy.order.OrderObject;
import pipimy.order.OrderResourceObject;
import pipimy.setting.activity.RatingObject;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

public class ApplicationController extends Application {

	private String TAG = "ApplicationController";

    private static ApplicationController mInstance;
    private static Context mAppContext;
    private RequestQueue mRequestQueue;
    
	/** =============================================================== */
	/** ========================== Global Variables ========================== */
	/** =============================================================== */
    public static boolean isCartChanged;
    
	public static ArrayList<ProductObject> nearProductList = new ArrayList<ProductObject>();
	public static ArrayList<ProductObject> searchProductList = new ArrayList<ProductObject>();
	public static ArrayList<ProductObject> RatingProductList = new ArrayList<ProductObject>();
	public static ArrayList<ProductObject> someoneProductList = new ArrayList<ProductObject>();
	
	public static ArrayList<ChatIndexObject> chatIndexList_seller = new ArrayList<ChatIndexObject>();
	public static ArrayList<ChatIndexObject> chatIndexList_buyer = new ArrayList<ChatIndexObject>();
	public static ArrayList<ChatContentObject> chatContentList = new ArrayList<ChatContentObject>();

	
	//test shopping cart
	//cart
	public static ArrayList<CartItemsObject> cartItemList = new ArrayList<CartItemsObject>();
	public static ArrayList<String> cartStoreCount = new ArrayList<String>();
	public static int[] cartStoreProductCount;
	public static HashMap<String, Integer> cartStoreProductMap = 
			new HashMap<String, Integer>();
	
	//order
	public static ArrayList<OrderObject> boughtList = new ArrayList<OrderObject>();
	public static ArrayList<OrderObject> soldList = new ArrayList<OrderObject>();
	public static HashMap<String, String> orderPayData = new HashMap<String, String>(); 
	public static HashMap<String, String> orderDetail = new HashMap<String, String>();
	public static HashMap<String, String> orderResources = new HashMap<String, String>();
	public static ArrayList<OrderResourceObject> resObj = new ArrayList<OrderResourceObject>();


	//CVS store info
	public static ArrayList<CVSObject> cvsInfoList = new ArrayList<CVSObject>();
	public static ArrayList<CVSObject> famiStoreInfoList = new ArrayList<CVSObject>();
	public static ArrayList<CVSObject> uniStoreInfoList = new ArrayList<CVSObject>();
	
	public static ArrayList<RatingObject> myBoughtRatingList = new ArrayList<RatingObject>();
	
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        mAppContext = getApplicationContext();
        //setAppContext(getApplicationContext());
    }
    
    public static Context getAppContext() {
        return mAppContext;
    }

    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }
    
    public void sendGoogleAnalytic(String path) {
		Tracker v3EasyTracker = EasyTracker.getInstance( mAppContext);
		v3EasyTracker.set(Fields.SCREEN_NAME, path);
		v3EasyTracker.send(MapBuilder.createAppView().build());
	}
    
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}

package pipimy.main;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.DialogUtil.OnTypeClickListener;
import pipimy.others.Http;
import pipimy.others.PreferenceUtil;
import pipimy.others.TransferProgressBar;
import pipimy.others.TransferProgressBar.AmazonUpLoadCompleteListener;
import pipimy.others.Util;
import pipimy.post.PicActivity;
import pipimy.post.VideoActivity;
import pipimy.service.R;
import pipimy.third.TransferModel;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class PostFragment extends Fragment {

	private final static String TAG = "PostFragment";
	
	private final static int DONE_TEXT = 20001;

	private View doneView;
	private View videoLayout;
	private View picLayout;
	private View mPostVideoText;
	private View mPostPicText;
	public static TransferProgressBar videoProgressBar;
	private TransferProgressBar picProgressBar;
	private ProgressBar mPictureIsUploading;
	private ProgressBar mVideoIsUploading;
	private EditText titleEditText;
	private EditText priceEditText;
	private EditText typeEditText;
	//private EditText dealTypeEditText;
	private EditText stockCountEditText;
	private EditText descriptionEditText;
	//private View contactView;

	private ImageView checkVideoImageView;
	private ImageView checkPicImageView;
	private ImageView checkTitleImageView;
	private ImageView checkPriceImageView;
	private ImageView checkTypeImageView;
	//private ImageView checkDealTypeImageView;
	//private ImageView checkStockCountImageView;
	private ImageView checkContactImageView;
	private Handler handler;

	private MainActivity activity;
	private boolean isVideoDoneInvoke;
	private boolean isPicDoneInvoke;
	
	private String product_title;
	private String product_price;
	private String product_des;
	private String product_contact;
	private String product_type;
	private String product_Count;
	private String types[];
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_post, container, false);
	}

	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		activity = (MainActivity) getActivity();
		types = getResources().getStringArray(R.array.types);
		doneView = (View) view.findViewById(R.id.UpLoadProduct_doneLayout);

		videoLayout = (View) view.findViewById(R.id.videoLayout);
		picLayout = (View) view.findViewById(R.id.picLayout);

		videoProgressBar = (TransferProgressBar) view
				.findViewById(R.id.UpLoadProduct_videoProgressBar);
		picProgressBar = (TransferProgressBar) view
				.findViewById(R.id.UpLoadProduct_picProgressBar);

		mVideoIsUploading = (ProgressBar) view
				.findViewById(R.id.videoIsUploading);
		mPictureIsUploading = (ProgressBar) view
				.findViewById(R.id.pictureIsUploading);

		// picCount = (TextView)
		// rootView.findViewById(R.id.UpLoadProduct_picCount);

		titleEditText = (EditText) view
				.findViewById(R.id.UpLoadProduct_titleEditText);
		priceEditText = (EditText) view
				.findViewById(R.id.UpLoadProduct_priceEditText);
		typeEditText = (EditText) view
				.findViewById(R.id.UpLoadProduct_typeEditText);
		//dealTypeEditText = (EditText) view
			//	.findViewById(R.id.UpLoadProduct_dealTypeEditText);
		stockCountEditText = (EditText) view
					.findViewById(R.id.UpLoadProduct_stockCountEditText);
		descriptionEditText = (EditText) view
				.findViewById(R.id.UpLoadProduct_descriptionEditText);

		checkVideoImageView = (ImageView) view
				.findViewById(R.id.UpLoadProduct_videoCheckImageView);
		checkPicImageView = (ImageView) view
				.findViewById(R.id.UpLoadProduct_picCheckImageView);
		checkTitleImageView = (ImageView) view
				.findViewById(R.id.UpLoadProduct_titleCheckImageView);
		checkPriceImageView = (ImageView) view
				.findViewById(R.id.UpLoadProduct_priceCheckImageView);
		checkTypeImageView = (ImageView) view
				.findViewById(R.id.UpLoadProduct_typeCheckImageView);
		//checkStockCountImageView = (ImageView) view
			//	.findViewById(R.id.UpLoadProduct_dealTypeCheckImageView);

		mPostVideoText = (View) view.findViewById(R.id.post_video_text);
		mPostPicText = (View) view.findViewById(R.id.post_pic_text);

		/*
		 * item click listener
		 */
		videoLayout.setOnClickListener(itemClickListener);
		picLayout.setOnClickListener(itemClickListener);
		doneView.setOnClickListener(itemClickListener);
		typeEditText.setOnClickListener(itemClickListener);
		
		titleEditText.addTextChangedListener(titleWatcher);
		priceEditText.addTextChangedListener(priceWatcher);
		
	//	stockCountEditText.addTextChangedListener(countWatcher);
		
		/*if(PreferenceUtil.getBoolean(activity, Constant.USER_LOGIN)) {
			contactView.setVisibility(View.GONE);
		}*/

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Log.e("onActivityCreated","Post Fragment");
	}

	@Override
	public void onResume() {
		super.onResume();
		// Log.e("onResume","Post Fragment");
		if (handler == null) {
			handler = new Handler();
		}

		if (Constant.isUploadingVideo) {
			Log.e(TAG, "isUploadingVideo");
			mVideoIsUploading.setVisibility(View.VISIBLE);
			handler.post(upLoadRunnable);
		}
		
		if(Constant.isUploadingPics){
			mPictureIsUploading.setVisibility(View.VISIBLE);
			handler.post(upLoadRunnable);
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		// Log.e("onPause","Post Fragment");
	}
	
	
	private TextWatcher priceWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (s.length() < 1) {
				checkPriceImageView.setBackgroundResource(R.drawable.left_arrow_red);
			} else {
				checkPriceImageView.setBackgroundResource(R.drawable.check48_48);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	private TextWatcher titleWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (s.length() < 1) {
				checkTitleImageView.setBackgroundResource(R.drawable.left_arrow_red);
			} else {
				checkTitleImageView.setBackgroundResource(R.drawable.check48_48);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
		}
		
	};

	/*
	 * Items click listener
	 */
	private OnClickListener itemClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.videoLayout: {
				if(checkLogin()) {
					Intent intent = new Intent(getActivity(), VideoActivity.class);
					startActivityForResult(intent, 0);
					
				}
				break;
			}
			case R.id.picLayout: {
				if(checkLogin()) {
					Intent intent = new Intent(getActivity(), PicActivity.class);
					intent.putExtra("isSell", true);
					startActivityForResult(intent, 0);
					
				}
				break;
			}
			case R.id.UpLoadProduct_doneLayout:{
				if(checkLogin()) {
					doneTextInvoke();
					
				}
				break;
			}
			case R.id.UpLoadProduct_typeEditText:{
				doPickProductType();
				break;
			}
			
				
			}

		}
	};
	
	protected boolean checkLogin() {
		// TODO Auto-generated method stub
		boolean isLogin = PreferenceUtil.getBoolean(activity, Constant.USER_LOGIN);
		if(!isLogin) {
			DialogUtil.pushPureDialog(activity, "Alert", "Login please", "ok");
		}
		
		return isLogin;
	}

	/** =============================================================== */
	/** ================= Amazon upLoad even Callback ================= */
	/** =============================================================== */

	/*----- runnable for upLoad progress refresh -----*/
	private Runnable upLoadRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			// run until upload complete

			if (!activity.isVideoUpLoaded || !activity.isPicUpLoaded) {
				//Log.e(TAG, "upLoadRunnable activity.isVideoUpLoaded = "+activity.isVideoUpLoaded);
				//Log.e(TAG, "upLoadRunnable activity.isPicUpLoaded = "+activity.isPicUpLoaded);
				syncModels();
				handler.postDelayed(upLoadRunnable, 500);
			}

			// video complete
			if (activity.isVideoUpLoaded) {
				videoProgressBar.setProgress(videoProgressBar.getMax());
				mVideoIsUploading.setVisibility(View.INVISIBLE);
				Constant.isUploadingVideo = false;
				checkVideoImageView
						.setBackgroundResource(R.drawable.check48_48);
			} else {
				checkVideoImageView
						.setBackgroundResource(R.drawable.left_arrow_red);
			}

			// pic complete
			if (activity.isPicUpLoaded) {
				picProgressBar.setProgress(picProgressBar.getMax());
				mPictureIsUploading.setVisibility(View.INVISIBLE);
				Constant.isUploadingPics = false;
				checkPicImageView.setBackgroundResource(R.drawable.check48_48);
			} else {
				checkPicImageView
						.setBackgroundResource(R.drawable.left_arrow_red);
			}

		}

	};

	/*----- makes sure that we are up to date on the transfers -----*/
	private void syncModels() {
		TransferModel[] models = TransferModel.getAllTransfers();
		for (int i = 0; i < models.length; i++) {

			if (Constant.uploadProductID != null
					&& models[i].getStatus() != TransferModel.Status.CANCELED) {

				if (models[i].getFileName().contains(".mp4")) {
					// progress for video upload
					// Log.e(TAG, "syncModels refresh transfer progress");
					videoProgressBar.initTransferProgressBar(getActivity(),
							models[i], videoCompleteListener);
					videoProgressBar.refresh();

				} else if (models[i].getFileName().contains(".jpg")
						&& Constant.picList != null) {
					// progress for pic upload
					picProgressBar.initTransferProgressBar(activity, models[i],
							picCompleteListener, Constant.picList.size());
				}
			}
		}
	}


	/*
	 *  Product information done
	 */
	protected void doPickProductType() {
		// TODO Auto-generated method stub
		DialogUtil.typeSearchDialog(activity, true, new OnTypeClickListener(){

			@Override
			public void onTypeClick(int position) {
				// TODO Auto-generated method stub
				typeEditText.setText(types[position]);
				checkTypeImageView.setBackgroundResource(R.drawable.check48_48);
				product_type = "" + position;
			}});
	}
	
	protected void doneTextInvoke() {
		// TODO Auto-generated method stub
		
		Log.e(TAG, " BEGIN");
		if(checkDone()){
			//Util.showProgressDialog(getActivity(), "", getString(R.string.loading));
			new Thread() {
				@Override
				public void run() {
					
					String[] params = {"ProductID", "Title", "Type", "Price", "CC", "Lat", "Lng", 
							"Des", "PostTime", "Stock"};
					String[] data = { Constant.uploadProductID, product_title, product_type,
							product_price, "TW", PreferenceUtil.getString(getActivity(), 
									Constant.USER_LAT), PreferenceUtil.getString(getActivity(), 
											Constant.USER_LNG), product_des, getTime(),
											product_Count};
				
					for(int i=0; i<data.length; i++){
						Log.e(TAG, "doneTextInvoke"+ params[i]+ "= "+data[i]);
					}
					
					String result = Http.post(Constant.DONE_TEXT_M, params,
							data, getActivity());
					Log.e(TAG, "doneTextInvoke res = "+result);
					Message msg = new Message();
					msg.arg1 = DONE_TEXT;
					msg.obj = result;
					apiCallbackHandler.sendMessage(msg);
				}

			}.start();
		}
	}

	protected void doneVideoInvoke() {
		// TODO Auto-generated method stub
		Log.e(TAG, "doneVideoInvoke BEGIN");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "ProductID" };
				String[] data = { Constant.uploadProductID };
				String result = Http.post(Constant.DONE_VIDEO_URL_M, params,
						data, getActivity());
				try {
					JSONObject jsonObject = new JSONObject(result);
					if(jsonObject.getString("result").equals("success"))
						isVideoDoneInvoke = true;

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}

		}.start();
	}
	
	protected void donePicInvoke() {
		// TODO Auto-generated method stub
		Log.e(TAG, "donePicInvoke BEGIN");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "ProductID", "PicNumber" };
				String[] data = { Constant.uploadProductID, Integer.toString(Constant.picList.size()) };
				String result = Http.post(Constant.DONE_PIC_URL_M, params,
						data, getActivity());
				try {
					JSONObject jsonObject = new JSONObject(result);
					if(jsonObject.getString("result").equals("success"))
						isPicDoneInvoke = true;

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}

		}.start();
	}
	
	Handler apiCallbackHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch(msg.arg1) {
			case DONE_TEXT:{
				String str = (String)msg.obj;
				Toast.makeText(activity, str, Toast.LENGTH_LONG).show();
				break;
			}
			}
		}
	};
	
	private boolean checkDone() {
		// TODO Auto-generated method stub
		product_title = Util.getEditText(titleEditText).trim();
		product_price = Util.getEditText(priceEditText);
		product_des = Util.getEditText(descriptionEditText);
		product_Count = Util.getEditText(stockCountEditText);
		
		if(!isVideoDoneInvoke) {
			DialogUtil.pushPureDialog(activity, "alert", "please upload a video", "ok");
		} else if(!isPicDoneInvoke) {
			DialogUtil.pushPureDialog(activity, "alert", "please upload at least a pic", "ok");
		} else if(product_title.equals("")) {
			DialogUtil.pushPureDialog(activity, "alert", "title cannot be empty", "ok");
		} else if(product_price.equals("")) {
			DialogUtil.pushPureDialog(activity, "alert", "price cannot be empty", "ok");
		} else if(product_type.equals("")) {
			DialogUtil.pushPureDialog(activity, "alert", "type cannot be empty", "ok");
		} else if(product_Count.equals("") || Integer.parseInt(product_Count)<1) {
			DialogUtil.pushPureDialog(activity, "alert", "stock count must > 1", "ok");
		} else {
			return true;
		}
		
		return false;
	}
	
	protected String getTime() {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dt=new Date();
		String dts=sdf.format(dt);
		return dts;
	}

	/*
	 * Amazon upload callback
	 */
	private AmazonUpLoadCompleteListener videoCompleteListener = new AmazonUpLoadCompleteListener() {

		@Override
		public void onAmazonUpLoadComplete() {
			if (!activity.isVideoUpLoaded) {
				activity.isVideoUpLoaded = true;
				doneVideoInvoke();
			}
		}
	};

	private AmazonUpLoadCompleteListener picCompleteListener = new AmazonUpLoadCompleteListener() {

		@Override
		public void onAmazonUpLoadComplete() {
			if (!activity.isPicUpLoaded) {
				activity.isPicUpLoaded = true;
				donePicInvoke();
			}
		}
	};


	
}

package pipimy.main;

import pipimy.others.Constant;
import pipimy.others.PreferenceUtil;
import pipimy.service.R;

import android.support.v4.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingFragment_login extends Fragment {
	private View myProductView;
	private View myFansView;
	private View myDealView;
	private View myDealViewAsBuyer;
	private View myRatingView;
	private View changePWDView;
	private View feedbackView;
	private View logoutView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
    }
	
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		myProductView = (View) view.findViewById(R.id.setting_myProductLayout);
		myFansView= (View) view.findViewById(R.id.setting_myFansLayout);
		myDealView = (View) view.findViewById(R.id.setting_myDealLayout);
		myDealViewAsBuyer = (View) view.findViewById(R.id.setting_myDealLayoutAsBuyer);
		myRatingView = (View) view.findViewById(R.id.setting_myRatingLayout);
		changePWDView = (View) view.findViewById(R.id.setting_changePWDLayout);
		feedbackView = (View) view.findViewById(R.id.setting_feedBackLayout);
		logoutView = (View) view.findViewById(R.id.setting_logoutLayout);

		myProductView.setOnClickListener(itemClickListener);
		myFansView.setOnClickListener(itemClickListener);
		myDealView.setOnClickListener(itemClickListener);
		myRatingView.setOnClickListener(itemClickListener);
		changePWDView.setOnClickListener(itemClickListener);
		feedbackView.setOnClickListener(itemClickListener);
		logoutView.setOnClickListener(itemClickListener);
		myDealViewAsBuyer.setOnClickListener(itemClickListener);
	
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		

		
		
		
		
		
	return inflater.inflate(R.layout.fragment_setting_login, container, false);
	}
	
	@Override  
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState);
    }  
  
	@Override
	public void onResume() {
        super.onResume();
    }
	
	@Override
    public void onPause() {
        super.onPause();
    }
	
	private OnClickListener itemClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {

			/*----- my product -----*/
			case R.id.setting_myProductLayout:
				
				
				intent.putExtra("memberID",PreferenceUtil.getString(getActivity(), Constant.USER_ID));
				intent.setClass(getActivity(), MyProductActivity.class);
				startActivity(intent);
				
				//startActivityForResult(intent, 0);
				//Constant.getInstance().sendGoogleAnalytic("android_setting_myproduct");
				
			//	Log.d("settt","按下第一個東西");
				
				break;
				
			case R.id.setting_myFansLayout:
			
				
				break;

			/*-----  my deal seller -----*/
			case R.id.setting_myDealLayout:
			
				
				
				break;
				
			/*-----  my deal buyer -----*/
			case R.id.setting_myDealLayoutAsBuyer:
			
				break;

			/*----- my rating -----*/
			case R.id.setting_myRatingLayout:
			
				
				
				break;

			/*----- update password -----*/
			case R.id.setting_changePWDLayout:

			
				
				
				break;

			/*----- send Eamil feedback -----*/
			case R.id.setting_feedBackLayout:
			
				break;

			/*----- Logout -----*/
			case R.id.setting_logoutLayout:
			
				break;

			}
		}
	};

	
	
	
	
}

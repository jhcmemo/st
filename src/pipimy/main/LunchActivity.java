package pipimy.main;


import java.util.List;

import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Window;

public class LunchActivity extends Activity {

	final static String TAG = "LunchActivity";
	final static int GOTO_MAIN = 10001;
	final static int GOTO_STORE = 10002;
	
	String traceID;
	boolean isGoToStore;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lunch);
		
		int destination = GOTO_MAIN;
		
		Bundle params = getIntent().getExtras();
		if(params != null) {
			destination = GOTO_STORE;
			traceID = params.getString("traceID");
			isGoToStore = params.getBoolean("isFromTracer");
		}
		for(int i=0;i<VolleySingleton.temp_storimg.size();i++){
			VolleySingleton.removeURLCache(VolleySingleton.temp_storimg.get(i));
		}
		VolleySingleton.temp_storimg.clear();
		
		Handler handler = new Handler();
		handler.postDelayed(launchRunnable, 2000);
		
		
	}
	private Runnable launchRunnable = new Runnable() {
		
		@Override
		public void run() {
			Log.e(TAG, "isGoToStore = "+isGoToStore);
			if(isGoToStore) {
				Intent it =new Intent(LunchActivity.this, MyProductActivity.class);
				it.putExtra("memberID", traceID);
				it.putExtra("isFromTracer", isGoToStore);
				startActivity(it);
				finish();
			} else {
				Intent it =new Intent(LunchActivity.this,MainActivity.class);
				startActivity(it);
				finish();
			}
		}
	};
	

}

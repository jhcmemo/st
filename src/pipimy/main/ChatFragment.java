package pipimy.main;

import java.util.ArrayList;

import pipimy.chat.ChatContentActivity;
import pipimy.chat.ChatIndexObject;
import pipimy.chat.ChatUtil;
import pipimy.chat.ChatUtil.onDeleteChatIndexCallback;
import pipimy.chat.ChatUtil.onGetChatIndexCallback;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.DialogListUtil.OnChatIndexLongClickListener;
import pipimy.others.Http;
import pipimy.others.PreferenceProxy;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class ChatFragment extends Fragment implements onGetChatIndexCallback,
		onDeleteChatIndexCallback {

	private final static String TAG = "ChatFragment";
	private final static int DELETE_CHAT_INDEX = 10001;

	ListView ListView;
	private TextView hintTextView;
	private MainActivity activity;
	int http_position = 0;
	CommonAdapter<ChatIndexObject> adapter;

	ArrayList<ChatIndexObject> list;

	TextView tv_seller, tv_buyer, tv_bage_seller, tv_bage_buyer, tv_no_data;
	ImageLoader mImageLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_chat, container,
				false);

		tv_no_data = (TextView) rootView.findViewById(R.id.tv_chat_no_data);
		tv_seller = (TextView) rootView.findViewById(R.id.tv_seller);
		tv_buyer = (TextView) rootView.findViewById(R.id.tv_buyer);

		tv_bage_seller = (TextView) rootView.findViewById(R.id.tv_bage_seller);
		tv_bage_buyer = (TextView) rootView.findViewById(R.id.tv_bage_buyer);

		ListView = (ListView) rootView.findViewById(R.id.ListView);
		hintTextView = (TextView) rootView
				.findViewById(R.id.chatIndex_hintTextView);
		tv_buyer.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
		tv_seller.setBackgroundColor(Color.parseColor("#FFECECEC"));
		

		tv_buyer.setOnClickListener(click);
		tv_seller.setOnClickListener(click);
		init();

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Log.e("onActivityCreated","Chat Fragment");
	}

	@Override
	public void onResume() {
		super.onResume();
		// Log.e("onResume","Chat Fragment");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.gc();
	}

	@Override
	public void onPause() {
		super.onPause();
		// Log.e("onPause","Chat Fragment");
	}

	private void init() {
		// TODO Auto-generated method stub
		// refresh_bage();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();

		activity = (MainActivity) getActivity();
		ChatUtil.setOnGetChatIndexCallback(this);
		ChatUtil.setOnDeleteChatIndexCallback(this);

		if (!PreferenceUtil.getBoolean(getActivity(), Constant.USER_LOGIN)) {

			tv_buyer.setVisibility(View.GONE);
			tv_seller.setVisibility(View.GONE);
			ListView.setVisibility(View.GONE);
			hintTextView.setVisibility(View.VISIBLE);
			hintTextView.setText(getActivity().getString(R.string.plz_login));
		} else {
			if (ApplicationController.chatIndexList_buyer.size() == 0) {
				tv_no_data.setVisibility(View.VISIBLE);
				ListView.setVisibility(View.GONE);
			}

			list = ApplicationController.chatIndexList_buyer;
			setIndexAdaper();

		}

	}

	@SuppressWarnings("unchecked")
	private void setIndexAdaper() {
		// TODO Auto-generated method stub
		/*
		 * adapter = new ChatIndexAdapter(activity, R.layout.cell_chatindex,
		 * ApplicationController.chatIndexList_seller,
		 * ApplicationController.chatIndexList_buyer);
		 */

		adapter = new CommonAdapter(getActivity(), list,
				R.layout.cell_chatindex) {
			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				// TODO Auto-generated method stub
				// Log.d("ada", "???????????);
				final ChatIndexObject chatObject = (ChatIndexObject) item;

				NetworkImageView imageView = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView,
								R.id.cell_chatList_imageView);

				TextView titleTextView = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.cell_chatList_title_TextView);

				TextView userToTextView = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.cell_chatList_userToTextView);

				TextView updateTimeTextView = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.cell_chatList_updateTimeTextView);

				TextView lastSentenceTextView = (TextView) commonViewHolder
						.get(commonViewHolder, currentView,
								R.id.cell_chatList_lastSentenceTextView);

				TextView badgeTextView = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.cell_chatList_badgeTextView);

				/*
				 * ImageView handImageView = (ImageView) commonViewHolder.get(
				 * commonViewHolder, currentView,
				 * R.id.cell_chatList_handImageView);
				 */
				// userToTextView.setOnClickListener(userIDClickListener);

				titleTextView.setText(chatObject.getProductTitle());
				// userToTextView.setText(chatObject.getUserIDTo());
				updateTimeTextView.setText(chatObject.getUpdateTime()
						.substring(5, 16));
				lastSentenceTextView.setText(chatObject.getLastSentence());

				String memberID = PreferenceUtil
						.getString(ApplicationController.getAppContext(),
								Constant.USER_ID);
				if (chatObject.getUserIDFrom().equals(memberID)) {
					// from
					userToTextView.setText(chatObject.getUserIDTo());
					userToTextView.setTag(chatObject.getUserIDTo());

					if (chatObject.getUserIDFromBadge() != 0) {
						badgeTextView.setText(""
								+ chatObject.getUserIDFromBadge());
					} else {
						badgeTextView.setVisibility(View.INVISIBLE);
					}
					// setHandImageView(true, handImageView, chatObject);
				} else {
					// to
					userToTextView.setText(chatObject.getUserIDFrom());
					userToTextView.setTag(chatObject.getUserIDFrom());

					if (chatObject.getUserIDToBadge() != 0) {
						badgeTextView.setText(""
								+ chatObject.getUserIDToBadge());
					} else {
						badgeTextView.setVisibility(View.INVISIBLE);
					}
					// setHandImageView(false, handImageView, chatObject);
				}
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setImageUrl(
						Constant.PicServerURL + chatObject.getProductId()
								+ "-pic1.jpg", mImageLoader);
			//	Log.d("pic","chat pic url:"+Constant.PicServerURL + chatObject.getProductId()
				//		+ "-pic1.jpg");

			}
		};

		ListView.setAdapter(adapter);
		ListView.setOnItemClickListener(itemClickListener);
		ListView.setOnItemLongClickListener(itemLongClickListener);
		/*
		 * int groupCount = ListView.getCount(); for (int i = 0; i < groupCount;
		 * i++) { ListView.expandGroup(i); }
		 */

	}

	protected void goChatContent(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(activity, ChatContentActivity.class);

		// seller
		if (groupPosition == 1) {
			intent.putExtra(
					ChatUtil.CHAT_ID,
					ApplicationController.chatIndexList_seller.get(
							childPosition).getChatId());
			intent.putExtra(
					ChatUtil.PRODUCT_ID,
					ApplicationController.chatIndexList_seller.get(
							childPosition).getProductId());
			intent.putExtra(ChatUtil.SELECT_GROUP, groupPosition);
			intent.putExtra(ChatUtil.SELECT_CHILD, childPosition);
		}
		// buyer
		else {

			intent.putExtra(ChatUtil.CHAT_ID,
					ApplicationController.chatIndexList_buyer
							.get(childPosition).getChatId());
			intent.putExtra(ChatUtil.PRODUCT_ID,
					ApplicationController.chatIndexList_buyer
							.get(childPosition).getProductId());
			intent.putExtra(ChatUtil.SELECT_GROUP, groupPosition);
			intent.putExtra(ChatUtil.SELECT_CHILD, childPosition);
		}
		startActivityForResult(intent, 0);
	}

	private void refresh_bage() {

		// Log.d("chat","������");
		int buyer_count = 0;
		int seller_count = 0;
		String memberID = PreferenceProxy.getUserID(getActivity());

if(groupPosition==0){
	tv_bage_buyer.setTextColor(Color.parseColor("#4A9ED8"));
	tv_bage_seller.setTextColor(Color.parseColor("#ffc9c9ca"));

}else{
	tv_bage_buyer.setTextColor(Color.parseColor("#ffc9c9ca"));
	tv_bage_seller.setTextColor(Color.parseColor("#4A9ED8"));
	
}

		/*
		 * if(groupPosition==0){ list=ApplicationController.chatIndexList_buyer;
		 * } else{
		 * 
		 * list=ApplicationController.chatIndexList_seller;
		 * 
		 * }
		 */

		for (int i = 0; i < ApplicationController.chatIndexList_buyer.size(); i++) {
			ChatIndexObject object = ApplicationController.chatIndexList_buyer
					.get(i);
			if (object.getUserIDFrom().equals(memberID)) {

				buyer_count = buyer_count + object.getUserIDFromBadge();
			} else {
				buyer_count = buyer_count + object.getUserIDToBadge();

			}

		}

		if (buyer_count != 0) {
			tv_bage_buyer.setVisibility(View.VISIBLE);
			tv_bage_buyer.setText("("+buyer_count + ")");
		} else {
			tv_bage_buyer.setVisibility(View.INVISIBLE);

		}

		for (int i = 0; i < ApplicationController.chatIndexList_seller.size(); i++) {

			ChatIndexObject object = ApplicationController.chatIndexList_seller
					.get(i);
			if (object.getUserIDFrom().equals(memberID)) {

				seller_count = seller_count + object.getUserIDFromBadge();
			} else {
				seller_count = seller_count + object.getUserIDToBadge();

			}
		}

		if (seller_count != 0) {
			tv_bage_seller.setVisibility(View.VISIBLE);
			tv_bage_seller.setText("("+seller_count + ")");
		} else {
			tv_bage_seller.setVisibility(View.INVISIBLE);

		}
		Log.d("chat", " buyer_count :" + buyer_count);

		Log.d("chat", "seller_count :" + seller_count);
		// adapter.notifyDataSetChanged();

		// handler_refresh_listview.sendMessage(handler_refresh_listview.obtainMessage());
	}

	private void check_now_state() {

		if (PreferenceUtil.getBoolean(getActivity(), Constant.USER_LOGIN)) {
			
			
			
			
			
			if (ApplicationController.chatIndexList_seller.size() == 0
					&& ApplicationController.chatIndexList_buyer.size() == 0) {
				tv_buyer.setVisibility(View.GONE);
				tv_seller.setVisibility(View.GONE);

				ListView.setVisibility(View.GONE);
				// hintTextView.setVisibility(View.VISIBLE);
				// hintTextView.setText("no data");
			}
			/*
			 * else if(ApplicationController.chatIndexList_buyer.size() == 0){
			 * tv_no_data.setVisibility(View.VISIBLE);
			 * ListView.setVisibility(View.GONE);
			 * 
			 * }
			 */
			/*
			 * else if(ApplicationController.chatIndexList_buyer.size() == 0){
			 * tv_no_data.setVisibility(View.VISIBLE);
			 * ListView.setVisibility(View.GONE);
			 * 
			 * }
			 */

			else {
				
				if(groupPosition==0){
					
					  if(ApplicationController.chatIndexList_buyer.size() == 0){
						  ListView.setVisibility(View.GONE);
						  tv_no_data.setVisibility(View.VISIBLE);

					  }else{
						  tv_no_data.setVisibility(View.GONE);

						  ListView.setVisibility(View.VISIBLE);
					  }
					
					
				}else{
					
					if(ApplicationController.chatIndexList_seller.size() == 0){
						  tv_no_data.setVisibility(View.VISIBLE);

						  ListView.setVisibility(View.GONE);
						 // Log.d("chat","賣家無資料");

					  }else{
						//  Log.d("chat","賣家有資料");

						  tv_no_data.setVisibility(View.GONE);

						  ListView.setVisibility(View.VISIBLE);
					  }
					
				}
				
				
				
				/*hintTextView.setVisibility(View.GONE);
				tv_no_data.setVisibility(View.GONE);
				tv_buyer.setVisibility(View.VISIBLE);
				tv_seller.setVisibility(View.VISIBLE);
				ListView.setVisibility(View.VISIBLE);
*/
			}

		} else {
			tv_buyer.setVisibility(View.GONE);
			tv_seller.setVisibility(View.GONE);
			tv_no_data.setVisibility(View.GONE);

			ListView.setVisibility(View.GONE);
			hintTextView.setVisibility(View.VISIBLE);
			hintTextView.setText(getActivity().getString(R.string.plz_login));
		}
	}

	/*
	 * Click listener
	 */

	private OnClickListener click = new OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.tv_buyer:

				tv_bage_buyer.setTextColor(Color.parseColor("#4A9ED8"));
				tv_bage_seller.setTextColor(Color.parseColor("#ffc9c9ca"));
				
				tv_buyer.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
				tv_buyer.setTextColor(Color.parseColor("#FF4A9ED8"));
				tv_seller.setBackgroundColor(Color.parseColor("#FFECECEC"));
				tv_seller.setTextColor(Color.parseColor("#FFC9C9CA"));
				groupPosition = 0;
				if (ApplicationController.chatIndexList_buyer.size() == 0) {
					tv_no_data.setVisibility(View.VISIBLE);
					ListView.setVisibility(View.GONE);
				} else {
					tv_no_data.setVisibility(View.GONE);
					ListView.setVisibility(View.VISIBLE);
					list = ApplicationController.chatIndexList_buyer;
					// adapter.notifyDataSetChanged();

					setIndexAdaper();
				}
  
				break;

			case R.id.tv_seller:
				tv_bage_buyer.setTextColor(Color.parseColor("#ffc9c9ca"));
				tv_bage_seller.setTextColor(Color.parseColor("#4A9ED8"));
				
				tv_seller.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
				tv_seller.setTextColor(Color.parseColor("#FF4A9ED8"));
				tv_buyer.setBackgroundColor(Color.parseColor("#FFECECEC"));
				tv_buyer.setTextColor(Color.parseColor("#FFC9C9CA"));
				groupPosition = 1;

				if (ApplicationController.chatIndexList_seller.size() == 0) {

					tv_no_data.setVisibility(View.VISIBLE);
					ListView.setVisibility(View.GONE);
				} else {
					tv_no_data.setVisibility(View.GONE);
					ListView.setVisibility(View.VISIBLE);
					list = ApplicationController.chatIndexList_seller;
					// adapter.notifyDataSetChanged();

					setIndexAdaper();
				}

				break;

			}

		}
	};

	private static int groupPosition = 0;
	private static int childPosition;
	private OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			// TODO Auto-generated method stub
			// Log.e(TAG, "onItemLongClick position = "+position);
			/*
			 * if (position == 0 || position ==
			 * ApplicationController.chatIndexList_seller.size() + 1 ) { //
			 * Log.e(TAG, "onItemLongClick return false"); // return false; }
			 */

			// buye
			// if(groupPosition==0){
			// http_position =position - 1-
			// ApplicationController.chatIndexList_buyer.size() - 1;

			// }
			// else{

			http_position = position;

			// }

			childPosition = position;

			DialogListUtil.showChatIndexLongClickDialog(activity,
					new OnChatIndexLongClickListener() {
						@Override
						public void onChatIndexLongClick(int position) {
							// delete
							Log.d("chat", "delete position :" + position);
							deleteChatIndex(groupPosition, http_position);
							// GlobalVariable.getInstance().sendGoogleAnalytic("android_chat_delete");
						}

					});
			return true;
		}

	};

	private OnItemClickListener itemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub

			goChatContent(groupPosition, arg2);
		}
	};

	String isFrom = "";

	private void deleteChatIndex(int groupPosition, final int childPosition) {
		// TODO Auto-generated method stub
		final ArrayList<ChatIndexObject> chatIndexList;

		if (groupPosition == 0) {
			chatIndexList = ApplicationController.chatIndexList_buyer;
		} else {
			chatIndexList = ApplicationController.chatIndexList_seller;
		}

		String memberID = PreferenceUtil.getString(
				ApplicationController.getAppContext(), Constant.USER_ID);
		if (chatIndexList.get(childPosition).getUserIDFrom().equals(memberID)) {
			isFrom = "1";
		} else {
			isFrom = "2";
		}

		Util.showProgressDialog(getActivity(), "wait", "loading");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "ChatId", "IsFrom" };
				String[] data = { chatIndexList.get(childPosition).getChatId(),
						isFrom };
				String result = Http.post(Constant.DELETE_CHAT_INDEX, params,
						data, getActivity());

				Message msg = new Message();
				msg.arg1 = DELETE_CHAT_INDEX;
				msg.obj = result;
				handler.sendMessage(msg);

			}
		}.start();

	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case DELETE_CHAT_INDEX:
				Util.dismissProgressDialog();
				String res = msg.obj.toString();
				if (res.contains("success"))
					clientDeleteChatIndex();
				break;
			}

		}
	};

	private void clientDeleteChatIndex() {
		// TODO Auto-generated method stub
		ArrayList<ChatIndexObject> chatIndexList;
		if (groupPosition == 0) {
			chatIndexList = ApplicationController.chatIndexList_buyer;
		} else {
			chatIndexList = ApplicationController.chatIndexList_seller;
		}
		chatIndexList.remove(childPosition);

		if (ApplicationController.chatIndexList_seller.size() == 0
				&& ApplicationController.chatIndexList_buyer.size() == 0) {

			tv_seller.setVisibility(View.GONE);
			tv_buyer.setVisibility(View.GONE);
			tv_no_data.setVisibility(View.VISIBLE);

			// hintTextView.setVisibility(View.VISIBLE);
		} else {

			if (groupPosition == 0) {
				list = ApplicationController.chatIndexList_buyer;

			} else {

				list = ApplicationController.chatIndexList_seller;

			}
			// adapter.notifyDataSetChanged();

			setIndexAdaper();
		}
	}

	@Override
	public void onDeleteChatIndex() {
		// TODO Auto-generated method stub
		Log.e(TAG, "onDeleteChatIndex BEGIN");
		check_now_state();

	}

	@Override
	public void onGetChatIndex() {
		// TODO Auto-generated method stub
		Log.e(TAG, "onGetChatIndex callback BEGIN");

		if (!PreferenceUtil.getBoolean(activity, Constant.USER_LOGIN)) {
			Log.d("onGetChatIndex", "logout");
			tv_no_data.setVisibility(View.GONE);

			tv_buyer.setVisibility(View.GONE);
			tv_seller.setVisibility(View.GONE);
			ListView.setVisibility(View.GONE);
			hintTextView.setVisibility(View.VISIBLE);
			hintTextView.setText(getActivity().getString(R.string.plz_login));
		} else {
			
			if (ApplicationController.chatIndexList_seller.size() == 0
					&& ApplicationController.chatIndexList_buyer.size() == 0) {
				hintTextView.setVisibility(View.GONE);
				tv_buyer.setVisibility(View.GONE);
				tv_seller.setVisibility(View.GONE);
				tv_no_data.setVisibility(View.VISIBLE);

				Log.d("onGetChatIndex", "login no data ");
				// hintTextView.setVisibility(View.VISIBLE);
			} else {
				Log.d("onGetChatIndex", "login have data ");

				try {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							hintTextView.setVisibility(View.GONE);

							
							if(groupPosition==0){
								tv_buyer.setVisibility(View.VISIBLE);
								tv_seller.setVisibility(View.VISIBLE);
								list = ApplicationController.chatIndexList_buyer;

								  if(ApplicationController.chatIndexList_buyer.size() == 0){
									  ListView.setVisibility(View.GONE);
									  tv_no_data.setVisibility(View.VISIBLE);

								  }else{
									  tv_no_data.setVisibility(View.GONE);

									  ListView.setVisibility(View.VISIBLE);
								  }
								
								
							}else{
								tv_buyer.setVisibility(View.VISIBLE);
								tv_seller.setVisibility(View.VISIBLE);
								list = ApplicationController.chatIndexList_seller;

								if(ApplicationController.chatIndexList_seller.size() == 0){
									  tv_no_data.setVisibility(View.VISIBLE);

									  ListView.setVisibility(View.GONE);
									  Log.d("chat","賣家無資料");

								  }else{
									  Log.d("chat","賣家有資料");

									  tv_no_data.setVisibility(View.GONE);

									  ListView.setVisibility(View.VISIBLE);
								  }
							}
							
							
							setIndexAdaper();
							refresh_bage();

						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		}

	}

	Handler handler_refresh_listview = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			ListView.invalidate();

		}
	};
}

package pipimy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;

import pipimy.nearby.ProductObject;
import pipimy.object.FavoriteObject;
import pipimy.others.CircleNetwork_ImageView;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.PIPI_Tool;
import pipimy.others.DialogListUtil.OnTypeClickListener;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.product.activity.PlayVideo_Activity;
import pipimy.product.activity.Product_Detail_Activity;
import pipimy.service.R;
import pipimy.setting.activity.SomeoneRatingActivity;
import pipimy.third.AmazonUtil;
import pipimy.third.TransferController;
import pipimy.third.TransferModel;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;

public class MyProductActivity extends Activity {

	String TAG = "MyProductActivity";
	public static String gpsCity = "";
	private ArrayList<FavoriteObject> favor_list ;

	GridView product_gridview;
	PullToRefreshGridView mPullRefreshGridView;
	TextView mIntro, mStoreName, mType, mCity, mMemberIdText;
	TextView mAvgCount, mRatingCount;
	CircleNetwork_ImageView mMyImage;
	NetworkImageView store_background;
	LinearLayout  lv_rating;
	LinearLayout mStoreInfoLayout;
	RatingBar mAvgRatingBar;
	ScrollView mIntroScrollView;

	boolean mIsPicked = false;
	public static boolean member_delete = false;
	int mStoreType = 0;
	String[] mTypes;
	public static String mMemberID;
	String mMemberPicUrl = "";
	HashMap<String, String> storeInfo = new HashMap<String, String>();
	ArrayList<ProductObject> productList = new ArrayList<ProductObject>();
	private static final int PICK = 3129;
	ProgressDialog progressDialog;

	ImageLoader mImageLoader;
	RequestQueue queue;
	private Menu mMenu;
	private static final int LARGE_IMAGE = 300;
	private static final int SMALL_IMAGE = 100;
	final static int GET_STORE_INFO = 10001;
	final static int GET_SOMEONE_PRODUCT = 10002;
	String id = "";
	String mDirPath;
	boolean isTrackingMsg;
	boolean isFromMyTracer;
	//NetworkImageView member_background;

   // LinearLayout lv_member_des;
    RelativeLayout rl_member_des;
    RelativeLayout rl_member_store_name;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_myproduct);
		displayProgressDialog("Loading...", "Please...wait");

		id = PreferenceUtil.getString(MyProductActivity.this, Constant.USER_ID);

		mMemberID = getIntent().getStringExtra("memberID");
		isFromMyTracer = getIntent().getBooleanExtra("isFromTracer", false);
		member_delete = false;
		Log.d("myproduct", "mMemberID :" + mMemberID);
		//lv_member_des=(LinearLayout) findViewById(R.id.lv_member_des);
		rl_member_des=(RelativeLayout) findViewById(R.id.rl_member_des);
		rl_member_store_name=(RelativeLayout) findViewById(R.id.rl_member_store_name);
		//tv_nodata_myproduct = (TextView) findViewById(R.id.tv_nodata_myproduct);
		mIntro = (TextView) findViewById(R.id.detail_info);
		mStoreName = (TextView) findViewById(R.id.name);
		mType = (TextView) findViewById(R.id.type);
		mStoreInfoLayout = (LinearLayout) findViewById(R.id.StoreInfo);
		mCity = (TextView) findViewById(R.id.city);
		mMemberIdText = (TextView) findViewById(R.id.memberID);
		mMyImage = (CircleNetwork_ImageView) findViewById(R.id.store_img);
		store_background= (NetworkImageView) findViewById(R.id.store_background);
		
		mAvgCount = (TextView) findViewById(R.id.avg_count);
		mRatingCount = (TextView) findViewById(R.id.rating_count);
		mAvgRatingBar = (RatingBar) findViewById(R.id.avg_ratingBar);

		// mAvgRatingBar.setOnClickListener(rating_click);
		mIntroScrollView = (ScrollView) findViewById(R.id.intro_scrollview);
		product_gridview = (GridView) findViewById(R.id.product_gridview);
		lv_rating = (LinearLayout) findViewById(R.id.lv_rating);
		lv_rating.setOnClickListener(rating_click);

		//member_background=findViewById(R.id)
		
		//member_background
		
		//member_background=(NetworkImageView) findViewById(R.id.member_background);
		
		init();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (member_delete) {
			displayProgressDialog("Refresh...", "Please...wait");
			getSomeoneProduct();

		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.gc();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.clear();

		Intent intent = getIntent();

		if (PreferenceUtil.getBoolean(MyProductActivity.this,
				Constant.USER_LOGIN) && id.equals(mMemberID)) {
			//MenuInflater inflater = getMenuInflater();
			//inflater.inflate(R.menu.my_store_edit, menu);
			//getActionBar().setTitle(getString(R.string.setting_my_store));
		} else {

			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.my_store_normal, menu);
			getActionBar().setTitle(intent.getStringExtra("memberID"));

		}

		this.mMenu = menu;
		// if (!mMemberID.equals(GlobalVariable.memberID))
		// HTTP_getCheckTrace(false, mMemberID);

		// Drawable colorDrawable = new ColorDrawable(getResources().getColor(
		// R.color.style_red));
		// Drawable bottomDrawable = getResources().getDrawable(
		// R.drawable.actionbar_bottom);
		// LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable,
		// bottomDrawable });
		// getActionBar().setBackgroundDrawable(ld);
		getActionBar().setIcon(R.drawable.no_icon);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		getTracingID();


		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			//finish();
			onActivityFinish();
			return true;
		case R.id.update_position:
			Builder alert = new Builder(MyProductActivity.this);
			// AlertDialog.Builder alert = new AlertDialog.Builder(this,
			// R.style.selectorDialog);
			alert.setNegativeButton(getString(R.string.cancel), null);
			alert.setPositiveButton(getString(R.string.confirm),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							SetNowCity(
									Double.valueOf(PreferenceUtil.getString(
											MyProductActivity.this,
											Constant.USER_LAT)), Double
											.valueOf(PreferenceUtil.getString(
													MyProductActivity.this,
													Constant.USER_LNG)));

							// HTTP_updateStroeLoc();

							dialog.cancel();

						}

					});

			alert.setMessage(getString(R.string.change_store_location_alert));
			alert.show();

			return true;

		/*case R.id.edit:
			editMode();
			break;*/

		case R.id.follow:
			HTTP_getDeleteTrace(mMemberID);

			// HTTP_getAddTrace(mMemberID);

			break;
		case R.id.unfollow:
			getTheSellerMsg();


			break;

		default:
			return true;

		}
		return true;
	}
	
	@Override
	public void onBackPressed(){
		//super.onBackPressed();
		onActivityFinish();
	}

	private void onActivityFinish() {
		// TODO Auto-generated method stub
		if(isFromMyTracer) {
			Intent i = new Intent();
			i.setClass(this, MainActivity.class);
			this.startActivity(i);
			this.finish();
		} else {
			this.finish();
		}
	}

	private void init() {
		// TODO Auto-generated method stub
		// initActionBar();
		mDirPath = getFilesDir().getAbsolutePath() + "/";
     	favor_list = PIPI_Tool.getFavorites(MyProductActivity.this,favor_list,true);

		queue = VolleySingleton.getInstance().getRequestQueue();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		mTypes = getResources().getStringArray(R.array.types);


if(getIntent().getIntExtra("layout", 0)==2){
		
	mStoreInfoLayout.setVisibility(View.GONE);
	rl_member_des.setVisibility(View.GONE);
}
else{
	
	getStore();

}
		getSomeoneProduct();
		
		
	}

	/*
	 * private void initActionBar() { // TODO Auto-generated method stub
	 * getActionBar().setDisplayHomeAsUpEnabled(true);
	 * getActionBar().setTitle(mMemberID); }
	 */
	private void editMode() {

	

		mIntro.setVisibility(View.GONE);
		mStoreName.setVisibility(View.GONE);
		mType.setBackgroundResource(R.drawable.bg_grey_with_rounded_stroke);
		mIntroScrollView
				.setBackgroundResource(R.drawable.bg_grey_with_rounded_stroke);
		mType.setOnClickListener(typeOnClickListener);
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setListView() {
		/*
		 * if(member_delete){
		 * 
		 * // Log.d("myproduct","??嚙踐???? ?嚙踝蕭謚減?? :"+productList.size()); }
		 */
		// final TableAdapter adapter_last_day = new TableAdapter(productList);
		product_gridview.setAdapter(new CommonAdapter(MyProductActivity.this,
				productList, R.layout.product_gridview_item) {

			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				// TODO Auto-generated method stub
				ProductObject productObj = (ProductObject) item;
				
				
				/*----- favor -----*/
				boolean isLove = false;
						for (int i = 0; i < favor_list.size(); i++) {
							if (favor_list.get(i).getProductID().equals(productObj.getProductID())) {
								isLove = true;
								break;
							}
						}
						
						Button favorButton =(Button) commonViewHolder.get(commonViewHolder, currentView, R.id.btn_favor);	
						if (isLove) {
							favorButton.setBackgroundResource(R.drawable.cell_product_love);
							favorButton.setTag(R.id.tag_boolean,"true");
						} else {
							favorButton.setBackgroundResource(R.drawable.cell_product_unlove);
							favorButton.setTag(R.id.tag_boolean,"false");
						} 
						favorButton.setTag(R.id.tag_object,productObj);
						
						  
						
						favorButton.setOnClickListener(productObj_click);
				
				
				NetworkImageView iv_product_pic = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView, R.id.iv_product_pic);
				iv_product_pic.setScaleType(ImageView.ScaleType.CENTER_CROP);
				iv_product_pic.setTag(productObj);
				iv_product_pic.setOnClickListener(productObj_click);

				NetworkImageView iv_member_icon = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView, R.id.iv_member_icon);

				iv_member_icon.setTag(productObj);
				iv_member_icon.setOnClickListener(productObj_click);

				ImageView btn_play_video = (ImageView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.btn_play_video);
				btn_play_video.setTag(productObj);

				btn_play_video.setOnClickListener(productObj_click);

				TextView tv_prdouct_title = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_prdouct_title);
				TextView 	tv_brand = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_brand);
				
			
				

				TextView tv_price = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_price);

				// TextView tv_prdouct_type = (TextView) commonViewHolder.get(
				// commonViewHolder, currentView, R.id.tv_prdouct_type);

				TextView tv_member_name = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_member_name);

				TextView tv_date = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_date);

				TextView tv_location = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.tv_location);

				iv_member_icon.setImageUrl(Constant.STORE_IMAGE_URL + mMemberID + ".jpg",
						mImageLoader);
				iv_member_icon.setDefaultImageResId(R.drawable.nohead);
				// iv_member_icon.setErrorImageResId(R.drawable.ic_launcher);
				// iv_member_icon.setOnClickListener(productObjClick);
				iv_member_icon.setTag(productObj);

				iv_product_pic.setImageUrl(productObj.getProductPicUrl(),
						mImageLoader);
				// iv_product_pic.setDefaultImageResId(R.drawable.ic_launcher);
				// iv_product_pic.setErrorImageResId(R.drawable.ic_launcher);
				tv_brand.setText(productObj.getBrand());
				tv_prdouct_title.setText(productObj.getTitle());

				tv_date.setText(productObj.getTime().substring(5, 10)
						.replace("-", "/"));
				// tv_prdouct_type.setText(mTypes[Integer.parseInt(productObj.getType())]);
				tv_price.setTypeface(Typeface.createFromAsset(getAssets(),
						"fonts/Century_Gothic.ttf"));

				tv_price.setText(productObj.getPrice());

				/*----- set ID -----*/
				if (productObj.getID().equals("null")
						&& !productObj.getContact().equals("null")) {
					tv_member_name.setText(productObj.getContact());
					tv_member_name.setTextColor(getResources().getColor(
							R.color.light_grey));
				} else {
					tv_member_name.setText(productObj.getID());
					tv_member_name.setTextColor(getResources().getColor(
							R.color.style_red));
				}

				/*----- set distance -----*/
				if (productObj.getLat() == 0 || productObj.getLng() == 0) {
					tv_location.setText("--km");
				} else {
					float d = productObj.getDistance() / 1000;
					if (d >= 1000) {
						tv_location.setText(">"
								+ (int) (productObj.getDistance() / 1000 / 1000)
								+ "K km");
					} else if (d > 100) {
						tv_location.setText(String.format("%.0f",
								(productObj.getDistance() / 1000))
								+ "km");
					} else if (d > 10) {
						tv_location.setText(String.format("%.1f",
								(productObj.getDistance() / 1000))
								+ "km");
					} else {
						tv_location.setText(String.format("%.2f",
								(productObj.getDistance() / 1000))
								+ "km");
					}

				}

			}
		});

	}

	protected void initStoreInfo() {
		// TODO Auto-generated me-thod stub

		if (handleNull(storeInfo.get("storeType")).equals("")
				&& handleNull(storeInfo.get("storeName")).equals("")
				&& handleNull(storeInfo.get("storeIntro")).equals("")) {
		//	mCity.setVisibility(View.GONE);
			//mStoreName.setVisibility(View.GONE);
			//mIntro.setVisibility(View.GONE);
			rl_member_store_name.setVisibility(View.GONE);
			//lv_rating.setVisibility(View.GONE);
			//tv_nodata_myproduct.setVisibility(View.VISIBLE);
		
		} else {
			rl_member_store_name.setVisibility(View.VISIBLE);

			mCity.setText(handleNull(storeInfo.get("storeCity")));
			mStoreName.setText(handleNull(storeInfo.get("storeName")));
			mIntro.setText(handleNull(storeInfo.get("storeIntro")));
			if (storeInfo.get("storeType").equals("null")) {
				mType.setText(handleNull(storeInfo.get("storeType")));
			} else {
				mType.setText(mTypes[Integer.valueOf(storeInfo.get("storeType"))]);
				mStoreType = Integer.valueOf(storeInfo.get("storeType"));
			}
		}
		mAvgCount.setText(storeInfo.get("average") + ".0");
		mRatingCount.setText("(" + storeInfo.get("count") + ")");
		mAvgRatingBar.setRating(Float.valueOf(storeInfo.get("average")));

		mMemberPicUrl = Constant.STORE_IMAGE_URL + mMemberID + ".jpg";
		

		Log.d("Myproduct", "");
		mMyImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
		mMyImage.setImageResource(R.drawable.nohead);
		mMyImage.setImageUrl(mMemberPicUrl, mImageLoader);
		store_background.setImageUrl(Constant.STORE_BACKGROUNDS_URL + mMemberID + "-bg.jpg", mImageLoader);

		Log.d("bg_path","bg_path :"+Constant.STORE_BACKGROUNDS_URL + mMemberID + "-bg.jpg");
		
		mMemberIdText.setText(mMemberID);
		
	//	member_background.setImageUrl(Constant.STORE_BACKGROUNDS_URL + mMemberID + "-bg.jpg", mImageLoader);
	 
	
	}

	private String handleNull(String temp) {
		if (temp.equals("null"))
			return "";
		else {
			return temp;
		}
	}

	// added by Henry, tracking and receiving push msg from this seller
	private void getTheSellerMsg() {
		final Dialog dialog = new Dialog(this, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_add_tracking);
		dialog.show();

		final CheckedTextView isTrackMsg = (CheckedTextView) dialog
				.findViewById(R.id.check_get_msg);
		isTrackMsg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((CheckedTextView) v).toggle();
				isTrackingMsg = ((CheckedTextView) v).isChecked();
			}
		});

		TextView btnConfirm = (TextView) dialog.findViewById(R.id.btn_confirm);
		btnConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isTrackingMsg) {
					HTTP_getAddTrace("2");
					dialog.dismiss();
				} else {
					HTTP_getAddTrace("1");
					dialog.dismiss();
				}
			}
		});

		TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	private void getTracingID() {
		if (PreferenceUtil.getBoolean(MyProductActivity.this,
				Constant.USER_LOGIN) && mMemberID.equals(id) == false) {

			new Thread() {
				@Override
				public void run() {
					String[] params = { "TracingID" };
					String[] data = { mMemberID };
					String result = Http.post(Constant.POST_CHECK_TRACE,
							params, data, MyProductActivity.this);
					Log.d("result", "tacing result: " + result);

					Bundle b = new Bundle();
					Message m = new Message();
					if (result.contains("show")) {
						b.putBoolean("tracing", false);
					} else {
						b.putBoolean("tracing", true);

					}
					m.setData(b);
					refresh_menu.sendMessage(m);

				}

			}.start();

		} else {

		}

	}

	private void getStore() {
		// TODO Auto-generated method stub
		Log.e(TAG, "getStore BEGIN");

		new Thread() {
			@Override
			public void run() {
				String[] params = { "MemberID" };
				String[] data = { mMemberID };
				String result = Http.post(Constant.GET_STORE, params, data,
						MyProductActivity.this);
				try {
					storeInfo = JSONParserTool.getStore(result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Message msg = new Message();
				msg.arg1 = GET_STORE_INFO;
				handler.sendMessage(msg);
			}

		}.start();
	}

	private void getSomeoneProduct() {
		Log.e(TAG, "getSomeoneProduct BEGIN");

		new Thread() {
			@Override
			public void run() {
				String[] params = { "MemberID" };
				String[] data = { mMemberID };
				String result = Http.post(Constant.GET_SOMEONE_PRODUCT, params,
						data, MyProductActivity.this);

				Log.d("mypro", "result :" + result);
				try {
					JSONParserTool.getNearProductList(result,
							ApplicationController.searchProductList, false);
					// productList.clear();
					productList = ApplicationController.searchProductList;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Message msg = new Message();
				msg.arg1 = GET_SOMEONE_PRODUCT;
				handler.sendMessage(msg);
			}

		}.start();
	}

	private void SetNowCity(final double Lat, final double Lng) {

		new Thread() {
			@Override
			public void run() {

				Http.setLocation(Lat, Lng);

				HTTP_updateStroeLoc();

			}

		}.start();

	}

	private void HTTP_getAddTrace(final String isPush) {

		new Thread() {
			@Override

			public void run() {
				String[] params = { "TracingID", "AddTime", "Push" };
				String[] data = { mMemberID, Util.getTime(), isPush };

				String result = Http.post(Constant.POST_ADD_TRACE, params,
						data, MyProductActivity.this);

				Bundle b = new Bundle();
				Message m = new Message();
				b.putString("State", "tracing");
				m.setData(b);

				handler_toast.sendMessage(m);

			}

		}.start();

	}

	private void HTTP_getDeleteTrace(final String memberID) {

		new Thread() {
			@Override
			public void run() {
				String[] params = { "TracingID", "AddTime" };
				String[] data = { memberID, Util.getTime() };

				String result = Http.post(Constant.POST_DELETE_TRACE, params,
						data, MyProductActivity.this);

				Bundle b = new Bundle();
				Message m = new Message();
				b.putString("State", "untracing");
				m.setData(b);
				handler_toast.sendMessage(m);

			}

		}.start();
	}

	private void HTTP_updateStroeLoc() {

		new Thread() {
			@Override
			public void run() {

				String[] params = { "Lat", "Lng", "StoreCity" };
				String[] data = {
						PreferenceUtil.getString(MyProductActivity.this,
								Constant.USER_LAT),
						PreferenceUtil.getString(MyProductActivity.this,
								Constant.USER_LNG), Constant.NowCity };

				String result = Http.post(Constant.UPDATE_STORE_LOC, params,
						data, MyProductActivity.this);

				Bundle b = new Bundle();
				Message m = new Message();
				b.putString("State", "Loc");
				m.setData(b);
				handler_toast.sendMessage(m);

			}

		}.start();

	}

	Handler handler_toast = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			

			if (msg.getData().getString("State").equals("Loc")) {
				Toast.makeText(MyProductActivity.this,
						getString(R.string.update_successful),
						Toast.LENGTH_LONG).show();
				mCity.setText(Constant.NowCity);

			} else if (msg.getData().getString("State").equals("tracing")) {
				
				MenuItem item_unfollow = mMenu.findItem(R.id.unfollow);
				item_unfollow.setVisible(false);
				item_unfollow.setEnabled(false);
				
				MenuItem item_follow = mMenu.findItem(R.id.follow);
				item_follow.setVisible(true);
				item_follow.setEnabled(true);
				
				Toast.makeText(MyProductActivity.this,
						getString(R.string.followed), Toast.LENGTH_LONG).show();

			} else if (msg.getData().getString("State").equals("untracing")) {
				MenuItem item_follow = mMenu.findItem(R.id.follow);
				item_follow.setVisible(false);
				item_follow.setEnabled(false);
				
				MenuItem item = mMenu.findItem(R.id.unfollow);
				item.setVisible(true);
				item.setEnabled(true);
				
				
				Toast.makeText(MyProductActivity.this,
						getString(R.string.unfollow), Toast.LENGTH_LONG).show();

			}

		}
	};

	Handler handler_refresh_store_info = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			mCity.setText(msg.getData().getString("storeCity"));
			mStoreName.setText(msg.getData().getString("storeName"));
			mIntro.setText(msg.getData().getString("storeIntro"));
			if (msg.getData().getString("storeType").equals("null")) {
				mType.setText(msg.getData().getString("storeType"));
			} else {
				mType.setText(mTypes[Integer.valueOf(msg.getData().getString(
						"storeType"))]);
				mStoreType = Integer.valueOf(msg.getData().getString(
						"storeType"));
			}
			mAvgCount.setText(msg.getData().getString("average"));
			mRatingCount.setText(msg.getData().getString("count"));
			mAvgRatingBar.setRating(Float.valueOf(msg.getData().getString(
					"average")));

		}

	};

	Handler refresh_menu = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.getData().getBoolean("tracing")) {

				MenuItem item = mMenu.findItem(R.id.follow);
				item.setVisible(true);
				item.setEnabled(true);

			}

			else {
				MenuItem item = mMenu.findItem(R.id.unfollow);
				item.setVisible(true);
				item.setEnabled(true);

			}

		}
	};

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case GET_STORE_INFO:
				initStoreInfo();
				break;
			case GET_SOMEONE_PRODUCT:
				setListView();
				dismissProgressDialog();
				break;
			}

		}
	};

	

	private void uploadImageToS3() {
		AmazonUtil.isVideo = false;
		AmazonUtil.isStore = true;

		// Clear pic
		TransferModel[] models = TransferModel.getAllTransfers();
		for (int i = 0; i < models.length; i++) {
			if (models[i].getFileName().contains(".jpg")) {
				models[i].abort();
			}
		}
		// upload pic

		String dirPath = getFilesDir().getAbsolutePath() + "/";
		File tmpFile = new File(dirPath
				+ PreferenceUtil.getString(MyProductActivity.this,
						Constant.USER_ID) + ".jpg");
		Uri uri = Uri.fromFile(tmpFile);
		TransferController.upload(this, uri);

		tmpFile = new File(dirPath
				+ PreferenceUtil.getString(MyProductActivity.this,
						Constant.USER_ID) + "-s.jpg");
		uri = Uri.fromFile(tmpFile);
		TransferController.upload(this, uri);

	}

	private OnClickListener productObj_click = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent it = new Intent();
			ProductObject productObj;

			switch (v.getId()) {

			case R.id.iv_member_icon:

				productObj = (ProductObject) v.getTag();

				it = new Intent(MyProductActivity.this,
						Product_Detail_Activity.class);
				it.putExtra("product_object", productObj);
				if(getIntent().getIntExtra("layout", 0)==2){
				it.putExtra("edit", true);
				}else{
					it.putExtra("edit", false);
	
					
				}
				startActivity(it);

				break;
				
			case R.id.btn_favor:
				productObj = (ProductObject) v.getTag(R.id.tag_object);

				//Log.d("product","productObj.getTitle():"+productObj.getTitle());
				//Log.d("product","v.getTag().toString()):"+v.getTag(R.id.tag_boolean).toString());
                
				add_favor_object(productObj);
						
				
				int lovePosition = 0;
				if (v.getTag(R.id.tag_boolean).toString().equals("true")) {
					v.setTag(R.id.tag_boolean,"false");

					for (int i = 0; i < favor_list.size(); i++) {
						if (favor_list.get(i).getProductID().equals(productObj.getProductID())) {
							lovePosition = i;
							break;
						}
					}
					v.setBackgroundResource(R.drawable.cell_product_unlove);
					
					Log.d("product","lovePosition :"+lovePosition);
					Log.d("product","	favor_list size :"+	favor_list.size());

				
					PIPI_Tool.removeFavorite(MyProductActivity.this, lovePosition, favor_list, true);
					//favor_list.remove(lovePosition);

				 
				}
				else {
					v.setTag(R.id.tag_boolean,"true");
					v.setBackgroundResource(R.drawable.cell_product_love);
				}
				
				
				break;
				
				

			case R.id.iv_product_pic:
				// Log.d("myproduct", "?嚙踐??嚙踝?嚙踝?");
				productObj = (ProductObject) v.getTag();

				it = new Intent(MyProductActivity.this,
						Product_Detail_Activity.class);
				it.putExtra("product_object", productObj);
				if(getIntent().getIntExtra("layout", 0)==2){
					it.putExtra("edit", true);
					}else{
						it.putExtra("edit", false);
		
						
					}

				startActivity(it);

				break;

			case R.id.btn_play_video:
				productObj = (ProductObject) v.getTag();

				it = new Intent(MyProductActivity.this,
						PlayVideo_Activity.class);
				it.putExtra("position",
						Integer.valueOf(productObj.getProductID()));

				it.putExtra("productId", productObj.getProductID());
				it.putExtra("picNumber", productObj.getPicNumber());
				it.putExtra("ID", productObj.getID());
				it.putExtra("title", productObj.getTitle());
				it.putExtra("type", productObj.getType());
				it.putExtra("time", productObj.getTime());
				it.putExtra("des", productObj.getDes());
				it.putExtra("price", productObj.getPrice());
				it.putExtra("Lng", productObj.getLng());
				it.putExtra("Lat", productObj.getLat());
				it.putExtra("dis", productObj.getDistance());
				it.putExtra("Delivery", productObj.getDelivery());

				if (productObj.getF1() != null)
					it.putExtra("F1_name", productObj.getF1().getName());
				if (productObj.getF2() != null)
					it.putExtra("F3_name", productObj.getF1().getName());
				if (productObj.getF3() != null)
					it.putExtra("F2_name", productObj.getF1().getName());

				it.setAction(Constant.ACTION_SINGLE_PLAY);
				startActivity(it);
				break;

			}

		}
	};
	private OnClickListener rating_click = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Log.d("myproduct", "click my!");
			Intent intent = new Intent();
			intent.setClass(MyProductActivity.this, SomeoneRatingActivity.class);
			intent.putExtra("memberID", mMemberID);
			startActivity(intent);

		}
	};

	private OnClickListener typeOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			DialogListUtil.showTypeDialog(MyProductActivity.this, true,
					new OnTypeClickListener() {

						@Override
						public void onTypeClick(int position) {
							mStoreType = position;
							mType.setText(mTypes[position]);
						}
					});
		}
	};

	private OnClickListener cancel() {
		return new OnClickListener() {

			@Override
			public void onClick(View v) {
				// hideUi()
				mIntro.setVisibility(View.VISIBLE);
				mStoreName.setVisibility(View.VISIBLE);
				mType.setBackgroundColor(Color.WHITE);
				mType.setOnClickListener(null);

				mIntroScrollView.setBackgroundColor(Color.WHITE);

			
			}
		};
	}


	private OnClickListener changeImageListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"), PICK);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		/*----- get someone product Sell-----*/
		if (resultCode == 4) {
			setResult(4);
			finish();
		}
		/*----- get someone product Buy-----*/
		if (resultCode == 7) {
			setResult(7);
			finish();
		}

		/*----- chat start -----*/
		else if (resultCode == 5) {
			setResult(5);
			finish();
		}
		// mIsPicked = false;

		if (resultCode == RESULT_OK) {
			// ImageLoader.getInstance().clearMemoryCache();

			Uri selectedImageUri = data.getData();
			String selectedImagePath = getPath(selectedImageUri);
			Bitmap newImg = cropBitmap(BitmapFactory
					.decodeFile(selectedImagePath));
			mMyImage.setImageBitmap(newImg);
			// mIsPicked = true;
			mIsPicked = true;
		}

	}
private void add_favor_object(ProductObject ob){
		
		FavoriteObject favoriteObject = new FavoriteObject();
		    favoriteObject.setProductID(ob.getProductID());
		    favoriteObject.setProductTitle(ob.getTitle());
		    favoriteObject.setTime(ob.getTime());
	     	favoriteObject.setContact(ob.getContact());
			favoriteObject.setContact(ob.getContact());	
			favoriteObject.setMemberID(ob.getID());
			favoriteObject.setPicNumber(ob.getPicNumber());
			favoriteObject.setPayment(ob.getPayment());
			favoriteObject.setDelievery(ob.getDelivery());
 
			favoriteObject.setType(ob.getType());
			favoriteObject.setPrice(ob.getPrice());
			favoriteObject.setDistance(ob.getDistance());
			favoriteObject.setStock(ob.getStock());
			
		PIPI_Tool.addFavorite(MyProductActivity.this, favoriteObject, favor_list, true);
		if (PreferenceUtil.getBoolean(MyProductActivity.this, Constant.USER_LOGIN)){
			
			HTTP_favoriteProduct(ob.getProductID());
		
	    }
	}
private void HTTP_favoriteProduct(final String productID){
	
	 new Thread() {			
			@Override
			public void run() {

		String[] params = { "ProductID","AddTime"};
				String[] data = {productID, Util.getTime() };
				
				String result = Http.post(Constant.FAVORITE_PRODUCT, params,
						data, MyProductActivity.this); 
		Log.d("product","product add fav result:"+result);
			}

		}.start();
		
}
	private String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

	private Bitmap cropBitmap(Bitmap srcBmp) {

		Bitmap dstBmp;
		if (srcBmp.getWidth() >= srcBmp.getHeight()) {
			/*
			 * dstBmp = Bitmap.createBitmap(srcBmp, srcBmp.getWidth() / 2 -
			 * srcBmp.getHeight() / 2, 0, srcBmp.getHeight(),
			 * srcBmp.getHeight());
			 */

			dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getHeight(),
					srcBmp.getHeight());

		} else {

			/*
			 * dstBmp = Bitmap.createBitmap(srcBmp, 0, srcBmp.getHeight() / 2 -
			 * srcBmp.getWidth() / 2, srcBmp.getHeight(), srcBmp.getHeight());
			 */

			dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getWidth(),
					srcBmp.getWidth());
		}

		srcBmp.recycle();
		Bitmap temp = dstBmp;

		FileOutputStream fop;
		try {
			// LARGE
			dstBmp = Bitmap.createScaledBitmap(dstBmp, LARGE_IMAGE,
					LARGE_IMAGE, false);
			fop = new FileOutputStream(mDirPath + mMemberID + ".jpg");
			dstBmp.compress(Bitmap.CompressFormat.JPEG, 100, fop);

			// SMALL
			dstBmp = Bitmap.createScaledBitmap(dstBmp, SMALL_IMAGE,
					SMALL_IMAGE, false);
			fop = new FileOutputStream(mDirPath + mMemberID + "-s.jpg");
			dstBmp.compress(Bitmap.CompressFormat.JPEG, 100, fop);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dstBmp.recycle();

		return temp;
	}

	/** ProgressDialog methods **/
	private void displayProgressDialog(String title, String message) {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(MyProductActivity.this, title,
					message);
		}
	}

	private void dismissProgressDialog() {
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}

}

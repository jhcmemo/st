package pipimy.main;

import java.util.ArrayList;

import pipimy.cart.CartCheckoutActivity;
import pipimy.cart.ShoppingCartUtil;
import pipimy.cart.ShoppingCartUtil.onGetCartListIndexCallback;
import pipimy.object.CartItemsObject;
import pipimy.order.OrderListActivity;
import pipimy.others.ApiProxy;
import pipimy.others.CircleNetwork_ImageView;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.IGenericDialogUtil;
import pipimy.others.JSONParserTool;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.others.DialogUtil.OnMsgInputListener;
import pipimy.service.R;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.franlopez.flipcheckbox.FlipCheckBox;
import com.franlopez.flipcheckbox.OnFlipCheckedChangeListener;

public class ShoppingFragment extends Fragment implements
		onGetCartListIndexCallback {

	final static String TAG = "ShoppingFragment";
	final static int DELETE_FROM_CART = 10002;

	MainActivity activity;
	ListView cartIndexListView;
	SwipeRefreshLayout swipeLayout;

	ImageLoader mImageLoader;
	RequestQueue queue;

	int removePosition;
	int chooseAllCounter;
	int toBuyCount;
	LinearLayout rootView;
	View storeProductView;
	CommonAdapter<CartItemsObject> adapter;
	Handler timeoutHandler = new Handler();
	boolean isRefresh;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_shopping, container,
				false);
		setHasOptionsMenu(true);

		rootView = (LinearLayout) view.findViewById(R.id.cart_cell_layout);
		swipeLayout = (SwipeRefreshLayout) view
				.findViewById(R.id.swipe_container);
		swipeLayout.setColorSchemeResources(android.R.color.holo_red_light,
				android.R.color.holo_blue_light,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light);

		swipeLayout.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub

				if (!isRefresh) {
					swipeLayout.setRefreshing(true);
					getShoppingList();
				}
			}

		});

		init();

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Log.e("onActivityCreated","Chat Fragment");
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		//menu.clear();
		super.onCreateOptionsMenu(menu, inflater);

		getActivity().getMenuInflater().inflate(R.menu.shopping, menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_shopping:
			Intent it = new Intent(getActivity(), OrderListActivity.class);
			startActivity(it);
			break;
		}
		return true;
	}

	private void init() {
		// TODO Auto-generated method stub
		ShoppingCartUtil.setOnGetCartListIndexCallback(this);
		queue = VolleySingleton.getInstance().getRequestQueue();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		// initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		Log.e(TAG, "isCart changed = " + ApplicationController.isCartChanged);

		if (ApplicationController.isCartChanged) {
			ApplicationController.isCartChanged = false;
			rootView.removeAllViews();
			if (ApplicationController.cartItemList.size() > 0) {
				LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				layoutParams.setMargins(20, 20, 20, 20);

				for (int i = 0; i < ApplicationController.cartStoreCount.size(); i++) {
					rootView.addView(initStoreView(storeProductView, i),
							layoutParams);
				}
			}
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private View initStoreView(View v, int sellerIndex) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		storeProductView = inflater.inflate(R.layout.view_cell_cart, null);

		final TextView seller_name = (TextView) storeProductView
				.findViewById(R.id.txt_seller_name);
		final FlipCheckBox sellerCheckBox = (FlipCheckBox) storeProductView
				.findViewById(R.id.store_check_box);
		final ListView productList = (ListView) storeProductView
				.findViewById(R.id.listView1);
		final CircleNetwork_ImageView sellerPic = (CircleNetwork_ImageView) storeProductView
				.findViewById(R.id.member_icon);

		ImageView commitBtn = (ImageView) storeProductView
				.findViewById(R.id.btn_commit);
		ImageView removeBtn = (ImageView) storeProductView
				.findViewById(R.id.btn_remove);
		String seller = ApplicationController.cartStoreCount.get(sellerIndex);

		int subIndexFrom = ApplicationController.cartStoreProductMap
				.get(seller);
		int subIndexDes = subIndexFrom
				+ ApplicationController.cartStoreProductCount[sellerIndex];

		final ArrayList<CartItemsObject> list = new ArrayList<CartItemsObject>(
				ApplicationController.cartItemList.subList(subIndexFrom,
						subIndexDes));

		String url = Constant.STORE_IMAGE_URL + list.get(0).getSellerID()
				+ ".jpg";
		sellerPic.setImageUrl(url, mImageLoader);

		adapter = new CommonAdapter(getActivity(), list,
				R.layout.cell_shopping_car) {

			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {

				final CartItemsObject obj = (CartItemsObject) item;

				final NetworkImageView product_img = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView,
								R.id.img_product_pic);
				final TextView product_price = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_price);
				final TextView product_title = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_title);
				final TextView product_post_time = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.txt_product_post_time);
				final FlipCheckBox productCheckBox = (FlipCheckBox) commonViewHolder
						.get(commonViewHolder, currentView, R.id.product_check);

				final Spinner spStock = (Spinner) commonViewHolder.get(
						commonViewHolder, currentView, R.id.stock_spinner);
				final ArrayList<String> spinnerChoice = new ArrayList<String>();
				if (Integer.parseInt(obj.getProductStock()) <= 0) {
					spinnerChoice.add("0");
				} else {
					spinnerChoice.add(getResources().getString(
							R.string.layout_txt_stock_spinner));
					for (int i = 0; i <= Integer
							.parseInt(obj.getProductStock()); i++) {
						spinnerChoice.add(Integer.toString(i));
					}
				}
				spStock.setAdapter(new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_spinner_dropdown_item,
						spinnerChoice));
				if (Integer.parseInt(obj.getProductStock()) <= 0)
					spStock.setSelection(0);
				else {
					spStock.setSelection(2);
					spStock.setOnItemSelectedListener(new OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							if (position == 0) {
								inputStockCount(
										spStock,
										Integer.parseInt(obj.getProductStock()),
										spinnerChoice);
								toBuyCount = userInputStock;
							} else {
								toBuyCount = position-1;
							}
							
							obj.setNumberToBuy(toBuyCount);
							product_img.setImageUrl(obj.getProductPicUrl(), mImageLoader);
							seller_name.setText(obj.getSellerID());
							product_price.setText(obj.getPrice());
							product_title.setText(obj.getTitle());
							product_post_time.setText(obj.getPostTime());
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
						}
					});
				}
				
				productCheckBox.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						boolean sellerIsChecked = ((FlipCheckBox) v).isChecked();
						productCheckBox.setChecked(!sellerIsChecked);
						obj.setChecked(!sellerIsChecked);
						int checkPostiviveCount = 0;
						for (int i = 0; i < list.size(); i++) {
							if (list.get(i).isChecked()) {
								checkPostiviveCount++;
							}
						}
						
						if (checkPostiviveCount == list.size()) {
							setFlipCheckBox(sellerCheckBox, true);
						} else {
							setFlipCheckBox(sellerCheckBox, false);
						}
					}
					
				});

			}

		};
		productList.setFocusable(false);
		productList.setAdapter(adapter);
		productList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		Util.setListViewHeightBasedOnChildren(productList);

		commitBtn.setTag(list);
		commitBtn.setOnClickListener(itemClickListener);

		removeBtn.setTag(list);
		removeBtn.setOnClickListener(itemClickListener);

		sellerCheckBox.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean sellerIsChecked = ((FlipCheckBox) v).isChecked();
				sellerCheckBox.setChecked(!sellerIsChecked);

				for (int i = 0; i < productList.getChildCount(); i++) {
					View view = productList.getChildAt(i);
					FlipCheckBox cb = (FlipCheckBox) view.findViewById(R.id.product_check);
					if (!sellerIsChecked) {
						if (Integer.parseInt(list.get(i).getProductStock()) > 0) {
							cb.setChecked(true);
							list.get(i).setChecked(true);
						}
					} else {
						cb.setChecked(false);
						list.get(i).setChecked(false);
					}

				}

			}
		});

		return storeProductView;
	}

	int userInputStock;

	private void inputStockCount(final Spinner spStock, int maxStock,
			final ArrayList<String> strList) {
		userInputStock = 0;
		DialogUtil.stockInputDialog(getActivity(), Integer.toString(maxStock),
				new OnMsgInputListener() {
					@Override
					public void onMsgInputClick(View v, String message) {
						userInputStock = strList.indexOf(message);
						spStock.setSelection(userInputStock);
						Log.e(TAG, "inputStockCount = " + userInputStock);
					}
				});
	}

	private void setFlipCheckBox(FlipCheckBox box, boolean isCheck) {
		if (box.isChecked() != isCheck) {
			box.setChecked(isCheck);
		}
			
	}

	/*
	 * Btn click behaviors: buy or remove
	 */
	private OnClickListener itemClickListener = new OnClickListener() {
		ArrayList<CartItemsObject> list;

		@SuppressWarnings("unchecked")
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_commit: {
				list = (ArrayList<CartItemsObject>) v.getTag();
				if (list.size() > 0)
					checkoutShoppingList(list);
				break;
			}
			case R.id.btn_remove: {
				list = (ArrayList<CartItemsObject>) v.getTag();
				if (list.size() > 0)
					showRemoveConfirm(list);
				break;
			}

			}
		}
	};

	protected void checkoutShoppingList(ArrayList<CartItemsObject> list) {
		Log.e(TAG, "checkoutShoppingList BEGIN");

		ArrayList<CartItemsObject> checkOutList = new ArrayList<CartItemsObject>();

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).isChecked()) {
				checkOutList.add(list.get(i));
			}
		}

		if (checkOutList.size() > 0) {
			Intent intent = new Intent();
			intent.putExtra("shopping_list", checkOutList);
			intent.setClass(getActivity(), CartCheckoutActivity.class);
			getActivity().startActivityForResult(intent, 66);
			getActivity().overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);

		} else {
			Toast.makeText(
					getActivity(),
					getResources()
							.getString(R.string.toast_str_no_shoppinglist),
					1500).show();
		}

	}

	ArrayList<CartItemsObject> removeList = new ArrayList<CartItemsObject>();
	String removeProductIdList;

	protected void showRemoveConfirm(ArrayList<CartItemsObject> list) {

		String productList = "";
		removeProductIdList = "";

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).isChecked()) {
				removeList.add(list.get(i));
				productList = productList + list.get(i).getTitle() + "\n";
				if (i != list.size() - 1)
					removeProductIdList = removeProductIdList
							+ list.get(i).getProductID() + ",";
				else
					removeProductIdList = removeProductIdList
							+ list.get(i).getProductID();
			}

		}

		if (removeList.size() > 0) {
			String msg = getResources()
					.getString(R.string.shopping_list_remove)
					+ "\n"
					+ productList;

			DialogUtil.pushGeneralDialog(getActivity(), "Alert", msg, "ok",
					"cancel",
					new IGenericDialogUtil.IGenericBtnClickListener() {
						@Override
						public void PositiveMethod(DialogInterface dialog,
								int id) {
							deleteCartItems(removeProductIdList);
						}

						@Override
						public void NegativeMethod(DialogInterface dialog,
								int id) {
						}
					});
		} else {
			Toast.makeText(
					getActivity(),
					getResources()
							.getString(R.string.toast_str_no_shoppinglist),
					1500).show();
		}
	}

	protected void getShoppingList() {
		// TODO Auto-generated method stub
		calcaulateTimeout();
		apiRequest(Constant.CODE_GET_CART_INDEX);
	}

	private void calcaulateTimeout() {
		// TODO Auto-generated method stub
		timeoutHandler.postDelayed(connectionTimer, 0);
	}

	int elapsedTime = 0;
	private Runnable connectionTimer = new Runnable() {
		@Override
		public void run() {
			Log.e(TAG, "Runnable isRefresh = " + isRefresh + " elapsedTime = "
					+ elapsedTime);
			if (isRefresh) {
				elapsedTime = elapsedTime + 1000;
				if (elapsedTime >= 10000) {
					elapsedTime = 0;
					swipeLayout.setRefreshing(false);
					isRefresh = false;
					timeoutHandler.removeCallbacks(connectionTimer);
				} else {
					timeoutHandler.postDelayed(connectionTimer, 1000);
				}

			} else {
				timeoutHandler.removeCallbacks(connectionTimer);
			}
		}

	};

	String removeIds = "";

	protected void deleteCartItems(final String removeProductIdList) {
		// TODO Auto-generated method stub
		Log.e(TAG, "deleteCartItems BEGIN, remove id = " + removeProductIdList);
		Util.showProgressDialog(getActivity(), "wait", "loading");
		removeIds = removeProductIdList;
		apiRequest(Constant.CODE_DEL_CART_ITEM);
	}

	private void apiRequest(final int code) {
		new Thread() {
			@Override
			public void run() {
				switch (code) {
				case Constant.CODE_GET_CART_INDEX: {
					Message msg = ApiProxy
							.getMyShoppingCartIndex(getActivity());
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_DEL_CART_ITEM: {
					Message msg = ApiProxy.removeItemsFromCart(removeIds,
							getActivity());
					handler.sendMessage(msg);
					break;
				}
				}
			}
		}.start();
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case Constant.CODE_GET_CART_INDEX: {
				isRefresh = false;
				swipeLayout.setRefreshing(false);
				ApplicationController.isCartChanged = true;
				onGetCartListIndex();
				break;
			}
			case Constant.CODE_DEL_CART_ITEM: {
				Util.dismissProgressDialog();
				try {
					if (msg.obj.toString().contains("success")) {
						doDeleteIndex();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}
			case Constant.CODE_RETURN_ERR:
				isRefresh = false;
				swipeLayout.setRefreshing(false);
				break;
			}

		}
	};

	protected void doDeleteIndex() {
		// TODO Auto-generated method stub
		Log.e(TAG, "doDeleteIndex BEGIN");
		
		for (int i = 0; i < removeList.size(); i++) {
			if(removeList.get(i).isChecked()) {
				Log.e(TAG, "doDeleteIndex id = "+removeList.get(i).getProductID());
				ApplicationController.cartItemList.remove(removeList.get(i));
			}
				
		}

		JSONParserTool.getNewShoppingCart(ApplicationController.cartItemList);
		ApplicationController.isCartChanged = true;
		onGetCartListIndex();
	}

	@Override
	public void onGetCartListIndex() {
		// TODO Auto-generated method stub
		Log.e(TAG, "onGetCartListIndex BEGIN");
		try {
			adapter.notifyDataSetChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}

		initView();
	}

}
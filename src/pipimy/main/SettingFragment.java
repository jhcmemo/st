package pipimy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gcm.GCMRegistrar;

import pipimy.chat.ChatUtil;
import pipimy.login.LoginActivity;
import pipimy.others.BitmapUtils;
import pipimy.others.CircleNetwork_ImageView;
import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.DialogListUtil.OnChangeLocClickListener;
import pipimy.others.DialogListUtil.OnChangeStroeFlowClickListener;
import pipimy.others.DialogUtil;
import pipimy.others.Http;
import pipimy.others.IGenericDialogUtil;
import pipimy.others.JSONParserTool;
import pipimy.others.LibBase64;
import pipimy.others.MediaUtils;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.others.DialogListUtil.OnChangePWDClickListener;
import pipimy.others.FragmentManagerUtil;
import pipimy.others.PreferenceUtil;
import pipimy.service.R;
import pipimy.setting.activity.ModifyStoreInfoActivity;
import pipimy.setting.activity.MyDealActivity;
import pipimy.setting.activity.PostActivity;
import pipimy.setting.activity.SomeoneRatingActivity;
import pipimy.setting.activity.StoreFlowSettingActivity;
import pipimy.setting.activity.TrackActivity;
import pipimy.setting.activity.MyCommentListActivity;
import pipimy.third.AmazonUtil;
import pipimy.third.TransferController;
import pipimy.third.TransferModel;


import android.support.v4.app.Fragment;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class SettingFragment extends Fragment {
	ImageLoader mImageLoader;

	private View member_trade;
	private View member_post_product;
	private View myProductView;
	private View myFansView;
	private View myDealView;
	private View myDealViewAsBuyer;
	private View myRatingView;
	private View changePWDView;
	private View feedbackView;
	private View logoutView;
	private View refresh_location;

	
	private View member_intro;
	private View member_pre_store;
	//private Button btn_login;
	private ScrollView scrollView1;
	CircleNetwork_ImageView member_icon;
	NetworkImageView member_background;
	Bitmap newImg_store;
	Bitmap newImg_store_background;

	Bitmap out_put_img;
	Bitmap temp  ;
    View lv_login;
	private String mAndroidToken;


	private View  tv_login;

	private static MainActivity activity;

    
	//private static final int LARGE_IMAGE = 300;
	//private static final int SMALL_IMAGE = 100;
	
	private final static int UPDATE_ANDROID_TOKEN = 10001;
	private final static int LOGIN = 10002;

	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
		
		


    }
	
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		activity = (MainActivity) getActivity();
		mAndroidToken = GCMRegistrar.getRegistrationId(activity);
		Log.d("token","mAndroidToken :"+mAndroidToken);
		
		member_icon= (CircleNetwork_ImageView) view.findViewById(R.id.member_icon);
		member_background=(NetworkImageView) view.findViewById(R.id.member_background);
		member_trade = (View) view.findViewById(R.id.member_trade);		
		member_post_product = (View) view.findViewById(R.id.member_post_product);		
		member_intro= (View) view.findViewById(R.id.member_intro);		
		myProductView = (View) view.findViewById(R.id.refrsh_product);
		refresh_location= (View) view.findViewById(R.id.refresh_location);
		member_pre_store=(View) view.findViewById(R.id.member_pre_store);
		myFansView= (View) view.findViewById(R.id.setting_myFansLayout);
		myDealView = (View) view.findViewById(R.id.setting_myDealLayout);
		myDealViewAsBuyer = (View) view.findViewById(R.id.setting_myDealLayoutAsBuyer);
		myRatingView = (View) view.findViewById(R.id.setting_myRatingLayout);
		changePWDView = (View) view.findViewById(R.id.setting_changePWDLayout);
		feedbackView = (View) view.findViewById(R.id.setting_feedBackLayout);
		logoutView = (View) view.findViewById(R.id.setting_logoutLayout);
		scrollView1= (ScrollView) view.findViewById(R.id.scrollView1);
		
		lv_login= (LinearLayout) view.findViewById(R.id.lv_login);
	
		tv_login=(View)view.findViewById(R.id.tv_login);
		//btn_login= (Button) view.findViewById(R.id.btn_login);
		
		
		//btn_login.setOnClickListener(login_click);
		tv_login.setOnClickListener(itemClickListener);
     	member_trade.setOnClickListener(itemClickListener);
		member_post_product.setOnClickListener(itemClickListener);		
		myProductView.setOnClickListener(itemClickListener);
		member_intro.setOnClickListener(itemClickListener);
		member_pre_store.setOnClickListener(itemClickListener);
		refresh_location.setOnClickListener(itemClickListener);
		
		
		myFansView.setOnClickListener(itemClickListener);
		myDealView.setOnClickListener(itemClickListener);
		myRatingView.setOnClickListener(itemClickListener);
		changePWDView.setOnClickListener(itemClickListener);
		feedbackView.setOnClickListener(itemClickListener);
		logoutView.setOnClickListener(itemClickListener);
		myDealViewAsBuyer.setOnClickListener(itemClickListener);
		member_background.setOnClickListener(itemClickListener);
		member_icon.setOnClickListener(itemClickListener);
		
	    member_icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
		member_background.setScaleType(ImageView.ScaleType.CENTER_CROP);
		mImageLoader = VolleySingleton.getInstance().getImageLoader();

		init_member_icon();
		 
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		

		
		
		
		
		
	return inflater.inflate(R.layout.fragment_setting_login, container, false);
	}
	
	@Override  
    public void onActivityCreated(Bundle savedInstanceState) {  
        super.onActivityCreated(savedInstanceState);
    }  
  
	@Override
	public void onResume() {
        super.onResume();
                
        if( PreferenceUtil.getBoolean(getActivity(), Constant.USER_LOGIN)){
            Log.d("sef","replace true");
            
            init_member_icon();
            lv_login.setVisibility(View.GONE);
            scrollView1.setVisibility(View.VISIBLE);

       
        }
        else{
        	
        }
        
    }
	
	@Override
    public void onPause() {
        super.onPause();
    }
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if(newImg_store!=null){
			newImg_store.recycle();
		}
		if(temp!=null){
			temp.recycle();
		}
		
		if(newImg_store_background!=null){
			newImg_store_background.recycle();
		}
		if(out_put_img!=null){
			out_put_img.recycle();
		}
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	   if( requestCode == 99 && resultCode == Activity.RESULT_OK){
		
		   
		 /*  if(out_put_img!=null){
			   
			   out_put_img.recycle();
		   }*/
		   if(newImg_store!=null){
			   newImg_store.recycle();
		   }
		  
		  /* if(temp!=null){
			   temp.recycle();
		   }*/

			//member_icon.setDefaultImageResId(R.drawable.nohead);
			Uri selectedImageUri = data.getData();
			String selectedImagePath  = getPath(selectedImageUri);
	    	Log.d("setting","storePic--selectedImageUri : "+selectedImagePath);
	    	/**�����釭���*/
	    	
   
	    Options options = new Options();
		options.inJustDecodeBounds = true;
	    options.inSampleSize = 2;
	    options.inJustDecodeBounds = false;
	    
	    
	    
	    
	   
	        out_put_img = cropBitmap(BitmapFactory.decodeFile(selectedImagePath,options),true);
	    	save_bitmap(out_put_img,true);
	    	 
	    	
	    	temp.recycle();
	    	 

	    	options.inSampleSize = 5;
			/**自己放低品質*/

			/**�撌望雿�釭*/

	    	newImg_store=	cropBitmap(BitmapFactory.decodeFile(selectedImagePath,options),true);
	    	
	
			member_icon.setImageBitmap(newImg_store);
			
			uploadImageToS3(true);
	    	//out_put_img.recycle();

	
	   }
	     else if(requestCode == 100&& resultCode == Activity.RESULT_OK){
	    	/* if(out_put_img!=null){
				   
				   out_put_img.recycle();
			   }*/
			 
			   if(newImg_store_background!=null){
				   newImg_store_background.recycle();
			   }
			  /* if(temp!=null){
				   temp.recycle();
			   }*/
			    Options options = new Options();
				options.inJustDecodeBounds = true;
			    options.inSampleSize = 2;
			    options.inJustDecodeBounds = false;

	    		Uri selectedImageUri = data.getData();
				String selectedImagePath  = getPath(selectedImageUri);
		    	Log.d("setting","backgroundPic--selectedImageUri : "+selectedImagePath);

		    	out_put_img = cropBitmap(BitmapFactory.decodeFile(selectedImagePath,options),false);
		    	save_bitmap(out_put_img,false);
		    	temp.recycle();
		    	
		    	 options.inSampleSize=5;
		    	newImg_store_background=cropBitmap(BitmapFactory.decodeFile(selectedImagePath,options),true);

				member_background.setImageBitmap(newImg_store_background);
				
				uploadImageToS3(false);
		
		    //	out_put_img.recycle();

		   
		   
	   }
	   
	   
	   
	}


	private void init_member_icon(){
		
		member_icon.setDefaultImageResId(R.drawable.nohead);
        String memberID=PreferenceUtil.getString(getActivity(),Constant.USER_ID);
		
        String mMemberPicUrl = Constant.STORE_IMAGE_URL + memberID + ".jpg";
		String mMemberBackGroundUrl = Constant.STORE_BACKGROUNDS_URL + memberID + "-bg.jpg";

		member_icon.setImageUrl(mMemberPicUrl, mImageLoader);
		member_background.setImageUrl(mMemberBackGroundUrl, mImageLoader);
	}
	
	private void uploadImageToS3(boolean StorePic) {
		if(StorePic){
			Log.d("am","is store");
			AmazonUtil.isVideo = false;
			AmazonUtil.isStore = true;
			AmazonUtil.isBackGround = false;
		}else{
			Log.d("am","is bg");

			AmazonUtil.isVideo = false;
			AmazonUtil.isStore = false;
			AmazonUtil.isBackGround = true;
		}

		// Clear pic
		TransferModel[] models = TransferModel.getAllTransfers();
		for (int i = 0; i < models.length; i++) {
			if (models[i].getFileName().contains(".jpg")) {
				models[i].abort();
			}
		}
		// upload pic
if(StorePic){		
	String dirPath = getActivity().getFilesDir().getAbsolutePath() + "/";
		File tmpFile = new File(dirPath
				+ PreferenceUtil.getString(getActivity(),
						Constant.USER_ID) + ".jpg");
		Uri uri = Uri.fromFile(tmpFile);
		TransferController.upload(getActivity(), uri);
		
		/*tmpFile = new File(dirPath
		+ PreferenceUtil.getString(getActivity(),
				Constant.USER_ID) + "-s.jpg");
uri = Uri.fromFile(tmpFile);
TransferController.upload(getActivity(), uri);*/
}else{

	String dirPath = getActivity().getFilesDir().getAbsolutePath() + "/";
	File tmpFile = new File(dirPath
			+ PreferenceUtil.getString(getActivity(),
					Constant.USER_ID)+"-bg.jpg");
	
	Log.d("am","upload path2 :"+tmpFile.toString());
	Uri uri = Uri.fromFile(tmpFile);
	TransferController.upload(getActivity(), uri);
	
	
}	
		

	

	}
	
	
	
	
	private void save_bitmap(Bitmap bitmap,boolean Store_pic){
		
		if (bitmap == null || bitmap.isRecycled()) {
			return;			
		}		
		FileOutputStream fop;

		String mDirPath = getActivity().getFilesDir().getAbsolutePath() + "/";
        String mMemberID=PreferenceUtil.getString(getActivity(),
				Constant.USER_ID);
        

		//Log.d("filter","now bitpath:"+dirPath + fileName);
        String fileName="";
       if(Store_pic){
        fileName=  mDirPath + mMemberID + ".jpg";
       }else{  
         
    	 fileName= mDirPath + mMemberID  + "-bg.jpg";
       }
		

		try {
			
			fop = new FileOutputStream(fileName);		
			bitmap.compress(Bitmap.CompressFormat.JPEG,100, fop);
			fop.close();
			
			
			
		//	bitmap.recycle();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.d("filter","exception :"+e.getMessage());
			e.printStackTrace();
		}
		
    	out_put_img.recycle();

	}
	
	private String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}
	
	private Bitmap cropBitmap(Bitmap srcBmp,boolean StorePic) {

		Bitmap dstBmp;
		
    	

		
 	//	float scaleWidth = (float) ((srcBmp.getWidth()/1.5)/(float)srcBmp.getWidth());
	//	float scaleHeight =(float) ((srcBmp.getWidth()/1.5)/(float)srcBmp.getHeight());
		//Log.i("Alex","SettingManager====> set scale value : "+scaleWidth + ":" + scaleHeight);	
	//	Matrix matrix = new Matrix();
	//	matrix.postScale(scaleWidth, scaleHeight);
 				
 		//smll_bitmap=Bitmap.createBitmap(smll_bitmap, 0, 0, smll_bitmap.getWidth(),smll_bitmap.getHeight(), matrix, true);
		
		
		
		if (srcBmp.getWidth() >= srcBmp.getHeight()) {
		
			dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getHeight(),
					srcBmp.getHeight());

		} else {

			dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getWidth(),
					srcBmp.getWidth() );
		}

		/*FileOutputStream fop;
		try {
			// LARGE
			dstBmp = Bitmap.createScaledBitmap(dstBmp, LARGE_IMAGE,
					LARGE_IMAGE, false);
			fop = new FileOutputStream(mDirPath + mMemberID + ".jpg");
			dstBmp.compress(Bitmap.CompressFormat.JPEG, 100, fop);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		 temp = dstBmp;

		//dstBmp.recycle();
		srcBmp.recycle();
		return temp;
	}
	
	private OnClickListener login_click =new OnClickListener() {
		
		@Override
		public void onClick(View v) {

			Intent it =new Intent(getActivity(),LoginActivity.class);
			startActivityForResult(it, 0);
			
		}
	};
	
	private OnClickListener itemClickListener = new OnClickListener() {

		@SuppressWarnings("static-access")
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.tv_login:
				 intent = new Intent(getActivity(),LoginActivity.class);
					startActivityForResult(intent,2);
				 
				break;
			
		
			case R.id.member_icon:
			//	Log.d("setting","member_icon.getDrawable() :"+member_icon.getDrawable());

			    intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Select Picture"),99);
				
				
				
				break;
			
			case R.id.member_background:
				
				   intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(
							Intent.createChooser(intent, "Select Picture"),100);
					
				
				
				break;
				
			
		  
			/*----- flow -----*/
			case R.id.member_trade:
				
				
				settingStoreFlow();
						
				
				break;
				
				/*----- post -----*/
			case R.id.member_post_product:
				intent.setClass(getActivity(), PostActivity.class);
				startActivity(intent);
		
				
				break;
				/*----- my product -----*/


			
			case R.id.member_pre_store:	
				
				
				intent.putExtra("memberID",PreferenceUtil.getString(getActivity(), Constant.USER_ID));
				intent.putExtra("layout", 1);

				intent.setClass(getActivity(), MyProductActivity.class);
				startActivity(intent);
				
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_myproduct");
				
				break;

			case R.id.member_intro:
				
				intent.setClass(getActivity(), ModifyStoreInfoActivity.class);
				startActivity(intent);
				
				break;
				
				
			case R.id.refrsh_product:
				
				
				intent.putExtra("memberID",PreferenceUtil.getString(getActivity(), Constant.USER_ID));
				intent.putExtra("layout", 2);

				intent.setClass(getActivity(), MyProductActivity.class);
				startActivity(intent);
				
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_myproduct");

				break;
				
			case R.id.refresh_location:
				
				
				DialogListUtil.showChangeLocDialog(getActivity(), new OnChangeLocClickListener() {
					 
					@Override
					public void onChangeLocClick() {
						// TODO Auto-generated method stub
						Log.d("change","loc change");
						
						SetNowCity(
								Double.valueOf(PreferenceUtil.getString(
										getActivity(),
										Constant.USER_LAT)), Double
										.valueOf(PreferenceUtil.getString(
												getActivity(),
												Constant.USER_LNG)));
					}
				});
				
			
				
				break;
				
				
				
			
			case R.id.setting_myFansLayout:
			
				intent.setClass(getActivity(), TrackActivity.class);
				intent.setAction(Constant.ACTION_FANS_LIST);
				startActivity(intent);
				
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_my_fans");

				break;

			/*-----  my deal seller -----*/
			case R.id.setting_myDealLayout:
			
				intent.setClass(getActivity(), MyDealActivity.class);
				intent.setAction(Constant.ACTION_TRANSACTION_AS_SELLER);
				startActivity(intent);
				
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_mydeal");

				
				
				break;
				
			/*-----  my deal buyer -----*/
			case R.id.setting_myDealLayoutAsBuyer:
				
				//intent.setClass(getActivity(), MyDealActivity.class);
				intent.setClass(getActivity(), MyCommentListActivity.class);
				intent.setAction(Constant.ACTION_TRANSACTION_AS_BUYER);
				startActivity(intent);
				
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_mydeal");
	
				break;

			/*----- my rating -----*/
			case R.id.setting_myRatingLayout:
				intent.putExtra("memberID",PreferenceUtil.getString(getActivity(), Constant.USER_ID));
				intent.setClass(getActivity(), SomeoneRatingActivity.class);
				startActivityForResult(intent, 0);
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_myrating");

				break;

			/*----- update password -----*/
			case R.id.setting_changePWDLayout:
				DialogListUtil.showChangePWDDialog(getActivity(), new OnChangePWDClickListener() {
 
					@Override
					public void onChangePWDClick(String pwd) {
						
						//HTTP_updatePWD(pwd);
					    Change_PWD(pwd);
					   
					}
				});
			
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_updatepwd");

				
				break;

			/*----- send Eamil feedback -----*/
			case R.id.setting_feedBackLayout:
				sendEmail();
				 ApplicationController.getInstance().sendGoogleAnalytic("android_setting_opinion");

				break;

			/*----- Logout -----*/
			case R.id.setting_logoutLayout:
				
				
			     	Logout();					
		            scrollView1.setVisibility(View.GONE);
			     	lv_login.setVisibility(View.VISIBLE);
		            ApplicationController.getInstance().sendGoogleAnalytic("android_setting_logout");

				break;

			}
		}
	};
	
	

	
	
	private void SetNowCity(final double Lat, final double Lng) {

		Util.showProgressDialog(activity,"" , getString(R.string.refresh_loc));

				//Http.setLocation(Lat, Lng);

				HTTP_updateStroeLoc(Lat,  Lng);

		

	}
	private void settingStoreFlow() {
		

		if (PreferenceUtil.getBoolean(getActivity(), Constant.USER_LOGIN)) {
			if (PreferenceUtil.getString(getActivity(), Constant.USER_CASH_FLOW).equals(
					"")
					|| PreferenceUtil.getString(getActivity(), Constant.USER_STORE_FLOW)
							.equals("")) {
				Intent i = new Intent();
				i.setClass(getActivity(), StoreFlowSettingActivity.class);
				startActivity(i);

			} else if (!PreferenceUtil.getString(getActivity(), Constant.USER_CASH_FLOW)
					.equals("")
					&& !PreferenceUtil
							.getString(getActivity(), Constant.USER_STORE_FLOW).equals(
									"")) {
				DialogListUtil.showChangeStoreFlowDialog(getActivity(), new OnChangeStroeFlowClickListener() {
					 
				

					@Override
					public void onChangeStoreFlowClick() {
						// TODO Auto-generated method stub
						Intent intent = new Intent();
						intent.setClass(getActivity(), StoreFlowSettingActivity.class);
						startActivity(intent);
					}
				});
				
				
				
				
				/*	DialogUtil.pushGeneralDialog(getActivity(), getString(R.string.storeflow), getString(R.string.store_flow_title), 
						getString(R.string.confirm), getString(R.string.cancel), new IGenericDialogUtil.IGenericBtnClickListener() {

							@Override
							public void PositiveMethod(DialogInterface dialog,
									int id) {
								// TODO Auto-generated method stub
								Intent intent = new Intent();
								intent.setClass(getActivity(), StoreFlowSettingActivity.class);
								startActivity(intent);
							}

							@Override
							public void NegativeMethod(DialogInterface dialog,
									int id) {
								// TODO Auto-generated method stub
								
							}});
				*/
				
				
				
			}
		} else {
			DialogUtil.pushPureDialog(getActivity(), "Alert", "please login", "ok");
		}
	}
	private void HTTP_updateStroeLoc(final double Lat, final double Lng) {

		new Thread() {
			@Override
			public void run() {
			
				Http.setLocation(Lat, Lng);

				Log.d("StoreCity","更新前StoreCity :"+Constant.NowCity);				
				
				
				String[] params = { "Lat", "Lng", "StoreCity" };
				Log.d("StoreCity","更新後StoreCity :"+Constant.NowCity);
				String[] data = {
						PreferenceUtil.getString(getActivity(),
								Constant.USER_LAT),
						PreferenceUtil.getString(getActivity(),
								Constant.USER_LNG), Constant.NowCity };

				String result = Http.post(Constant.UPDATE_STORE_LOC, params,
						data, getActivity());

				Bundle b = new Bundle();
				Message m = new Message();
				b.putString("State", "Loc");
				m.setData(b);
				handler_change_loc.sendMessage(m);

			}

		}.start();

	}

	private void  Change_PWD(final String pwd){
		
		
		new Thread() {
			@Override
			public void run() {
				 
				String[] params = { "ID","Pwd" };
				String[] data = { PreferenceUtil.getString(getActivity(), Constant.USER_ID),pwd};
				String result = Http.post(Constant.UPDATE_PWD, params,data, getActivity()); 
				
				
			}

		}.start();
		
		
		
	}
	
	private void Logout(){
		

		new Thread() {
			@Override
			public void run() {
				 
				String[] params = { "OS" };
				String[] data = { "android"};
				String result = Http.post(Constant.LOGOUT_URL, params,data, getActivity()); 
				
			    Bundle b =new Bundle();
			    b.putString("Key_toast", "Logout");
				Message m =new Message();
				m.setData(b);
				
				handler_toast.sendMessage(m);
				
				
			}

		}.start();
		
		
	}
	
	
	Handler handler_change_loc = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Util.dismissProgressDialog();
			Toast.makeText(getActivity(),
			getString(R.string.update_successful),
			Toast.LENGTH_LONG).show();
		}
	};
	
	
	
	
	
	Handler handler_toast = new Handler() {
		@Override
		public void handleMessage(Message msg) {

		if(msg.getData().getString("Key_toast").equals("Logout")){
			Toast.makeText(getActivity(), getString(R.string.alert_logout_success), Toast.LENGTH_SHORT).show();
			
			PreferenceUtil.setBoolean(getActivity(), Constant.USER_LOGIN, false);


			PreferenceUtil.SavePreferences(getActivity(), Constant.COOKIE1, "null");
			PreferenceUtil.SavePreferences(getActivity(), Constant.COOKIE2, "null");
			PreferenceUtil.SavePreferences(getActivity(), Constant.COOKIE3, "null");
			PreferenceUtil.SavePreferences(getActivity(), Constant.COOKIE4, "null");
			PreferenceUtil.SavePreferences(getActivity(), Constant.COOKIE5, "null");
			   ChatUtil.onChatIndexCallback.onGetChatIndex();

			

		}else if(msg.getData().getString("Key_toast").equals("email")){
			Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();

			
		}
	

			
		}
	};
	private void sendEmail() {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "smalltrade.service@gmail.com" });
		// i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
		// i.putExtra(Intent.EXTRA_TEXT, "body of email");
		try {
			startActivity(Intent.createChooser(i, "Send mail"));
		} catch (android.content.ActivityNotFoundException ex) {
			   Bundle b =new Bundle();
			    b.putString("Key_toast", "email");
				Message m =new Message();
				m.setData(b);				
				handler_toast.sendMessage(m);
				
		}
	}
	
	
}
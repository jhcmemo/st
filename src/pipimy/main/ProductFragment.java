package pipimy.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pipimy.chat.ChatUtil;
import pipimy.chat.ChatUtil.onDeleteChatIndexCallback;
import pipimy.chat.ChatUtil.onMainCallback;

import org.json.JSONException;
import pipimy.login.LoginActivity;
import pipimy.nearby.ProductListSorter;
import pipimy.nearby.ProductObject;
import pipimy.object.FavoriteObject;
import pipimy.others.CircleNetwork_ImageView;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.PIPI_Tool;
import pipimy.others.PIPI_Tool.Product_Menu;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.others.DialogUtil.OnSearchClickListener;
import pipimy.others.DialogUtil.OnTypeClickListener;
import pipimy.product.activity.FavorTracing_Activity;
import pipimy.product.activity.PlayVideo_Activity;
import pipimy.product.activity.Product_Detail_Activity;
import pipimy.service.R;
import pipimy.setting.activity.PostActivity;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.capricorn.ArcMenu;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;

public class ProductFragment extends Fragment implements Runnable,onMainCallback,OnScrollListener {

	//private int lastVisibleItem = 0;
	//private boolean isSortBarShow = true;
	private boolean refresh=false;
	private boolean first=true;
	private boolean pull_refresh=false;
	private boolean change_view=false;
	private boolean ani_back=false;

	AnimatorSet animSetXYC;
	boolean animation_click=true;
	int now_x_move;
	int now_y_move;
	
	public static boolean refresh_gridview=true;
	private static final int[] ITEM_DRAWABLES = { R.drawable.new_menu_near,R.drawable.new_menu_price, R.drawable.new_menu_popular,
		R.drawable.new_menu_latest,R.drawable.menu_post,R.drawable.menu_heart,R.drawable.menu_play};
	int now_sort=0;
	String product_title[] = null;
	
	// GridView
	GridView product_gridview;
	PullToRefreshGridView mPullRefreshGridView;
	private ArrayList<FavoriteObject> favor_list ;
	final static String searchByKeyWord = "_searchByKeyWord";
	final static String searchByType = "_searchByType";
	RelativeLayout ProductList_sortTimeLayout;
	RelativeLayout ProductList_sortNearLayout;
	RelativeLayout ProductList_sortHotLayout;
	RelativeLayout ProductList_sortPriceLayout;

	TextView tv_sort_lastTime;
	TextView tv_sort_location;
	TextView tv_sort_hot;
	TextView tv_sort_price;
	TextView tv_nodata;

	private int mx,my;
	int rse_icon=R.drawable.new_acr_show02;
	ImageView iv_menu,iv_price,iv_popular,iv_near,iv_latest,iv_heart,iv_post;

	// added by Henry
	ImageLoader mImageLoader;
	RequestQueue queue;
	ArrayList<ProductObject> productList = new ArrayList<ProductObject>();
    ProgressDialog progressDialog;
    MainActivity activity;   
	CommonAdapter <ProductObject>adapter;
	CommonAdapter <ProductObject>adapter_new;

	String[] types;
	String refresh_time="";
	Handler handler = new Handler();
	
	
	int res=R.layout.product_gridview_item;
	int click =2;
	boolean click_menu=false;
	
    String now_list="default";
    String now_keyword="";
    int now_position=0;
    int vWidth;
    int vHeight;
    int count =0;
	MenuItem menu_item,acbr_search,acbr_actlog,acbr_home;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ChatUtil.setOnMainCallback(this);

		    DisplayMetrics dm = new DisplayMetrics();
	       getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
	        
	         vWidth = dm.widthPixels;
	          vHeight = dm.heightPixels;
		
		refresh_gridview=true;
		activity = ( MainActivity) getActivity();
		setHasOptionsMenu(true);

	}

	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	
		
	//	refresh_time=getActivity().getString(R.string.last_refresh_time)+" "+getActivity().getString(R.string.no_data);
		
		tv_nodata=(TextView) view.findViewById(R.id.tv_nodata);	
		iv_menu=(ImageView) view.findViewById(R.id.iv_menu);	
		iv_price=(ImageView) view.findViewById(R.id.iv_price);	
		iv_popular=(ImageView) view.findViewById(R.id.iv_popular);
		iv_near=(ImageView) view.findViewById(R.id.iv_near);	
		iv_latest=(ImageView) view.findViewById(R.id.iv_latest);
		iv_heart=(ImageView) view.findViewById(R.id.iv_heart);
		iv_post=(ImageView) view.findViewById(R.id.iv_post);
		
		init();

		//iv_menu.setOnTouchListener(menu_touch);
		iv_menu.setOnClickListener(menu_click);
		iv_price.setOnClickListener(menu_click);
		iv_popular.setOnClickListener(menu_click);
		iv_near.setOnClickListener(menu_click);
		iv_latest.setOnClickListener(menu_click);
		iv_heart.setOnClickListener(menu_click);
		iv_post.setOnClickListener(menu_click);

		 
  
		mPullRefreshGridView = (PullToRefreshGridView) view
				.findViewById(R.id.product_gridview);
	
	

		// GridView ?????怏?????????????
		product_gridview = mPullRefreshGridView.getRefreshableView();
		/*if(){
			
		}*/
	//	refresh_time=PreferenceUtil.getString(activity,Constant.REFRESH_TIME);
if(PreferenceUtil.getString(activity,Constant.REFRESH_TIME).equals("")){
		mPullRefreshGridView.setReleaseLabel(refresh_time);
}else{
	refresh_time=" "+PreferenceUtil.getString(activity,Constant.REFRESH_TIME);
	mPullRefreshGridView.setReleaseLabel(refresh_time);
}
		//mPullRefreshGridView.setReleaseLabel(refresh_time);

		//mPullRefreshGridView.setLoadingDrawable(drawable); 
		//mPullRefreshGridView.setRefreshingLabel("123");
		//mPullRefreshGridView.setReleaseLabel("123");
		mPullRefreshGridView.setOnScrollListener(this);  
		mPullRefreshGridView.setOnRefreshListener(new OnRefreshListener2<GridView>() {
			
					@SuppressWarnings("deprecation")
					@Override
					public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
						
						//if(PreferenceUtil.getString(activity,Constant.REFRESH_TIME).equals("")){
					   mPullRefreshGridView.setReleaseLabel(refresh_time);
					//}else{
						refresh_time=PreferenceUtil.getString(activity,Constant.REFRESH_TIME);
						mPullRefreshGridView.setReleaseLabel(refresh_time);
					//}
						
						
					   
						//mPullRefreshGridView.setReleaseLabel(refresh_time);
						//getActivity().getString(R.string.last_refresh_time);
						
						pull_refresh=true;
						if(now_list.equals("default")){		
							//displayProgressDialog("",getString(R.string.loading));	
							new Thread(ProductFragment.this).start();	
							
							
							mPullRefreshGridView.onRefreshComplete();
						}
						else if(now_list.equals("key_word")){
							getProductByKeyword(now_keyword);
							now_list="key_word";
							mPullRefreshGridView.onRefreshComplete();

						}
                        else if(now_list.equals("type")){
							getProductByType(now_position);
							now_list="type";
							mPullRefreshGridView.onRefreshComplete();

						}
						
						
						Date d = new Date();						
						CharSequence s  = DateFormat.format("MM/dd", d.getTime());				
		    			CharSequence s2  = DateFormat.format("HH:mm", d.getTime());									
						PreferenceUtil.setString(activity,Constant.REFRESH_TIME," "+getActivity().getString(R.string.last_refresh_time)+" "+s.toString()+" "+s2.toString());
						//  iv_menu.setBackgroundResource(R.drawable.new_ch_menu_latest);
                       //   MenuFucntion_activity.res=R.drawable.new_ch_menu_latest;	
						
						
					}

					@Override
					public void onPullUpToRefresh(
							PullToRefreshBase<GridView> refreshView) {
						mPullRefreshGridView.onRefreshComplete();

						
						
						
						
					}

				});


	}
	
	@Override
	public void onScroll(AbsListView arg0, int arg1, int visibleItemCount, int arg3) {
		// TODO Auto-generated method stub
		//Log.d("product","visibleItemCount :"+visibleItemCount);	
		
		if(visibleItemCount==5){
			Log.d("product","visibleItemCount 5!!");	

		}
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_product, container, false);

	}

	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Log.e("onActivityCreated","Product Fragment");
	}

	
	@Override
	public void onResume() {
		super.onResume();
		
		Log.d("proc","pro resume!");
		
	 
		    check_menu_state();
			iv_menu.setVisibility(View.VISIBLE);
	     	favor_list = PIPI_Tool.getFavorites(getActivity(),favor_list,true);
			//menu_item.setIcon(rse_icon);
/** maybe problem*/
		if(refresh){
			if(productList.size()!=0){
	     	adapter.notifyDataSetChanged();
	     	mPullRefreshGridView.invalidate();
	     //	Log.e("proc","更新資料集");
	     	
			}
			menu_item.setIcon(rse_icon);
		}
	
		 
	}

	@Override
	public void onPause() {
		super.onPause();
		// Log.e("onPause","Product Fragment");
	}
	@Override
	public void run() {

  Update_Product_data();
		
		
	}
	
	@Override
	public void menu_state_check() {

		check_menu_state();
		Log.d("proc","page !=0");
		
	}

	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
	
		switch(resultCode){
		   
		 case 1:
			     now_sort=3;
			  //  iv_menu.setBackgroundResource(R.drawable.new_ch_menu_near);		
			    ProductListSorter.getSortByDistance(productList);
				setListView();
				Log.d("activity_result","catch 1");
			break;
			
		 case 2:
		     now_sort=2;

			  //  iv_menu.setBackgroundResource(R.drawable.new_ch_menu_price);
			 	ProductListSorter.getSortByPrice_L(productList);
				setListView();
			 
				Log.d("activity_result","catch 2");

				break;
		 case 5:
		     now_sort=1;

		    //	iv_menu.setBackgroundResource(R.drawable.new_ch_menu_popular);
		    	
				ProductListSorter.getSortByHot(productList);
				setListView();
				Log.d("activity_result","catch 5");

				break;
				
		 case 7:
		     now_sort=0;

			  //  iv_menu.setBackgroundResource(R.drawable.new_ch_menu_latest);
			 
			    ProductListSorter.getSortByTime(productList);
				setListView();
				Log.d("activity_result","catch 7");

				break;
		
		 case 9:
		     now_sort=0;

			    ProductListSorter.getSortByTime(productList);
				setListView();
				Log.d("activity_result","catch 9");

				break;
		  }
		
		
	}
	


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		super.onCreateOptionsMenu(menu, inflater);


		switch (activity.currentFragment) {
		case 0:
			activity.getMenuInflater().inflate(R.menu.product, menu);
			 menu_item= menu.findItem(R.id.menu_item_view);
			 acbr_search= menu.findItem(R.id.menu_item_search);
			// acbr_actlog= menu.findItem(R.id.menu_item_actlog);
			 acbr_home= menu.findItem(R.id.menu_item_home);


			activity.getActionBar().setTitle(
					getResources().getString(R.string.product_title));
			menu_item.setIcon(rse_icon);

			break;
		case 1:
			activity.getActionBar()
					.setTitle(getResources().getString(R.string.shopping));
			break;
		case 2:
			if (PreferenceUtil.getBoolean(activity,
					Constant.USER_LOGIN)) {
				activity.getActionBar().setTitle(
						PreferenceUtil.getString(activity,
								Constant.USER_ID));
			} else {
				activity.getActionBar().setTitle(
						getResources().getString(R.string.chat_title));
			}
			break;
		case 3:
			if (PreferenceUtil.getBoolean(activity,
					Constant.USER_LOGIN)) {
				activity.getActionBar().setTitle(
						PreferenceUtil.getString(activity,
								Constant.USER_ID));
			} else {
				activity.getActionBar().setTitle(
						getResources().getString(R.string.setting_title));
			}
			break;
		default:
			break;
		}
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent();
		//MenuItem menu_item_view=findi
		
		switch (item.getItemId()) {
		
		
		case R.id.menu_item_search:
			
			userSearch(searchByKeyWord);
			break;
		
		/*case R.id.menu_item_actlog:
			userSearch(searchByType);
			
			break;*/

			
		case R.id.submenu1:
			displayProgressDialog("",getString(R.string.loading));	

			menu_item.setIcon(R.drawable.new_acr_show01);
			change_view=true;
			rse_icon=R.drawable.new_acr_show01;
			res =R.layout.product_gridview_item_small;
			product_gridview.setNumColumns(1);
			product_gridview.setVerticalSpacing(12);
            setListView();
        	displayProgressDialog("",getString(R.string.loading));	
            dismissProgressDialog();
			break;
      case R.id.submenu2:

    	    menu_item.setIcon(R.drawable.new_acr_show02);
			rse_icon=R.drawable.new_acr_show02;
			change_view=true;

			res =R.layout.product_gridview_item;
			product_gridview.setNumColumns(2);
			product_gridview.setVerticalSpacing(5);
            setListView();
			displayProgressDialog("",getString(R.string.loading));	
            dismissProgressDialog();
    	  
			break;
       case R.id.submenu3:
			displayProgressDialog("",getString(R.string.loading));	

    	    menu_item.setIcon(R.drawable.new_acr_show03);
			rse_icon=R.drawable.new_acr_show03;
			change_view=true;
			res =R.layout.product_gridview_item_large;
			product_gridview.setNumColumns(1);

			product_gridview.setVerticalSpacing(25);
           setListView();
       	displayProgressDialog("",getString(R.string.loading));	
        dismissProgressDialog();
	      break;
			
			
		case R.id.menu_item_home:
			now_list="default";
			acbr_home.setIcon(R.drawable.new_actionbar_home);
			acbr_search.setIcon(R.drawable.new_actionbar_search00);
			//acbr_actlog.setIcon(R.drawable.new_actionbar_catalog00);
			
			displayProgressDialog("",getString(R.string.loading));	
		     now_sort=0;

			new Thread(ProductFragment.this).start();

            iv_menu.setImageResource(R.drawable.ch_menu_latest);
			// Log.e("=======","play");
			break;
		default:
			break;
		}
		return true;
	}
	



	private void init() {
		// TODO Auto-generated method stub
		
   //     initArcMenu(iv_menu, ITEM_DRAWABLES); 

		displayProgressDialog("",getString(R.string.loading));	

		activity = (MainActivity) getActivity();
		productList=ApplicationController.nearProductList;
		queue = VolleySingleton.getInstance().getRequestQueue();
	
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		types = getResources().getStringArray(R.array.types);
		new Thread(this).start();
	}
	
	
	private void check_menu_state(){
		
		if( animation_click==false){
			
			iv_price.setVisibility(View.INVISIBLE);
			iv_popular.setVisibility(View.INVISIBLE);
			iv_near.setVisibility(View.INVISIBLE);
			iv_latest.setVisibility(View.INVISIBLE);
			iv_heart.setVisibility(View.INVISIBLE);
			iv_post.setVisibility(View.INVISIBLE);
			
			iv_price.setX(iv_menu.getX()+3);
			iv_price.setY(iv_menu.getY()-2);
			
			iv_popular.setX(iv_menu.getX()+3);
			iv_popular.setY(iv_menu.getY()-2);
			
			iv_near.setX(iv_menu.getX()+3);
			iv_near.setY(iv_menu.getY()-2);
			
			iv_latest.setX(iv_menu.getX()+3);
			iv_latest.setY(iv_menu.getY()-2);
			
			iv_heart.setX(iv_menu.getX()+(vWidth/39));
			iv_heart.setY(iv_menu.getY()+(vWidth/34));
			
			iv_post.setX(iv_menu.getX()+(vWidth/39));
			iv_post.setY(iv_menu.getY()+(vWidth/34));
			animation_click=true;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void setListView() {
		
	 if(first){
		 adapter= new CommonAdapter(getActivity(),
				 productList, res) {
				@Override 
				public void setViewData(CommonViewHolder commonViewHolder,
						View currentView, Object item) {
					// TODO Auto-generated method stub
				//	Log.d("ada", "???????????);
					final ProductObject productObj = (ProductObject) item;
					
					/*----- favor -----*/
					boolean isLove = false;
							for (int i = 0; i < favor_list.size(); i++) {
								if (favor_list.get(i).getProductID().equals(productObj.getProductID())) {
									isLove = true;
									break;
								}
							}
					
							
							
							
							Button favorButton =(Button) commonViewHolder.get(commonViewHolder, currentView, R.id.btn_favor);	
							
							//if(PreferenceUtil.getString(getActivity(),Constant.USER_ID).equals(productObj.getID())){
							//	favorButton.setVisibility(View.GONE);
							//}
							
							//else{
							
							   if (isLove) {
								favorButton.setBackgroundResource(R.drawable.cell_product_love);
								favorButton.setTag(R.id.tag_boolean,"true");
							   } else {
								favorButton.setBackgroundResource(R.drawable.cell_product_unlove);
								favorButton.setTag(R.id.tag_boolean,"false");
							   } 
							    favorButton.setTag(R.id.tag_object,productObj);							
							    favorButton.setOnClickListener(productObjClick);
						//	}
							
							
							

					NetworkImageView iv_product_pic = (NetworkImageView) commonViewHolder
							.get(commonViewHolder, currentView, R.id.iv_product_pic);
					iv_product_pic.setScaleType(ImageView.ScaleType.CENTER_CROP);
					
					iv_product_pic.setTag(productObj);
					iv_product_pic.setOnClickListener(productObjClick);
					
					CircleNetwork_ImageView iv_member_icon = (CircleNetwork_ImageView) commonViewHolder
							.get(commonViewHolder, currentView, R.id.iv_member_icon);

					ImageView btn_play_video = (ImageView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.btn_play_video);

					// click_play_video
					btn_play_video.setTag(productObj);
					btn_play_video.setOnClickListener(productObjClick);

					
					TextView tv_brand= (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_brand);
					
					
					TextView tv_prdouct_title = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_prdouct_title);
					TextView tv_price = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_price);
				
					
					//TextView tv_prdouct_type = (TextView) commonViewHolder.get(
						//	commonViewHolder, currentView, R.id.tv_prdouct_type);
					
					
					TextView tv_member_name = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_member_name);
					TextView tv_date = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_date);
					TextView tv_location = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_location);

					//queue.getCache().remove(Constant.STORE_IMAGE_URL+productObj.getID()+".jpg");
				
					iv_member_icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
					iv_member_icon.setDefaultImageResId(R.drawable.nohead);
					iv_member_icon.setImageUrl(Constant.STORE_IMAGE_URL+productObj.getID()+".jpg",
							mImageLoader);
					
			
					iv_product_pic.setImageUrl(productObj.getProductPicUrl(),
							mImageLoader);
					tv_brand.setText(productObj.getBrand());
					tv_prdouct_title.setText(productObj.getTitle());
					
				//Log.d("product","time :"+productObj.getTime());
					tv_date.setText(productObj.getTime().substring(5, 10).replace("-","/"));
					//tv_prdouct_type.setText(types[Integer.parseInt(productObj
					//		.getType())]);
					tv_price.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Century_Gothic.ttf"));
					
					tv_price.setText(productObj.getPrice());
					iv_member_icon.setTag(productObj);
					iv_member_icon.setOnClickListener(productObjClick);
					/*----- set ID -----*/
					if (productObj.getID().equals("null")
							&& !productObj.getContact().equals("null")) {
						tv_member_name.setText(productObj.getContact());
					
					} else {
						tv_member_name.setText(productObj.getID());
						
					}

					/*----- set distance -----*/
					if (productObj.getLat() == 0 || productObj.getLng() == 0) {
						tv_location.setText("--km");
					} else {
						float d = productObj.getDistance() / 1000;
						if (d >= 1000) {
							tv_location.setText(">"
									+ (int) (productObj.getDistance() / 1000 / 1000)
									+ "K km");
						} else if (d > 100) {
							tv_location.setText(String.format("%.0f",
									(productObj.getDistance() / 1000))
									+ "km");
						} else if (d > 10) {
							tv_location.setText(String.format("%.1f",
									(productObj.getDistance() / 1000))
									+ "km");
						} else {
							tv_location.setText(String.format("%.2f",
									(productObj.getDistance() / 1000))
									+ "km");
						}

					}

				}
			};
			   first=false;
	           product_gridview.setAdapter(adapter);
	 }	
	  else{
			if(pull_refresh){
		           adapter.notifyDataSetChanged();	         
		           product_gridview.invalidate();
		           product_gridview.setSelection(0);
		           pull_refresh=false;
			}
			else{ 
				
				if(change_view){
					
					  change_gridview();
					  product_gridview.setAdapter(adapter);
					  change_view=false;
					
					Log.d("change","change_view !");
				 }
				else{
					Log.d("change","no change_view !");
					
					 adapter.notifyDataSetChanged();
	
				}
			

			}
			

			
	 }	
			

			
	}
	
	@SuppressWarnings("unchecked")
	private void change_gridview(){
		 
	
		adapter_new= new CommonAdapter(getActivity(),
				 productList, res) {
				@Override 
				public void setViewData(CommonViewHolder commonViewHolder,
						View currentView, Object item) {
					// TODO Auto-generated method stub
				//	Log.d("ada", "???????????);
					final ProductObject productObj = (ProductObject) item;
					
					/*----- favor -----*/
					boolean isLove = false;
							for (int i = 0; i < favor_list.size(); i++) {
								if (favor_list.get(i).getProductID().equals(productObj.getProductID())) {
									isLove = true;
									break;
								}
							}
					
							Button favorButton =(Button ) commonViewHolder.get(commonViewHolder, currentView, R.id.btn_favor);	
							if (isLove) {
								favorButton.setBackgroundResource(R.drawable.cell_product_love);
								favorButton.setTag(R.id.tag_boolean,"true");
							} else {
								favorButton.setBackgroundResource(R.drawable.cell_product_unlove);
								favorButton.setTag(R.id.tag_boolean,"false");
							} 
							favorButton.setTag(R.id.tag_object,productObj);
							
							  
							
							favorButton.setOnClickListener(productObjClick);

					NetworkImageView iv_product_pic = (NetworkImageView) commonViewHolder
							.get(commonViewHolder, currentView, R.id.iv_product_pic);
					iv_product_pic.setScaleType(ImageView.ScaleType.CENTER_CROP);
					
					iv_product_pic.setTag(productObj);
					iv_product_pic.setOnClickListener(productObjClick);
					
					CircleNetwork_ImageView iv_member_icon = (CircleNetwork_ImageView) commonViewHolder
							.get(commonViewHolder, currentView, R.id.iv_member_icon);

					ImageView btn_play_video = (ImageView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.btn_play_video);

					// click_play_video
					btn_play_video.setTag(productObj);
					btn_play_video.setOnClickListener(productObjClick);

					

					TextView tv_brand= (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_brand);
					TextView tv_prdouct_title = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_prdouct_title);
					TextView tv_price = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_price);
				
					
					//TextView tv_prdouct_type = (TextView) commonViewHolder.get(
						//	commonViewHolder, currentView, R.id.tv_prdouct_type);
					
					
					TextView tv_member_name = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_member_name);
					TextView tv_date = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_date);
					TextView tv_location = (TextView) commonViewHolder.get(
							commonViewHolder, currentView, R.id.tv_location);

					// /////////////////////////////////////
					//iv_product_pic.setBackgroundResource(R.drawable.nohead);
					iv_member_icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
					iv_member_icon.setDefaultImageResId(R.drawable.nohead);
					
					iv_member_icon.setImageUrl(Constant.STORE_IMAGE_URL+productObj.getID()+".jpg",
							mImageLoader);
					
					
					iv_product_pic.setImageUrl(productObj.getProductPicUrl(),
							mImageLoader);
					 tv_brand.setText(productObj.getBrand());
					tv_prdouct_title.setText(productObj.getTitle());
					
				//Log.d("product","time :"+productObj.getTime());
					tv_date.setText(productObj.getTime().substring(5, 10).replace("-","/"));
					//tv_prdouct_type.setText(types[Integer.parseInt(productObj
					//		.getType())]);
					tv_price.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Century_Gothic.ttf"));

					tv_price.setText( productObj.getPrice());
					iv_member_icon.setTag(productObj);
					iv_member_icon.setOnClickListener(productObjClick);
					/*----- set ID -----*/
					if (productObj.getID().equals("null")
							&& !productObj.getContact().equals("null")) {
						tv_member_name.setText(productObj.getContact());
					
					} else {
						tv_member_name.setText(productObj.getID());
						
					}

					/*----- set distance -----*/
					if (productObj.getLat() == 0 || productObj.getLng() == 0) {
						tv_location.setText("--km");
					} else {
						float d = productObj.getDistance() / 1000;
						if (d >= 1000) {
							tv_location.setText(">"
									+ (int) (productObj.getDistance() / 1000 / 1000)
									+ "K km");
						} else if (d > 100) {
							tv_location.setText(String.format("%.0f",
									(productObj.getDistance() / 1000))
									+ "km");
						} else if (d > 10) {
							tv_location.setText(String.format("%.1f",
									(productObj.getDistance() / 1000))
									+ "km");
						} else {
							tv_location.setText(String.format("%.2f",
									(productObj.getDistance() / 1000))
									+ "km");
						}

					}

				}
			};
			adapter=adapter_new;
	}
	
	

	
	
	private OnClickListener menu_click=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
		switch(v.getId()){
		case R.id.iv_menu: 
			iv_menu.setEnabled(false);

		
			if(animation_click){	
				
				animation_click=false;
				ani_back=false;
				iv_price.setVisibility(View.VISIBLE);
				iv_popular.setVisibility(View.VISIBLE);
				iv_near.setVisibility(View.VISIBLE);
				iv_latest.setVisibility(View.VISIBLE);
				iv_heart.setVisibility(View.VISIBLE);
				iv_post.setVisibility(View.VISIBLE);
				
				go_animation(true,false,iv_price,  iv_price.getX(),   iv_price.getY()-(vHeight/3)-20,0,360,150,150,300);
				go_animation(true,false,iv_popular,iv_popular.getX(), iv_popular.getY()-(vHeight/4)-10,0,360,150,150,300);
				go_animation(true,false,iv_near,   iv_near.getX(),    iv_near.getY()-(vHeight/6)-5,0,360,150,150,300);
				go_animation(true,false,iv_latest, iv_latest.getX(),  iv_latest.getY()-(vHeight/11)+10,0,360,150,150,300);
				
				go_animation(true,true,iv_heart,  iv_heart.getX()-(vWidth/5)+10,iv_heart.getY(),0,360,150,150,300);
				go_animation(true,true,iv_post,   iv_post.getX()- (vWidth/3),iv_post.getY(),0,360,150,150,300);			
		      
				
				
			}
			else{
				animation_click=true;
				ani_back=true;
	//View view,float view_x,float view_y,float x ,float y, float rotation1,float rotation2,
	//int x_duration,int y_duration,int rotation_duration			
				go_animation(false,false,iv_price,      iv_price.getX(),iv_price.getY()+(vHeight/3)+20,720,0,300,300,300);
				go_animation(false,false,iv_popular,    iv_popular.getX(),iv_popular.getY()+(vHeight/4)+10,720,0,300,300,300);
				go_animation(false,false,iv_near,       iv_near.getX(),iv_near.getY()+(vHeight/6)+5,720,0,300,300,300);
				go_animation(false,false,iv_latest,     iv_latest.getX(),iv_latest.getY()+(vHeight/11)-10,720,0,300,300,300);
			  
				go_animation(false,false,iv_heart,      iv_heart.getX()+(vWidth/5)-10,iv_heart.getY(),360,0,200,200,300);
				go_animation(false,false,iv_post,       iv_post.getX()+ (vWidth/3),   iv_post.getY(),360,0,200,200,300);
				
				
				

			}
			break;
			
			
		case R.id.iv_latest: 
			iv_menu.setEnabled(false);
		//	iv_menu.setImageResource(R.drawable.new_menu_latest);
		           
			        now_sort=0;
				  
					Log.d("activity_result","catch 7");
					

			animation_click=true;
			go_item_animation(iv_latest,true);
			go_item_animation(iv_near,false);
			go_item_animation(iv_popular,false);
			go_item_animation(iv_price,false);
			
			go_item_animation(iv_heart,false);
			go_item_animation(iv_post,false);
			
			  ProductListSorter.getSortByTime(productList);
				setListView();
			
			 change_menu_icon(R.id.iv_latest );


			break;
			
		case R.id.iv_near :
			iv_menu.setEnabled(false);
			
			        now_sort=3;
				  //  iv_menu.setBackgroundResource(R.drawable.new_ch_menu_near);		
				  
					Log.d("activity_result","catch 1");

			animation_click=true;
			go_item_animation(iv_latest,false);
			go_item_animation(iv_near,true);
			go_item_animation(iv_popular,false);
			go_item_animation(iv_price,false);
			
			go_item_animation(iv_heart,false);
			go_item_animation(iv_post,false);

			  ProductListSorter.getSortByDistance(productList);
				setListView();
			 change_menu_icon(R.id.iv_near );
			
			break;
			
       case R.id.iv_popular :
			iv_menu.setEnabled(false);
			
			        now_sort=1;			    	
				
					Log.d("activity_result","catch 5");
			
			

    	   animation_click=true;
			go_item_animation(iv_latest,false);
			go_item_animation(iv_near,false);
			go_item_animation(iv_popular,true);
			go_item_animation(iv_price,false);
			
			go_item_animation(iv_heart,false);
			go_item_animation(iv_post,false);
			ProductListSorter.getSortByHot(productList);
			setListView();
			 change_menu_icon(R.id.iv_popular );

			break;

		
       case R.id.iv_price :
			iv_menu.setEnabled(false);

			    now_sort=2;
			 	
			 
				Log.d("activity_result","catch 2");
			
			
    	    animation_click=true;
			go_item_animation(iv_latest,false);
			go_item_animation(iv_near,false);
			go_item_animation(iv_popular,false);
			go_item_animation(iv_price,true);
			
			go_item_animation(iv_heart,false);
			go_item_animation(iv_post,false);
			ProductListSorter.getSortByPrice_L(productList);
			setListView();
			 change_menu_icon(R.id.iv_price );

			break;
       
       case R.id.iv_heart :
			iv_menu.setEnabled(false);
			
    	   animation_click=true;
			go_item_animation(iv_latest,false);
			go_item_animation(iv_near,false);
			go_item_animation(iv_popular,false);
			go_item_animation(iv_price,false);
			
			go_item_animation(iv_heart,true);
			go_item_animation(iv_post,false);
			
			if(PreferenceUtil.getBoolean(getActivity(),Constant.USER_LOGIN)){
				Intent it =new Intent();
				it.setClass(getActivity(), FavorTracing_Activity.class);
    			startActivity(it);
    		}
    		else{
				Intent it =new Intent();
    			it.setClass(getActivity(), LoginActivity.class);
    			startActivity(it);
    		}
			
			break;
			
			
       case R.id.iv_post :
			iv_menu.setEnabled(false);

    	    animation_click=true;
			go_item_animation(iv_latest,false);
			go_item_animation(iv_near,false);
			go_item_animation(iv_popular,false);
			go_item_animation(iv_price,false);
			
			go_item_animation(iv_heart,false);
			go_item_animation(iv_post,true);
			
			if(PreferenceUtil.getBoolean(getActivity(),Constant.USER_LOGIN)){
				Intent it =new Intent();

				it.setClass(getActivity(), PostActivity.class);
    			startActivity(it);
    		}
    		else{
				Intent it =new Intent();

				it.setClass(getActivity(), LoginActivity.class);
    			startActivity(it);
    		}
			
			break;
		
		}
			
			
			
			
			
		/*	if(animation_click){
				now_x_move=-300;
				now_y_move=-500;
				go_animation(0,200,-300,-500);
			    animation_click=false;

			}
			else{
				now_x_move=300;
				now_y_move=500;
				
				go_animation(0,-800,300,500);
			    animation_click=true;

			} */
  
			   // iv_menu.layout(iv_menu.getLeft(),iv_menu.getTop()-500 ,iv_menu.getRight(), iv_menu.getTop()-500+iv_menu.getHeight());

			 
		//	if( click_menu ){
  				/*iv_menu.setVisibility(View.GONE);
				Intent it=new Intent(getActivity(),MenuFucntion_activity.class);
				startActivityForResult(it, 0);
				refresh_gridview=false;*/
			//	}else{
				 	
					
			//	}
		}
	};
	

	
	
	private void go_animation(boolean updown,boolean row,final View view,float x ,float y, float rotation1,float rotation2,int x_duration,int y_duration,int rotation_duration){
	
		view.clearAnimation();
		
		animSetXYC = new AnimatorSet();	

		ObjectAnimator animX = ObjectAnimator.ofFloat(view, "x",x );
		ObjectAnimator animY = ObjectAnimator.ofFloat(view, "y", y);
		ObjectAnimator animC=	ObjectAnimator.ofFloat(view, "rotation", rotation1, rotation2);
	
		animX.setDuration(x_duration);
		animY.setDuration(y_duration);
		animC.setDuration(rotation_duration);
		animSetXYC.play(animX).with(animY).with(animC);//
		
		
		if(updown){
			
			if(row){
				ObjectAnimator anim_up=	ObjectAnimator.ofFloat(view, "x", x-10);
				ObjectAnimator anim_down=	ObjectAnimator.ofFloat(view, "x",+x);		
				anim_up.setDuration(150);
				anim_down.setDuration(150);		
				animSetXYC.play(anim_up).after(animX).before(anim_down);
			}
			else{
				ObjectAnimator anim_up=	ObjectAnimator.ofFloat(view, "y", y-10);
				ObjectAnimator anim_down=	ObjectAnimator.ofFloat(view, "y", y);		
				anim_up.setDuration(150);
				anim_down.setDuration(150);		
				animSetXYC.play(anim_up).after(animX).before(anim_down);
				
			}
		
	  }	
		
		
		
		animSetXYC.start();
		animSetXYC.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				// TODO Auto-generated method stub
				iv_menu.setEnabled(true);
				count++;
				Log.d("ani","count :"+count);
				if(ani_back==true &&count==6){
					

					iv_price.setVisibility(View.INVISIBLE);
					iv_popular.setVisibility(View.INVISIBLE);
					iv_near.setVisibility(View.INVISIBLE);
					iv_latest.setVisibility(View.INVISIBLE);
					iv_heart.setVisibility(View.INVISIBLE);
					iv_post.setVisibility(View.INVISIBLE);
				}
				if(count==6){
					count=0;
				}

			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	private void go_item_animation(final View view,boolean scale){
	
		view.clearAnimation();
		animSetXYC = new AnimatorSet();		
		if(scale){
		ObjectAnimator scalex =ObjectAnimator.ofFloat(view, "scaleX", 1, 1.3f);
		ObjectAnimator scaley =ObjectAnimator.ofFloat(view, "scaleY", 1, 1.3f);
		
		ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha",1,0 );
		
		alpha.setDuration(750);
		
		 animSetXYC.playTogether( scalex, scaley,alpha);
		
		}else{
	
			ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha",1,0 );
			alpha.setDuration(700);	
			
			animSetXYC.play(alpha);

		}
		
		animSetXYC.start();
		animSetXYC.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			 
			@Override
			public void onAnimationEnd(Animator arg0) {
				// TODO Auto-generated method stub
				iv_menu.setEnabled(true);
				count++;
				Log.d("ani","apha count :"+count);
				
				if(view.getId()==R.id.iv_post ||view.getId()==R.id.iv_heart){
					view.setX(iv_menu.getX()+(vWidth/39));

					view.setY(iv_menu.getY()+(vWidth/34));
 
				}else{
					view.setX(iv_menu.getX()+3);

					view.setY(iv_menu.getY()-2);
 
				}
				
				view.setScaleX(1);
				view.setScaleY(1);
				view.setAlpha(100);
				//view.setVisibility(View.INVISIBLE);
				if(count==6){
					iv_price.setVisibility(View.INVISIBLE);
					iv_popular.setVisibility(View.INVISIBLE);
					iv_near.setVisibility(View.INVISIBLE);
					iv_latest.setVisibility(View.INVISIBLE);
					iv_heart.setVisibility(View.INVISIBLE);
					iv_post.setVisibility(View.INVISIBLE);
					
					//handler.postDelayed(ani_delay, 100);

				
					count=0;
				}
			
				
				
			}
			
			@Override
			public void onAnimationCancel(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		
	}
	private Runnable ani_delay = new Runnable() {

		@SuppressWarnings("deprecation")
		@Override
		public void run() {
			
			iv_price.setVisibility(View.INVISIBLE);
			iv_popular.setVisibility(View.INVISIBLE);
			iv_near.setVisibility(View.INVISIBLE);
			iv_latest.setVisibility(View.INVISIBLE);
			iv_heart.setVisibility(View.INVISIBLE);
			iv_post.setVisibility(View.INVISIBLE);
		
		}
	};
	
	private void change_menu_icon(int id){
		
		switch(id){
		
		case R.id.iv_latest:
			
			
			iv_menu.setImageResource(R.drawable.ch_menu_latest);
			break;
			
        case R.id.iv_near:
			
        	
			iv_menu.setImageResource(R.drawable.ch_menu_near);

			break;
			
        case R.id.iv_popular:
			
			iv_menu.setImageResource(R.drawable.ch_menu_popular);

        	
			break;
			
        case R.id.iv_price:
			
        	
			iv_menu.setImageResource(R.drawable.ch_menu_price);

        	
			break;
		
		}
		
	}
	
	

	private OnClickListener productObjClick = new OnClickListener() {
		ProductObject productObj;

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent it =new Intent();
			switch (v.getId()) {
			case R.id.iv_member_icon: {
				productObj = (ProductObject) v.getTag();
				if (productObj.getID().equals("null")) {
					Log.e("productObjClick", "non-member");
				} else {
					Log.e("productObjClick", "pipimy memeber, id = "
							+ productObj.getID());
					it = new Intent(getActivity(),
							MyProductActivity.class);
					it.putExtra("memberID", productObj.getID());
					it.putExtra("layout", 3);
					//it.putExtra("product_object", productObj);
					//it.pu
					
					startActivity(it);
				}
				break;
			   }
			
			case R.id.btn_play_video:
				
				productObj = (ProductObject) v.getTag();

				it = new Intent(getActivity(),PlayVideo_Activity.class);
				
				it.putExtra("position",Integer.valueOf(productObj.getProductID()));				
				it.putExtra("productId",productObj.getProductID());
				it.putExtra("picNumber", productObj.getPicNumber());
				it.putExtra("ID", productObj.getID());
				it.putExtra("title", productObj.getTitle());
				it.putExtra("type", productObj.getType());
				it.putExtra("time", productObj.getTime());
				it.putExtra("des", productObj.getDes());
				it.putExtra("price", productObj.getPrice());
				it.putExtra("Lng", productObj.getLng());
				it.putExtra("Lat", productObj.getLat());
				it.putExtra("dis", productObj.getDistance());
				it.putExtra("Delivery", productObj.getDelivery());
				
				  if( productObj.getF1()!=null)
					it.putExtra("F1_name", productObj.getF1().getName());
				  if( productObj.getF2()!=null)
						it.putExtra("F3_name", productObj.getF1().getName());
				  if( productObj.getF3()!=null)
						it.putExtra("F2_name", productObj.getF1().getName());
				
				
				it.setAction(Constant.ACTION_SINGLE_PLAY);
				startActivity(it);
				
				
				break;
				
			case R.id.iv_product_pic :
				productObj = (ProductObject) v.getTag();
				
			    it =new Intent(getActivity(),Product_Detail_Activity.class);
			    it.putExtra("product_object", productObj);
				it.putExtra("edit", false);
			    startActivityForResult(it, 3);
				//startActivity(it);

				
				break;
				
			case R.id.btn_favor:
				productObj = (ProductObject) v.getTag(R.id.tag_object);

				//Log.d("product","productObj.getTitle():"+productObj.getTitle());
				//Log.d("product","v.getTag().toString()):"+v.getTag(R.id.tag_boolean).toString());
                
				add_favor_object(productObj);
						
				
				int lovePosition = 0;
				if (v.getTag(R.id.tag_boolean).toString().equals("true")) {
					v.setTag(R.id.tag_boolean,"false");

					for (int i = 0; i < favor_list.size(); i++) {
						if (favor_list.get(i).getProductID().equals(productObj.getProductID())) {
							lovePosition = i;
							break;
						}
					}
					v.setBackgroundResource(R.drawable.cell_product_unlove);
					
					Log.d("product","lovePosition :"+lovePosition);
					Log.d("product","	favor_list size :"+	favor_list.size());

				
					PIPI_Tool.removeFavorite(getActivity(), lovePosition, favor_list, true);
					//favor_list.remove(lovePosition);

				 
				}
				else {
					v.setTag(R.id.tag_boolean,"true");
					v.setBackgroundResource(R.drawable.cell_product_love);
				}
				
				
				break;
			
			}
		}
	};
	
	private void add_favor_object(ProductObject ob){
		
		FavoriteObject favoriteObject = new FavoriteObject();
		    favoriteObject.setProductID(ob.getProductID());
		    favoriteObject.setProductTitle(ob.getTitle());
		    favoriteObject.setTime(ob.getTime());
	     	favoriteObject.setContact(ob.getContact());
			favoriteObject.setMemberID(ob.getID());
			favoriteObject.setPicNumber(ob.getPicNumber());
			favoriteObject.setPayment(ob.getPayment());
			favoriteObject.setDelievery(ob.getDelivery());
			favoriteObject.setDes(ob.getDes());

			favoriteObject.setType(ob.getType());
			favoriteObject.setPrice(ob.getPrice());
			favoriteObject.setDistance(ob.getDistance());
			favoriteObject.setStock(ob.getStock());
			
			favoriteObject.setLat(ob.getLat());
			favoriteObject.setLng(ob.getLng());
			favoriteObject.setBrand(ob.getBrand());
			favoriteObject.setNewOld((ob.getNewOld()));
			favoriteObject.setSNO((ob.getSNO()));

			
		PIPI_Tool.addFavorite(getActivity(), favoriteObject, favor_list, true);
		if (PreferenceUtil.getBoolean(getActivity(), Constant.USER_LOGIN)){
			
			HTTP_favoriteProduct(ob.getProductID());
		
	    }
	}
private void HTTP_favoriteProduct(final String productID){
	
	 new Thread() {			
			@Override
			public void run() {

		String[] params = { "ProductID","AddTime"};
				String[] data = {productID, Util.getTime() };
				
				String result = Http.post(Constant.FAVORITE_PRODUCT, params,
						data, getActivity()); 
		Log.d("product","product add fav result:"+result);
			}

		}.start();
		
}
	private void Update_Product_data(){		
		String[] params = { "Lat", "Lng", "CC" };
		String[] data = {
				PreferenceUtil.getString(getActivity(),
						Constant.USER_LAT),
				PreferenceUtil.getString(getActivity(),
						Constant.USER_LNG), "TW" };
		String result = Http.post(Constant.GET_NEAR_PRODUCT, params,
				data, getActivity());
		
		Log.d("product","result :"+result);
		try {
			JSONParserTool.getNearProductList(result,
					ApplicationController.nearProductList, false);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		handler_setview.sendMessage(handler_setview.obtainMessage());

	} 
	
	
	
	private void userSearch(String str) {

		// TODO Auto-generated method stub
		if (str.equals(searchByKeyWord)) {

			DialogUtil.keywordSearchDialog(getActivity(), new OnSearchClickListener() {
				@Override
				public void onSearchClick(int searchBy,String keyword) {
					
					switch(searchBy){
					
					case 0:
						
						
						break;
					
					case 1:
						
						if (!keyword.equals("")) {
							getProductByKeyword(keyword);
						    now_keyword=keyword;
						    
						    acbr_search.setIcon(R.drawable.new_actionbar_search);
					//		acbr_actlog.setIcon(R.drawable.new_actionbar_catalog00);
							acbr_home.setIcon(R.drawable.new_actionbar_home00);
						}
					
						break;
					case 2:
						if (!keyword.equals("")) {
							
							getProductByType(keyword,"1");
						    now_keyword=keyword;
						    
						    acbr_search.setIcon(R.drawable.new_actionbar_search);
						//	acbr_actlog.setIcon(R.drawable.new_actionbar_catalog00);
							acbr_home.setIcon(R.drawable.new_actionbar_home00);
						}
					
						break;
					case 3:
						
						
						if (!keyword.equals("")) {
							getProductByType(keyword,"2");
						    now_keyword=keyword;
						    
						    acbr_search.setIcon(R.drawable.new_actionbar_search);
						//	acbr_actlog.setIcon(R.drawable.new_actionbar_catalog00);
							acbr_home.setIcon(R.drawable.new_actionbar_home00);
						}
					
						break;
					case 4:
						
						if (!keyword.equals("")) {
							getProductByType(keyword,"3");
						    now_keyword=keyword;
						    
						    acbr_search.setIcon(R.drawable.new_actionbar_search);
						//	acbr_actlog.setIcon(R.drawable.new_actionbar_catalog00);
							acbr_home.setIcon(R.drawable.new_actionbar_home00);
						}
					
						break;
					case 5:
						if (!keyword.equals("")) {
							getProductByType(keyword,"4");
						    now_keyword=keyword;
						    
						    acbr_search.setIcon(R.drawable.new_actionbar_search);
						//	acbr_actlog.setIcon(R.drawable.new_actionbar_catalog00);
							acbr_home.setIcon(R.drawable.new_actionbar_home00);
						}
					
						break;
					}

					// TODO Auto-generated method stub
					Log.e("keywordSearchDialog", "user input kerword = "
							+ keyword);
					
				}
			});
		} else if (str.equals(searchByType)) {

			DialogUtil.typeSearchDialog(getActivity(), true, new OnTypeClickListener() {
				@Override
				public void onTypeClick(int position) {
					// TODO Auto-generated method stub
					//acbr_actlog.setIcon(R.drawable.new_actionbar_catalog);
					acbr_search.setIcon(R.drawable.new_actionbar_search00);
					acbr_home.setIcon(R.drawable.new_actionbar_home00);
					Log.e("typeSearchDialog", "type number = " + position);
					if (position >= 0 && position <= 10) {
						getProductByType(position);
					     now_position=position;

					}
				}
			});
		}
	}
	
	
	// get product list by keyword
	protected void getProductByKeyword(final String keyword) {
        now_list="key_word";

		// TODO Auto-generated method stub
		Util.showProgressDialog(getActivity(), "", getString(R.string.loading));
		new Thread() {
			@Override
			public void run() {
				String[] params = { "CC", "Keyword" };
				String[] data = { "TW", keyword };
				String result = Http.post(Constant.GET_SEARCH_PRODUCT, params,
						data, getActivity());
				try {
					JSONParserTool.getNearProductList(result,
							ApplicationController.nearProductList, false);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler_setview.sendMessage(handler_setview.obtainMessage());	

			
			}

		}.start();
	}
	
	protected void getProductByType(final String keyword,final String type) {
        now_list="key_word";

		// TODO Auto-generated method stub
		Util.showProgressDialog(getActivity(), "", getString(R.string.loading));
		new Thread() {
			@Override
			public void run() {
				String[] params = { "CC", "Keyword","SearchType" };
				String[] data = { "TW", keyword ,type};
				String result = Http.post(Constant.GET_SPECIAL_SEARCH_PRODUCT, params,
						data, getActivity());
				Log.d("proc","special result: "+result);
				try {
					JSONParserTool.getNearProductList(result,
							ApplicationController.nearProductList, false);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler_setview.sendMessage(handler_setview.obtainMessage());	

			
			}

		}.start();
	}
	

	// get product list by type
	protected void getProductByType(final int position) {
		now_list="type";
		Util.showProgressDialog(getActivity(), "", getString(R.string.loading));
		new Thread() {
			@Override
			public void run() {
				String type;
				if (position == 0)
					type = "all";
				else
					type = Integer.toString(position);

				String[] params = { "CC", "Type" };
				String[] data = { "TW", type };
				String result = Http.post(Constant.GET_TYPE_PRODUCT, params,
						data, getActivity());
				try {
					JSONParserTool.getNearProductList(result,
							ApplicationController.nearProductList, false);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler_setview.sendMessage(handler_setview.obtainMessage());	
			
			}

		}.start();
	}
	
	
	
	
	
	
	
	Handler handler_setview = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			//productList.clear();
			  productList = ApplicationController.nearProductList;
			  
			  if(productList.size()==0){
				 // iv_menu.setVisibility(View.GONE);
				  product_gridview.setVisibility(View.GONE);
				  tv_nodata.setVisibility(View.VISIBLE);
			  }
			  else{
				 // iv_menu.setVisibility(View.VISIBLE);
				  tv_nodata.setVisibility(View.GONE);
				  product_gridview.setVisibility(View.VISIBLE);
				//  iv_menu.setBackgroundResource(R.drawable.new_ch_menu_latest);
					
	              switch(now_sort){
	            	  
	              case 0:
	  			    ProductListSorter.getSortByTime(productList);
				 //   iv_menu.setBackgroundResource(R.drawable.new_ch_menu_latest);		

	            	  break;
	            	  
	              case 1:
	            	  
	  				ProductListSorter.getSortByHot(productList);
				  //  iv_menu.setBackgroundResource(R.drawable.new_ch_menu_popular);		

	              break;
	              
	              case 2:
	            	  
	  			 	ProductListSorter.getSortByPrice_L(productList);
				//    iv_menu.setBackgroundResource(R.drawable.new_ch_menu_price);		

	            	  break;
	            	  
	              case 3:
	   	       	   ProductListSorter.getSortByDistance(productList);
				   // iv_menu.setBackgroundResource(R.drawable.new_ch_menu_near);		

	            	  break;
	              
	              }
	       	  // ProductListSorter.getSortByDistance(productList);
			 //	ProductListSorter.getSortByPrice_L(productList);
				//ProductListSorter.getSortByHot(productList);
			  //  ProductListSorter.getSortByTime(productList);			    
	            //  ProductListSorter.getSortByTime(productList);
				
					setListView();						
			  }
			
				refresh=true;

				
				
			dismissProgressDialog();
			Util.dismissProgressDialog();
		}
	};
	private Runnable sort_progress = new Runnable() {

		@Override
		public void run() {
			
			dismissProgressDialog();
		}
	};

	
	/** ProgressDialog methods **/
	private void displayProgressDialog(String title, String message) {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(getActivity(), title, message);
		}
	}

	private void dismissProgressDialog() {
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}

	

	

	
	
	
}

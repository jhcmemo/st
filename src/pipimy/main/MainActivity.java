package pipimy.main;

import pipimy.cart.ShoppingCartUtil;
import pipimy.chat.ChatIndexObject;
import pipimy.chat.ChatUtil;
import pipimy.others.ApiProxy;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.IGenericDialogUtil;
import pipimy.others.LocationListenerGPS;
import pipimy.others.LocationListenerWIFI;
import pipimy.others.LocationSingleton;
import pipimy.others.PIPI_Tool;
import pipimy.others.PIPI_Tool.Product_Menu;
import pipimy.others.PreferenceProxy;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.service.CommonUtilities;
import pipimy.service.R;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ActionBar.Tab;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.readystatesoftware.viewbadger.BadgeView;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {

	MFragmentPagerAdapter mFragmentPagerAdapter;
	public ViewPager mViewPager;
	int currentFragment;
	private int screenW;
	private int screenH;
	private int tab_height = 5;
	private int offset;
	int currIndex = 0;

	ActionBar actionBar;
	boolean initLocation;

	PIPI_Tool.Product_Menu pro_menu;

	public boolean isVideoUpLoaded = false;
	public boolean isPicUpLoaded = false;
	public static LocationManager locationManager;
	public static LocationListenerGPS locationListenerGPS;
	public static LocationListenerWIFI locationListenerWIFI;
	ProgressDialog progressDialog;

	final static String TAG = "MainActivity";
	String chatid;
	String isFrom;
	String registerTokenId;

	BadgeView chatTabBadge;
	ImageView iv_browse, iv_cart, iv_dialog, iv_setting, iv_underline;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ApplicationController.isCartChanged = true;

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListenerGPS = new LocationListenerGPS(this);
		locationListenerWIFI = new LocationListenerWIFI(this);
		iv_browse = (ImageView) findViewById(R.id.iv_browse);
		iv_cart = (ImageView) findViewById(R.id.iv_cart);
		iv_dialog = (ImageView) findViewById(R.id.iv_dialog);
		iv_setting = (ImageView) findViewById(R.id.iv_setting);
		iv_underline = (ImageView) findViewById(R.id.iv_underline);

		iv_browse.setOnClickListener(change_fragment);
		iv_cart.setOnClickListener(change_fragment);
		iv_dialog.setOnClickListener(change_fragment);
		iv_setting.setOnClickListener(change_fragment);

		// Set up the action bar
		actionBar = getActionBar();
		// actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setIcon(R.drawable.no_icon);
		// actionBar.setDisplayShowTitleEnabled(false);

		chatTabBadge = new BadgeView(this, iv_dialog);
		chatTabBadge.setScaleX(0.8f);
		chatTabBadge.setScaleY(0.8f);
		chatTabBadge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
		Log.d("cache","VolleySingleton.temp_storimg.size(): "+VolleySingleton.temp_storimg.size());
		//VolleySingleton.removeAllCache();
		initUnderLine();

		initGCM();
		initPagerFragment();
		initData();

	}

	@Override
	protected void onResume() {
		super.onResume();

		LocationSingleton.getInstance().setLatLng(this);

		locationManager.requestLocationUpdates("gps", 300 * 1000, 0,
				locationListenerGPS);
		locationManager.requestLocationUpdates("network", 30 * 1000, 0,
				locationListenerWIFI);
	}

	@Override
	protected void onPause() {
		super.onPause();
		locationManager.removeUpdates(locationListenerGPS);
	}

	@Override
	public void onBackPressed() {

		if (currentFragment == 0) {
			final Dialog dialog = new Dialog(this, R.style.selectorDialog);
			dialog.setContentView(R.layout.dialog_leave_app);
			dialog.show();

			TextView btn_confirm = (TextView) dialog.findViewById(R.id.btn_confirm);
			btn_confirm.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});

		

			TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
			btnCancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

		

		} else {

			mViewPager.setCurrentItem(0);
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		locationManager.removeUpdates(locationListenerWIFI);
		locationManager.removeUpdates(locationListenerGPS);
	}

	private OnClickListener change_fragment = new OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.iv_browse:
				mViewPager.setCurrentItem(0);
				break;

			case R.id.iv_cart:
				mViewPager.setCurrentItem(1);
				break;

			case R.id.iv_dialog:
				mViewPager.setCurrentItem(2);
				break;

			case R.id.iv_setting:
				mViewPager.setCurrentItem(3);
				break;

			}

		}
	};

	private void initData() {
		Constant.picList.clear();
		// TODO Auto-generated method stub
		Constant.uploadProductID = PreferenceProxy.getProductID(this);
		if (Constant.uploadProductID.equals("")) {
			apiRequest(Constant.CODE_GET_PRODUCTID);
		}
Log.d("productid",Constant.uploadProductID);
		if (PreferenceProxy.isUserLogin(this)) {
			apiRequest(Constant.CODE_GET_CART_INDEX);
			apiRequest(Constant.CODE_GET_CHAT_INDEX);
		}

	}

	private void initUnderLine() {
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenW = dm.widthPixels;
		screenH = dm.heightPixels;
		offset = screenW / 3;

		Matrix matrix = new Matrix();
		matrix.postTranslate(offset, 0);
		iv_underline.setImageMatrix(matrix);
	}

	protected void initPagerFragment() {

		mFragmentPagerAdapter = new MFragmentPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mFragmentPagerAdapter);

		// When swiping between different fragments, select the corresponding
		// one. We can also use ActionBar.Tab#select() to do this
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {

						currentFragment = position;

						if (position != 0) {

							ChatUtil.mainCallback.menu_state_check();

						}

						if (position == 2) {
							actionBar.setDisplayShowTitleEnabled(true);
							actionBar.setTitle(getResources().getString(
									R.string.chat_title));
							actionBar.setDisplayShowHomeEnabled(true);
							
							iv_browse
									.setImageResource(R.drawable.new_tab_browse0);
							iv_cart.setImageResource(R.drawable.new_tab_cart0);
							iv_dialog
									.setImageResource(R.drawable.new_tab_dialog);
							iv_setting
									.setImageResource(R.drawable.new_tab_setting0);
							iv_underline.setBackgroundColor(Color
									.parseColor("#FF4A9ED8"));

							
							if (PreferenceProxy.isUserLogin(MainActivity.this)) {
							
								apiRequest(Constant.CODE_GET_CHAT_INDEX);
							}



						}

						else if (position == 0) {
							//actionBar.setDisplayHomeAsUpEnabled(true);
							//actionBar.setDisplayShowHomeEnabled(true);
							actionBar.setDisplayShowTitleEnabled(true);
							actionBar.setTitle(getString(R.string.product_title));
							//actionBar.setDisplayUseLogoEnabled(true);
							actionBar.setDisplayShowHomeEnabled(true);
							
							
							iv_browse
									.setImageResource(R.drawable.new_tab_browse);
							iv_cart.setImageResource(R.drawable.new_tab_cart0);
							iv_dialog
									.setImageResource(R.drawable.new_tab_dialog0);
							iv_setting
									.setImageResource(R.drawable.new_tab_setting0);
							iv_underline.setBackgroundColor(Color
									.parseColor("#FFEA7A7A"));

						} else if (position == 1) {
							
							actionBar.setDisplayShowTitleEnabled(true);
							actionBar.setTitle(getResources().getString(
									R.string.shopping));
							actionBar.setDisplayShowHomeEnabled(false);
							
							
							iv_browse
									.setImageResource(R.drawable.new_tab_browse0);
							iv_cart.setImageResource(R.drawable.new_tab_cart);
							iv_dialog
									.setImageResource(R.drawable.new_tab_dialog0);
							iv_setting
									.setImageResource(R.drawable.new_tab_setting0);
							iv_underline.setBackgroundColor(Color
									.parseColor("#FFF7C934"));

							
							/*getActionBar()
									.setTitle(
											getResources().getString(
													R.string.shopping));*/
							apiRequest(Constant.CODE_GET_CART_INDEX);
						} else {
							//actionBar.setDisplayShowHomeEnabled(false);
							actionBar.setDisplayShowTitleEnabled(true);
							actionBar.setTitle(getResources().getString(
									R.string.action_settings));
							actionBar.setDisplayShowHomeEnabled(false);
							
							
							iv_browse
									.setImageResource(R.drawable.new_tab_browse0);
							iv_cart.setImageResource(R.drawable.new_tab_cart0);
							iv_dialog
									.setImageResource(R.drawable.new_tab_dialog0);
							iv_setting
									.setImageResource(R.drawable.new_tab_setting);

							
							/*getActionBar().setTitle(
									getResources().getString(
											R.string.action_settings));*/
							iv_underline.setBackgroundColor(Color
									.parseColor("#FF40CCA7"));

						}

					}

					@Override
					public void onPageScrolled(int position,
							float positionOffset, int arg2) {

						//Log.d(TAG, "position: " + position);
						//Log.d(TAG, "current Index: " + currentFragment);
						LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
								screenW / 4, tab_height);
						if (currentFragment > position) {
							lp.leftMargin = (int) (currentFragment * screenW
									/ 4 + (positionOffset - 1) * screenW / 4);
						} else if (currentFragment <= position) {
							lp.leftMargin = (int) (currentFragment * screenW
									/ 4 + positionOffset * screenW / 4);
						}
						iv_underline.setLayoutParams(lp);

					}

				});

	}

	/*
	 * API threads
	 */
	
	public void login_callBack(){
		
		initData();
		ChatUtil.onChatIndexCallback.onGetChatIndex();
		//mViewPager.setCurrentItem(0);
		
	}
	
	
	private void apiRequest(final int requestCode) {
		new Thread() {
			@Override
			public void run() {
				switch (requestCode) {
				case Constant.CODE_RETURN_ERR: {
					requestError();
					break;
				}
				case Constant.CODE_GET_PRODUCTID: {
					ApiProxy.getProductID(MainActivity.this);
					break;
				}
				case Constant.CODE_GET_CHAT_INDEX: {
					Message msg = ApiProxy.getChatIndex(MainActivity.this);
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_GET_CART_INDEX: {
					Message msg = ApiProxy
							.getMyShoppingCartIndex(MainActivity.this);
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_CLEAN_CHAT_BADGE: {
					Message msg = ApiProxy.cleanChatBadge(MainActivity.this,
							chatid, isFrom);
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_UPD_ANDROID_TOKEN: {
					ApiProxy.updateAndroidToken(MainActivity.this,
							registerTokenId);
					break;
				}
				}
			}
		}.start();
	}

	protected void requestError() {
		DialogUtil.pushPureDialog(this, "Alert", "Error in API request", "ok");
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case Constant.CODE_GET_CART_INDEX: {
				if (ShoppingCartUtil.cartListIndexCallback != null)
					ShoppingCartUtil.cartListIndexCallback.onGetCartListIndex();
				break;
			}
			case Constant.CODE_GET_CHAT_INDEX: {
				if (ChatUtil.onChatIndexCallback != null)
					ChatUtil.onChatIndexCallback.onGetChatIndex();

				setChatTabBadge();
				break;
			}
			case Constant.CODE_CLEAN_CHAT_BADGE: {
				Util.dismissProgressDialog();
				if (ChatUtil.onChatIndexCallback != null)
					ChatUtil.onChatIndexCallback.onGetChatIndex();

				//chatTabBadge.setVisibility(View.GONE);
				apiRequest(Constant.CODE_GET_CHAT_INDEX);
				break;
			}
			}

		}
	};

	// Location callback by locationListenerWIFI
	public void onLocationChangeCallBack(double latitude, double longitude) {

		PreferenceUtil.setString(this, Constant.USER_LNG,
				Double.toString(longitude));
		PreferenceUtil.setString(this, Constant.USER_LAT,
				Double.toString(latitude));

		// SetNowCity(latitude, longitude);
	}

	protected void setChatTabBadge() {
		// TODO Auto-generated method stub
		Log.e(TAG, "setChatTabBadge BEGIN");

		String memberID = PreferenceProxy.getUserID(this);
		int badge = 0;
		for (int i = 0; i < ApplicationController.chatIndexList_seller.size(); i++) {
			ChatIndexObject object = ApplicationController.chatIndexList_seller
					.get(i);
			// I'm from
			if (object.getUserIDFrom().equals(memberID)) {
				badge += object.getUserIDFromBadge();
			}
			// I'm to
			else {
				badge += object.getUserIDToBadge();
			}
		}
		for (int i = 0; i < ApplicationController.chatIndexList_buyer.size(); i++) {
			ChatIndexObject object = ApplicationController.chatIndexList_buyer
					.get(i);
			// I'm from
			if (object.getUserIDFrom().equals(memberID)) {
				badge += object.getUserIDFromBadge();
			}
			// I'm to
			else {
				badge += object.getUserIDToBadge();
			}
		}

		Log.e(TAG, "setChatTabBadge badge count = " + badge);

		if (badge > 0) {
			chatTabBadge.setText(Integer.toString(badge));
			chatTabBadge.show();
		} else {
			chatTabBadge.setVisibility(View.GONE);
		}

	}

	// called when swiped and selected
	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
		currentFragment = tab.getPosition();

		// important!!! only when adding this line, onCreateOptionsMenu can be
		// re-called
		invalidateOptionsMenu();
		// Log.e("=======","onTabSelected");
	}

	@Override
	public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
		// ft.remove(fragment);
		// Log.e("=======","onTabUnselected"+" "+tab.getPosition());
	}

	// called when selected
	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
		// Log.e("=======","onTabReselected");
	}

	/**
	 * A FragmentPagerAdapter that returns a fragment
	 */
	class MFragmentPagerAdapter extends FragmentPagerAdapter {

		public MFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;

			switch (position) {
			case 0:
				fragment = new ProductFragment();
				break;
			case 1:
				fragment = new ShoppingFragment();
				break;
			case 2:
				fragment = new ChatFragment();
				break;
			case 3:
				fragment = new SettingFragment();
				break;
			}

			return fragment;
		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
		}

	}

	/** =============================================================== */
	/** ============================== GCM ============================ */
	/** =============================================================== */
	private void initGCM() {
		this.registerReceiver(GCM_getChatMessageReceiver, new IntentFilter(
				CommonUtilities.DISPLAY_MESSAGE_CHAT));

		// must register first and then can call
		// GCMRegistrar.getRegistrationId(this)
		GCMRegistrar.register(this, CommonUtilities.SENDER_ID);

		if (GCMRegistrar.getRegistrationId(this).length() > 0) {
			String prefToken = PreferenceUtil.getString(this,
					Constant.USER_TOKEN);
			if (!GCMRegistrar.getRegistrationId(this).equals(prefToken)) {
				PreferenceUtil.setString(this, Constant.USER_TOKEN,
						GCMRegistrar.getRegistrationId(this));
			}

			// Log.e("GCMToken", Constant.GCMToken);
			if (PreferenceUtil.getBoolean(this, Constant.USER_LOGIN)) {
				registerTokenId = GCMRegistrar.getRegistrationId(this);
				apiRequest(Constant.CODE_UPD_ANDROID_TOKEN);
			}

		} else
			Log.e("GCMToken", "no GCMToken");
	}

	/*----- GCM for get chat message -----*/
	private final BroadcastReceiver GCM_getChatMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(CommonUtilities.DISPLAY_MESSAGE_CHAT)) {
				apiRequest(Constant.CODE_GET_CHAT_INDEX);
				Log.d("main", "catch chat");
			}
		}
	};

	/** =============================================================== */
	/** ================== Activities call back result ================ */
	/** =============================================================== */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == 6) {
			chatid = data.getStringExtra("chatId");
			isFrom = data.getStringExtra("isFrom");
			apiRequest(Constant.CODE_CLEAN_CHAT_BADGE);
		} else if (resultCode == 4) {

		}
		/*----- start chat content -----*/
		else if (resultCode == 3) {
			// Log.d("main","CHATTTTT");
			
			mViewPager.setCurrentItem(2);
//

		//	 apiRequest(GET_CHAT_INDEX);
		//	apiRequest(Constant.CODE_GET_CHAT_INDEX);
			
		//	ChatUtil.onChatIndexCallback.onGetChatIndex();


		} else if (resultCode == 2) {
			initData();
			
			mViewPager.setCurrentItem(0);
		} else if (resultCode == 66) {
			/*----  checkout products finish  ----*/
			Log.e(TAG, "checkout products finish");
			ApplicationController.isCartChanged = true;
			apiRequest(Constant.CODE_GET_CART_INDEX);
		}
		
		
		
		

	}

}

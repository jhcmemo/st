package pipimy.others;

import java.util.ArrayList;

import pipimy.object.MyDealObject;
import pipimy.service.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class MyDealAdapter extends BaseExpandableListAdapter {

	private ImageLoader imageLoader = ImageLoader.getInstance();
	private Context context;
	private int layout;
	private ArrayList<MyDealObject> mList;
	private boolean mIsSeller;
	private OnClickListener idClickListener;
	private OnClickListener ratingClickListener;
	private String memberID;

	public MyDealAdapter(Context context, int layout,
			ArrayList<MyDealObject> list, OnClickListener idClickListener, OnClickListener ratingClickListener, boolean isSeller,String memberID) {
		this.context = context;
		this.layout = layout;
//		this.list_seller = list_seller;
//		this.list_buyer = list_buyer;
		this.mList = list;
		this.mIsSeller = isSeller;
		this.idClickListener = idClickListener;
		this.ratingClickListener = ratingClickListener;
		this.memberID= memberID;
	}

	@Override
	public int getGroupCount() {
		return 1;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
//		if (groupPosition == 0) {
//			return list_seller.size();
//		} else {
//			return list_buyer.size();
//		}
		return mList.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View view = LayoutInflater.from(context).inflate(R.layout.chat_group, null);
		TextView chat_group = (TextView) view.findViewById(R.id.chat_group);
		/*if (!mIsSeller) {
			chat_group.setText(context.getString(R.string.mydeal_deal_with_seller));
		} else {
			chat_group.setText(context.getString(R.string.mydeal_deal_with_buyer));
		}*/
		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		MyDealObject dealObject;
		// seller
//		if (groupPosition == 0) {
//			dealObject = list_seller.get(childPosition);
//		}
//		// buyer
//		else {
//			dealObject = list_buyer.get(childPosition);
//		}
		
		dealObject = mList.get(childPosition);
		
		View view = LayoutInflater.from(context).inflate(layout, parent, false);
		ImageView imageView = (ImageView) view.findViewById(R.id.cell_myDeal_imageView);
		TextView memberIDButton = (TextView) view.findViewById(R.id.cell_myDeal_memberIDButton);
		// Button ratingButton = (Button)
		// view.findViewById(R.id.bucell_myDeal_ratingBtton);
		TextView titleTextView = (TextView) view.findViewById(R.id.cell_myDeal_titleTextView);
		TextView priceTextView = (TextView) view.findViewById(R.id.cell_myDeal_priceTextView);
		TextView timeTextView = (TextView) view.findViewById(R.id.cell_myDeal_timeTextView);

		titleTextView.setText(dealObject.getProductTitle());
		priceTextView.setText("$" + dealObject.getProductPrice());
		timeTextView.setText(dealObject.getTime().substring(5,16).replace("-", "/"));

		View ratingView = (View) view.findViewById(R.id.cell_myDeal_ratingLayout);
		RatingBar ratingBar = (RatingBar) ratingView.findViewById(R.id.cell_myDeal_ratingBar);
		TextView ratingTextView = (TextView) ratingView.findViewById(R.id.cell_myDeal_ratingTextView);

		String r = dealObject.getRatingComment();
		if (r.equals("null")) {
			ratingTextView.setText(context.getString(R.string.mydeal_rating_hint));
		} else {
			ratingTextView.setText(r);
		}

		int score = dealObject.getRatingScore();
		if (score != -1) {
			ratingBar.setProgress(score);
		} else {
			ratingBar.setProgress(0);
		}

		// other product
		if (dealObject.getUserIDFrom().equals(memberID)) {
			memberIDButton.setText(dealObject.getUserIDTo());

			ratingView.setVisibility(View.VISIBLE);
			ratingView.setTag(dealObject.getProductId() + "," + dealObject.getRatingScore() + ","
					+ dealObject.getRatingComment());
			ratingView.setOnClickListener(ratingClickListener);
			memberIDButton.setTag(dealObject.getUserIDTo());
		}

		// self product
		else {
			memberIDButton.setText(dealObject.getUserIDFrom());
			ratingView.setVisibility(View.GONE);
			memberIDButton.setTag(dealObject.getUserIDFrom());
		}

		memberIDButton.setOnClickListener(idClickListener);
		
		DisplayImageOptions options = new DisplayImageOptions.Builder()
		// this will make circle, pass the width of image
		.displayer(new RoundedBitmapDisplayer(3)).cacheOnDisc(true)
		.build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		imageLoader.displayImage(Constant.PicServerURL + dealObject.getProductId() + "-pic1.jpg", imageView,
				options);

		view.setTag(dealObject.getProductId());

		return view;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}

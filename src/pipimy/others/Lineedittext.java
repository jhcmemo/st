package pipimy.others;


import android.content.Context;  
import android.graphics.Canvas;  
import android.graphics.Color;  
import android.graphics.Paint;  
import android.util.AttributeSet;  
import android.widget.EditText;  
  
public class Lineedittext extends EditText {  
  
    private Paint mPaint;  
    /** 
     * @param context 
     * @param attrs 
     */  
    public Lineedittext(Context context, AttributeSet attrs) {  
        super(context, attrs);  
        // TODO Auto-generated constructor stub  
       mPaint = new Paint();  
       mPaint .setStrokeWidth(3);
       mPaint.setStyle(Paint.Style.STROKE);  
       mPaint.setColor(Color.parseColor("#8e8e8d"));  
    }  
      
   @Override  
   public void onDraw(Canvas canvas)  
    {  
       super.onDraw(canvas);  
          
       

        canvas.drawLine(0,this.getHeight()-2,  this.getWidth()-1, this.getHeight()-2, mPaint);  
    }  
}  

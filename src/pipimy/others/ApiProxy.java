package pipimy.others;

import org.json.JSONException;
import org.json.JSONObject;

import pipimy.cart.CartCheckoutActivity;
import pipimy.cart.CheckFeeObject;
import pipimy.main.ApplicationController;
import pipimy.main.MainActivity;
import pipimy.nearby.ProductObject;
import android.app.Activity;
import android.os.Message;
import android.util.Log;
import android.view.View;

public class ApiProxy {
	
	final static String TAG = "ApiProxy";
	
	public static Message verifyOTPCode(String[] params, String[] data, Activity activity) {
		String result = Http.post(Constant.SET_OTP_CODE, params, data, activity);
		Log.e(TAG, "verifyOTPCode res = "+result);
		
		Message msg = new Message();
		msg.obj = result;
		
		return msg;
	}
	
	public static Message setOtherLogistics(String[] params, String[] data, Activity activity) {
		String result = Http.post(Constant.SET_OTHER_LOGISTIC, params, data, activity);
		Log.e(TAG, "setOtherLogistics res = "+result);
		
		Message msg = new Message();
		if(result.contains("success")) msg.arg1 = Constant.CODE_SET_OTHER_LOGISTIC;
		else msg.arg1 = Constant.CODE_RETURN_ERR;
		
		return msg;
	}
	
	public static Message getSoldList(String[] params, String[] data, Activity activity) {
		String result = Http.get(Constant.GET_SELL_LIST, params, data, activity);
		//Log.e(TAG, "getSoldList res = " + result);
		
		Message msg = new Message();
		try {
			JSONParserTool.getMyboughtListIndex(result, ApplicationController.soldList);
			msg.arg1 = Constant.CODE_GET_SOLD_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
	public static Message checkPayFee(String[] params, String[] data, Activity activity) {
		String result = Http.post(Constant.CART_CHECK_PAY, params, data, activity);
		Log.e(TAG, "checkPayFee res = "+result);
		
		Message msg = new Message();
		CheckFeeObject object = new CheckFeeObject();
		
		if(result != null) {
			try{
				object = JSONParserTool.checkPay(result);
				msg.arg1 = Constant.CODE_CHECK_PAY;
				msg.obj = object;
			} catch(Exception e) {
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
	public static Message getOneProduct(String[] params, String[] data, Activity activity) {
		String result = Http.post(Constant.GET_ONE_PRODUCT, params, data, activity);
		Log.e(TAG, "getOneProduct result = "+result);
		
		ProductObject object = new ProductObject();
		Message msg = new Message();
		if(result != null) {
			try {
				object = JSONParserTool.getOneProduct(result);
				msg.obj = object;
				msg.arg1 = Constant.CODE_GET_ONE_PRODUCT;
			} catch(Exception e) {
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else msg.arg1 = Constant.CODE_RETURN_ERR;
		
		return msg;
	}
	
	public static Message sendChatContent(String[] params, String[] data, Activity activity) {
		String result = Http.post(Constant.SEND_CHAT_CONTENT, params, data, activity);
		//Log.e(TAG, "sendChatContent result = "+result);
		
		Message msg = new Message();
		if(result != null) {
			msg.arg1 = Constant.CODE_SEND_CHAT_CONTENT;
		}
		
		return msg;
	}
	
	public static Message getChatContent(String[] params, String[] data, Activity activity) {
		String result = Http.post(Constant.GET_CHAT_CONTENT, params, data, activity);
		//Log.e(TAG, "getChatContent result =" +result);
		
		Message msg = new Message();
		if(result != null) {
			try {
				JSONParserTool.getChatContent(result,
						ApplicationController.chatContentList);
				msg.arg1 = Constant.CODE_GET_CHAT_CONTENT;
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return msg;
	}
	
	public static Message bargainProduct(String[] params, String[] data, Activity activity) {
		String result = Http.post(Constant.SET_BARGAIN_PRODUCT, params, data, activity);
		//Log.e(TAG, "bargainProduct result = "+result);
		
		Message msg = new Message();
		if(result != null) {
			msg.arg1 = Constant.CODE_SET_BARGAIN;
			msg.obj = result;
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
	public static Message removeItemsFromCart(String removeList, Activity activity) {
		String[] params = { "ProductID" };
		String[] data = { removeList };
		String result = Http.post(Constant.DELETE_FROM_CART, params,
				data, activity);

		Message msg = new Message();
		if(result != null) {
			msg.arg1 = Constant.CODE_DEL_CART_ITEM;
			msg.obj = result;
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
	public static Message getOrderResources(String orderID, Activity activity) {
		String[] params = {"orderId"};
		String[] data = {orderID};
		String result = Http.post(Constant.GET_ORDER_RESOURCES, params, data, activity);
		//Log.e(TAG, "getOrderResources result = "+result);
		
		Message msg = new Message();
		if(result != null) {
			try {
				JSONParserTool.getOrderResource(result, ApplicationController.orderResources
						, ApplicationController.resObj);
				msg.arg1 = Constant.CODE_ORDER_RESOURCE;
			} catch(Exception e) {
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
	public static Message getProductID(Activity activity) {
		String[] params = { "" };
		String[] data = { "" };
		String result = Http.get(Constant.GET_PRODUCTID_URL, params,
				data, activity);
		Log.e(TAG, "getProductID result = "+result);
		Message msg = new Message();
		
		if(result != null) {
			try {
				JSONObject jsonObject = new JSONObject(result);
				String myProductID = jsonObject.getString("result");
				PreferenceUtil.setString(activity,
						Constant.USER_PRODUCTID, myProductID);
				msg.arg1 = Constant.CODE_GET_PRODUCTID;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
	public static Message getNearByProductList(Activity activity){
		String[] params = { "Lat", "Lng", "CC" };
		String[] data = {
				PreferenceUtil.getString(activity, Constant.USER_LAT),
				PreferenceUtil.getString(activity, Constant.USER_LNG), "TW" };
		String result = Http.post(Constant.GET_NEAR_PRODUCT, params,
				data, activity);
		
		Message msg = new Message();
		
		if(result != null) {
			try {
				JSONParserTool.getNearProductList(result,
						ApplicationController.nearProductList, false);
				msg.arg1 = Constant.CODE_GET_NEARPRODUCT;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
	
		return msg;
	}
	
	//update GCM�@Token
	public static void updateAndroidToken(Activity activity, String registerID) {
		String[] params = { "Token" };
		String[] data = { registerID };
		String result = Http.post(Constant.UPDATE_ANDROID_TOKEN,
				params, data, activity);
	}
	
	//clean badge in the chat index
	public static Message cleanChatBadge(Activity activity, String chatID, String isFrom){
		String[] params = { "ChatId", "IsFrom" };
		String[] data = { chatID, isFrom };
		String result = Http.post(Constant.CLEAN_CHAT_BADGE, params,
				data, activity);
		
		Message msg = new Message();
		
		if(result != null) {
			try {
				JSONParserTool.getChatIndex(result,
						ApplicationController.chatIndexList_seller,
						ApplicationController.chatIndexList_buyer);
				msg.arg1 = Constant.CODE_CLEAN_CHAT_BADGE;
			} catch (Exception e) {
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
	//get user chat index list
	public static Message getChatIndex(Activity activity) {
		Log.e(TAG, "getChatIndex BEGIN");
		String[] params = { "" };
		String[] data = { "" };
		String result = Http.get(Constant.GET_CHAT_INDEX, params, data,
				activity);
		//Log.e(TAG, "getChatIndex result = "+result);
		Message msg = new Message();
		
		if(result != null) {
			try {
				JSONParserTool.getChatIndex(result,
						ApplicationController.chatIndexList_seller,
						ApplicationController.chatIndexList_buyer);
				msg.arg1 = Constant.CODE_GET_CHAT_INDEX;
			} catch (Exception e) {
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}

		return msg;
	}
	
	//check out from shopping cart
	public static Message checkoutShoppingList(Activity activity, String[] params, String[] data) {
		String result = Http.post(Constant.CHECKOUT_SHOPPING_LIST,
				params, data, activity);
		
		Log.e(TAG, "checkoutShoppingList res = "+result);
		
		Message msg = new Message();
		
		if(result != null) {
			msg.arg1 = Constant.CODE_CHECKOUT_SHOPPING_LIST;
			msg.obj = result;
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
			
		return msg;
	}

	//get user shopping cart index
	public static Message getMyShoppingCartIndex(Activity activity){
		Log.e(TAG, "getMyShoppingCartIndex BEGIN");
		
		String[] params = { "" };
		String[] data = { "" };
		String result = Http.get(Constant.GET_CART_LIST, params, data, activity);
		
		Log.e(TAG, "getMyShoppingCartIndex result = "+result);
		
		Message msg = new Message();
		
		if(result != null) {
			try {
				JSONParserTool.getMyShoppingCart(result,
						ApplicationController.cartItemList);
				msg.arg1 = Constant.CODE_GET_CART_INDEX;
			} catch (Exception e) {
				e.printStackTrace();
				msg.arg1 = Constant.CODE_RETURN_ERR;
			}
		} else {
			msg.arg1 = Constant.CODE_RETURN_ERR;
		}
		
		return msg;
	}
	
}

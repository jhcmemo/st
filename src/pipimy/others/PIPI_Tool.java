package pipimy.others;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pipimy.chat.ChatUtil.onDeleteChatIndexCallback;
import pipimy.object.FavoriteObject;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

public class PIPI_Tool {
	public static final String FAVORITE_LIST_SELL = "favoriteList_sell";
	public static final String FAVORITE_LIST_BUY = "favoriteList_buy";
	public static final String PrefrenceTag = "Preference";
	
	public static Product_Menu pro;

	
	public interface Product_Menu{
	    public  abstract  void check();
	
	}
	
	public static void removeFavorite(Activity activity, int position,
			List<FavoriteObject> favorites, boolean isSell) {
		favorites.remove(position);
		saveFavorites(activity, favorites, isSell);
	}

	public static ArrayList<FavoriteObject> getFavorites(Activity activity,
			List<FavoriteObject> favorites, boolean isSell) {
		SharedPreferences settings;

		settings = activity.getSharedPreferences(PrefrenceTag, 0);
		if (isSell) {
			if (settings.contains(FAVORITE_LIST_SELL)) {
				String jsonFavorites = settings.getString(FAVORITE_LIST_SELL,
						null);
				Gson gson = new Gson();
				FavoriteObject[] favoriteItems = gson.fromJson(jsonFavorites,
						FavoriteObject[].class);

				favorites = Arrays.asList(favoriteItems);
				favorites = new ArrayList<FavoriteObject>(favorites);
			} else
				return new ArrayList<FavoriteObject>();
		} else {
			if (settings.contains(FAVORITE_LIST_BUY)) {
				String jsonFavorites = settings.getString(FAVORITE_LIST_BUY,
						null);
				Gson gson = new Gson();
				FavoriteObject[] favoriteItems = gson.fromJson(jsonFavorites,
						FavoriteObject[].class);

				favorites = Arrays.asList(favoriteItems);
				favorites = new ArrayList<FavoriteObject>(favorites);
			} else
				return new ArrayList<FavoriteObject>();
		}

		return (ArrayList<FavoriteObject>) favorites;
	}
	/*---------- favorite ----------*/

	public static void saveFavorites(Activity activity,
			List<FavoriteObject> favorites, boolean isSell) {
		SharedPreferences settings;
		Editor editor;

		settings = activity.getSharedPreferences(PrefrenceTag, 0);
		editor = settings.edit();

		Gson gson = new Gson();
		String jsonFavorites = gson.toJson(favorites);
		if (isSell)
			editor.putString(FAVORITE_LIST_SELL, jsonFavorites);
		else
			editor.putString(FAVORITE_LIST_BUY, jsonFavorites);
		editor.commit();
	}
	
	public static void addFavorite(Activity activity, FavoriteObject product,
			List<FavoriteObject> favorites, boolean isSell) {

		boolean isSame = false;
		for (int i = 0; i < favorites.size(); i++) {
			if (favorites.get(i).getProductID().equals(product.getProductID())) {
				isSame = true;
			}
		}
		if (!isSame) {
			favorites.add(product);

			saveFavorites(activity, favorites, isSell);
		}
	}
}

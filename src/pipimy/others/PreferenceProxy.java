package pipimy.others;

import android.content.Context;

public class PreferenceProxy {
	
	public static void setIsMovingCvsDBDone(Context context){
		PreferenceUtil.setBoolean(context, Constant.MOVE_CVS_DB_DONE, true);
	}
	
	public static boolean getIsMovingCvsDBDone(Context context) {
		return PreferenceUtil.getBoolean(context, Constant.MOVE_CVS_DB_DONE);
	}
	
	public static String getUserID(Context context) {
		return PreferenceUtil.getString(context, Constant.USER_ID);
	}

	public static boolean isUserLogin(Context context) {
		return PreferenceUtil.getBoolean(context, Constant.USER_LOGIN);
	}
	
	public static String getProductID(Context context) {
		return PreferenceUtil.getString(context, Constant.USER_PRODUCTID);
	}
}

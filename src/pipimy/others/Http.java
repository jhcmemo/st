package pipimy.others;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pipimy.login.LoginActivity;


import android.app.Activity;
import android.util.Log;

public class Http
{
	
	private static String serverResponse = "No Response";
	private final static String NULL_EXCEPTION = "Null Exception";
	public static  boolean isDownloadSucceed = false;



	
	private static final int CONNECTION_TIMEOUT = 30000;
	
	public static String get(String url, String[] parameter, String[] data, Activity activity)
	{	
		Log.e("=======","Http Get");
		
		String URL = null;
		
		if(parameter == null)
		{
			URL = url;
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			int length = parameter.length;
			
			try {
			for(int i=0; i<length; i++)
			{
				if(i == 0)
					sb.append("?"+parameter[0]+"="+URLEncoder.encode(data[0], "UTF-8"));
				else
					sb.append("&"+parameter[i]+"="+URLEncoder.encode(data[i], "UTF-8"));
			}			
				URL = url + sb.toString();
				//Log.e("URL", URL);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try
		{
		    HttpParams httpParam = setConnectionTimeOut();
		    HttpClient httpClient = new DefaultHttpClient(httpParam);
	        HttpResponse httpResponse = null;
		
			HttpGet httpGet = new HttpGet(URL);
			// add cookies
			Cookie.AddCookies(activity, httpGet);
			
	        httpResponse = httpClient.execute(httpGet);
	        
	        return EntityUtils.toString(httpResponse.getEntity());
	        
		} catch (Exception e) {
			e.printStackTrace();	
			return null;
		}
	}
	
	public static String post(String url, String[] parameter, String[] data, Activity activity)
	{	
		Log.e("=======","Http Post");
		
        int length = parameter.length;
		
		ArrayList<NameValuePair> pair = new ArrayList<NameValuePair>();
		for(int i=0; i<length; i++)
		{
		    pair.add(new BasicNameValuePair(parameter[i], data[i]));
		}
		
		try 
		{
			HttpParams httpParam = setConnectionTimeOut();
			HttpClient httpClient = new DefaultHttpClient(httpParam);
	        HttpResponse httpResponse = null;  
		
			HttpPost httpPost = new HttpPost(url);
			// add cookies
			Cookie.AddCookies(activity, httpPost);

	        if (pair != null) {
				for (int i=0; i<length; i++) {
					String headerNameTmp = parameter[i];
					String headerValueTmp = data[i];

					httpPost.setHeader(headerNameTmp, headerValueTmp);
				}
			}
	        
	        httpPost.setEntity(new UrlEncodedFormEntity(pair, "UTF-8"));
	        httpResponse = httpClient.execute(httpPost);
	        
	        if (activity instanceof LoginActivity) {
	        	Cookie.saveCookies(activity, httpResponse.getHeaders("Set-Cookie"));
	        	Log.e("=======","This is the LoginActivity");
	        }
	        
	        return EntityUtils.toString(httpResponse.getEntity());
	        
		} catch (Exception e) {
			Log.d("http_error","http error :"+e.getMessage());
			e.printStackTrace();	
			return null;
		}
	}	
	
	/** Time out parameter of Connection methods **/
	private static HttpParams setConnectionTimeOut() {
		/* Setup the timeout */
		HttpParams httpParameters = new BasicHttpParams();

		/*
		 * Set the timeout in milliseconds until a connection is established.
		 * The default value is zero, that means the timeout is not used.
		 */
		int timeoutConnection = CONNECTION_TIMEOUT;

		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);

		return httpParameters;
	}
	
	
	public static void startDownloadFile(String targetURL, File fileName) {

		isDownloadSucceed = false;

		try {
			URL url = new URL(targetURL);
			URLConnection connection = url.openConnection();
			connection.connect();

			/* download the file */
			InputStream input = new BufferedInputStream(url.openStream());
			OutputStream output = new FileOutputStream(fileName);

			byte data[] = new byte[1024];
			int count;
			while ((count = input.read(data)) != -1) {
				output.write(data, 0, count);
			}

			output.flush();
			output.close();
			input.close();

//			Log.d(TAG, "File download succeed, path: " + fileName);

			isDownloadSucceed = true;


		} catch (MalformedURLException e) {
			isDownloadSucceed = false;

			serverResponse = printException(e);
//			Log.e(TAG, "Exception happened in downloadFile(): " + serverResponse);

		} catch (IOException e) {
			isDownloadSucceed = false;

			serverResponse = printException(e);
//			Log.e(TAG, "Exception happened in downloadFile(): " + serverResponse);
		}

	
	}
	
	/** Use to Print Exception **/
	private static String printException(Exception exception) {
		String exceptionMessage = NULL_EXCEPTION;

		// if (exception != null) {
		// exceptionMessage = exception.getMessage().toString();
		// }

		return exceptionMessage;
	}
	
	
	
	
	public static void setLocation(double lat, double lng) {
		// String jsonStr = getLocationInfo(lat, lng).toString();

		// Log.i("location--??", jsonStr);

		JSONObject jsonObj;
		try {
			jsonObj = getLocationInfo(lat, lng);

			String Status = jsonObj.getString("status");
			if (Status.equalsIgnoreCase("OK")) {
				JSONArray Results = jsonObj.getJSONArray("results");
				JSONObject zero = Results.getJSONObject(0);
				JSONArray address_components = zero
						.getJSONArray("address_components");

				for (int i = 0; i < address_components.length(); i++) {
					JSONObject zero2 = address_components.getJSONObject(i);
					String long_name = zero2.getString("long_name");
					JSONArray mtypes = zero2.getJSONArray("types");
					String Type = mtypes.getString(0);
					
					if (Type.equalsIgnoreCase("administrative_area_level_2")) {
						// Address2 = Address2 + long_name + ", ";
						Constant.NowCity = long_name;
						// Log.d(" CityName --->", City + "");
					} else if (Type
							.equalsIgnoreCase("administrative_area_level_1")) {
						Constant.NowCity = long_name;
					}
				}
			}
		}

		catch (JSONException e) {

			e.printStackTrace();
		}

	}
	
	
	
	
	private static JSONObject getLocationInfo(double lat, double lng) {
		String language = Locale.getDefault().getLanguage();
		
		// language = "zh-TW";
//		 Log.d("GG", Locale.getDefault().getDisplayCountry());
		 if(Locale.getDefault().getDisplayCountry().equals("台灣"))
			 language = "zh-TW";

		HttpGet httpGet = new HttpGet(
				"http://maps.googleapis.com/maps/api/geocode/json?latlng="
						+ lat + "," + lng + "&sensor=false&language="
						+ language);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		// StringBuilder stringBuilder = new StringBuilder();
		String result = "";
		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			result = EntityUtils.toString(entity, HTTP.UTF_8);
			// InputStream stream = new ByteArrayInputStream(result.getBytes());
			// int b;
			// while ((b = stream.read()) != -1) {
			// stringBuilder.append((char) b);
			// }
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return jsonObject;
	}
	
	
	
	
}

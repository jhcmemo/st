package pipimy.others;

import java.util.ArrayList;

import pipimy.third.TransferModel;
import pipimy.third.TransferModel.Status;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class TransferProgressBar extends ProgressBar {
//	private int finishCount = 0;
//	private int count;
	private int totalProgress;
	private TransferModel model;
	private ArrayList<TransferModel> modelList = new ArrayList<TransferModel>();
	private AmazonUpLoadCompleteListener completeListener;

	public TransferProgressBar(Context context) {
		super(context);
	}

	public TransferProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void initTransferProgressBar(Context context, TransferModel model,
			AmazonUpLoadCompleteListener completeListener) {
		this.model = model;
		this.completeListener = completeListener;
		refresh();
	}

	public void initTransferProgressBar(Context context, TransferModel model,
			AmazonUpLoadCompleteListener completeListener, int count) {
//		this.count = count;
		this.totalProgress = count * 100;
		this.setMax(totalProgress);
		this.model = model;
		this.completeListener = completeListener;

		if (modelList.size() == 0) {
			modelList.add(model);
		} else {
			for (int i = 0; i < modelList.size(); i++) {
				if (modelList.get(i).getId() == model.getId()) {
					break;
				} else {
					if (i == modelList.size() - 1) {
						modelList.add(model);
					}
				}
			}
		}
		refresh(modelList);
	}

	public TransferModel getModel() {
		return this.model;
	}

	/* refresh method for public use */
	public void refresh() {
		refresh(model.getStatus());
	}

	private void refresh(Status status) {
		int progress = 0;
		switch (status) {
		case IN_PROGRESS:
			progress = model.getProgress();
			break;
		case PAUSED:
			progress = model.getProgress();
			break;
		case CANCELED:
			// progress = 0;
			break;
		case COMPLETED:
			progress = 100;
			model.abort();
			completeListener.onAmazonUpLoadComplete();
			break;
		}
		this.setProgress(progress);
	}

	private void refresh(ArrayList<TransferModel> list) {
		int progress = 0;
		for (int i = 0; i < list.size(); i++) {
			switch (list.get(i).getStatus()) {
			case IN_PROGRESS:
				progress += list.get(i).getProgress();
				break;
			case PAUSED:
				// progress = model.getProgress();
				break;
			case CANCELED:
				// progress = 0;
				break;
			case COMPLETED:
				progress += 100;
				// list.remove(i);
				break;
			}
		}

		if (totalProgress == progress) {
			for (int i = 0; i < list.size(); i++) {
				list.get(i).abort();
			}
			completeListener.onAmazonUpLoadComplete();
		}

		this.setProgress(progress);
	}

	public interface AmazonUpLoadCompleteListener {
		public void onAmazonUpLoadComplete();
	}
	/* What to do when user presses pause button */
	// private void onPause() {
	// if (model.getStatus() == Status.IN_PROGRESS) {
	// TransferController.pause(mContext, mModel);
	// refresh(Status.PAUSED);
	// } else {
	// TransferController.resume(mContext, mModel);
	// refresh(Status.IN_PROGRESS);
	// }
	// }

	/* What to do when user presses abort button */
	// private void onAbort() {
	// TransferController.abort(context, model);
	// refresh(Status.CANCELED);
	// }
}

package pipimy.others;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;
import android.util.Log;

public class AES {
	/**Test1*/
	//private static String HashKey = "Zf1AjVRlwE4XjlF9|";
	//private static String HashIV = "Ps8hPWGtUW0PE3Gk";
	/**Test2*/
	//private static String HashKey = "5294y06JbISpM5x9";
	//private static String HashIV = "v77hoKGq4kWxNNIS";
	/**Final*/
	private static String HashKey = "KPW2XIv40bQaJ9B0";
    private static String HashIV = "T12tvLwYRDmh1MVR";
	
	final protected static char[] hexArray = "0123456789abcdef".toCharArray();
	                                
	                                
	
	
	
	public static String EncryptAESToHex(String code)
	 {
	   try
	   {
	     AlgorithmParameterSpec mAlgorithmParameterSpec = new IvParameterSpec(HashIV.getBytes("UTF-8"));
	     SecretKeySpec mSecretKeySpec = new SecretKeySpec(HashKey.getBytes("UTF-8"), "AES");
	     Cipher mCipher = null;
	     mCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	     mCipher.init(Cipher.ENCRYPT_MODE,mSecretKeySpec,mAlgorithmParameterSpec);
	         
	     return bytesToHex(mCipher.doFinal(code.getBytes("UTF-8")));
	   }
	   catch(Exception ex)
	   {
	     return null;
	   }
	 }
	
	private  static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static String DecryptAESToString(String code)
	 {
		byte[] tmep_byte =hexStringToByteArray(code);

	   try
	   {
	     AlgorithmParameterSpec mAlgorithmParameterSpec = new IvParameterSpec(HashIV.getBytes("UTF-8"));
	     SecretKeySpec mSecretKeySpec = new SecretKeySpec(HashKey.getBytes("UTF-8"), "AES");
	     Cipher mCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	     mCipher.init(Cipher.DECRYPT_MODE, 
	                  mSecretKeySpec, 
	                  mAlgorithmParameterSpec);
	         
	    
	     String encode_string = new String( mCipher.doFinal(tmep_byte),"UTF-8");
	     return encode_string;
	   }
	   catch(Exception ex)
	   {
	     return null;
	   }
	 }
	
	
	
	public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];

        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }
}

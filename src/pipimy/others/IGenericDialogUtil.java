package pipimy.others;

import android.content.DialogInterface;

public class IGenericDialogUtil {
	public interface IGenericBtnClickListener {
		public abstract void PositiveMethod(DialogInterface dialog, int id);
		public abstract void NegativeMethod(DialogInterface dialog, int id);
	}

	public interface PositiveBtnClickListener {
		public abstract void PositiveMethod(DialogInterface dialog, int id);
	}
}




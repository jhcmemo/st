package pipimy.others;

import java.util.ArrayList;

import pipimy.main.ApplicationController;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by thkuo on 2014/12/22.
 */
public class VolleySingleton {

    private static VolleySingleton mInstance = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader; 
    static int maxMemory= (int) Runtime.getRuntime().maxMemory();    
    static int mCacheSize= maxMemory / 8;
    private static final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(mCacheSize);

    public static ArrayList<String> temp_storimg= new ArrayList<String>();
    private VolleySingleton(){
    	//maxMemory = (int) Runtime.getRuntime().maxMemory();    
       // mCacheSize = maxMemory / 8; 
        mRequestQueue = Volley.newRequestQueue(ApplicationController.getAppContext());
        mImageLoader = new ImageLoader(this.mRequestQueue, new ImageLoader.ImageCache() {
            public void putBitmap(String url, Bitmap bitmap) {
            	
            	if(url.contains(Constant.STORE_IMAGE_URL) ||url.contains(Constant.STORE_BACKGROUNDS_URL)){
            		temp_storimg.add(url);                 	
                 }
            	
                mCache.put(url, bitmap);
               
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    public static VolleySingleton getInstance(){
        if(mInstance == null){
            mInstance = new VolleySingleton();
        }
        return mInstance;
    }

    public static void removeURLCache(String Url){

    	mCache.remove(Url);
    }
    
    public static void removeAllCache(){
    	mCache.evictAll();
    	
    }
    
    
   /* public static void putCache(String Url){
    	mCache.put(key, value)(Url);
    	
    }*/
    
    public static void removeURLcache(String url){
    	mCache.remove(url);
    	
    }
    
   /* public static String getURLcache(String url){
    	String catch_url=mCache.get(url).toString();
		return catch_url;
    	
    }*/
    public RequestQueue getRequestQueue(){
        return this.mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        return this.mImageLoader;
    }
}


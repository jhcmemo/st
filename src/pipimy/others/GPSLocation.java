package pipimy.others;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.MathContext;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pipimy.main.MyProductActivity;
import pipimy.service.R;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

public class GPSLocation extends AsyncTask<Context, Void, Void> {
	private Context mContext;
	private static String mCountry = "";
	private static String mCity = "";

	private void initLocation() {
		LocationManager lm = (LocationManager) mContext
				.getSystemService(mContext.LOCATION_SERVICE);
		Location location = lm
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		setLocation(location.getLatitude(), location.getLongitude());
	}

	public void setLocation(double lat, double lng) {
		// String jsonStr = getLocationInfo(lat, lng).toString();

		// Log.i("location--??", jsonStr);

		JSONObject jsonObj;
		try {
			jsonObj = getLocationInfo(lat, lng);

			String Status = jsonObj.getString("status");
			if (Status.equalsIgnoreCase("OK")) {
				JSONArray Results = jsonObj.getJSONArray("results");
				JSONObject zero = Results.getJSONObject(0);
				JSONArray address_components = zero
						.getJSONArray("address_components");

				for (int i = 0; i < address_components.length(); i++) {
					JSONObject zero2 = address_components.getJSONObject(i);
					String long_name = zero2.getString("long_name");
					JSONArray mtypes = zero2.getJSONArray("types");
					String Type = mtypes.getString(0);
					if (Type.equalsIgnoreCase("country")) {
						mCountry = long_name;
					}
					if (Type.equalsIgnoreCase("administrative_area_level_2")) {
						// Address2 = Address2 + long_name + ", ";
						mCity = long_name;
						// Log.d(" CityName --->", City + "");
					} else if (Type
							.equalsIgnoreCase("administrative_area_level_1")) {
						mCity = long_name;
					}
				}
			}

		}

		catch (JSONException e) {

			e.printStackTrace();
		}

	}

	private JSONObject getLocationInfo(double lat, double lng) {
		String language = Locale.getDefault().getLanguage();
		
		// language = "zh-TW";
//		 Log.d("GG", Locale.getDefault().getDisplayCountry());
		 if(Locale.getDefault().getDisplayCountry().equals("台灣"))
			 language = "zh-TW";

		HttpGet httpGet = new HttpGet(
				"http://maps.googleapis.com/maps/api/geocode/json?latlng="
						+ lat + "," + lng + "&sensor=false&language="
						+ language);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		// StringBuilder stringBuilder = new StringBuilder();
		String result = "";
		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			result = EntityUtils.toString(entity, HTTP.UTF_8);
			// InputStream stream = new ByteArrayInputStream(result.getBytes());
			// int b;
			// while ((b = stream.read()) != -1) {
			// stringBuilder.append((char) b);
			// }
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return jsonObject;
	}

	public static String getCountry() {
		return mCountry;
	}

	public static String getCity() {
		return mCity;
	}

	@Override
	protected Void doInBackground(Context... context) {
		mContext = context[0];
		LocationManager status = (LocationManager) (mContext
				.getSystemService(Context.LOCATION_SERVICE));
		if (status.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			initLocation();
		} else {
			Builder builder = new Builder(mContext);
			builder.setTitle(mContext.getString(R.string.gps_error_title));
			builder.setMessage(mContext.getString(R.string.gps_error_message));
			builder.setPositiveButton(mContext.getString(R.string.confirm),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							mContext.startActivity(new Intent(
									Settings.ACTION_LOCATION_SOURCE_SETTINGS));
						}
					});
			builder.setNegativeButton(mContext.getString(R.string.cancel),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			builder.show();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void nothing) {
		// TODO: check this.exception
		// TODO: do something with the feed
	
			MyProductActivity.gpsCity = mCity;
	
		
	}
}

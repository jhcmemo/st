package pipimy.others;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import android.app.Activity;
import android.util.Log;

public class Cookie
{
	private static HashMap<String, String> CookieContiner = new HashMap<String, String>();

	public static void saveCookies(Activity activity, Header[] headers) {
		if (headers == null)
			return;

		for (int i = 0; i < headers.length; i++) {
			String cookie = headers[i].getValue();
			//Log.e("save a cookie", headers[i].getValue().toString());
			String[] cookievalues = cookie.split(";");
			for (int j = 0; j < cookievalues.length; j++) {
				String[] keyPair = cookievalues[j].split("=");
				String key = keyPair[0].trim();
				String value = keyPair.length > 1 ? keyPair[1].trim() : "";
				PreferenceUtil.setString(activity, key, value);
				CookieContiner.put(key, value);
			}
		}
	}

	public static void AddCookies(Activity activity, HttpGet request) {
		StringBuilder sb = getCookies(activity);
		request.addHeader("cookie", sb.toString());
	}

	public static void AddCookies(Activity activity, HttpPost request) {
		StringBuilder sb = getCookies(activity);
		request.addHeader("cookie", sb.toString());
	}

	private static StringBuilder getCookies(Activity activity) {
		CookieContiner.put("crazy[1]", PreferenceUtil.getString(activity, "crazy[1]"));
		CookieContiner.put("crazy[2]", PreferenceUtil.getString(activity, "crazy[2]"));
		CookieContiner.put("crazy[3]", PreferenceUtil.getString(activity, "crazy[3]"));
		CookieContiner.put("crazy[4]", PreferenceUtil.getString(activity, "crazy[4]"));
		CookieContiner.put("crazy[5]", PreferenceUtil.getString(activity, "crazy[5]"));
		
		StringBuilder sb = new StringBuilder();
		
		Iterator<Entry<String, String>> it = CookieContiner.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String,String> entry = it.next();
			String key = entry.getKey().toString();
			String val = entry.getValue().toString();
			//Log.e("get a cookie", entry.getValue().toString());
			sb.append(key);
			sb.append("=");
			sb.append(val);
			sb.append(";");
		}
		
		return sb;
	}
}

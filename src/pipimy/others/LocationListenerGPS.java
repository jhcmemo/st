package pipimy.others;

import pipimy.main.MainActivity;
import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class LocationListenerGPS extends Activity implements LocationListener {
	MainActivity m_pOwnerMainActivity;

	public LocationListenerGPS(MainActivity mainActivity) {
		this.m_pOwnerMainActivity = mainActivity;
	}

	@Override
	public void onLocationChanged(Location location) {
		this.m_pOwnerMainActivity.onLocationChangeCallBack(location.getLatitude(), location.getLongitude());


		// Toast.makeText(this, location.getLatitude() + " " +
		// location.getLongitude(), Toast.LENGTH_LONG).show();
		// PreferenceUtil.setString(this, Constant.USER_LNG,
		// location.getLongitude()+"");
		// PreferenceUtil.setString(this, Constant.USER_LAT,
		// location.getLatitude() +"");
		Log.e("GPS Current Location",
				location.getLatitude() + " " + location.getLongitude());
	}

	@Override
	public void onProviderDisabled(String arg0) {
		Log.e("GPS=======", "onProviderDisable");
	}

	@Override
	public void onProviderEnabled(String arg0) {
		Log.e("GPS=======", "onProviderEnable");
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		Log.e("GPS=======", "onStatusChanged");
	}
}

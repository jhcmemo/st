package pipimy.others;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

public class FragmentManagerUtil {

	/* init Fragment */
	public static void init(Bundle savedInstanceState, FragmentManager fragmentManager, int layoutID, Fragment fragment) {
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		if (savedInstanceState == null) {
            Log.d("sef","加入");

			transaction.add(layoutID, fragment).commit();
		}
	}

	/* replace Fragment */
	public static void replaceFragment(FragmentManager fragmentManager, Fragment f, int layout) {
		FragmentTransaction ft = fragmentManager.beginTransaction();
		ft.replace(layout, f);
		ft.commit();
	}

	/* add Fragment */
	public static void addToBackStackFragment(FragmentManager fragmentManager, Fragment f, int layout) {
		FragmentTransaction ft = fragmentManager.beginTransaction();
		ft.replace(layout, f);
		//ft.addToBackStack(null);
		ft.commit();
	}

	/* remove */
	public static  void removeFragment(FragmentManager fragmentManager, Fragment f) {
		FragmentTransaction ft = fragmentManager.beginTransaction();
		ft.remove(f);
		ft.commit();
	}

	/* Pop Back */
	public static void popBackStackFragment(FragmentManager fragmentManager) {
		fragmentManager.popBackStack();
	}

	/* clear (back to home) */
	public static void CleanFragment(FragmentManager fragmentManager) {
		// // Clear all back stack.
		int backStackCount = fragmentManager.getBackStackEntryCount();
		for (int i = 0; i < backStackCount; i++) {

			// Get the back stack fragment id.
			int backStackId = fragmentManager.getBackStackEntryAt(i).getId();
			fragmentManager.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}
}

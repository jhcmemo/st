package pipimy.others;

import java.util.ArrayList;

import pipimy.object.SomeoneRatingObject;
import pipimy.service.R;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class DialogListUtil extends Activity{

	private static OnTypeClickListener onTypeClickListener;
	private static OnAgeClickListener onAgeClickListener;
	private static OnSomeoneClickListener onSomeoneClickListener;
	private static OnReportSubmitClickListener onReportSubmitClickListener;
	private static OnChatClickListener onChatClickListener;
	private static OnHandSureClickListener onHandSureClickListener;
	private static OnChatIndexLongClickListener onChatIndexLongClickListener;
	private static OnRatingClickListener onRatingClickListener;
	private static OnChangePWDClickListener onChangePWDClickListener;
	private static OnChangeLocClickListener onChangeLocClickListener;
	private static OnChangeStroeFlowClickListener onChangeStoreFlowClickListener;

	private static OnAllPlayClickListener onAllPlayClickListener;
	private static OnSearchClickListener onSearchClickListener;

	private static Dialog dialog;
	//private static EmotionListApapter adapter;
	
	
	/*----- Emotion -----*/
	/*public static void showEmotionDialog(final Context context, EmotionListApapter emotionListApapter){
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_emotion);
		ListView emotionList = (ListView)dialog.findViewById(R.id.emotion_list);
		
		adapter = emotionListApapter;
		emotionList.setAdapter(adapter);
		
		TextView addEmotion = (TextView) dialog.findViewById(R.id.edit_emotion);
		addEmotion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(context, EmotionActivity.class);
				((ChatContentActivity)context).startActivityForResult(intent, 0);
			}
		});

		dialog.show();
	}*/

	/*----- Type -----*/
	public static void showTypeDialog(Context context, boolean isUpload, OnTypeClickListener clickListener) {
		onTypeClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_type);

		TextView cancelText = (TextView) dialog.findViewById(R.id.Dialog_cancel);
		LinearLayout othersText = (LinearLayout) dialog.findViewById(R.id.Dialog_others);
		LinearLayout elecText = (LinearLayout) dialog.findViewById(R.id.Dialog_electronic);
		LinearLayout clothingText = (LinearLayout) dialog.findViewById(R.id.Dialog_clothing);
		LinearLayout bookdText = (LinearLayout) dialog.findViewById(R.id.Dialog_books);
		LinearLayout ticketsText = (LinearLayout) dialog.findViewById(R.id.Dialog_tickets);
		LinearLayout lifeText = (LinearLayout) dialog.findViewById(R.id.Dialog_life);
		LinearLayout foodsText = (LinearLayout) dialog.findViewById(R.id.Dialog_foods);
		LinearLayout serviceText = (LinearLayout) dialog.findViewById(R.id.Dialog_services);
		LinearLayout makeUpText = (LinearLayout) dialog.findViewById(R.id.Dialog_make_up);
		LinearLayout petsText = (LinearLayout) dialog.findViewById(R.id.Dialog_pets);

		cancelText.setTag(0);
		othersText.setTag(1);
		elecText.setTag(2);
		clothingText.setTag(3);
		bookdText.setTag(4);
		ticketsText.setTag(5);
		lifeText.setTag(6);
		foodsText.setTag(7);
		serviceText.setTag(8);
		makeUpText.setTag(9);
		petsText.setTag(10);

		cancelText.setOnClickListener(typeClickListener);
		othersText.setOnClickListener(typeClickListener);
		elecText.setOnClickListener(typeClickListener);
		clothingText.setOnClickListener(typeClickListener);
		bookdText.setOnClickListener(typeClickListener);
		ticketsText.setOnClickListener(typeClickListener);
		lifeText.setOnClickListener(typeClickListener);
		foodsText.setOnClickListener(typeClickListener);
		serviceText.setOnClickListener(typeClickListener);
		makeUpText.setOnClickListener(typeClickListener);
		petsText.setOnClickListener(typeClickListener);

		if (isUpload)
			cancelText.setVisibility(View.GONE);
		dialog.show();
	}

	private static OnClickListener typeClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			onTypeClickListener.onTypeClick(Integer.valueOf(v.getTag().toString()));
		}
	};

	public interface OnTypeClickListener {
		public void onTypeClick(int position);
	}

	/*----- Age -----*/
	public static void showAgeDialog(Context context, OnAgeClickListener clickListener) {
		onAgeClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_age);

		TextView textView0 = (TextView) dialog.findViewById(R.id.Dialog_cancel);
		TextView textView1 = (TextView) dialog.findViewById(R.id.Dialog_others);
		TextView textView2 = (TextView) dialog.findViewById(R.id.Dialog_electronic);
		TextView textView3 = (TextView) dialog.findViewById(R.id.Dialog_clothing);
		TextView textView4 = (TextView) dialog.findViewById(R.id.Dialog_books);
		TextView textView5 = (TextView) dialog.findViewById(R.id.Dialog_tickets);
		TextView textView6 = (TextView) dialog.findViewById(R.id.Dialog_life);
		TextView textView7 = (TextView) dialog.findViewById(R.id.Dialog_foods);
		TextView textView8 = (TextView) dialog.findViewById(R.id.Dialog_services);
		TextView textView9 = (TextView) dialog.findViewById(R.id.Dialog_make_up);
		TextView textView10 = (TextView) dialog.findViewById(R.id.Dialog_pets);
		TextView textView11 = (TextView) dialog.findViewById(R.id.Dialog_textView11);
		TextView textView12 = (TextView) dialog.findViewById(R.id.Dialog_textView12);

		textView0.setOnClickListener(ageClickListener);
		textView1.setOnClickListener(ageClickListener);
		textView2.setOnClickListener(ageClickListener);
		textView3.setOnClickListener(ageClickListener);
		textView4.setOnClickListener(ageClickListener);
		textView5.setOnClickListener(ageClickListener);
		textView6.setOnClickListener(ageClickListener);
		textView7.setOnClickListener(ageClickListener);
		textView8.setOnClickListener(ageClickListener);
		textView9.setOnClickListener(ageClickListener);
		textView10.setOnClickListener(ageClickListener);
		textView11.setOnClickListener(ageClickListener);
		textView12.setOnClickListener(ageClickListener);

		dialog.show();
	}

	private static OnClickListener ageClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			onAgeClickListener.onAgeClick(((TextView) v).getText().toString());
		}
	};

	public interface OnAgeClickListener {
		public void onAgeClick(String age);
	}

	/*----- Someone -----*/
	public static void showSomeoneDialog(Context context, SomeoneRatingObject someoneRatingObject,
			OnSomeoneClickListener clickListener,String MemberID) {

		onSomeoneClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_someone);

		TextView textView0 = (TextView) dialog.findViewById(R.id.Dialog_cancel);
		TextView textView1 = (TextView) dialog.findViewById(R.id.Dialog_others);
		

		TextView idTextView = (TextView) dialog.findViewById(R.id.Dialog_someone_id);
		TextView averageTextView = (TextView) dialog.findViewById(R.id.Dialog_someone_average);
		TextView countTextView = (TextView) dialog.findViewById(R.id.Dialog_someone_count);
		TextView dealTimeTextView = (TextView) dialog.findViewById(R.id.Dialog_someone_dealTime);
		TextView registerTimeTextView = (TextView) dialog.findViewById(R.id.Dialog_someone_registerTime);

		textView0.setTag(0);
		textView1.setTag(1);


		textView0.setOnClickListener(someoneClickListener);
		textView1.setOnClickListener(someoneClickListener);


		idTextView.setText(MemberID);
		averageTextView.setText(": " + String.format("%.1f", someoneRatingObject.getAverage()));
		countTextView.setText(": " + someoneRatingObject.getCount());
		dealTimeTextView.setText(": " + someoneRatingObject.getDealTimes());
		registerTimeTextView.setText(": " + someoneRatingObject.getRegisterTime().split(" ")[0]);

		dialog.show();
	}

	private static OnClickListener someoneClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			onSomeoneClickListener.onSomeClick(Integer.valueOf(v.getTag().toString()));
		}
	};

	public interface OnSomeoneClickListener {
		public void onSomeClick(int position);
	}

	/*----- report -----*/
	public static void showReportDialog(Context context, OnReportSubmitClickListener clickListener) {

		onReportSubmitClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_report);

		TextView submitTextView = (TextView) dialog.findViewById(R.id.Dialog_report_submitTextView);

		submitTextView.setOnClickListener(reportClickListener);
		dialog.show();
	}

private static OnClickListener reportClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			EditText reportEditText = (EditText) dialog.findViewById(R.id.Dialog_report_submitEditText);
			onReportSubmitClickListener.onReportSubmitClick(reportEditText.getText().toString());
		}
	};

	public interface OnReportSubmitClickListener {
		public void onReportSubmitClick(String report);
	}

	/*----- chat -----*/
	public static void showChatDialog(Context context, OnChatClickListener clickListener) {

		onChatClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_chat);

		TextView submitTextView = (TextView) dialog.findViewById(R.id.Dialog_chatTextView);

		submitTextView.setOnClickListener(chatClickListener);
		dialog.show();
	}

	private static OnClickListener chatClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			EditText chatEditText = (EditText) dialog.findViewById(R.id.Dialog_chatEditText);
			onChatClickListener.onChatClick(chatEditText.getText().toString());
		}
	};

	public interface OnChatClickListener {
		public void onChatClick(String report);
	}

	/*----- hand -----*/
/*	public static void showhandDialog(Context context, String Des) {

		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_hand_des);

		TextView desTextView = (TextView) dialog.findViewById(R.id.Dialog_hand_des);
		desTextView.setText(Des);

		TextView okButton = (TextView) dialog.findViewById(R.id.Dialog_hand_ok);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		desTextView.setText(Des);
		dialog.show();
	}*/

	/*----- hand sure-----*/
	/*public static void showhandSureDialog(Context context, OnHandSureClickListener clickListener) {

		onHandSureClickListener = clickListener;

		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_handsure);

		TextView okTextView = (TextView) dialog.findViewById(R.id.Dialog_handSure_ok);
		TextView noTextView = (TextView) dialog.findViewById(R.id.Dialog_handSure_no);

		okTextView.setTag(1);
		noTextView.setTag(2);

		okTextView.setOnClickListener(handSureClickListener);
		noTextView.setOnClickListener(handSureClickListener);

		dialog.show();
	}*/

	private static OnClickListener handSureClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			if (v.getTag().equals(1)) {
				onHandSureClickListener.onHandSureClick(true);
			} else {
				onHandSureClickListener.onHandSureClick(false);
			}

		}
	};

	public interface OnHandSureClickListener {
		public void onHandSureClick(boolean b);
	}

	/*----- ChatIndex LongClick -----*/
	public static void showChatIndexLongClickDialog(Context context, OnChatIndexLongClickListener clickListener) {

		onChatIndexLongClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_chatindex_longclick);

		TextView textView1 = (TextView) dialog.findViewById(R.id.Dialog_others);

		textView1.setTag(1);

		textView1.setOnClickListener(chatIndexClickListener);

		dialog.show();
	}

	private static OnClickListener chatIndexClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			onChatIndexLongClickListener.onChatIndexLongClick(Integer.valueOf(v.getTag().toString()));
		}
	};

	public interface OnChatIndexLongClickListener {
		public void onChatIndexLongClick(int position);
	}

	/*----- check -----*/
	/*public static void showCheckDialog(Context context, String Des) {

		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_check_upload);

		TextView desTextView = (TextView) dialog.findViewById(R.id.Dialog_check_des);
		desTextView.setText(Des);

		TextView okButton = (TextView) dialog.findViewById(R.id.Dialog_check_ok);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		desTextView.setText(Des);
		dialog.show();
	}*/

	/*----- rating -----*/
	private static RatingBar ratingBar;
	private static EditText ratingEditText;

	public static void showRatingDialog(Context context, int score, String comment, OnRatingClickListener clickListener) {

		onRatingClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_rating);

		ratingBar = (RatingBar) dialog.findViewById(R.id.Dialog_rating_RatingBar);
		ratingEditText = (EditText) dialog.findViewById(R.id.Dialog_rating_editText);
		TextView textView0 = (TextView) dialog.findViewById(R.id.Dialog_rating_ok);

		ratingBar.setProgress(score);
		ratingEditText.setText(comment);

		textView0.setOnClickListener(ratingOkClickListener);

		dialog.show();
	}

	private static OnClickListener ratingOkClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			onRatingClickListener.onRatingClick((int) ratingBar.getRating(), ratingEditText.getText().toString());
		}
	};

	public interface OnRatingClickListener {
		public void onRatingClick(int rating, String comm);
	}

	/*----- change PWD -----*/
	private static EditText changeEditText;
	private static EditText changeRpEditText;

	public static void showChangePWDDialog(final Context context, OnChangePWDClickListener clickListener) {

		onChangePWDClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_changepwd);

		changeEditText = (EditText) dialog.findViewById(R.id.Dialog_changeEditText);
		changeRpEditText = (EditText) dialog.findViewById(R.id.Dialog_changeRpEditText);
		TextView textView0 = (TextView) dialog.findViewById(R.id.Dialog_changeDoneTextView);

		textView0.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkForm(context, changeEditText.getText().toString(), changeRpEditText.getText().toString())) {
					dialog.dismiss();
					onChangePWDClickListener.onChangePWDClick(changeEditText.getText().toString());
				}
			}
		});

		dialog.show();
	}

	public interface OnChangePWDClickListener {
		public void onChangePWDClick(String pwd);
	}
	
	public static void showChangeLocDialog(final Context context, OnChangeLocClickListener clickListener) {

		onChangeLocClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_changeloc);

	
		TextView textView0 = (TextView) dialog.findViewById(R.id.Dialog_changeDoneTextView);

		textView0.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
					dialog.dismiss();
					onChangeLocClickListener.onChangeLocClick();
				
			}
		});

		dialog.show();
	}

	public interface OnChangeLocClickListener {
		public void onChangeLocClick();
	}
	
	
	public static void showChangeStoreFlowDialog(final Context context, OnChangeStroeFlowClickListener clickListener) {

		onChangeStoreFlowClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_changestoreflow);

	
		TextView textView0 = (TextView) dialog.findViewById(R.id.Dialog_changeDoneTextView);
		TextView cancel = (TextView) dialog.findViewById(R.id.btn_cancel);

		textView0.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
					dialog.dismiss();
					onChangeStoreFlowClickListener.onChangeStoreFlowClick();
				
			}
		});
		
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
					dialog.dismiss();
				
			}
		});

		dialog.show();
	}

	public interface OnChangeStroeFlowClickListener {
		public void onChangeStoreFlowClick();
	}
	
	
	
	
	
	

	private static boolean checkForm(Context context, String pwd, String rpPwd) {

		if (pwd.equals("")) {
			Toast.makeText(context, "Password input error", Toast.LENGTH_SHORT).show();
		} else if (rpPwd.equals("")) {
			Toast.makeText(context, context.getString(R.string.updatepwd_checkPasswordError_title), Toast.LENGTH_SHORT)
					.show();
		} else if (!pwd.equals(rpPwd)) {
			Toast.makeText(context, context.getString(R.string.updatepwd_checkPasswordError_message),
					Toast.LENGTH_SHORT).show();
		} else {
			return true;
		}
		return false;
	}

	/*----- rating -----*/





	public interface OnAllPlayClickListener {
		public void onAllPlayClick(int position);
	}

	/*----- search -----*/
	public static void showSearchDialog(Context context, OnSearchClickListener clickListener) {

		onSearchClickListener = clickListener;
		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_search);

		TextView submitTextView = (TextView) dialog.findViewById(R.id.Dialog_searchTextView);
		TextView backTextView = (TextView) dialog.findViewById(R.id.Dialog_backTextView);

		backTextView.setTag("back");
		submitTextView.setTag("search");
		backTextView.setOnClickListener(searchClickListener);
		submitTextView.setOnClickListener(searchClickListener);
		dialog.show();
	}

	private static OnClickListener searchClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			if (v.getTag().toString().equals("back")) {
				onSearchClickListener.onSearchClick("");
			} else {
				EditText searchEditText = (EditText) dialog.findViewById(R.id.Dialog_searchEditText);
				onSearchClickListener.onSearchClick(Util.getEditText(searchEditText));
			}
		}
	};

	public interface OnSearchClickListener {
		public void onSearchClick(String keyword);
	}

	

	/*----- update location -----*/
	/*public static void showUpdateLocationDialog(Context context, String CC) {

		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_updatelocation);

		TextView desTextView = (TextView) dialog.findViewById(R.id.Dialog_location_textView);
		desTextView.setText(CC);

		TextView okButton = (TextView) dialog.findViewById(R.id.Dialog_location_ok);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}*/

	/*----- changeLocation -----*/
	/*public static void showChangeLocationDialog(Context context, double lat, double lng, String CC, String address) {

		dialog = new Dialog(context, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_change_location);

		TextView addressTextView = (TextView) dialog.findViewById(R.id.Dialog_location_message);
		if (!address.equals("")) {
			addressTextView.setText(address);
		} else {
			addressTextView.setText(CC);
		}

		TextView okButton = (TextView) dialog.findViewById(R.id.Dialog_change_location_ok);
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}*/

	/*----- FourSquare -----*/
	/*public static ArrayList<FoursquareObject> foursquare;

	public static void showFoursquareDialog(final MainActivity activity, boolean isSell, boolean isByPost,
			boolean isByFace, final OnFoursquareDialogCallback callback) {
		// copy ArrayList
		foursquare = new ArrayList<FoursquareObject>(GlobalVariable.foursquarePoint);

		dialog = new Dialog(activity, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_dealtype);

		final CheckBox postCheckBox = (CheckBox) dialog.findViewById(R.id.dialog_dealType_checkBox1);
		final CheckBox faceCheckBox = (CheckBox) dialog.findViewById(R.id.dialog_dealType_checkBox2);
		Button okButton = (Button) dialog.findViewById(R.id.dialog_dealType_ok);
		Button cancelButton = (Button) dialog.findViewById(R.id.dialog_dealType_cancel);
		final TextView selectLocation = (TextView) dialog.findViewById(R.id.dialog_dealType_selectLocation);
		final ListView listView = (ListView) dialog.findViewById(R.id.dialog_dealType_pointlist);

		final TextView point1 = (TextView) dialog.findViewById(R.id.dialog_dealType_loc1);
		final TextView point2 = (TextView) dialog.findViewById(R.id.dialog_dealType_loc2);
		final TextView point3 = (TextView) dialog.findViewById(R.id.dialog_dealType_loc3);

		final ImageView delete1 = (ImageView) dialog.findViewById(R.id.dialog_dealType_delete1);
		final ImageView delete2 = (ImageView) dialog.findViewById(R.id.dialog_dealType_delete2);
		final ImageView delete3 = (ImageView) dialog.findViewById(R.id.dialog_dealType_delete3);
		delete1.setVisibility(View.INVISIBLE);
		delete2.setVisibility(View.INVISIBLE);
		delete3.setVisibility(View.INVISIBLE);
		
		final View postLayout = (View) dialog.findViewById(R.id.dialog_dealType_fslayout);

		if (!isSell) {
			okButton.setBackgroundResource(R.drawable.selector_style_red);
			cancelButton.setBackgroundResource(R.drawable.selector_style_red);
			selectLocation.setTextColor(activity.getResources().getColor(R.color.style_red));
			listView.setBackgroundResource(R.drawable.bg_style_red);
			postLayout.setBackgroundResource(R.drawable.bg_style_red);
		}
		listView.setVisibility(View.GONE);

		postCheckBox.setChecked(isByPost);
		faceCheckBox.setChecked(isByFace);

		if (!isByFace) {
			postLayout.setVisibility(View.GONE);
			selectLocation.setVisibility(View.GONE);
		} else {
			setLocation(point1, point2, point3, delete1, delete2, delete3);
		}

		faceCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (!isChecked) {
					postLayout.setVisibility(View.GONE);
					selectLocation.setVisibility(View.GONE);
				} else {
					selectLocation.setVisibility(View.VISIBLE);
				}
			}
		});

		delete1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				foursquare.remove(0);
				setLocation(point1, point2, point3, delete1, delete2, delete3);
			}
		});

		delete2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				foursquare.remove(1);
				setLocation(point1, point2, point3, delete1, delete2, delete3);
			}
		});

		delete3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				foursquare.remove(2);
				setLocation(point1, point2, point3, delete1, delete2, delete3);
			}
		});

		selectLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				postLayout.setVisibility(View.VISIBLE);
				activity.HTTP_foursquare("");
				activity.setOnFoursquareCallBack(new OnFoursquareCallBack() {

					@Override
					public void onFoursquareCallBack(ArrayList<FoursquareObject> list) {
						listView.setVisibility(View.VISIBLE);
						setLocation(point1, point2, point3, delete1, delete2, delete3);
						listView.setAdapter(new FoursquareAdapter(activity, GlobalVariable.foursquareList));
					}
				});
				v.setVisibility(View.GONE);
			}
		});

		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				callback.onFoursquareDialogCallback(postCheckBox.isChecked(), faceCheckBox.isChecked(), foursquare);
				dialog.dismiss();
			}
		});

		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				FoursquareObject select = GlobalVariable.foursquareList.get(position);
				FoursquareObject object = new FoursquareObject(select.getName(), select.getAddress(), select.getLat(),
						select.getLng());
				if (foursquare.size() >= 3) {
					foursquare.remove(2);
				}
				foursquare.add(object);
				setLocation(point1, point2, point3, delete1, delete2, delete3);
			}
		});

		dialog.show();
	}*/

	/*private static void setLocation(TextView point1, TextView point2, TextView point3, ImageView delete1,
			ImageView delete2, ImageView delete3) {

		point1.setVisibility(View.INVISIBLE);
		point2.setVisibility(View.INVISIBLE);
		point3.setVisibility(View.INVISIBLE);
		point1.setText("");
		point2.setText("");
		point3.setText("");
		delete1.setVisibility(View.INVISIBLE);
		delete2.setVisibility(View.INVISIBLE);
		delete3.setVisibility(View.INVISIBLE);

		for (int i = 0; i < foursquare.size(); i++) {
			if (i == 0) {
				point1.setText(foursquare.get(i).getName());
				delete1.setVisibility(View.VISIBLE);
				point1.setVisibility(View.VISIBLE);
			}
			if (i == 1) {
				point2.setText(foursquare.get(i).getName());
				delete2.setVisibility(View.VISIBLE);
				point2.setVisibility(View.VISIBLE);
			}
			if (i == 2) {
				point3.setText(foursquare.get(i).getName());
				delete3.setVisibility(View.VISIBLE);
				point3.setVisibility(View.VISIBLE);
			}
		}
	}*/

	/*public interface OnFoursquareDialogCallback {
		public void onFoursquareDialogCallback(boolean isByPost, boolean isByFace, ArrayList<FoursquareObject> list);
	}*/
}

package pipimy.others;

import pipimy.nearby.ProductObject;
import pipimy.object.*;

import java.util.ArrayList;
import android.widget.TextView;

public class Constant {
	/** =============================================================== */
	/** ======================== Amazon update ======================== */
	/** =============================================================== */
	/*
	 * public OnDetailCallSomeoneProduct onDetailCallSomeoneProduct;
	 * 
	 * public interface OnDetailCallSomeoneProduct { public void
	 * onDetailCallSomeoneProduct(String memberID, boolean isSell); }
	 */

	public static String uploadVideoPath;
	public static ArrayList<PicObject> picList = new ArrayList<PicObject>();
	public static ArrayList<VideoHistoryObject> videoHistry = new ArrayList<VideoHistoryObject>();
	public static ArrayList<FavoriteObject> favorList_seller = new ArrayList<FavoriteObject>();

	// public static ArrayList<MyDealObject> myDealList_seller = new
	// ArrayList<MyDealObject>();
	// public static ArrayList<MyDealObject> myDealList_buyer = new
	// ArrayList<MyDealObject>();
	// Rating

	// Deal
	public static ArrayList<MyDealObject> myDealList_seller = new ArrayList<MyDealObject>();
	public static ArrayList<MyDealObject> myDealList_buyer = new ArrayList<MyDealObject>();
	// Rating

	public static SomeoneRatingObject someoneRatingObject = new SomeoneRatingObject();
	public static ArrayList<SomeoneRatingDesObject> someoneRatingList = new ArrayList<SomeoneRatingDesObject>();

	public static String uploadProductID;
	public static double MIN_FREE_SPACE = 8.0 * 1024 * 1024;

	public static boolean isVideoUpLoaded = false;
	public static boolean isPicUpLoaded = false;

	public static String NowCity = "";

	/** =============================================================== */
	/** ======================== Intetn Action ======================== */
	/** =============================================================== */
	public static String ACTION_CHAT = "actionChat";
	public static String ACTION_SINGLE_PLAY = "singlePlay";
	public static String ACTION_ALL_PLAY = "ALLPlay";

	/** =============================================================== */
	/** ========================== Preference Data ==================== */
	/** =============================================================== */
	public static final String COOKIE1 = "crazy[1]";
	public static final String COOKIE2 = "crazy[2]";
	public static final String COOKIE3 = "crazy[3]";
	public static final String COOKIE4 = "crazy[4]";
	public static final String COOKIE5 = "crazy[5]";

	public final static String REFRESH_TIME = "userRefreshTime";
	public final static String USER_PIPIMY = "userPipimy";
	public final static String USER_MOBILE_NUMBER = "userMobileNumber";
	public final static String USER_LOGIN = "userLogin";
	public final static String USER_ID = "userId";
	public final static String USER_TOKEN = "userToken";
	public static final String GCM_TOKEN = "gcmToken";
	public final static String USER_LAT = "userLat";
	public final static String USER_LNG = "userLng";
	public final static String USER_CITY = "userCity";
	public final static String USER_CC = "Taiwan";
	public final static String USER_PRODUCTID = "userProductID";
	public final static String USER_NEARPRODUCT = "userNearProduct";
	public final static String USER_STORE_FLOW = "userStorFlow";
	public final static String USER_CASH_FLOW = "userCashFlow";
	public final static String USER_STOREFLOW_UPDTIME = "userStoreFlowUpdTime";
	public final static String USER_FACE_LOC1 = "userFaceLoc1";
	public final static String USER_FACE_LOC2 = "userFaceLoc2";
	public final static String USER_DELIVERY_FREE = "userDeliveryFree";
	public final static String USER_DELIVERY_FEE1 = "userDeliveryFee1";
	public final static String USER_DELIVERY_FEE2 = "userDeliveryFee2";
	public final static String USER_DELIVERY_FEE3 = "userDeliveryFee3";

	public final static String RECEIVER_NAME = "receiverName";
	public final static String MOVE_CVS_DB_DONE = "moveDBDone";

	public static boolean isGCMRegistered;
	public static boolean isInChatContent = false;
	public static boolean isSell = true;
	public static int chatBadge = 0;
	public static boolean isCertified = true;
	public static String updateOpenTime;
	public static boolean isUploadingVideo;
	public static boolean isUploadingPics;
	public static TextView badge;
	public static double userLatitude;
	public static double userLongitude;

	/** =============================================================== */
	/** ============================= Server API ====================== */
	/** =============================================================== */

	// AllPay
	// public final static String AllPayURL =
	// "http://member-stage.allpay.com.tw/App/Platform/BindAccount";
	public final static String AllPayURL = "https://member.allpay.com.tw/App/Platform/BindAccount";
	// Get Store Info
	public final static String GET_STORE_INFO = "http://logistics-stage.allpay.com.tw/Helper/GetStoreInfo";

	// AWS
	public final static String VedioServerURL = "https://d1yjq3haxfphfj.cloudfront.net/";
	public final static String PicServerURL = "https://d2pqiif4nwmp3r.cloudfront.net/";
	public final static String STORE_IMAGE_URL = "https://s3.amazonaws.com/pipimy-stores/";
	public final static String STORE_BACKGROUNDS_URL = "https://s3.amazonaws.com/pipimy-backgrounds/";

	// Domain

	//private final static String PRODUCTION = "http://www.pipimy.net/1.1/";
	//private final static String TEST = "http://54.169.1.132/1.1/";
	//private final static String DOMAIN = PRODUCTION;
	//private final static String DOMAIN = TEST;

	// /BargainProduct in chat room

	// private final static String PRODUCTION = "http://www.pipimy.net/1.1/";
   private final static String TEST = "http://54.169.1.132/1.1/";
	private final static String DOMAIN = TEST;
	
	///BargainProduct in chat room

	public final static String SET_BARGAIN_PRODUCT = DOMAIN + "BargainProduct";

	// cart
	public final static String SET_OTP_CODE = DOMAIN + "cart/verifyOTP";
	public final static String ADD_TO_CART = DOMAIN + "cart/add";
	public final static String DELETE_FROM_CART = DOMAIN + "cart/delete";
	public final static String GET_CART_LIST = DOMAIN + "cart/list";
	public final static String CHECKOUT_SHOPPING_LIST = DOMAIN + "cart/pay";
	public final static String CART_CHECK_PAY = DOMAIN + "cart/checkpay";
	public final static String GET_BUY_LIST = DOMAIN + "order/buyer-list";
	public final static String GET_SELL_LIST = DOMAIN + "order/seller-list";
	public final static String GET_ORDER_DETAIL = DOMAIN + "order/detail";
	public final static String GET_ORDER_PAY_DATA = DOMAIN + "order/paydata";
	public final static String GET_ORDER_RESOURCES = DOMAIN + "order/resource";
	public final static String SET_TRADE_COMPLETE = DOMAIN
			+ "order/confirmOrderFinish";
	public final static String SET_OTHER_LOGISTIC = DOMAIN
			+ "order/setOtherLogistics";

	// login
	public final static String LOGIN_URL = DOMAIN + "loginAndroid";
	public final static String REGISTER_URL = DOMAIN + "register";
	public final static String CHECK_EMAIL_CODE = DOMAIN + "checkEmailCode";
	public final static String FORGET_PWD = DOMAIN + "forgetPWD";
	public final static String CHECK_ID = DOMAIN + "checkID";

	// post
	public final static String GET_PRODUCTID_URL = DOMAIN + "getProductID";
	public final static String DONE_VIDEO_URL = DOMAIN + "doneVideo";
	public final static String DONE_VIDEO_URL_M = DOMAIN + "doneVideoM";
	public final static String DONE_PIC_URL = DOMAIN + "donePic";
	public final static String DONE_PIC_URL_M = DOMAIN + "donePicM";
	public final static String DONE_TEXT_SELLER_URL = DOMAIN + "doneTextSeller";
	public final static String DONE_TEXT_SELLER_URL_M = DOMAIN
			+ "doneTextSellerM";
	public final static String DONE_TEXT_M = DOMAIN + "doneTextM";
	public final static String UPDATE_STORE_FLOW = DOMAIN
			+ "trace/updateStoreFlow";
	public final static String GET_USER_STORE_FLOW = DOMAIN
			+ "trace/getStoreFlow";

	// product
	public final static String GET_PRODUCT_VIEW = DOMAIN + "canProductView";
	public final static String GET_NEAR_PRODUCT = DOMAIN + "getNearProduct";
	public final static String GET_TYPE_PRODUCT = DOMAIN + "getTypeProduct";
	public final static String GET_SEARCH_PRODUCT = DOMAIN + "getSearchProduct";
	public final static String GET_SPECIAL_SEARCH_PRODUCT = DOMAIN
			+ "getSearchProductSpecial";

	public final static String GET_SOMEONE_PRODUCT = DOMAIN
			+ "getSomeoneProduct";
	public final static String GET_SOMEONE_PRODUCT_B = DOMAIN
			+ "getSomeoneProductB";
	public final static String REPORT_PRODUCT = DOMAIN + "reportProduct";
	public final static String FAVORITE_PRODUCT = DOMAIN + "favoriteProduct";
	// track
	public static String ACTION_FANS_LIST = "actionFans";
	public static String ACTION_TRACE_LIST = "actionTrace";
	public final static String GET_FANS_LIST = DOMAIN + "trace/listTraced";
	public final static String GET_TRACK_LIST = DOMAIN + "trace/listTracing";
	public final static String POST_CHECK_TRACE = DOMAIN + "trace/checkTrace";
	public final static String POST_ADD_TRACE = DOMAIN + "trace/addTrace";
	public final static String POST_DELETE_TRACE = DOMAIN + "trace/deleteTrace";
	public final static String POST_MSG_TRIGGER = DOMAIN + "trace/pushTrigger";
	public final static String POST_MSG_TO_FANS = DOMAIN + "trace/broadcast";

	// deal
	public static String ACTION_TRANSACTION_AS_SELLER = "actionTransactionAsSeller";
	public static String ACTION_TRANSACTION_AS_BUYER = "actionTransactionAsBuyer";

	// rating
	public final static String GET_PRODUCT_RATING = DOMAIN + "getProductRating";
	public final static String GET_SOMEONE_RATING = DOMAIN + "getSomeoneRating";
	public final static String SET_ORDER_RATING = DOMAIN + "rating/pushOrderRating";
	public final static String GET_BUYER_RATING = DOMAIN + "rating/getbuyerRating";
	public final static String GET_SELLER_RATING = DOMAIN + "rating/getSellerRating";

	// chat
	public final static String GET_CHAT_INDEX = DOMAIN + "push/getChatIndex";
	public final static String GET_CHAT_CONTENT = DOMAIN
			+ "push/getChatContent";
	public final static String SEND_CHAT_CONTENT = DOMAIN
			+ "push/sendChatContent";
	public final static String START_CHAT_CONTENT = DOMAIN
			+ "push/startChatContent";
	public final static String DELETE_CHAT_INDEX = DOMAIN
			+ "push/deleteChatIndex";
	public final static String CLEAN_CHAT_BADGE = DOMAIN
			+ "push/cleanChatBadge";
	public final static String GET_ONE_PRODUCT = DOMAIN + "getOneProduct";
	public final static String SEND_CHAT_HAND = DOMAIN + "push/sendChatHand";

	// setting
	public final static String UPDATE_PWD = DOMAIN + "updatePWD";
	public final static String GET_MY_DEAL = DOMAIN + "getMyDeal";
	public final static String RATING_PRODUCT = DOMAIN + "ratingProduct";
	public final static String LOGOUT_URL = DOMAIN + "logout.php";
	// my store
	public final static String UPDATE_STORE = DOMAIN + "trace/updateStore";
	public final static String UPDATE_STORE_LOC = DOMAIN
			+ "trace/updateStoreLoc";
	public final static String GET_STORE = DOMAIN + "trace/getStore";
	// my product
	public final static String GET_MY_PRODUCT = DOMAIN + "getMyPostProduct";
	public final static String GET_MY_PRODUCT_DELETE = DOMAIN
			+ "getMyProductDelete";
	public final static String GET_MY_PRODUCT_UPDATE = DOMAIN
			+ "getMyProductUpdate";

	// others
	public final static String UPDATE_OPEN_TIME = DOMAIN
			+ "updateOpenTimesAndroid";
	public final static String FOURSQUARE = "https://api.foursquare.com/v2/venues/search";
	public final static String FOURSQUARE_CLIENTID = "U1EUBZX5BY3FJ45OR5RNBOU1HTX22CXZYQGUJYXMWOWB0MD2";
	public final static String FOURSQUARE_SECRETID = "1ZVSLDCM4DG15DOEEJAB0CXE0JHTHSCUVDOIEGNRW2QB4TLJ";

	// GCM
	public final static String UPDATE_ANDROID_TOKEN = DOMAIN
			+ "push/updateAndroidToken";

	// Setting fragment
	public static boolean LOGIN_SUCCESS = false;

	// Product fragment
	public static boolean GO_SOMEONE_PRODUCT = false;
	public static String SOMEONE_MEMBER_ID = "";

	/** =============================================================== */
	/** ============================= Request Code ====================== */
	/** =============================================================== */

	// APIProxy method codes 10000~~~~
	public final static int CODE_RETURN_ERR = 10000;
	public final static int CODE_GET_PRODUCTID = 10001;
	public final static int CODE_GET_NEARPRODUCT = 10002;
	public final static int CODE_GET_SEARCHPRODUCT = 10003;
	public final static int CODE_CLEAN_CHAT_BADGE = 10004;
	public final static int CODE_GET_CHAT_INDEX = 10005;
	public final static int CODE_GET_CART_INDEX = 10006;
	public final static int CODE_CHECKOUT_SHOPPING_LIST = 10007;
	public final static int CODE_UPD_ANDROID_TOKEN = 10008;
	public final static int CODE_ORDER_RESOURCE = 10009;
	public final static int CODE_DEL_CART_ITEM = 10010;
	public final static int CODE_SET_BARGAIN = 10011;
	public final static int CODE_GET_ONE_PRODUCT = 10012;
	public final static int CODE_GET_CHAT_CONTENT = 10013;
	public final static int CODE_SEND_CHAT_CONTENT = 10014;
	public final static int CODE_CHECK_PAY = 10015;
	public final static int CODE_PUSH_TRIGGER = 10016;
	public final static int CODE_PUSH_MSG = 10017;
	public final static int CODE_SET_OTHER_LOGISTIC = 10018;
	public final static int CODE_GET_SOLD_LIST = 10019;
	public final static int CODE_VERIFY_OTP = 10020;
	public final static int CODE_GET_BUYER_RATING = 10021;
}

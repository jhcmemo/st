package pipimy.others;

import pipimy.others.IGenericDialogUtil.PositiveBtnClickListener;
import pipimy.service.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DialogUtil {
	
	private static Dialog keywordSearchDialog; 
	private static Dialog typeSearchDialog;
	private static OnSearchClickListener onSearchClickListener;
	private static OnTypeClickListener onTypeClickListener;
	private static OnMsgInputListener onMsgInputListener;
	
	/*----- OTP input dialog -----*/
	public static void stockInputDialog(Context context, String msg, OnMsgInputListener clickListener) {
		onMsgInputListener = clickListener;
		keywordSearchDialog = new Dialog(context, R.style.selectorDialog);
		keywordSearchDialog.setContentView(R.layout.dialog_stock_input);
		
		TextView titleTextView = (TextView) keywordSearchDialog.findViewById(R.id.txt_dialog_title); 
		TextView submitTextView = (TextView) keywordSearchDialog.findViewById(R.id.Dialog_searchTextView);
		TextView messageTextView = (TextView) keywordSearchDialog.findViewById(R.id.txt_stock_count);
		String str = messageTextView.getText().toString();
		messageTextView.setText(str+msg);
		submitTextView.setTag("search");
		submitTextView.setOnClickListener(msgInputListener);
		keywordSearchDialog.show();
	}
	
	/*----- OTP input dialog -----*/
	public static void optInputDialog(Context context, String dialogTitle, OnMsgInputListener clickListener) {
		onMsgInputListener = clickListener;
		keywordSearchDialog = new Dialog(context, R.style.selectorDialog);
		keywordSearchDialog.setContentView(R.layout.dialog_opt_input);
		
		TextView titleTextView = (TextView) keywordSearchDialog.findViewById(R.id.txt_dialog_title); 
		TextView submitTextView = (TextView) keywordSearchDialog.findViewById(R.id.Dialog_searchTextView);
		titleTextView.setText(dialogTitle);
		submitTextView.setTag("search");
		submitTextView.setOnClickListener(msgInputListener);
		keywordSearchDialog.show();
	}
	
	/*----- Keyword search dialog -----*/
	public interface OnSearchClickListener {
		public void onSearchClick(int searchBy,String keyword);
	}
	
	public static void keywordSearchDialog(Context context, OnSearchClickListener clickListener) {
		onSearchClickListener = clickListener;
		keywordSearchDialog = new Dialog(context, R.style.selectorDialog);
		keywordSearchDialog.setContentView(R.layout.dialog_search);

		//TextView submitTextView = (TextView) keywordSearchDialog.findViewById(R.id.Dialog_searchTextView);
		//TextView backTextView = (TextView) keywordSearchDialog.findViewById(R.id.Dialog_backTextView);

		Button btn_search_normal =(Button) keywordSearchDialog.findViewById(R.id.btn_search_normal);
		Button btn_search_name =(Button) keywordSearchDialog.findViewById(R.id.btn_search_name);
		Button btn_search_title =(Button) keywordSearchDialog.findViewById(R.id.btn_search_title);
		Button btn_search_brand =(Button) keywordSearchDialog.findViewById(R.id.btn_search_brand);
		Button btn_search_des =(Button) keywordSearchDialog.findViewById(R.id.btn_search_des);

	

		btn_search_normal.setTag(1);
		btn_search_name.setTag(2);
		btn_search_title.setTag(3);
		btn_search_brand.setTag(4);
		btn_search_des.setTag(5);

		
		//backTextView.setTag("back");
		//submitTextView.setTag("search");
		//backTextView.setOnClickListener(searchClickListener);
		btn_search_normal.setOnClickListener(searchClickListener);
		btn_search_name.setOnClickListener(searchClickListener);
		btn_search_title.setOnClickListener(searchClickListener);
		btn_search_brand.setOnClickListener(searchClickListener);
		btn_search_des.setOnClickListener(searchClickListener);

		//submitTextView.setOnClickListener(searchClickListener);
		keywordSearchDialog.show();
	}
	
	/*----- Keyword search dialog -----*/
	public interface OnMsgInputListener {
		public void onMsgInputClick(View v, String message);
	}
	
	private static OnClickListener msgInputListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			EditText inputMsg = (EditText)keywordSearchDialog.findViewById(R.id.Dialog_searchEditText);
			keywordSearchDialog.dismiss();
			onMsgInputListener.onMsgInputClick(v, Util.getEditText(inputMsg));
		}
		
	};
	
	private static OnClickListener searchClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			keywordSearchDialog.dismiss();

			//if (v.getTag().toString().equals("back")) {

			//	onSearchClickListener.onSearchClick(0,"");
				//Log.d("search","back");

			//} else {
				//Log.d("search","no back");
				EditText searchEditText = (EditText) keywordSearchDialog.findViewById(R.id.Dialog_searchEditText);

				switch(Integer.valueOf(v.getTag().toString())){
				case 1:

					onSearchClickListener.onSearchClick(1,Util.getEditText(searchEditText));

					break;
				
				case 2:
					onSearchClickListener.onSearchClick(2,Util.getEditText(searchEditText));

					break;
				case 3:
					onSearchClickListener.onSearchClick(3,Util.getEditText(searchEditText));
					break;
				case 4:
					onSearchClickListener.onSearchClick(4,Util.getEditText(searchEditText));
					break;
				case 5:
					onSearchClickListener.onSearchClick(5,Util.getEditText(searchEditText));
					break;
				}
				
				
			}
		//}
	};
	
	/*----- Type search dialog -----*/
	public static void typeSearchDialog(Context context, boolean isUpload, OnTypeClickListener clickListener) {
		onTypeClickListener = clickListener;
		typeSearchDialog = new Dialog(context, R.style.selectorDialog);
		typeSearchDialog.setContentView(R.layout.dialog_type);

		TextView cancelText = (TextView) typeSearchDialog.findViewById(R.id.Dialog_cancel);
		LinearLayout othersText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_others);
		LinearLayout elecText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_electronic);
		LinearLayout clothingText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_clothing);
		LinearLayout bookdText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_books);
		LinearLayout ticketsText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_tickets);
		LinearLayout lifeText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_life);
		LinearLayout foodsText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_foods);
		LinearLayout serviceText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_services);
		LinearLayout makeUpText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_make_up);
		LinearLayout petsText = (LinearLayout) typeSearchDialog.findViewById(R.id.Dialog_pets);

		cancelText.setTag(0);
		othersText.setTag(1);
		elecText.setTag(2);
		clothingText.setTag(3);
		bookdText.setTag(4);
		ticketsText.setTag(5);
		lifeText.setTag(6);
		foodsText.setTag(7);
		serviceText.setTag(8);
		makeUpText.setTag(9);
		petsText.setTag(10);

		cancelText.setOnClickListener(typeClickListener);
		othersText.setOnClickListener(typeClickListener);
		elecText.setOnClickListener(typeClickListener);
		clothingText.setOnClickListener(typeClickListener);
		bookdText.setOnClickListener(typeClickListener);
		ticketsText.setOnClickListener(typeClickListener);
		lifeText.setOnClickListener(typeClickListener);
		foodsText.setOnClickListener(typeClickListener);
		serviceText.setOnClickListener(typeClickListener);
		makeUpText.setOnClickListener(typeClickListener);
		petsText.setOnClickListener(typeClickListener);

		if (isUpload)
			cancelText.setVisibility(View.GONE);
		typeSearchDialog.show();
	}

	private static OnClickListener typeClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			typeSearchDialog.dismiss();
			onTypeClickListener.onTypeClick(Integer.valueOf(v.getTag().toString()));
		}
	};

	public interface OnTypeClickListener {
		public void onTypeClick(int position);
	}
	
	/****  show dialog with given title and msg, should implement click interface ****/
	
	
	static PositiveBtnClickListener positiveBtnClickListener;
	public static void pushAlertDialog(Context mContext, String title, String msg,
			final PositiveBtnClickListener target) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

		builder.setTitle(title)
				.setMessage(msg)
				.setCancelable(false)
				.setPositiveButton("ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								target.PositiveMethod(dialog, id);
							}
						});
		AlertDialog alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}
	
	public static void pushPureDialog(Context mContext, String mAlertTitle,
			String mAlertMsg, String mPositiveMsg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

		builder.setTitle(mAlertTitle)
				.setMessage(mAlertMsg)
				.setCancelable(false)
				.setPositiveButton(mPositiveMsg,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
							}
						});

		AlertDialog alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	public static void pushGeneralDialog(Context mContext, String mAlertTitle,
			String mAlertMsg, String mPositiveMsg, String mNegativeMsg,
			final IGenericDialogUtil.IGenericBtnClickListener target) {

		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

		builder.setTitle(mAlertTitle)
				.setMessage(mAlertMsg)
				.setCancelable(false)
				.setPositiveButton(mPositiveMsg,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								target.PositiveMethod(dialog, id);
							}
						})
				.setNegativeButton(mNegativeMsg,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								target.NegativeMethod(dialog, id);
							}
						});

		AlertDialog alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}
	
	static int choiceId = 0;
	public static void pushSingleChoiceDialog(Context mContext, String[] choice,
			String mPositiveMsg, String mNegativeMsg, final IGenericDialogUtil.PositiveBtnClickListener target) {
		choiceId = 0;
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setSingleChoiceItems(choice, 0,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						choiceId = which;
					}
				})
				.setPositiveButton(mPositiveMsg,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								target.PositiveMethod(dialog, choiceId);
							}
						});

		AlertDialog alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}
}

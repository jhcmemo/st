package pipimy.others;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pipimy.cart.CVSObject;
import pipimy.cart.CheckFeeObject;
import pipimy.chat.ChatContentObject;
import pipimy.chat.ChatIndexObject;
import pipimy.main.ApplicationController;
import pipimy.nearby.ProductObject;
import pipimy.object.CartItemsObject;
import pipimy.object.MyDealObject;
import pipimy.object.ProductRatingDesObject;
import pipimy.object.ProductRatingObject;
import pipimy.object.SomeoneRatingDesObject;
import pipimy.object.SomeoneRatingObject;
import pipimy.object.TrackObject;
import pipimy.order.OrderObject;
import pipimy.order.OrderResourceObject;
import pipimy.order.OrderUtil;
import pipimy.setting.activity.RatingObject;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.Log;

public class JSONParserTool {
	
	public static void getBoughtRatingHistoryList(String resultString, ArrayList<RatingObject> list) throws JSONException {
		
		list.clear();
		
		JSONArray jsonArray = new JSONArray(resultString);
		for(int i = 0; i<jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			RatingObject obj = new RatingObject();
			
			obj.setRatingComment(jsonObject.getString("ratingComment"));
			obj.setRatingOrderId(jsonObject.getString("orderId"));
			obj.setRatingScore(jsonObject.getString("ratingScore"));
			obj.setRatingTime(jsonObject.getString("ratingTime"));
			obj.setRatingTradeNo(jsonObject.getString("tradeNo"));
			obj.setSellerId(jsonObject.getString("seller_user_id"));
			
			list.add(obj);
		}
	}

	public static CheckFeeObject checkPay(String resultString)
			throws JSONException {
		CheckFeeObject obj = new CheckFeeObject();

		JSONObject jsonobject = new JSONObject(resultString);
		String[] aryStrFees = new String[7];
		JSONObject feeObject = jsonobject.getJSONObject("storeDeliveryFees");
		for (int i = 0; i < 7; i++) {
			DecimalFormat df = new DecimalFormat("0");
			aryStrFees[i] = feeObject.getString(df.format(Math.pow(2, i)));
		}

		obj.setProductsAmount(jsonobject.getString("productsAmount"));
		obj.setDeliveryCost(jsonobject.getString("deliveryCost"));
		obj.setFreeFee(jsonobject.getString("storeDeliveryFree"));
		obj.setDeliveryFees(aryStrFees);
		obj.setTotalAmountFee(jsonobject.getString("amount"));

		return obj;
	}

	public static ProductObject getOneProduct(String resultString)
			throws JSONException {
		ProductObject obj = new ProductObject();

		JSONObject jsonobject = new JSONObject(resultString);
		obj.setProductID(jsonobject.getString("productId"));
		obj.setID(jsonobject.getString("ID"));
		obj.setTitle(jsonobject.getString("title"));
		obj.setType(jsonobject.getString("type"));
		obj.setPrice(jsonobject.getString("price"));
		obj.setDes(jsonobject.getString("des"));
		obj.setLat(Double.valueOf(jsonobject.getString("lat")));
		obj.setLng(Double.valueOf(jsonobject.getString("lng")));
		obj.setTime(jsonobject.getString("time"));
		obj.setFavorite(Integer.valueOf(jsonobject.getString("favorite")));
		obj.setPicNumber(Integer.valueOf(jsonobject.getString("picNumber")));
		obj.setCC(jsonobject.getString("cc"));
		obj.setPayment(Integer.valueOf(jsonobject.getString("payment")));
		obj.setDelivery(Integer.valueOf(jsonobject.getString("delivery")));
		obj.setStock(jsonobject.getString("stock"));
		obj.setHide(jsonobject.getString("hide"));
		obj.setParentID(jsonobject.getString("parentId"));
		obj.setSno(jsonobject.getString("sno"));
		
		if (jsonobject.getString("brand").equals("null")) {
			obj.setBrand("");
		} else {
			obj.setBrand(jsonobject.getString("brand"));
		}
		
		if (jsonobject.getString("newold").equals("null")) {
			obj.setNewOld(1);
		} else {
			obj.setNewOld(Integer.valueOf(jsonobject.getString("newold")));
		}
		

		return obj;
	}

	public static void getOrderResource(String resultString,
			HashMap<String, String> map, ArrayList<OrderResourceObject> list)
			throws JSONException {
		JSONObject obj = new JSONObject(resultString);

		JSONArray itemAry = new JSONArray(obj.getString("items"));
		JSONObject orderObj = obj.getJSONObject("order");
		JSONObject paymentObj = obj.getJSONObject("payment");
		JSONObject logisticObj = obj.getJSONObject("logistics");

		map.clear();

		map.put("sum", orderObj.getString("sum"));

		if (paymentObj.getString("isHand").equals("1")) {
			map.put("PaymentIsHand", "1");
		} else {
			map.put("PaymentIsHand", "0");
			if (paymentObj.has("PaymentType")) {
				map.put("PaymentType", paymentObj.getString("PaymentType"));
				if (paymentObj.getString("PaymentType").contains("ATM")) {
					map.put("BankCode", paymentObj.getString("BankCode"));
					map.put("vAccount", paymentObj.getString("vAccount"));
					map.put("ExpireDate", paymentObj.getString("ExpireDate"));
				} else if (paymentObj.getString("PaymentType").contains("CVS")) {
					map.put("ExpireDate", paymentObj.getString("ExpireDate"));
					map.put("PaymentNo", paymentObj.getString("PaymentNo"));
				} else if (paymentObj.getString("PaymentType").contains(
						"Credit")) {
					map.put("TradeDate", paymentObj.getString("TradeDate"));
				}
			}

		}

		String receiveNo;
		if (logisticObj.getString("CVSValidationNo").equals("")) {
			receiveNo = logisticObj.getString("CVSPaymentNo");
		} else {
			receiveNo = logisticObj.getString("CVSPaymentNo")
					+ logisticObj.getString("CVSValidationNo");
		}

		if (logisticObj.getString("isHand").equals("1")) {
			map.put("LogisticIsHand", "1");
		} else {
			map.put("LogisticIsHand", "0");
			map.put("CVSPaymentNo", receiveNo);
			map.put("ReceiverName", logisticObj.getString("ReceiverName"));
			map.put("ReceiverCellPhone",
					logisticObj.getString("ReceiverCellPhone"));
			map.put("receiverStoreID", logisticObj.getString("receiverStoreID"));
		}

		list.clear();
		for (int i = 0; i < itemAry.length(); i++) {
			JSONObject jsonObject = itemAry.getJSONObject(i);
			OrderResourceObject resObj = new OrderResourceObject();
			resObj.setItemAmount(jsonObject.getString("amount"));
			resObj.setItemPrice(jsonObject.getString("price"));
			resObj.setItemTitle(jsonObject.getString("title"));
			list.add(resObj);
		}
	}

	/* ----- get CVS store info ----- */
	public static void getStoreInfoList(String resultString,
			ArrayList<CVSObject> list) throws JSONException {

		list.clear();
		JSONArray jsonArray = new JSONArray(resultString);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			CVSObject obj = new CVSObject();
			obj.setStoreAddress(jsonObject.getString("Store_Addr"));
			obj.setStoreClose(jsonObject.getString("Store_Close"));
			obj.setStoreId(jsonObject.getString("Store_id"));
			obj.setStoreName(jsonObject.getString("Store_Name"));
			obj.setStorePhone(jsonObject.getString("Store_Phone"));

			list.add(obj);
		}

	}

	/* ----- get my bought/sold detail ----- */
	public static void getMyboughtDetail(String resultString,
			HashMap<String, String> map) throws JSONException {

		JSONObject jsonObject = new JSONObject(resultString);

		map.clear();
		map.put("id", jsonObject.getString("id"));
		map.put("order_id", jsonObject.getString("order_id"));
		map.put("product_id", jsonObject.getString("product_id"));
		map.put("title", jsonObject.getString("title"));
		map.put("price", jsonObject.getString("price"));
		map.put("des", jsonObject.getString("des"));
		map.put("amount", jsonObject.getString("amount"));
		map.put("lat", jsonObject.getString("lat"));
		map.put("lng", jsonObject.getString("id"));

	}

	/* ----- get my bought/sold list paydata ----- */
	public static void getMyboughtPayData(String resultString,
			HashMap<String, String> map) throws JSONException {
		JSONObject jsonObject = new JSONObject(resultString);

		map.clear();
		map.put("id", jsonObject.getString("id"));
		map.put("MerchantID", jsonObject.getString("MerchantID"));
		map.put("MerchantTradeNo", jsonObject.getString("MerchantTradeNo"));
		map.put("RtnCode", jsonObject.getString("RtnCode"));
		map.put("RtnMsg", jsonObject.getString("RtnMsg"));
		map.put("TradeNo", jsonObject.getString("TradeNo"));
		map.put("TradeAmt", jsonObject.getString("TradeAmt"));
		map.put("PaymentType", jsonObject.getString("PaymentType"));
		map.put("TradeDate", jsonObject.getString("TradeDate"));
		map.put("created_at", jsonObject.getString("created_at"));
		map.put("BankCode", jsonObject.getString("BankCode"));
		map.put("vAccount", jsonObject.getString("vAccount"));
		map.put("ExpireDate", jsonObject.getString("ExpireDate"));
	}

	/* ----- get my bought/sold list items ----- */
	public static void getMyboughtListIndex(String resultString,
			ArrayList<OrderObject> list) throws JSONException {
		JSONArray jsonArray = new JSONArray(resultString);
		list.clear();

		// Log.e("getMyboughtListIndex", "size ="+jsonArray.length());
		for (int i = 0; i < jsonArray.length(); i++) {
			OrderObject obj = new OrderObject();
			JSONObject jsonObject = jsonArray.getJSONObject(i);

			obj.setDeliveryFee(jsonObject.getString("logisticsCost"));
			obj.setOrderID(jsonObject.getString("id"));
			obj.setOrderTradeNo(jsonObject.getString("tradeNo"));
			obj.setSellerID(jsonObject.getString("seller_user_id"));
			obj.setOrderStatus(jsonObject.getString("status"));
			obj.setSumOfThisOrder(jsonObject.getString("sum"));
			obj.setOrderPaymentType(jsonObject.getString("paymentType"));
			obj.setOrderSubPaymentType(jsonObject.getString("subPaymentType"));
			obj.setOrderLogistics(jsonObject.getString("logisticsType"));
			obj.setOrderSubLogistics(jsonObject.getString("subLogisticsType"));
			obj.setOrderCreateTime(jsonObject.getString("created_at"));
			obj.setPaymentIsFace(jsonObject.getString("paymentIsHand"));
			obj.setLagisticsIsFace(jsonObject.getString("logisticsIsHand"));
			obj.setPending(jsonObject.getString("pending"));
			obj.setQueue(jsonObject.getString("queue"));
			obj.setPaymentStatus(jsonObject.getString("paymentStatus"));
			obj.setLogisticsStatus(jsonObject.getString("logisticsStatus"));
			obj.setLogisticsDate3(jsonObject.getString("logisticsDate3"));
			obj.setLogisticsDate4(jsonObject.getString("logisticsDate4"));
			obj.setLogisticsDate5(jsonObject.getString("logisticsDate5"));
			obj.setLogisticsDate6(jsonObject.getString("logisticsDate6"));
			obj.setPaymentDate(jsonObject.getString("paymentDate"));
			obj.setPendingExpireDate(jsonObject.getString("pendingExpireDate"));
			
			boolean statusOfBtn2 = OrderUtil.isShippingBtnActivate(jsonObject.getString("logisticsType"), 
					jsonObject.getString("paymentIsHand"));
			boolean statusOfBtn3 = OrderUtil.isReceiverBtnActivate(jsonObject.getString("id"),
					jsonObject.getString("paymentType"), jsonObject.getString("paymentStatus"), 
					jsonObject.getString("pending"));
			obj.setShippingBtnActive(statusOfBtn2);
			obj.setReceiverBtnActive(statusOfBtn3);
			
			if(!jsonObject.getString("status").equals("null")) {
				boolean dateOfBtn1 = OrderUtil.isDisplayDate1(jsonObject.getString("status"));
				if(dateOfBtn1) {
					obj.setDisplayTime1(true);
					obj.setDate1(jsonObject.getString("paymentDate"));
				}
				boolean dateOfBtn2 = OrderUtil.isDisplayDate2(jsonObject.getString("status"));
				if(dateOfBtn2) {
					obj.setDisplayTime2(true);
					obj.setDate2(jsonObject.getString("logisticsDate3"));
				}

			}
			
			JSONArray itemArray = new JSONArray(jsonObject.getString("items"));
			String productTitle = "";
			for (int j = 0; j < itemArray.length(); j++) {
				String item = itemArray.getString(j);
				productTitle = productTitle + item.toString() + ";";
			}
			obj.setOrderProductTitle(productTitle);

			list.add(obj);
		}

		Collections.reverse(list);
	}

	/* ----- get my shopping cart items ----- */
	public static void getNewShoppingCart(ArrayList<CartItemsObject> list) {

		ArrayList<String> storeCount = new ArrayList<String>();
		HashMap<String, Integer> map = new HashMap<String, Integer>();

		for (int i = 0; i < list.size(); i++) {
			if (!storeCount.contains(list.get(i).getSellerID())) {
				storeCount.add(list.get(i).getSellerID());
			}
		}

		Collections.sort(storeCount, new Comparator<String>() {

			@Override
			public int compare(String lhs, String rhs) {
				return lhs.compareTo(rhs);
			}
		});

		Collections.sort(list, new Comparator<CartItemsObject>() {

			@Override
			public int compare(CartItemsObject lhs, CartItemsObject rhs) {
				return lhs.getSellerID().compareTo(rhs.getSellerID());
			}
		});

		int[] sellerProductCount = new int[storeCount.size()];
		for (int index = 0; index < list.size(); index++) {
			String sellerID = list.get(index).getSellerID();
			if (!map.containsKey(sellerID)) {
				map.put(sellerID, index);
			}
			for (int sellerIndex = 0; sellerIndex < storeCount.size(); sellerIndex++) {
				if (sellerID.equals(storeCount.get(sellerIndex))) {
					sellerProductCount[sellerIndex] = sellerProductCount[sellerIndex] + 1;
				}
			}
		}

		ApplicationController.cartStoreCount = storeCount;
		ApplicationController.cartStoreProductCount = sellerProductCount;
		ApplicationController.cartStoreProductMap = map;
	}

	public static void getMyShoppingCart(String resultString,
			ArrayList<CartItemsObject> list) throws JSONException {

		JSONArray jsonArray = new JSONArray(resultString);
		list.clear();

		ArrayList<String> storeCount = new ArrayList<String>();
		HashMap<String, Integer> map = new HashMap<String, Integer>();

		for (int i = 0; i < jsonArray.length(); i++) {
			CartItemsObject obj = new CartItemsObject();
			JSONObject jsonObject = jsonArray.getJSONObject(i);

			obj.setSellerID(jsonObject.getString("store_user_id"));
			obj.setDeliveryType(jsonObject.getString("delivery"));
			obj.setCashType(jsonObject.getString("cash"));
			obj.setFavoriteCount(Integer.parseInt(jsonObject
					.getString("favorite")));
			obj.setPostTime(jsonObject.getString("onlineTime"));
			obj.setProductDes(jsonObject.getString("des"));
			obj.setProductID(jsonObject.getString("id"));
			obj.setProductPrice(jsonObject.getString("price"));
			obj.setProductTitle(jsonObject.getString("title"));
			obj.setProductType(jsonObject.getString("type"));
			obj.setDisplayType("product");
			obj.setProductStock(jsonObject.getString("stock"));
			obj.setProductPicUrl(Constant.PicServerURL
					+ jsonObject.getString("id") + "-pic1.jpg");
			obj.setIsHide(jsonObject.getString("hide"));
			if (jsonObject.getString("hide").equals("4")) {
				String parentProductID = jsonObject.getString("parentId");
				obj.setProductPicUrl(Constant.PicServerURL + parentProductID
						+ "-pic1.jpg");
			}

			if (!storeCount.contains(jsonObject.getString("store_user_id"))) {
				storeCount.add(jsonObject.getString("store_user_id"));
				// map.put(jsonObject.getString("store_user_id"), i);
			}

			list.add(obj);
		}

		Collections.sort(storeCount, new Comparator<String>() {

			@Override
			public int compare(String lhs, String rhs) {
				return lhs.compareTo(rhs);
			}
		});

		Collections.sort(list, new Comparator<CartItemsObject>() {

			@Override
			public int compare(CartItemsObject lhs, CartItemsObject rhs) {
				return lhs.getSellerID().compareTo(rhs.getSellerID());
			}
		});

		int[] sellerProductCount = new int[storeCount.size()];
		for (int index = 0; index < list.size(); index++) {
			String sellerID = list.get(index).getSellerID();
			if (!map.containsKey(sellerID)) {
				map.put(sellerID, index);
			}
			for (int sellerIndex = 0; sellerIndex < storeCount.size(); sellerIndex++) {
				if (sellerID.equals(storeCount.get(sellerIndex))) {
					sellerProductCount[sellerIndex] = sellerProductCount[sellerIndex] + 1;
				}

			}

		}

		ApplicationController.cartStoreCount = storeCount;
		ApplicationController.cartStoreProductCount = sellerProductCount;
		ApplicationController.cartStoreProductMap = map;

	}

	/* ----- get one product ----- */
	public static void getOneProduct(String resultString,
			ProductObject productObject) throws JSONException {

		JSONObject jsonObject = new JSONObject(resultString);

		productObject.setID(jsonObject.getString("ID"));
		productObject.setProductID(jsonObject.getString("productId"));
		productObject.setTitle(jsonObject.getString("title"));
		productObject.setType(jsonObject.getString("type"));
		productObject.setPrice(jsonObject.getString("price"));
		productObject.setDes(jsonObject.getString("des"));
		productObject.setLat(jsonObject.getDouble("lat"));
		productObject.setLng(jsonObject.getDouble("lng"));
		productObject.setTime(jsonObject.getString("time"));
		productObject.setFavorite(jsonObject.getInt("favorite"));
		productObject.setPicNumber(jsonObject.getInt("picNumber"));
		productObject.setCC(jsonObject.getString("cc"));
		productObject.setDelete((jsonObject.getInt("delete")));

		productObject.setDelivery(Integer.valueOf(jsonObject.getString(
				"delivery").trim()));

		/*
		 * String loc1 = jsonObject.getString("loc1"); if (!loc1.equals("") &&
		 * !loc1.equals("null")) productObject.setF1(new
		 * FoursquareObject(loc1.split(".@")[0], loc1.split(".@")[1],
		 * Double.valueOf(loc1 .split(".@")[2]),
		 * Double.valueOf(loc1.split(".@")[3])));
		 * 
		 * String loc2 = jsonObject.getString("loc2"); if (!loc2.equals("") &&
		 * !loc2.equals("null")) productObject.setF2(new
		 * FoursquareObject(loc2.split(".@")[0], loc2.split(".@")[1],
		 * Double.valueOf(loc2 .split(".@")[2]),
		 * Double.valueOf(loc2.split(".@")[3])));
		 * 
		 * String loc3 = jsonObject.getString("loc3"); if (!loc3.equals("") &&
		 * !loc3.equals("null")) productObject.setF3(new
		 * FoursquareObject(loc3.split(".@")[0], loc3.split(".@")[1],
		 * Double.valueOf(loc3 .split(".@")[2]),
		 * Double.valueOf(loc3.split(".@")[3])));
		 */
	}

	/* ------- Chat Content -------- */
	public static void getChatContent(String JSONString,
			ArrayList<ChatContentObject> list) throws JSONException {

		JSONArray jsonArray = new JSONArray(JSONString);
		list.clear();
		for (int i = 0; i < jsonArray.length(); i++) {

			ChatContentObject object = new ChatContentObject();

			JSONObject jsonObject = jsonArray.getJSONObject(i);
			object.setIsDate(false);
			object.setContent(jsonObject.getString("content"));
			object.setIsFrom(jsonObject.getString("isFrom"));
			object.setSendTime(jsonObject.getString("sendTime"));
			object.setType(jsonObject.getString("type"));
			list.add(i, object);
		}

		Collections.reverse(list);
	}

	/* -------- Chat Index --------- */
	public static void getChatIndex(String JSONString,
			ArrayList<ChatIndexObject> list_seller,
			ArrayList<ChatIndexObject> list_buyer) throws JSONException {

		JSONArray jsonArray = new JSONArray(JSONString);
		list_seller.clear();
		list_buyer.clear();
		for (int i = 0; i < jsonArray.length(); i++) {

			ChatIndexObject object = new ChatIndexObject();

			JSONObject jsonObject = jsonArray.getJSONObject(i);
			object.setChatId(jsonObject.getString("chatId"));
			object.setUserIDFrom(jsonObject.getString("userIDFrom"));
			object.setUserIDTo(jsonObject.getString("userIDTo"));
			object.setProductId(jsonObject.getString("productId"));
			object.setProductTitle(jsonObject.getString("productTitle"));
			object.setUpdateTime(jsonObject.getString("updateTime"));
			object.setUpdateTime(jsonObject.getString("updateTime"));
			object.setLastSentence(jsonObject.getString("lastSentence"));
			object.setUserIDFromBadge(jsonObject.getInt("userIDFromBadge"));
			object.setUserIDToBadge(jsonObject.getInt("userIDToBadge"));
			object.setUserIDFromHand(jsonObject.getInt("userIDFromHand"));
			object.setUserIDToHand(jsonObject.getInt("userIDToHand"));

			String memberID = PreferenceUtil.getString(
					ApplicationController.getAppContext(), Constant.USER_ID);
			if (object.getUserIDFrom().equals(memberID)) {
				list_buyer.add(object);
			} else {
				list_seller.add(object);
			}
		}
	}

	/* ----- get store info ----- */
	public static HashMap<String, String> getStore(String JSONString)
			throws JSONException {
		HashMap<String, String> info = new HashMap<String, String>();

		JSONObject jsonObject = new JSONObject(JSONString);

		Log.d("store","store intro :"+jsonObject.getString("storeIntro"));
		
		info.put("storeCity", jsonObject.getString("storeCity"));
		info.put("storeName", jsonObject.getString("storeName"));
		info.put("storeIntro", jsonObject.getString("storeIntro"));
		info.put("storeType", jsonObject.getString("storeType"));
		info.put("average", String.valueOf(jsonObject.getInt("average")));
		info.put("count", String.valueOf(jsonObject.getInt("count")));

		return info;
	}

	/* ----- get trace list ----- */
	public static ArrayList<TrackObject> getTraceList(String JSONString)
			throws JSONException {
		JSONArray jsonArray = new JSONArray(JSONString);

		ArrayList<TrackObject> dst = new ArrayList<TrackObject>();

		for (int i = 0; i < jsonArray.length(); i++) {
			int pic[] = new int[3];

			TrackObject temp = new TrackObject();
			temp.setIsTrackingMsg(jsonArray.getJSONObject(i).getString("push"));
			temp.setMemberID(jsonArray.getJSONObject(i).getString("tracingId"));
			temp.setTime(jsonArray.getJSONObject(i).getString("addTime"));

			for (int y = 0; y < jsonArray.getJSONObject(i)
					.getJSONArray("productIDs").length(); y++) {
				pic[y] = jsonArray.getJSONObject(i).getJSONArray("productIDs")
						.getInt(y);
				Log.d("json", "pic[y] :" + pic[y]);
			}

			temp.setProduct_pic(pic);

			dst.add(i, temp);
		}

		return dst;
	}

	/* ----- get product rating ----- */
	public static void getProductRating(String resultString,
			ProductRatingDesObject ratingDesObject,
			ArrayList<ProductRatingObject> list) throws JSONException {

		list.clear();
if(resultString!=null){
		JSONArray jsonArray = new JSONArray(resultString);

		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject jsonObject = jsonArray.getJSONObject(i);
			if (i == (jsonArray.length() - 1)) {

				ratingDesObject.setCount(jsonObject.getInt("count"));
				ratingDesObject.setAverage(jsonObject.getDouble("average"));
				ratingDesObject.setDealTimes(jsonObject.getString("dealTimes"));

			} else {

				ProductRatingObject object = new ProductRatingObject();
				object.setUserIdFrom(jsonObject.getString("userIDFrom"));
				object.setScore(jsonObject.getInt("score"));
				object.setComment(jsonObject.getString("comment"));
				object.setRatingTime(jsonObject.getString("ratingTime"));
				object.setDealTime(jsonObject.getString("dealTime"));
				list.add(object);
			}
		}
}
		
		
		
	}

	/* ------- My deal -------- */
	public static void getMyDeal(String JSONString,
			ArrayList<MyDealObject> list_seller,
			ArrayList<MyDealObject> list_buyer, String MemberID)
			throws JSONException {

		JSONArray jsonArray = new JSONArray(JSONString);
		list_seller.clear();
		list_buyer.clear();
		for (int i = 0; i < jsonArray.length(); i++) {

			MyDealObject object = new MyDealObject();

			JSONObject jsonObject = jsonArray.getJSONObject(i);
			object.setUserIDFrom(jsonObject.getString("userIDFrom"));
			object.setUserIDTo(jsonObject.getString("userIDTo"));
			object.setProductId(jsonObject.getString("productId"));
			object.setProductTitle(jsonObject.getString("productTitle"));
			object.setProductPrice(jsonObject.getString("productPrice"));
			object.setTime(jsonObject.getString("time"));
			object.setRatingScore(jsonObject.getInt("ratingScore"));
			object.setRatingComment(jsonObject.getString("ratingComment"));

			if (object.getUserIDFrom().equals(MemberID)) {
				list_buyer.add(object);
			} else {
				list_seller.add(object);
			}
		}
	}

	/* ----- get Someone rating ----- */
	public static void getSomeoneRating(String resultString,
			SomeoneRatingObject ratingDesObject,
			ArrayList<SomeoneRatingDesObject> list) throws JSONException {

		list.clear();

		JSONArray jsonArray = new JSONArray(resultString);

		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject jsonObject = jsonArray.getJSONObject(i);
			if (i == (jsonArray.length() - 1)) {

				ratingDesObject.setCount(jsonObject.getInt("count"));
				ratingDesObject.setAverage(jsonObject.getDouble("average"));
				ratingDesObject.setDealTimes(jsonObject.getString("dealTimes"));
				ratingDesObject.setRegisterTime(jsonObject
						.getString("registerTime"));
				ratingDesObject.setOpenTimes(jsonObject.getInt("openTimes"));
				ratingDesObject.setLastOpenTime(jsonObject
						.getString("lastOpenTime"));
			} else {

				SomeoneRatingDesObject object = new SomeoneRatingDesObject();
				object.setUserIdFrom(jsonObject.getString("userIDFrom"));
				object.setScore(jsonObject.getInt("score"));
				object.setComment(jsonObject.getString("comment"));
				object.setRatingTime(jsonObject.getString("ratingTime"));
				object.setDealTime(jsonObject.getString("dealTime"));
				list.add(object);
			}
		}

	}

	/* ----- get fans list ----- */
	public static ArrayList<TrackObject> getFansList(String JSONString)
			throws JSONException {
		JSONArray jsonArray = new JSONArray(JSONString);
		ArrayList<TrackObject> dst = new ArrayList<TrackObject>();

		for (int i = 0; i < jsonArray.length(); i++) {
			TrackObject temp = new TrackObject();
			temp.setMemberID(jsonArray.getJSONObject(i).getString("memberId"));
			temp.setTime(jsonArray.getJSONObject(i).getString("addTime"));
			dst.add(i, temp);
		}

		return dst;
	}

	public static void getNearProductList(String JSONString,
			ArrayList<ProductObject> list, boolean isEdit) throws JSONException {

		JSONArray jsonArray = new JSONArray(JSONString);
		list.clear();
		for (int i = 0; i < jsonArray.length(); i++) {

			ProductObject productObject = new ProductObject();

			JSONObject jsonObject = jsonArray.getJSONObject(i);
			productObject.setProductID(jsonObject.getString("productId"));
			productObject.setID(jsonObject.getString("ID"));
			// productObject.setContact(jsonObject.getString("contact"));
			productObject.setTitle(jsonObject.getString("title"));
			productObject.setType(jsonObject.getString("type"));
			productObject.setPrice(jsonObject.getString("price"));
			productObject.setDes(jsonObject.getString("des"));
			productObject.setLat(jsonObject.getDouble("lat"));
			productObject.setLng(jsonObject.getDouble("lng"));
			productObject.setTime(jsonObject.getString("time"));
			productObject.setFavorite(jsonObject.getInt("favorite"));
			productObject.setPicNumber(jsonObject.getInt("picNumber"));
			productObject.setCC(jsonObject.getString("cc"));
			productObject.setStock(jsonObject.getString("stock"));
			productObject.setProductPicUrl(Constant.PicServerURL
					+ jsonObject.getString("productId") + "-pic1.jpg");

			if (jsonObject.getString("delivery").trim().equals("null")) {
				productObject.setDelivery(0);

			} else {
				productObject.setDelivery(Integer.valueOf(jsonObject.getString(
						"delivery").trim()));
			}

			if (jsonObject.getString("payment").trim().equals("null")) {
				productObject.setPayment(0);
			} else {
				productObject.setPayment(Integer.valueOf(jsonObject.getString(
						"payment").trim()));

			}

			if (jsonObject.getString("brand").trim().equals("null")) {
				productObject.setBrand("");
			} else {
				productObject.setBrand(jsonObject.getString("brand").trim());

			}
			if (jsonObject.getString("newold").trim().equals("null")) {
				productObject.setNewOld(1);
			} else {
				productObject.setNewOld(Integer.valueOf(jsonObject.getString(
						"newold").trim()));

			}
			
			if (jsonObject.getString("sno").trim().equals("null") ||jsonObject.getString("sno").trim().equals("")) {
				productObject.setSNO("");
			} else {
				productObject.setSNO(jsonObject.getString(
						"sno").trim());

			}
			// productObject.setDelivery(Integer.valueOf(jsonObject.getString("delivery").trim()));
			// productObject.setPayment(Integer.valueOf(jsonObject.getString("payment").trim()));

			productObject.setSellerPicUrl(Constant.STORE_IMAGE_URL
					+ jsonObject.getString("ID") + "-s.jpg");

			/*
			 * String loc1 = jsonObject.getString("loc1");
			 * productObject.setF1(loc1);
			 * 
			 * if (!loc1.equals("") && !loc1.equals("null"))
			 * productObject.setF1(new FoursquareObject(loc1.split(".@")[0],
			 * loc1.split(".@")[1], Double.valueOf(loc1 .split(".@")[2]),
			 * Double.valueOf(loc1.split(".@")[3])));
			 * 
			 * String loc2 = jsonObject.getString("loc2"); if (!loc2.equals("")
			 * && !loc2.equals("null")) productObject.setF2(new
			 * FoursquareObject(loc2.split(".@")[0], loc2.split(".@")[1],
			 * Double.valueOf(loc2 .split(".@")[2]),
			 * Double.valueOf(loc2.split(".@")[3])));
			 * 
			 * String loc3 = jsonObject.getString("loc3"); if (!loc3.equals("")
			 * && !loc3.equals("null")) productObject.setF3(new
			 * FoursquareObject(loc3.split(".@")[0], loc3.split(".@")[1],
			 * Double.valueOf(loc3 .split(".@")[2]),
			 * Double.valueOf(loc3.split(".@")[3])));
			 */
			if (isEdit) {
				productObject.setIsBuyer(jsonObject.getInt("isBuyer"));
			}

			if (jsonObject.has("order")) {
				productObject.setOrder(jsonObject.getInt("order"));
			}

			float[] results = new float[1];
			SharedPreferences settings = ApplicationController.getAppContext()
					.getSharedPreferences(Constant.USER_PIPIMY, 0);
			double endLatitude = Double.parseDouble(settings.getString(
					Constant.USER_LAT, ""));
			double endLongitude = Double.parseDouble(settings.getString(
					Constant.USER_LNG, ""));

			Location.distanceBetween(productObject.getLat(),
					productObject.getLng(), endLatitude, endLongitude, results);
			productObject.setDistance(results[0]);

			list.add(productObject);
		}
	}
}

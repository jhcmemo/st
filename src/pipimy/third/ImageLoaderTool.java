package pipimy.third;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class ImageLoaderTool {

	public static DisplayImageOptions MemoryOptions = new DisplayImageOptions.Builder()
	// .showImageOnFail(R.drawable.ic_launcher)
	//
			.resetViewBeforeLoading(true)
			//
			.cacheOnDisk(false)
			//
			.cacheInMemory(true)
			//
			.imageScaleType(ImageScaleType.EXACTLY)
			//
			.bitmapConfig(Bitmap.Config.RGB_565)
			//
			.considerExifParams(true)
			//
			.displayer(new FadeInBitmapDisplayer(0))
			//
			.build();

	public static DisplayImageOptions DickOptions = new DisplayImageOptions.Builder()
	// .showImageOnFail(R.drawable.ic_launcher)
	//
			.resetViewBeforeLoading(true)
			//
			.cacheOnDisk(true)
			//
			.cacheInMemory(true)
			//
			.imageScaleType(ImageScaleType.EXACTLY)
			//
			.bitmapConfig(Bitmap.Config.RGB_565)
			//
			.considerExifParams(true)
			//
			.displayer(new FadeInBitmapDisplayer(0))
			//
			.build();
}

/*
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package pipimy.third;

public class Constants {
	// You should replace these values with your own

	// 690990735402
	public static final String AWS_ACCOUNT_ID = "252079443835";
	// us-east-1:011d0647-8517-491a-a8c2-c7b2150d239f
	public static final String COGNITO_POOL_ID = "us-east-1:b43a25bf-34a0-4465-964c-68a0159413e6";
	// arn:aws:iam::690990735402:role/Cognito_SmallSellerUnauth_DefaultRole
	public static final String COGNITO_ROLE_UNAUTH = "arn:aws:iam::252079443835:role/Cognito_SmallTradeCognitoUnauth_DefaultRole";
	// arn:aws:iam::690990735402:role/Cognito_SmallSellerAuth_DefaultRole
	public static final String COGNITO_ROLE_AUTH = "arn:aws:iam::252079443835:role/Cognito_SmallTradeCognitoAuth_DefaultRole";
	//smalltradepic
	public static final String BUCKET_NAME_PIC = "pipimy-pics";
	//smalltradevideo
	public static final String BUCKET_NAME_VIDEO = "pipimy-videos";
	//My store
	public static final String BUCKET_NAME_STORE = "pipimy-stores";
	
	public static final String BUCKET_NAME_STORE_BG = "pipimy-backgrounds";

}

package pipimy.order;

import pipimy.service.R;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;

public class OrderListActivity extends FragmentActivity implements
		ActionBar.TabListener {

	final static String TAG = "OrderListActivity";
	final static int GET_BUY_LIST = 10001;

	ActionBar actionbar;
	MFragmentPagerAdapter mFragmentPagerAdapter;
	ViewPager mViewPager;

	int currentFragment;
	boolean isGetBoughtListDone;
	boolean isGetSoldListDone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_list);

		actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		initViewPagers();
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction arg1) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction arg1) {
		mViewPager.setCurrentItem(tab.getPosition());
		currentFragment = tab.getPosition();
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {

	}

	private void initViewPagers() {
		// TODO Auto-generated method stub
		mFragmentPagerAdapter = new MFragmentPagerAdapter(
				getSupportFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mFragmentPagerAdapter);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionbar.setSelectedNavigationItem(position);
						currentFragment = position;
					}
				});

		// Add tabs to the action bar
		actionbar.addTab(actionbar.newTab()
				.setText(getResources().getString(R.string.tab_txt_buy_list))
				.setTabListener(this));
		actionbar.addTab(actionbar.newTab()
				.setText(getResources().getString(R.string.tab_txt_sell_list))
				.setTabListener(this));
	}

	/**
	 * A FragmentPagerAdapter that returns a fragment
	 */
	class MFragmentPagerAdapter extends FragmentPagerAdapter {

		public MFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;

			switch (position) {
			case 0:
				Log.d("frghg", "0");
				fragment = new BuyListFragment();
				break;
			case 1:
				Log.d("frghg", "1");
				fragment = new SellListFragment();
				break;
			}

			return fragment;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			return true;
		}
	}
}

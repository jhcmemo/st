package pipimy.order;

public class OrderObject {

	String orderID;
	String orderTradeNo;
	String sellerID;
	String queue;
	String pending;
	String orderStatus;
	String paymentStatus;
	String logisticsStatus;
	String deliveryFee;
	String sumOfThisOrder;
	String orderPaymentType;
	String orderSubPaymentType;
	String orderLogistics;
	String orderSubLogistics;
	String orderCreateTime;
	String paymentIsFace;
	String lagisticsIsFace;
	String orderProductTitle;
	String paymentDate;
	String pendingExpireDate;
	String logisticsDate3;
	String logisticsDate4;
	String logisticsDate5;
	String logisticsDate6;
	String date1;
	String date2;
	String date3;
	
	boolean isReceiverBtnActive;
	boolean isShippingBtnActive;
	boolean isDisplayTime1;
	boolean isDisplayTime2;
	boolean isDisplayTime3;
	
	
	
	public String getPendingExpireDate() {
		return pendingExpireDate;
	}

	public void setPendingExpireDate(String pendingExpireDate) {
		this.pendingExpireDate = pendingExpireDate;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public String getDate3() {
		return date3;
	}

	public void setDate3(String date3) {
		this.date3 = date3;
	}

	public boolean isDisplayTime1() {
		return isDisplayTime1;
	}

	public void setDisplayTime1(boolean isDisplayTime1) {
		this.isDisplayTime1 = isDisplayTime1;
	}

	public boolean isDisplayTime2() {
		return isDisplayTime2;
	}

	public void setDisplayTime2(boolean isDisplayTime2) {
		this.isDisplayTime2 = isDisplayTime2;
	}

	public boolean isDisplayTime3() {
		return isDisplayTime3;
	}

	public void setDisplayTime3(boolean isDisplayTime3) {
		this.isDisplayTime3 = isDisplayTime3;
	}

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public boolean isReceiverBtnActive() {
		return isReceiverBtnActive;
	}

	public void setReceiverBtnActive(boolean isReceiverBtnActive) {
		this.isReceiverBtnActive = isReceiverBtnActive;
	}

	public boolean isShippingBtnActive() {
		return isShippingBtnActive;
	}

	public void setShippingBtnActive(boolean isShippingBtnActive) {
		this.isShippingBtnActive = isShippingBtnActive;
	}
	
	public String getPending() {
		return pending;
	}

	public void setPending(String pending) {
		this.pending = pending;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getLogisticsStatus() {
		return logisticsStatus;
	}

	public void setLogisticsStatus(String logisticsStatus) {
		this.logisticsStatus = logisticsStatus;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getLogisticsDate3() {
		return logisticsDate3;
	}

	public void setLogisticsDate3(String logisticsDate3) {
		this.logisticsDate3 = logisticsDate3;
	}

	public String getLogisticsDate4() {
		return logisticsDate4;
	}

	public void setLogisticsDate4(String logisticsDate4) {
		this.logisticsDate4 = logisticsDate4;
	}

	public String getLogisticsDate5() {
		return logisticsDate5;
	}

	public void setLogisticsDate5(String logisticsDate5) {
		this.logisticsDate5 = logisticsDate5;
	}

	public String getLogisticsDate6() {
		return logisticsDate6;
	}

	public void setLogisticsDate6(String logisticsDate6) {
		this.logisticsDate6 = logisticsDate6;
	}

	public String getOrderProductTitle() {
		return orderProductTitle;
	}

	public void setOrderProductTitle(String orderProductTitle) {
		this.orderProductTitle = orderProductTitle;
	}

	public String getLagisticsIsFace() {
		return lagisticsIsFace;
	}

	public void setLagisticsIsFace(String lagisticsIsFace) {
		this.lagisticsIsFace = lagisticsIsFace;
	}

	public void setPaymentIsFace(String paymentFace) {
		this.paymentIsFace = paymentFace;
	}
	
	public String getPaymentIsFace(){
		return this.paymentIsFace;
	}
	
	public String getSellerID() {
		return sellerID;
	}

	public void setSellerID(String sellerID) {
		this.sellerID = sellerID;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getDeliveryFee() {
		return deliveryFee;
	}

	public void setDeliveryFee(String deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	public String getSumOfThisOrder() {
		return sumOfThisOrder;
	}

	public void setSumOfThisOrder(String sumOfThisOrder) {
		this.sumOfThisOrder = sumOfThisOrder;
	}

	public String getOrderPaymentType() {
		return orderPaymentType;
	}

	public void setOrderPaymentType(String orderPaymentType) {
		this.orderPaymentType = orderPaymentType;
	}

	public String getOrderSubPaymentType() {
		return orderSubPaymentType;
	}

	public void setOrderSubPaymentType(String orderSubPaymentType) {
		this.orderSubPaymentType = orderSubPaymentType;
	}

	public String getOrderLogistics() {
		return orderLogistics;
	}

	public void setOrderLogistics(String orderLogistics) {
		this.orderLogistics = orderLogistics;
	}

	public String getOrderSubLogistics() {
		return orderSubLogistics;
	}

	public void setOrderSubLogistics(String orderSubLogistics) {
		this.orderSubLogistics = orderSubLogistics;
	}

	public String getOrderCreateTime() {
		return orderCreateTime;
	}

	public void setOrderCreateTime(String orderCreateTime) {
		this.orderCreateTime = orderCreateTime;
	}

	public String getOrderTradeNo() {
		return orderTradeNo;
	}

	public void setOrderTradeNo(String tradeNo) {
		this.orderTradeNo = tradeNo;
	}
	
	public String getOrderId(){
		return this.orderID;
	}
	
	public void setOrderID(String id) {
		this.orderID = id;
	}
	
	
}

package pipimy.order;

import java.util.ArrayList;
import java.util.HashMap;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import pipimy.main.ApplicationController;
import pipimy.object.CartItemsObject;
import pipimy.others.ApiProxy;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OrderDetailActivity extends Activity {

	final static String TAG = "OrderDetailActivity";

	String m_pOwnerId;
	String m_pOwnerNo;
	String m_pOwnerPaymentStatus;
	String m_pOwnerPaymentType;
	boolean commentEnable;
	
	TextView txtOrderNo;
	TextView txtPaytype;
	TextView txtPayTotal;
	TextView txtReceiverName;
	TextView txtReceiverTel;
	TextView txtReceiverStoreId;
	TextView txtReceiverNo;
	
	ListView listView;
	LinearLayout paymentLayout;
	RelativeLayout bottomLayout;
	Button commentBtn;
	
	ImageLoader mImageLoader;
	RequestQueue queue;
	
	float orderRating;
	String orderComment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_detail);
		
		queue = VolleySingleton.getInstance().getRequestQueue();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		
		paymentLayout = (LinearLayout)findViewById(R.id.layout_order_paytype);
		bottomLayout = (RelativeLayout)findViewById(R.id.bottom_layout);
		txtOrderNo = (TextView)findViewById(R.id.txt_order_no);
		txtPaytype = (TextView)findViewById(R.id.txt_pay_type);
		txtPayTotal = (TextView)findViewById(R.id.txt_pay_money);
		txtReceiverName = (TextView)findViewById(R.id.txt_receive_name);
		txtReceiverTel = (TextView)findViewById(R.id.txt_receive_tel);
		txtReceiverStoreId = (TextView)findViewById(R.id.txt_receive_store);
		txtReceiverNo = (TextView)findViewById(R.id.txt_receive_no);
		commentBtn = (Button)findViewById(R.id.btn_comment_order);
		listView = (ListView)findViewById(R.id.listView1);
		
		commentBtn.setOnClickListener(onItemClickListener);
		
		Bundle params = getIntent().getExtras();
		if (params != null) {
			m_pOwnerId = params.getString("OrderID");
			m_pOwnerNo = params.getString("OrderNo");
			m_pOwnerPaymentStatus = params.getString("paymentStatus");
			m_pOwnerPaymentType = params.getString("paymentType");
			Log.e(TAG, "PaymentStatus = "+m_pOwnerPaymentStatus+ " PaymentType = "+m_pOwnerPaymentType);
			if(m_pOwnerPaymentStatus.equals("1") && !m_pOwnerPaymentType.equals("16")) {
				commentEnable = true;
				bottomLayout.setVisibility(View.VISIBLE);
			} else {
				commentEnable = false;
				bottomLayout.setVisibility(View.GONE);
			}
				
			Log.e(TAG, "OrderID = " + m_pOwnerId);
			getData();
		}

		getActionBar().setDisplayHomeAsUpEnabled(true);
		
	}
	
	private OnClickListener onItemClickListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			showCommentDialog();
		}};

	private void getData() {
		Util.showProgressDialog(this, "wait", "loading data");
		apiRequest(Constant.CODE_ORDER_RESOURCE);
	}

	
	protected void showCommentDialog() {
		Log.e(TAG, "showCommentDialog BEGIN");
		final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		final Dialog dialog = new Dialog(this, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_set_comment);
		dialog.show();
		
		RatingBar ratingBar_score = (RatingBar)dialog.findViewById(R.id.ratingBar_score);
		final EditText txt_comment = (EditText)dialog.findViewById(R.id.txt_comment);
		
		ratingBar_score.setOnRatingBarChangeListener(new OnRatingBarChangeListener(){
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				orderRating = ratingBar.getRating();
			}});
		
		
		TextView btnConfirm = (TextView) dialog.findViewById(R.id.btn_confirm);
		btnConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				dialog.dismiss();
				orderComment = Util.getEditText(txt_comment);
				pushOrderRating(orderRating, orderComment);
			}
		});
		TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				dialog.dismiss();
			}
		});
		
	}

	protected void pushOrderRating(final float rating, final String comment) {
		
		new Thread() {
			@Override
			public void run() {
				String[] params = new String[]{"orderID", "ratingScore", "ratingComment"};
				String[] data = new String[]{m_pOwnerId, Float.toString(rating), comment};
				String result = Http.post(Constant.SET_ORDER_RATING, params, data, OrderDetailActivity.this);
				Log.e(TAG, "pushOrderRating result = "+result);
			}
		}.start();
	}


	private void apiRequest(final int code) {
		new Thread() {
			@Override
			public void run() {
				switch(code) {
				case Constant.CODE_ORDER_RESOURCE:
					Message msg = ApiProxy.getOrderResources(m_pOwnerId, OrderDetailActivity.this);
					handler.sendMessage(msg);
					break;
				
				}
			}
		}.start();
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initView() {
		
		txtOrderNo.setText(m_pOwnerId);
		
		HashMap<String, String> map = ApplicationController.orderResources;
		
		txtPayTotal.setText(map.get("sum"));
		if(map.get("PaymentIsHand").equals("1")) {
			String str = getResources().getString(R.string.seller_cash_choose5);
			txtPaytype.setText(str);
		} else {
			if(map.containsKey("PaymentType")) {
				txtPaytype.setText(map.get("PaymentType"));
				
				if(map.get("PaymentType").contains("ATM")) {
					addATMTradeView(map.get("BankCode"), map.get("vAccount"), map.get("ExpireDate"));
				} else if(map.get("PaymentType").contains("CVS")) {
					addCVSTradeView(map.get("PaymentNo"), map.get("ExpireDate"));
				} else if(map.get("PaymentType").contains("Credit")) {
					addCreditTradeView(map.get("TradeDate"));
				}
			}
		}
		
		if(map.get("LogisticIsHand").equals("1")) {
			String str = getResources().getString(R.string.seller_cash_choose5);
			txtReceiverName.setText(str);
			txtReceiverTel.setText(str);
			txtReceiverStoreId.setText(str);
			txtReceiverNo.setText(str);
		} else {
			txtReceiverName.setText(map.get("ReceiverName"));
			txtReceiverTel.setText(map.get("ReceiverCellPhone"));
			txtReceiverStoreId.setText(map.get("receiverStoreID"));
			txtReceiverNo.setText(map.get("CVSPaymentNo"));
		}
		
		ArrayList<OrderResourceObject> list = ApplicationController.resObj;
		CommonAdapter<OrderResourceObject> adapter = 
				new CommonAdapter(this, list, R.layout.cell_checkout_list) {
			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				final OrderResourceObject obj = (OrderResourceObject)item;
				TextView txtProductTitle = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_title);
				TextView txtProductBuyCount = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.txt_product_buy_count);
				TextView txtProductPrice = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_price);
				NetworkImageView product_img = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView,
								R.id.img_product_pic);
				
				product_img.setVisibility(View.GONE);
				
				txtProductTitle.setText(obj.getItemTitle());
				txtProductBuyCount.setText(obj.getItemAmount());
				txtProductPrice.setText(obj.getItemPrice());
				
			}
		};
		
		listView.setFocusable(false);
		listView.setAdapter(adapter);
		Util.setListViewHeightBasedOnChildren(listView);
	}

	private void addCreditTradeView(String date) {
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.cell_order_detail_credit, null);
		paymentLayout.addView(v, layoutParams);
		
		TextView payDate = (TextView)v.findViewById(R.id.txt_pay_date);
		payDate.setText(date);
		
	}

	private void addCVSTradeView(String number, String date) {
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.cell_order_detain_cvstype, null);
		paymentLayout.addView(v, layoutParams);
		
		TextView txtPayExpireDate = (TextView)v.findViewById(R.id.txt_pay_date);
		TextView txtPayNo = (TextView)v.findViewById(R.id.txt_payment_number);
		txtPayExpireDate.setText(date);
		txtPayNo.setText(number);
	}

	private void addATMTradeView(String code, String account, String date) {
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.cell_order_detail_banktype, null);
		paymentLayout.addView(v, layoutParams);
		
		TextView txtPayBankCode = (TextView)v.findViewById(R.id.txt_pay_bankcode);
		TextView txtPayBankAccoun = (TextView)v.findViewById(R.id.txt_pay_backaccount);
		TextView txtPayExpireDate = (TextView)v.findViewById(R.id.txt_pay_date);
		txtPayBankCode.setText(code);
		txtPayBankAccoun.setText(account);
		txtPayExpireDate.setText(date);
		
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.arg1) {
			case Constant.CODE_ORDER_RESOURCE:
				Util.dismissProgressDialog();
				initView();
				break;
			case Constant.CODE_RETURN_ERR:
				Util.dismissProgressDialog();
				Toast.makeText(OrderDetailActivity.this, "something error", 1500).show();
				break;
			}
		}
	};

	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			overridePendingTransition(android.R.anim.slide_in_left,
					android.R.anim.slide_out_right);
			return true;

		default:
			return true;
		}
	}

}

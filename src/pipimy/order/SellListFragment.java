package pipimy.order;

import java.util.Collections;

import pipimy.main.ApplicationController;
import pipimy.others.ApiProxy;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.DialogUtil.OnMsgInputListener;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.service.R;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SellListFragment extends Fragment {

	final static String TAG = "SellListFragment";

	ListView listView;
	CommonAdapter<OrderObject> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_sell_list,
				container, false);

		listView = (ListView) rootView.findViewById(R.id.listView1);
		getSoldList();

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initViews() {
		// TODO Auto-generated method stub
		Collections.reverse(ApplicationController.soldList);
		adapter = new CommonAdapter(getActivity(),
				ApplicationController.soldList, R.layout.cell_buy_list_item) {
			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				final OrderObject obj = (OrderObject) item;

				TextView orderNo = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_order_no);
				TextView orderSumPrice = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.txt_order_total_price);
				TextView isPayInFace = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_pay_face);
				TextView isShipInFace = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_shipping_face);
				TextView isReceiveInFace = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_receive_face);
				TextView orderProduct = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_name);
				View receiverLayout = (View) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.layout_bought_receive);
				RelativeLayout shipLayout = (RelativeLayout) commonViewHolder.get(commonViewHolder,
						currentView, R.id.layout_seller_shipping);
				ImageView hint = (ImageView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.img_hint);

				orderNo.setText(obj.getOrderCreateTime());
				orderSumPrice.setText(obj.getSumOfThisOrder());
				orderProduct.setText(obj.getOrderProductTitle());

				if (!obj.getQueue().equals("null")) {
					receiverLayout.setBackgroundColor(0xffff0088);
				} else {
					receiverLayout.setBackgroundColor(0xffffffff);
				}

				if (obj.isShippingBtnActive) {
					hint.setVisibility(View.VISIBLE);
					Animation translate = AnimationUtils.loadAnimation(
							getActivity(), R.anim.up_down);
					hint.startAnimation(translate);
					shipLayout.setTag(obj.getOrderId());
					shipLayout.setEnabled(true);
					shipLayout.setOnClickListener(onItemClick);
				} else {
					hint.setVisibility(View.GONE);
					shipLayout.setEnabled(false);
				}

				if (obj.getPaymentIsFace().equals("1")) {
					isPayInFace.setVisibility(View.VISIBLE);
					isPayInFace.setTextColor(0xff00DDDD);
				} else {
					isPayInFace.setVisibility(View.GONE);
				}

				if (obj.getLagisticsIsFace().equals("1")) {
					isShipInFace.setVisibility(View.VISIBLE);
					isShipInFace.setTextColor(0xff00DDDD);
				} else {
					isShipInFace.setVisibility(View.GONE);
				}

			}

		};

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				String orderID = ApplicationController.soldList.get(position)
						.getOrderId();
				String orderNo = ApplicationController.soldList.get(position)
						.getOrderTradeNo();
				Intent i = new Intent();
				i.putExtra("OrderNo", orderNo);
				i.putExtra("OrderID", orderID);
				i.setClass(getActivity(), OrderDetailActivity.class);
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
	}

	private OnClickListener onItemClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.e(TAG, "onItemClick");
		/*	final String orderID = v.getTag().toString();
			String title = getResources().getString(R.string.dialog_str_title_set_otherLogistics);
			DialogUtil.generalInputDialog(getActivity(), title, "", new OnMsgInputListener(){
				@Override
				public void onMsgInputClick(View v, String message) {
					setOtherLogistics(orderID, message);
				}});*/
		}
	}; 
	
	private void setOtherLogistics(String id, String msg) {
		String[] params = {"orderId", "logisticsDesc"};
		String[] data = {id, msg};
		apiRequest(Constant.CODE_SET_OTHER_LOGISTIC, params, data);
	}

	private void getSoldList() {
		String[] params = {""};
		String[] data = {""};
		apiRequest(Constant.CODE_GET_SOLD_LIST, params, data);
	}
	
	private void apiRequest(final int code, final String[] params, final String[] data) {
		new Thread() {
			@Override
			public void run() {
				switch(code) {
				case Constant.CODE_GET_SOLD_LIST: {
					Message msg = ApiProxy.getSoldList(params, data, getActivity());
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_SET_OTHER_LOGISTIC: {
					Message msg = ApiProxy.setOtherLogistics(params, data, getActivity());
					handler.sendMessage(msg);
					break;
				}
				}
			}
		}.start();
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// Util.dismissProgressDialog();
			if (msg.arg1 == Constant.CODE_GET_SOLD_LIST) {
				initViews();
			} 
		}
	};

}

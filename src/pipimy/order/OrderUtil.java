package pipimy.order;

public class OrderUtil {

	public static final String paymentTypeCreditCard = "1";
	public static final String paymentTypeCode = "2";
	public static final String paymentTypeATM = "4";

	public static final String logisticTypeCustom = "4";

	public static final String negotiationUser = "1";
	public static final String negotiationPipimy = "2";

	public static final String queueStatusAskFunds = "1";
	public static final String queueStatusAskFundsNormal = "2";
	public static final String queueStatusAskFundsException = "3";

	public static final String isHand = "1";

	public static boolean isDisplayDate3(String status) {

		if (Integer.parseInt(status) >= 5) {
			return true;
		}

		return false;

	}

	public static boolean isDisplayDate2(String status) {

		if (Integer.parseInt(status) >= 3) {
			return true;
		}

		return false;

	}

	public static boolean isDisplayDate1(String status) {

		if (Integer.parseInt(status) >= 2) {
			return true;
		}

		return false;

	}

	public static boolean isShippingBtnActivate(String logisticsType,
			String paymentIsHand) {

		if (!paymentIsHand.equals("isHand")) {
			if (logisticsType.equals(logisticTypeCustom))
				return true;
		}

		return false;
	}

	public static boolean isReceiverBtnActivate(String id, String paymentType,
			String paymentStatuse, String pendingStatus) {

		if (paymentType.equals(paymentTypeCreditCard) || paymentType.equals(paymentTypeCode)
				|| paymentType.equals(paymentTypeATM)) {
			if (paymentStatuse.equals("1")) {
				if (!pendingStatus.equals("-1")) {
					return true;
				}
			}
		}

		return false;
	}
}

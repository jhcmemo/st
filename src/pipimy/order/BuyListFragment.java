package pipimy.order;

import java.util.ArrayList;
import java.util.Collections;

import pipimy.main.ApplicationController;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.Http;
import pipimy.others.IGenericDialogUtil;
import pipimy.others.JSONParserTool;
import pipimy.others.Util;
import pipimy.service.R;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class BuyListFragment extends Fragment {

	final static String TAG = "BuyListFragment";
	final static int GET_BUY_LIST = 10001;
	final static int SET_TRADE_COMPLETE = 10002;

	ListView listView;
	//LinearLayout linearLayout;
	CommonAdapter<OrderObject> adapter;
	ArrayList<OrderObject> list = new ArrayList<OrderObject>();
	String completeOrderId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_buy_list, container,
				false);

		listView = (ListView) rootView.findViewById(R.id.listView1);

		getBoughtList();
		// initViews();

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}
	
	protected void confirmTradeComplete(final String id) {
		// TODO Auto-generated method stub
		Util.showProgressDialog(getActivity(), "wait", "loading");
		new Thread() {
			@Override
			public void run() {
			
				String[] params = { "orderId" };
				String[] data = { id };
				String result = Http.post(Constant.SET_TRADE_COMPLETE, params,
						data, getActivity());

				Log.e(TAG, "confirmTradeComplete res = " + result);

				Message msg = new Message();
				msg.arg1 = SET_TRADE_COMPLETE;
				msg.obj = result;
				handler.sendMessage(msg);
			}
		}.start();
	}

	private void getBoughtList() {
		Util.showProgressDialog(getActivity(), "wait", "loading");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "" };
				String[] data = { "" };
				String result = Http.get(Constant.GET_BUY_LIST, params, data,
						getActivity());

				//Log.e(TAG, "getBoughtList res = " + result);
				try {
					JSONParserTool.getMyboughtListIndex(result,
							ApplicationController.boughtList);
					// isGetBoughtListDone = true;
				} catch (Exception e) {
					e.printStackTrace();
					// isGetBoughtListDone = false;
				}

				Message msg = new Message();
				msg.arg1 = GET_BUY_LIST;
				handler.sendMessage(msg);

			}
		}.start();
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Util.dismissProgressDialog();
			if(msg.arg1 == GET_BUY_LIST) {
				initViews();
			} else if(msg.arg1 == SET_TRADE_COMPLETE) {
				String res = (String)msg.obj;
				if(res.contains("success")) {
					refreshBoughtList();
					initViews();
				} else {
					String message = "Something error, you can try again later";
					Toast.makeText(getActivity(), message, 1500).show();
				}
				
			}
		}
	};

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initViews() {

		adapter = new CommonAdapter(getActivity(),
				ApplicationController.boughtList, R.layout.cell_buy_list_item) {
			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				final OrderObject obj = (OrderObject) item;

				RelativeLayout tradeComplete = (RelativeLayout) commonViewHolder
						.get(commonViewHolder, currentView,
								R.id.layout_bought_receive);
				TextView orderNo = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_order_no);
				TextView orderSumPrice = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.txt_order_total_price);
				TextView isPayInFace = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_pay_face);
				TextView isShipInFace = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_shipping_face);
				TextView isReceiveInFace = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_receive_face);
				TextView orderProduct = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_name);
				TextView txt_pay_time = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_pay_time);
				TextView txt_shipping_time = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_shipping_time);

				orderNo.setText(obj.getOrderCreateTime());
				orderSumPrice.setText(obj.getSumOfThisOrder());
				orderProduct.setText(obj.getOrderProductTitle());
				tradeComplete.setTag(obj.getOrderId());
				
				if(obj.isReceiverBtnActive) {
					tradeComplete.setEnabled(true);
					tradeComplete.setOnClickListener(itemClickListener);
				} else {
					tradeComplete.setEnabled(false);
					if(obj.getPending().equals("-1")) {
						tradeComplete.setBackgroundColor(0xffffbb00);
					}
				}
				
				if(obj.isDisplayTime1) {
					txt_pay_time.setText(obj.getDate1());
					txt_pay_time.setVisibility(View.VISIBLE);
				} else txt_pay_time.setVisibility(View.GONE);
				
				if(obj.isDisplayTime2) {
					txt_shipping_time.setText(obj.getDate2());
					txt_shipping_time.setVisibility(View.VISIBLE);
				} else txt_shipping_time.setVisibility(View.GONE);
			
				if(obj.getPaymentIsFace().equals("1") && obj.getLagisticsIsFace().equals("1")) {
					isPayInFace.setVisibility(View.VISIBLE);
					isPayInFace.setTextColor(0xff00DDDD);
					isShipInFace.setVisibility(View.VISIBLE);
					isShipInFace.setTextColor(0xff00DDDD);
					isReceiveInFace.setVisibility(View.VISIBLE);
					isReceiveInFace.setTextColor(0xff00DDDD);
				} else {
					isPayInFace.setVisibility(View.GONE);
					isShipInFace.setVisibility(View.GONE);
					isReceiveInFace.setVisibility(View.GONE);
				}

			}

		};

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				
				Intent i = new Intent();
				i.putExtra("OrderNo", ApplicationController.boughtList.get(position)
						.getOrderTradeNo());
				i.putExtra("OrderID", ApplicationController.boughtList.get(position)
						.getOrderId());
				i.putExtra("paymentStatus", ApplicationController.boughtList.get(position).getPaymentStatus());
				i.putExtra("paymentType", ApplicationController.boughtList.get(position).getOrderPaymentType());
				i.setClass(getActivity(), OrderDetailActivity.class);
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);

				//showOrderInfo(ApplicationController.boughtList.get(position));
			}
		});

	}
	
	protected void showOrderInfo(OrderObject obj) {
		// TODO Auto-generated method stub
		String msg = "id = "+obj.getOrderId() + "\n" + "paymentStatus = " + obj.getPaymentStatus() + "\n" + 
		"pending = " + obj.getPending() + "\n" + "pendingDate = " + obj.getPendingExpireDate() + "\n" +
		"logisticsStatus = "+obj.getLogisticsStatus() + "\n" + "paymentType = "+obj.getOrderPaymentType() + "\n" + 
		"logisticsType = " +obj.getOrderLogistics() + "\n" + "paymentDate = "+obj.getPaymentDate() + "\n" + 
		"logisticsDate3 = " +obj.getLogisticsDate3() + "\n" + "logisticsDate4 = " +obj.getLogisticsDate4() + 
		"\n" + "logisticsDate5 = " +obj.getLogisticsDate5() + "\n" + "logisticsDate6 = " +obj.getLogisticsDate6();
		
		DialogUtil.pushPureDialog(getActivity(), "title", msg, "ok");

	}

	protected void refreshBoughtList() {
		// TODO Auto-generated method stub
		//completeOrderId
		ArrayList<OrderObject> list = new ArrayList<OrderObject>();
		for(OrderObject obj : ApplicationController.boughtList) {
			if(obj.getOrderId().equals(completeOrderId)) {
				obj.setPending("-1");
				obj.setReceiverBtnActive(false);
			}
			list.add(obj);
		}
		
	//	ArrayList<OrderObject> list;// = ApplicationController.boughtList;
		//for()
		ApplicationController.boughtList.clear();
		ApplicationController.boughtList.addAll(list);
		adapter.notifyDataSetChanged();
	}

	private OnClickListener itemClickListener = new OnClickListener() {
		@Override
		public void onClick(final View v) {
			final String orderid = (String)v.getTag();
			completeOrderId = orderid;
			String title = getResources().getString(R.string.dailog_title_hint);
			String msg = getResources().getString(
					R.string.dialog_str_msg_received_complete);
			Log.e(TAG, "confirmTradeComplete id = "+orderid);
			DialogUtil.pushGeneralDialog(getActivity(), title, msg, "ok",
					"cancel",
					new IGenericDialogUtil.IGenericBtnClickListener() {
						@Override
						public void PositiveMethod(DialogInterface dialog,
								int id) {
							confirmTradeComplete(orderid);
						}

						@Override
						public void NegativeMethod(DialogInterface dialog,
								int id) {
						}
					});
		}
	};

	
}
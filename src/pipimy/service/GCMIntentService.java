package pipimy.service;

import java.util.List;

import pipimy.main.LunchActivity;
import pipimy.main.MainActivity;
import pipimy.others.Constant;
import pipimy.others.PreferenceUtil;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.LauncherActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";

	public GCMIntentService() {
		super(CommonUtilities.SENDER_ID);
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.e(TAG, "Device registered: regId = " + registrationId);
		Constant.isGCMRegistered = true;
		CommonUtilities.displayMessage(context, registrationId);
		String prefToken = PreferenceUtil.getString(this, Constant.USER_TOKEN);
		if (!registrationId.equals(prefToken)) {
			PreferenceUtil.setString(this, Constant.USER_TOKEN, registrationId);
		}
	}

	/**
	 * Method called on device unregistred
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		// Log.e(TAG, "Device unregistered");
		CommonUtilities.displayMessage(context,
				getString(R.string.gcm_unregistered));
	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.e(TAG, "Received message");
		
		//Bundle b = intent.getExtras();
		//String id = b.getString(CommonUtilities.CHAT_ID);

		try {
			/* --------------- get Data --------------- */
			String chatId = intent.getStringExtra(CommonUtilities.CHAT_ID);
			if(!chatId.equals("-2")) {
				String message = intent
						.getStringExtra(CommonUtilities.CHAT_MESSAGE);
				String isFrom = intent.getStringExtra(CommonUtilities.CHAT_ISFROM);
				String sendTime = intent
						.getStringExtra(CommonUtilities.CHAT_SENDTIME);
				String vibrate = intent
						.getStringExtra(CommonUtilities.CHAT_VIBRATE);
				String sound = intent.getStringExtra(CommonUtilities.CHAT_SOUND);

				/* ---------- check app is Running ---------- */
				ActivityManager am = (ActivityManager) context
						.getSystemService(Context.ACTIVITY_SERVICE);
				List<RunningTaskInfo> list = am.getRunningTasks(100);
				boolean isAppRunning = false;
				String MY_PKG_NAME = getPackageName();

				for (RunningTaskInfo info : list) {
					if (
					// info.topActivity.getPackageName().equals(MY_PKG_NAME)
					// ||
					info.baseActivity.getPackageName().equals(MY_PKG_NAME)) {
						isAppRunning = true;
						// Log.i(TAG, info.topActivity.getPackageName() +
						// " info.baseActivity.getPackageName()="
						// + info.baseActivity.getPackageName());
						break;
					}
				}
				/* ------------- send Broadcast ------------- */
				if (isAppRunning) {

					if (!Constant.isInChatContent || isFrom.equals("3")
							|| isFrom.equals("4")) {

						Intent popupIntent = new Intent("GCMPopup");
						popupIntent.putExtra("message", message);
						popupIntent.putExtra("isFrom", isFrom);
						startService(popupIntent);
					}
					CommonUtilities.displayMessage_chat(context, message, chatId,
							isFrom, sendTime, vibrate, sound);

				} else {
					generateNotification(context, message);
				}
			} else {
				String traceID = intent.getStringExtra(CommonUtilities.TRACE_ID);
				String msg = intent.getStringExtra(CommonUtilities.CHAT_MESSAGE);
				Log.e(TAG, "receive my tracking seller msg traceID = "+traceID+ " msg = "+msg);
				generatePushMsgNotification(context, msg, traceID);
			}
			

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		// Log.i(TAG, "Received deleted messages notification");
		String message = getString(R.string.gcm_deleted, total);
		CommonUtilities.displayMessage(context, message);
		// notifies user
		generateNotification(context, message);
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
		// Log.i(TAG, "Received error: " + errorId);
		CommonUtilities.displayMessage(context,
				getString(R.string.gcm_error, errorId));
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		// Log.i(TAG, "Received recoverable error: " + errorId);
		CommonUtilities.displayMessage(context,
				getString(R.string.gcm_recoverable_error, errorId));
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String message) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(context, MainActivity.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify(0, notification);
	}
	
	private void generatePushMsgNotification(Context context, String msg,
			String traceID) {
		/*
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, msg, when);
		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(context, LauncherActivity.class);
		//notificationIntent.putExtra("traceID", traceID);
		//notificationIntent.putExtra("msg", msg);
		//notificationIntent.putExtra("isFromTracer", true);
		// set intent so it does not start a new activity
		//notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
			//	| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, msg, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify(0, notification);
		*/
		
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).
				setContentTitle(context.getString(R.string.app_name))
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentText(msg);

		mBuilder.setTicker(msg);
		mBuilder.setAutoCancel(true);

		Intent resultIntent ;
		resultIntent = new Intent(this, LunchActivity.class);
		resultIntent.putExtra("traceID", traceID);
		resultIntent.putExtra("msg", msg);
		resultIntent.putExtra("isFromTracer", true);
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
				0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		mBuilder.setDefaults(Notification.DEFAULT_ALL);

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, mBuilder.build());
	}
}

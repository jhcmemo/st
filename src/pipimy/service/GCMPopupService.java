package pipimy.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class GCMPopupService extends Service {
	private Handler handler = new Handler();
	private String message;
	private String isFrom;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {
		if (intent != null) {
			this.message = intent.getStringExtra("message");
			handler.post(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
				}
			});

			this.isFrom = intent.getStringExtra("isFrom");
			if (isFrom.equals("3") || isFrom.equals("4")) {
				SoundPool soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 5);
				final int soundID = soundPool.load(this, R.raw.hand, 1);
				soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {

					@Override
					public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {

						soundPool.play(soundID, 1f, 1f, 0, 0, 1);
					}
				});
			}
		}
		// stopService(intent);

		return super.onStartCommand(intent, flags, startId);
	}
}

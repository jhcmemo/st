package pipimy.service;

import android.content.Context;
import android.content.Intent;

public final class CommonUtilities {

	// Google project id
	public static final String SENDER_ID = "765100822787";

	static final String TAG = "GCM";

	public static final String DISPLAY_MESSAGE_ACTION = "DISPLAY_MESSAGE";
	public static final String EXTRA_MESSAGE = "message";

	// for chat
	public static final String DISPLAY_MESSAGE_CHAT = "DISPLAY_MESSAGE_CHAT";
	public static final String CHAT_MESSAGE = "message";
	public static final String CHAT_ID = "chatId";
	public static final String CHAT_ISFROM = "isFrom";
	public static final String CHAT_SENDTIME = "sendTime";
	public static final String CHAT_VIBRATE = "vibrate";
	public static final String CHAT_SOUND = "sound";
	public static final String TRACE_ID = "userId";

	/*---------- get RegId message ----------*/
	static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}

	/*---------- get chat message ----------*/
	static void displayMessage_chat(Context context, String message, String chatId, String isFrom, String sendTime,
			String vibrate, String sound) {
		Intent intent = new Intent(DISPLAY_MESSAGE_CHAT);
		intent.putExtra(CHAT_MESSAGE, message);
		intent.putExtra(CHAT_ID, chatId);
		intent.putExtra(CHAT_ISFROM, isFrom);
		intent.putExtra(CHAT_SENDTIME, sendTime);
		intent.putExtra(CHAT_VIBRATE, vibrate);
		intent.putExtra(CHAT_SOUND, sound);
		context.sendBroadcast(intent);
	}
}

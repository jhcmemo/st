package pipimy.service;

import android.content.Context;

public final class ServerUtilities {
	static final String TAG = "GCM";

	private static final int MAX_ATTEMPTS = 5;

	/**
	 * Register this account/device pair within the server.
	 * 
	 */
	public static void register(final Context context, final String regId) {
		String message = context.getString(R.string.server_register_error, MAX_ATTEMPTS);
		CommonUtilities.displayMessage(context, message);
	}
}
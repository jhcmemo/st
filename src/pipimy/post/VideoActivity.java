package pipimy.post;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import pipimy.others.Constant;
import pipimy.others.PreferenceUtil;
import pipimy.service.R;
import android.R.color;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore.Video;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class VideoActivity extends Activity implements SurfaceHolder.Callback,
		SensorEventListener, AutoFocusCallback {

	// Camera
	private MediaPlayer mediaPlayer;
	private Camera camera;
	private MediaRecorder mediarecorder;
	public SensorManager sensorManager;
	public Sensor accelerometer;
	private final int MAX_PREVIEW_SIZE = 800;
	private int previewSize_width;
	private int previewSize_height;

	private float motionX = 0;
	private float motionY = 0;
	private float motionZ = 0;

	DisplayMetrics metrics;
	SurfaceHolder surfaceHolder;
	SurfaceView surfaceView;
	View startView, playView, doneView;
	//TextView startTextView, playTextView, doneTextView, timeMaxTextView;
	ImageView preview, startImageView, playImageView, doneImageView;
	ProgressBar timeBar;
	Handler timeHandler;
	boolean isVideoRecoring = false;
	boolean isVideoFinish = false;
	boolean hasAutoFocus;
	int videoTime;

	final static String TAG = "VideoActivity";
	final static int MEMBER_TIME = 10000;
	final static int UN_MEMBER_TIME = 5000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_video);

		surfaceView = (SurfaceView) findViewById(R.id.video_surfaceView);
		startView = (View) findViewById(R.id.video_startLayou);
		playView = (View) findViewById(R.id.video_playLayou);
		doneView = (View) findViewById(R.id.video_doneLayou);

		//startTextView = (TextView) findViewById(R.id.video_startTextView);
		//playTextView = (TextView) findViewById(R.id.video_playTextView);
		//doneTextView = (TextView) findViewById(R.id.video_doneTextView);
		//timeMaxTextView = (TextView) findViewById(R.id.video_timeMax);

		preview = (ImageView) findViewById(R.id.video_video_previewImageView);
		startImageView = (ImageView) findViewById(R.id.video_startImageView);
		playImageView = (ImageView) findViewById(R.id.video_playImageView);
		doneImageView = (ImageView) findViewById(R.id.video_doneImageView);

		timeBar = (ProgressBar) findViewById(R.id.video_progressBar);

		startView.setOnClickListener(btnClickListener);
		playView.setOnClickListener(btnClickListener);
		doneView.setOnClickListener(btnClickListener);

		init();
	}

	public void onResume() {
		super.onResume();
		if (hasAutoFocus)
			sensorManager.registerListener(this, accelerometer,
					SensorManager.SENSOR_DELAY_NORMAL);
	}

	public void onPause() {
		super.onPause();

		// when app in background
		if (mediarecorder != null)
			mediarecorder.release();
		if (camera != null)
			camera.release();
 
		isVideoRecoring = false;
		//startTextView.setTextColor(0xffffffff);
		startImageView.setImageResource(R.drawable.video_start_off);

		if (hasAutoFocus)
			sensorManager.unregisterListener(this, accelerometer);

		timeHandler.removeCallbacks(timeRunnable);
		timeBar.clearAnimation();
		timeBar.setProgress(100);

		startView.setEnabled(true);
		playView.setEnabled(true);
		doneView.setEnabled(true);
	}
@Override
protected void onDestroy() {
	super.onDestroy();
	camera.release();

	
	
}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
	getActionBar().setDisplayHomeAsUpEnabled(true);
	getActionBar().setTitle(getString(R.string.post_video));
	
	return true;
	
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	case android.R.id.home:
		finish();
		return true;

	default:
		return true;

	}

}
	private void init() {
		
		// TODO Auto-generated method stub
		checkDeviceHasAutoFocus();
		//startView.setBackgroundResource(R.drawable.selector_style_cube_green);
	//	playView.setBackgroundResource(R.drawable.selector_style_cube_green);
	//	doneView.setBackgroundResource(R.drawable.selector_style_cube_green);
		timeBar.setProgressDrawable(getResources().getDrawable(
				R.drawable.video_progress_style_green));

		if (PreferenceUtil.getBoolean(this, Constant.USER_LOGIN)) {
			videoTime = MEMBER_TIME;
		} else {
			videoTime = UN_MEMBER_TIME;
		}
		//timeMaxTextView.setText("" + videoTime / 1000);

		String videoName = PreferenceUtil.getString(this,
				Constant.USER_PRODUCTID) + "-video.mp4";
		Constant.uploadVideoPath = this.getFilesDir().getAbsolutePath() + "/"
				+ videoName;
//Log.d("video","上傳Product ID: "+Constant.USER_PRODUCTID);
//Log.d("video","儲存路徑: "+Constant.uploadVideoPath);


		if (hasAutoFocus) {
			Log.e(TAG, "hasAutoFocus =" + hasAutoFocus);
			sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
			accelerometer = sensorManager
					.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		}

		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);

		timeHandler = new Handler();
		mediaPlayer = new MediaPlayer();
		File videoFile = new File(getFilesDir() + "/"
				+ Constant.uploadProductID + "-video.mp4");
		if (videoFile.exists()) {
			timeBar.setProgress(100);
			getFirstFrame(videoFile.getAbsolutePath(), preview);
			isVideoFinish = true;
		}
	}

	private void getFirstFrame(String path, ImageView preview) {
		// surfaceView Can not be reused after set Bitmap with preview ,
		// so it should be add an ImageView to do it.
		Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path,
				Video.Thumbnails.FULL_SCREEN_KIND);
		if (bitmap == null) {
			preview.setVisibility(View.GONE);
		} else {
			preview.setImageBitmap(bitmap);
		}
	}

	private void checkDeviceHasAutoFocus() {

		PackageManager packageManager = getPackageManager();
		if (packageManager
				.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)) {
			hasAutoFocus = true;
		} else {
			hasAutoFocus = false;
		}
	}

	private OnClickListener btnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.video_startLayou: {
				doVideoRecording();
				break;
			}
			case R.id.video_playLayou: {
				doPlayTheVideo();
				break;
			}
			case R.id.video_doneLayou: {
				doUploadVideo();
				break;
			}

			}
		}

	};
	
	/*
	 *  Upload the video to Amazon
	 */
	protected void doUploadVideo() {
		// TODO Auto-generated method stub
		Log.e(TAG, "doUploadVideo BEGIN");
		if (isVideoFinish) {
		//	doneTextView.setTextColor(0xffff3333);
			//doneImageView.setImageResource(R.drawable.video_done_on);
			Constant.isUploadingVideo = true;
			Intent data = new Intent();
			setResult(1, data);
			this.finish();
		} else {
			Toast.makeText(VideoActivity.this, getString(R.string.video_play_error), Toast.LENGTH_LONG).show();
		}
	}

	// play the video if video file exists
	protected void doPlayTheVideo() {
		// TODO Auto-generated method stub
		try {
			File file = new File(Constant.uploadVideoPath);
			if (file.exists()) {

				initCamera();

				// for some low-level device.
				// Do not set file path in setDataSource().
				FileInputStream fis = new FileInputStream(file);
				mediaPlayer.setDataSource(fis.getFD());
				
				
				mediaPlayer.prepare();
				mediaPlayer.setDisplay(surfaceHolder);
				mediaPlayer.setOnCompletionListener(completionListener);
				mediaPlayer.start();

			//	playTextView.setTextColor(0xffff3333);
				playImageView.setImageResource(R.drawable.video_play_on);
				preview.setVisibility(View.INVISIBLE);

				startView.setEnabled(false);
				playView.setEnabled(false);
				doneView.setEnabled(false);

				ProgressbarAnimation anim = new ProgressbarAnimation(timeBar,
						0, 100);
				anim.setDuration(videoTime);
				timeBar.startAnimation(anim);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * video player listener
	 */
	private OnCompletionListener completionListener = new OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer mp) {
			mediaPlayer.reset();
		//	playTextView.setTextColor(0xffffffff);
			playImageView.setImageResource(R.drawable.video_play_off);
			startView.setEnabled(true);
			playView.setEnabled(true);
			doneView.setEnabled(true);
		}
	};

	//
	protected void doVideoRecording() {
		// TODO Auto-generated method stub
		if (!isVideoRecoring) {
			
			initMediarecorder();
			camera.unlock();
			isVideoFinish = false;
		//	startTextView.setTextColor(0xffff3333);
			startImageView.setImageResource(R.drawable.video_start_on);
			preview.setVisibility(View.INVISIBLE);

			startView.setEnabled(false);
			playView.setEnabled(false);
			doneView.setEnabled(false);

			try {
				mediarecorder.start();
				isVideoRecoring = true;

				ProgressbarAnimation anim = new ProgressbarAnimation(timeBar,
						0, 100);
				anim.setDuration(videoTime);
				timeBar.startAnimation(anim);
				
				timeHandler.postDelayed(timeRunnable, videoTime);

			} catch (Exception e) {
				e.printStackTrace();
				isVideoFinish = false;
			}
		}
	}

	private Runnable timeRunnable = new Runnable() {

		@Override
		public void run() {
		//	Log.d("video","5秒錄影完成");
			VideoRedordFinish();
		}
	};
 
	private void VideoRedordFinish() {
		timeHandler.removeCallbacks(timeRunnable);
		if (mediarecorder != null) {
			try {
				mediarecorder.stop();
				mediarecorder.release();
				initCamera();
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		isVideoRecoring = false;
		//startTextView.setTextColor(0xffffffff);
		startImageView.setImageResource(R.drawable.video_start_off);
		startView.setEnabled(true);
		playView.setEnabled(true);
		doneView.setEnabled(true);
		isVideoFinish = true;
	}

	@SuppressWarnings("deprecation")
	private void initCamera() {
		camera.release();
		camera = Camera.open();
		camera.setDisplayOrientation(90);
		Camera.Parameters parameters = camera.getParameters();
		camera.setParameters(parameters);
		parameters.setPictureFormat(PixelFormat.JPEG);
		parameters.setPreviewSize(previewSize_width, previewSize_height);
		camera.setParameters(parameters);
	}

	private void initMediarecorder() {
	//	CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
	//	mediarecorder.setProfile(cpHigh);
	
	    mediarecorder = new MediaRecorder();
		mediarecorder.setCamera(camera);

	/*	// initial
		mediarecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mediarecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

		// initialized
		mediarecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		// DataSourceConfigured
		mediarecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
		mediarecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
		mediarecorder.setOutputFile(Constant.uploadVideoPath);
		mediarecorder.setVideoSize(previewSize_width, previewSize_height);
		mediarecorder.setVideoEncodingBitRate(2000000);
		mediarecorder.setPreviewDisplay(surfaceHolder.getSurface());
		mediarecorder.setOrientationHint(90);
		 mediarecorder.setVideoSize(720, 480);
*/
		mediarecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		mediarecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
 
	        // Step 3: Set output format and encoding (for versions prior to API Level 8)
	        CamcorderProfile camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_CIF);
	       
	        
	        camcorderProfile.videoFrameWidth = 640;
	        camcorderProfile.videoFrameHeight = 480;
	        camcorderProfile.videoCodec = MediaRecorder.VideoEncoder.H264;
	        camcorderProfile.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
	    	mediarecorder.setProfile(camcorderProfile);

	        // Step 4: Set output file
	    	mediarecorder.setOutputFile(Constant.uploadVideoPath);

	        // Step 5: Set the preview output
	    	mediarecorder.setPreviewDisplay(surfaceHolder.getSurface());
	    	mediarecorder.setOrientationHint(90);
		//	mediarecorder.setVideoEncodingBitRate(200000);
 
		
			try {
			mediarecorder.prepare();
		} catch (Exception e) {
		//	Log.d("video","設置品質例外 :"+e.getMessage());
			e.printStackTrace();
			isVideoFinish = false;
		}
	}

	private void initSurfaceViewSize(int w, int h) {
		int padding = 50;

		// SurfaceView
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		//RelativeLayout.LayoutParams surfaceViewParams = new RelativeLayout.LayoutParams(
			//	metrics.widthPixels - padding * 2,
			//	(metrics.widthPixels - padding * 2) * w / h);
		//surfaceViewParams.topMargin = padding;
	//	surfaceViewParams.leftMargin = padding;
		//surfaceView.setLayoutParams(surfaceViewParams);

		// preview
		//RelativeLayout.LayoutParams previewParams = new RelativeLayout.LayoutParams(
		//		metrics.widthPixels - padding * 2, metrics.widthPixels
				//		- padding * 2);
		//previewParams.topMargin = padding;
		//previewParams.leftMargin = padding;
		//preview.setLayoutParams(previewParams);

		// ProgressView
		View view = (View) findViewById(R.id.video_surfaceViewLayout);
		RelativeLayout.LayoutParams progressParams = new RelativeLayout.LayoutParams(
				metrics.widthPixels, view.getHeight()
						- (metrics.widthPixels - padding * 2)-10);
		progressParams.topMargin = metrics.widthPixels - padding;

		View progressLayout = (View) findViewById(R.id.video_progressBarLayout);
		progressLayout.setLayoutParams(progressParams);
		progressLayout.setBackgroundColor(Color.parseColor("#514f4e"));
	}

	@Override
	public void onAutoFocus(boolean arg0, Camera arg1) {
		// TODO Auto-generated method stub

	}

	/*
	 * sensor call back
	 */
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (Math.abs(event.values[0] - motionX) > 1
				|| Math.abs(event.values[1] - motionY) > 1
				|| Math.abs(event.values[2] - motionZ) > 1) {
			// Log.d("Camera System", "Refocus");
			try {
				camera.autoFocus(this);
			} catch (RuntimeException e) {

			}
			motionX = event.values[0];
			motionY = event.values[1];
			motionZ = event.values[2];
		}
	}

	//
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

		camera = Camera.open();
		Camera.Parameters parameters = camera.getParameters();

		parameters.setPictureFormat(PixelFormat.JPEG);
 
		/*----- get best previewSize -----*/
		List<Size> previewSizes = parameters.getSupportedPreviewSizes();

		int bestPreviewSizeIndex = 0;
		int bestPreviewSize = 0;

		for (int i = 0; i < previewSizes.size(); i++) {
			if (previewSizes.get(i).width > previewSizes.get(i).height) {

				if (bestPreviewSize == 0) {
					bestPreviewSize = previewSizes.get(i).width;

				} else if (Math.abs(previewSizes.get(i).width
						- MAX_PREVIEW_SIZE) < Math.abs(bestPreviewSize
						- MAX_PREVIEW_SIZE)) {

					bestPreviewSize = previewSizes.get(i).width;
					bestPreviewSizeIndex = i;
				}
			}
		}

		previewSize_width = previewSizes.get(bestPreviewSizeIndex).width;
		previewSize_height = previewSizes.get(bestPreviewSizeIndex).height;

		parameters.setPreviewSize(previewSize_width, previewSize_height);

		try {
			camera.setParameters(parameters);
			camera.setPreviewDisplay(surfaceHolder);
			camera.setDisplayOrientation(90);
			camera.startPreview();
		} catch (Exception e) {
			e.printStackTrace();
		}

		initSurfaceViewSize(previewSizes.get(bestPreviewSizeIndex).width,
				previewSizes.get(bestPreviewSizeIndex).height);

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

	}
}

package pipimy.post;

public class FoursquareObject {

	private double lat = 0;
	private double lng = 0;
	private String address = "";
	private String name = "";

	public FoursquareObject(){
		
	}
	
	public FoursquareObject(String name, String address, double lat, double lng) {
		this.name = name;
		this.address = address;
		this.lat = lat;
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

package pipimy.post;

import java.util.ArrayList;

import pipimy.object.PicObject;
import pipimy.others.Util;
import pipimy.service.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class PhotoGridViewAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<PicObject> list;

	private int cur_selected = 99;
	private int count;
	private int size;
	private boolean isSell;

	public PhotoGridViewAdapter(Context context, int count, ArrayList<PicObject> list, 
			int size, boolean isSell) {

		this.context = context;
		this.count = count;
		this.list = list;
		this.size = size;
		this.isSell = isSell;
	}

	public void setCur_selected(int cur_selected) {
		this.cur_selected = cur_selected;
//		ImageLoader.getInstance().clearMemoryCache();
	}

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ImageView imageView = new ImageView(context);
		int p = (int) Util.convertDpToPixel(8, context);
		imageView.setLayoutParams(new GridView.LayoutParams(size - p, size - p));
		imageView.setPadding(10, 10, 10, 10);
		if (position < list.size()) {
//			ImageLoader.getInstance().displayImage(
//					"file:///" + list.get(position).getDirPath() + list.get(position).getFileName(), imageView,
//					ImageLoaderTool.DickOptions);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 8;
			Bitmap bm = BitmapFactory.decodeFile(list.get(position).getDirPath() + list.get(position).getFileName(),
					options);

			imageView.setImageBitmap(bm);
		}

		if (position == cur_selected) {
			imageView.setBackgroundResource(R.drawable.bg_style_red);
		} else {

			if (isSell)
				imageView.setBackgroundResource(R.drawable.bg_style_green);
			else
				imageView.setBackgroundResource(R.drawable.bg_style_red);
		}
		return imageView;
	}

}

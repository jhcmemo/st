package pipimy.post;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import pipimy.main.ApplicationController;
import pipimy.main.LunchActivity;
import pipimy.main.MainActivity;
import pipimy.nearby.ProductListSorter;
import pipimy.object.PicObject;
import pipimy.others.BitmapUtils;
import pipimy.others.Constant;
import pipimy.others.Http;
import pipimy.others.MediaUtils;
import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class Filter_Activity extends Activity implements Runnable{
ImageView imageViewPhoto;
Bitmap  mBitmap;
Bitmap  small_bitmap;
Bitmap output_bitmap;
ProgressDialog progressDialog;
ProgressBar progressBar1;
String mOriginalPhotoPath;
LinearLayout lv_filter;
Button button_upload;
float filter_c1 []= {
		  1, 0,0, 0, 0,
	       0,1, 0, 0, 0,
	       0, 0, 1, 0, 0,
	       0, 0, 0, 1, 0};

float filter_c2[]= {
		  1.2f, 0,0, 0, -25.6f,
	       0,1.2f, 0, 0, -25.6f,
	       0, 0, 1.2f, 0, -25.6f,
	       0, 0, 0, 1.2f, 0};

float[] filter_c3 = {
		1.5f,0,0,0,-40,
		0,1.5f,0,0,-40,
		0,0,1.5f,0,-40,
		0,0,0,1,0};
 
  float[] filter_c4 = {
		  0.299f, 0.299f,0.299f, 0, 0,
	       0.299f,0.299f, 0.299f, 0, 0,
	       0.299f, 0.299f, 0.299f, 0, 0,
	       0, 0, 0, 1, 0};
  
float[] filter_c5 = {
		 2,-1,0,0,0,
		 -1,2,0,0,0,
		 0,-1,2,0,0,
		 0,0,0,1,0};

float[] filter_c6 = {
		 3.074f,-1.82f,-0.24f,0,50.8f,
		 -0.92f,2.171f,-0.24f,0,50.8f,
		 -0.92f,-1.82f,3.754f,0,50.8f,
		 0,0,0,1,0};


float[] filter_c7 = {
		 1.7f,0,0,0,2,
		 0,1.7f,0,0,2,
		 0,0,1.4f,0,2,
		 0,0,0,5,5};

ArrayList<Object> filter_array= new ArrayList<Object>();


int now_pic_object;
ImageView filter1,filter2,filter3,filter4,filter5,filter6,filter7;
int filter_click[]={R.id.filter1,R.id.filter2,R.id.filter3,R.id.filter4,R.id.filter5,R.id.filter6,R.id.filter7};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_filter);	
		now_pic_object=getIntent().getIntExtra("now_pic_object",0);
		
		Log.d("filter","now_pic_object :"+now_pic_object);
		if(getIntent().getIntExtra("state",0)==0){
			Uri photoUri = Uri.parse(getIntent().getStringExtra("PicUri"));		
			mOriginalPhotoPath = MediaUtils.getPath(this, photoUri);
		}else{
			mOriginalPhotoPath=getIntent().getStringExtra("PicUri");
		}
		filter_array.clear();
		filter_array.add(filter_c1);
		filter_array.add(filter_c2);
		filter_array.add(filter_c3);
		filter_array.add(filter_c4);
		filter_array.add(filter_c5);
		filter_array.add(filter_c6);
		filter_array.add(filter_c7);

		displayProgressDialog("",getString(R.string.filter_loading));	

		 init();
		// displayProgressDialog("",getString(R.string.loading));	
		imageViewPhoto=(ImageView) findViewById(R.id.imageViewPhoto);
		button_upload=(Button) findViewById(R.id.button_upload);
		//progressBar1=(ProgressBar) findViewById(R.id.progressBar1);
		lv_filter=(LinearLayout) findViewById(R.id.lv_filter);
		
		filter1=(ImageView) findViewById(R.id.filter1);
		filter2=(ImageView) findViewById(R.id.filter2);
		filter3=(ImageView) findViewById(R.id.filter3);
		filter4=(ImageView) findViewById(R.id.filter4);
		filter5=(ImageView) findViewById(R.id.filter5);
		filter6=(ImageView) findViewById(R.id.filter6);
		filter7=(ImageView) findViewById(R.id.filter7);

		
		filter1.setOnClickListener(click);
		filter2.setOnClickListener(click);
		filter3.setOnClickListener(click);
		filter4.setOnClickListener(click);
		filter5.setOnClickListener(click);
		filter6.setOnClickListener(click);
		filter7.setOnClickListener(click);

		
		button_upload.setOnClickListener(click);
		/* new Thread() {			
				@Override
				public void run() {
					Looper.prepare();

					
				
				}

			}.start();*/
			
		Handler handler = new Handler();
		handler.postDelayed(launchRunnable, 50);
		
	
	}
	
	@Override
		protected void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
			if(small_bitmap!=null){
				small_bitmap.recycle();
			
			}
			if(mBitmap!=null){
				mBitmap.recycle();
			
			}	
			if(output_bitmap!=null){
				output_bitmap.recycle();
			
			}
			
			System.gc();
		}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle(getString(R.string.filter_title));
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			return true;

		}

	}
	
	@Override
	public void run() {
	
	}
	

	private Runnable launchRunnable = new Runnable() {

		@Override
		public void run() {
			

			 new Thread() {			
					@Override
					public void run() {
					
               Looper.prepare();
					//	for(int i=0;i<5;i++){
							
						/*filter1.setImageBitmap(Small_FilterView((float[])filter_array.get(0)));
						filter2.setImageBitmap(Small_FilterView((float[])filter_array.get(1)));
						filter3.setImageBitmap(Small_FilterView((float[])filter_array.get(2)));
						filter4.setImageBitmap(Small_FilterView((float[])filter_array.get(3)));
						filter5.setImageBitmap(Small_FilterView((float[])filter_array.get(4)));
		            	 dismissProgressDialog();

							*/
							
						//Small_FilterView((float[])filter_array.get(i));
						//	Log.d("now_count"," i :"+i);
						//	Bundle b=new Bundle();
						//	b.putInt("now_count", i);
						//	Message m=new Message();
                          //          m.setData(b);
							handler_setsmallbitmap.sendMessage(handler_setsmallbitmap.obtainMessage());
					//	}
			
					}

				}.start();
			
		 // new Thread(Filter_Activity.this).run();
		}
	};
	
	private void init(){
		
		loadPhoto(mOriginalPhotoPath);
		handler_setbitmap.sendMessage(handler_setbitmap.obtainMessage());
		
		//save_bitmap(mBitmap);
		
		
if(small_bitmap!=null){
	small_bitmap.recycle();
}
		
		//saveToCache(mBitmap);
		
		
		
	}
	private void loadPhoto(String path) {
		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		
		if (mBitmap != null) {
			mBitmap.recycle();
		}
		
		mBitmap = BitmapUtils.getSampledBitmap(path, displayMetrics.widthPixels, displayMetrics.heightPixels,3);
		output_bitmap=mBitmap;
		
		save_preview_bitmap(output_bitmap);

		//mBitmap=getSmallBitmap(mOriginalPhotoPath);
	}
	private void save_bitmap(Bitmap bitmap) {
		
		if (bitmap == null || bitmap.isRecycled()) {
			return;			
		}		
		FileOutputStream fop;

		String fileName = Constant.uploadProductID + "-pic"
				+ (Constant.picList.size() + 1) + ".jpg";
		String dirPath = getFilesDir().getAbsolutePath() + "/";

		Log.d("filter","now bitpath:"+dirPath + fileName);

		try {
			fop = new FileOutputStream(dirPath + fileName);		
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fop);
			fop.close();
		//	bitmap.recycle();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.d("filter","exception :"+e.getMessage());
			e.printStackTrace();
		}

	}
	private void save_preview_bitmap(Bitmap bitmap) {
		
		if (bitmap == null || bitmap.isRecycled()) {
			return;			
		}		
		FileOutputStream fop;

		String fileName = Constant.uploadProductID + "-pic"
				+ (now_pic_object + 1) + ".jpg";
		String dirPath = getFilesDir().getAbsolutePath() + "/";

		Log.d("filter","now bitpath:"+dirPath + fileName);

		try {
			fop = new FileOutputStream(dirPath + fileName);		
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fop);
			fop.close();
		//	bitmap.recycle();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.d("filter","exception :"+e.getMessage());
			e.printStackTrace();
		}

	}
	
private void save_small_bitmap(Bitmap bitmap) {
		
		if (bitmap == null || bitmap.isRecycled()) {
			return;			
		}		
		FileOutputStream fop;

		String fileName = Constant.uploadProductID + "-pic"
				+ (now_pic_object + 1) + "temp.jpg";
		String dirPath = getFilesDir().getAbsolutePath() + "/";

		Log.d("filter","now bitpath:"+dirPath + fileName);

		try {
			fop = new FileOutputStream(dirPath + fileName);		
			bitmap.compress(Bitmap.CompressFormat.JPEG, 25, fop);
			fop.close();
		//	bitmap.recycle();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.d("filter","exception :"+e.getMessage());
			e.printStackTrace();
		}

	}
	
	
	
	
	private  void image_filter(float [] colorTransform){
		//imageViewPhoto.clearColorFilter();
		imageViewPhoto.setImageURI(null);
		Log.d("filter","click filter");
	//	Bitmap newb = Bitmap.createBitmap( mBitmap.getWidth(), mBitmap.getHeight(), Config.ARGB_8888 );
		output_bitmap = Bitmap.createBitmap( mBitmap.getWidth(), mBitmap.getHeight(), Config.RGB_565 );
	 	ColorMatrix colorMatrix = new ColorMatrix();
	 	   colorMatrix.setSaturation(0f); //Remove Colour 
		   colorMatrix.set(colorTransform); 
		   ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);	    
		  
		   Paint p = new Paint();
			 p.setColorFilter(colorFilter);

		Canvas c = new Canvas(output_bitmap);
	//	c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
		c.drawBitmap(mBitmap, 0, 0, p);	
		
		//save_bitmap(newb);
		//newb.recycle();
		
		save_preview_bitmap(output_bitmap);
		//output_bitmap.recycle(); 
		//output_bitmap=newb;
		output_bitmap.recycle();
		String fileName = Constant.uploadProductID + "-pic"
				+ (now_pic_object + 1) + ".jpg";
		String dirPath = getFilesDir().getAbsolutePath() + "/";
		Log.d("filter","now bitpath:"+dirPath + fileName);
		
		//imageViewPhoto.setImageBitmap(output_bitmap);
		//imageViewPhoto.setImageURI(null);
		imageViewPhoto.setImageURI(Uri.parse(dirPath+fileName));
		
		//output_bitmap=newb;
		
		
	}
	
	
	
	private Bitmap Small_FilterView(float filter[]){
		
		
		//small_bitmap = Bitmap.createBitmap( mBitmap.getWidth(), mBitmap.getHeight(), Config.RGB_565 );
		/*BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inPurgeable = true;
		opts.inJustDecodeBounds = true;
		opts.inSampleSize=3;
		Log.d("filter","mOriginalPhotoPath :"+mOriginalPhotoPath);

		try {
			BitmapFactory.Options.class.getField("inNativeAlloc").setBoolean(opts,true);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
		//opts.inJustDecodeBounds = false;
		//opts.inPreferredConfig = Config.RGB_565;
		//opts.inDither = true;
		//small_bitmap = BitmapFactory.decodeFile(mOriginalPhotoPath, opts);
		//small_bitmap  = BitmapFactory.decodeFile(mOriginalPhotoPath).copy(Bitmap.Config.ARGB_4444, true);
		//small_bitmap  = BitmapFactory.decodeFile(mOriginalPhotoPath,opts).copy(Bitmap.Config.RGB_565, true);
		//small_bitmap=getSmallBitmap(mOriginalPhotoPath);
		
	/*	if(small_bitmap!=null){
			
			small_bitmap.recycle();
		}*/
		Log.d("filter","mBitmap.getWidth() :"+mBitmap.getWidth());
		
		Log.d("filter","mBitmap.getHeight() :"+mBitmap.getHeight());

		int width =(int)(mBitmap.getWidth()*0.8);
		int height =(int)(mBitmap.getHeight()*0.8);
		
		Log.d("filter","compress w :"+width);
		Log.d("filter","compress h :"+height);

		
		small_bitmap=	Bitmap.createBitmap(width, height, Config.RGB_565 );
		
		
		ColorMatrix colorMatrix = new ColorMatrix();
	 	   colorMatrix.setSaturation(0f); //Remove Colour 
		   colorMatrix.set(filter); 
		   ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);	    
		  
		   Paint p = new Paint();
			// p = new Paint();
			 p.setColorFilter(colorFilter);

		Canvas c = new Canvas(small_bitmap);
		c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
		c.drawBitmap(mBitmap, 0, 0, p);	
			
		return small_bitmap;

		
		
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {
	             final int heightRatio = Math.round((float) height/ (float) reqHeight);
	             final int widthRatio = Math.round((float) width / (float) reqWidth);
	             inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	        return inSampleSize;
	}
	
/*	public  Bitmap getSmallBitmap(String filePath) {
       final BitmapFactory.Options options = new BitmapFactory.Options();
     //   options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
 // options.inSampleSize = calculateInSampleSize(options, 480, 800);
      options.inSampleSize = 3;
        // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false;
 /////////////   
    
    
    
	small_bitmap = Bitmap.createBitmap( 150, 150, Config.RGB_565 );
 	ColorMatrix colorMatrix = new ColorMatrix();
 	   colorMatrix.setSaturation(0f); //Remove Colour 
	   colorMatrix.set(colorTransform); 
	   ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);	    
	  
	   Paint p = new Paint();
		 p.setColorFilter(colorFilter);

	Canvas c = new Canvas(output_bitmap);
	c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
	c.drawBitmap(mBitmap, 0, 0, p);	
	
    
    
    
    
    

    return BitmapFactory.decodeFile(filePath, options).copy(Bitmap.Config.RGB_565, true);
    
    
    
    
	  }
*/
	/** ProgressDialog methods **/
	private void displayProgressDialog(String title, String message) {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(Filter_Activity.this, title, message);
		}
	}

	private void dismissProgressDialog() {
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}
	
	
	
	
	
	private OnClickListener click =new OnClickListener() {
		
		@Override
		public void onClick(View v) {

			switch(v.getId()){
			case R.id.filter1 :
				
		          image_filter(filter_c1);
		          
		         // save_bitmap(output_bitmap);

				
				break;
			
             case R.id.filter2 :
				
   	          image_filter(filter_c2);
	          //save_bitmap(output_bitmap);

				break;
				
            case R.id.filter3 :
	
            	
  	          image_filter(filter_c3);
	         // save_bitmap(output_bitmap);

	               break;
	
            case R.id.filter4 :
	
            	
  	          image_filter(filter_c4);
	         // save_bitmap(output_bitmap);

	               break;
	
            case R.id.filter5 :
            	
  	          image_filter(filter_c5);

	          //save_bitmap(output_bitmap);

	             break;
	             
            case R.id.filter6 :
            	
    	          image_filter(filter_c6);

  	          //save_bitmap(output_bitmap);

  	             break;
  	             
            case R.id.filter7 :
            	
  	          image_filter(filter_c7);

	          //save_bitmap(output_bitmap);

	             break;
	             
  	             
	             
            case R.id.button_upload:
            	
            	//save_bitmap(output_bitmap);
            	
            	//context.getCacheDir();
            	//File delete_ca =Filter_Activity.this.getCacheDir();
            	//deleteDir(delete_ca);
            	
            	// save_bitmap(output_bitmap);
            	Constant.picList.clear();
            	 

        		String fileName = Constant.uploadProductID + "-pic"
        				+ (now_pic_object + 1) + ".jpg";
        		String dirPath = getFilesDir().getAbsolutePath() + "/";
         		
         		Log.d("filter","set test :"+ dirPath+fileName);
         		
         		
         	//	save_small_bitmap(out);
         		//save_bitmap(bitmap);
         		
            	PicObject picObject = new PicObject();
    			picObject.setProductID(Constant.uploadProductID);
    			picObject.setFileName(fileName);
    			picObject.setDirPath(dirPath);
    			Constant.picList.add(picObject);
            	
            	Constant.isUploadingPics = true;
				Intent data = new Intent();
				setResult(2, data);
				finish();
            	
            	break;
	             
	             
			
			
			}
			
			
		}
	};
	
	Handler handler_setbitmap = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			imageViewPhoto.setImageBitmap(null);
			imageViewPhoto.setImageBitmap(mBitmap);
		
		}
	};
	
	
	Handler handler_setsmallbitmap = new Handler() {
		@Override
		public void handleMessage(Message msg) {
	
		for(int i=0 ;i<7;i++){
			Small_FilterView((float[])filter_array.get(i));
			 set_bitmap(i);
			
		}
			
			
			dismissProgressDialog();

		/*	int now_cont=msg.getData().getInt("now_count",0);
			
			Log.d("now_count","now_count :"+now_cont);

			switch(now_cont){
			
			
			case 0:
				filter1.setImageBitmap(small_bitmap);
				break;
				
           case 1:
        	   filter2.setImageBitmap(small_bitmap);
				break;
				
            case 2:
            	filter3.setImageBitmap(small_bitmap);
	             break;
	
            case 3:
            	filter4.setImageBitmap(small_bitmap);
	            break;
	
             case 4:
            	 filter5.setImageBitmap(small_bitmap);
            	 
            	
            	 dismissProgressDialog();
            	   // progressBar1.setVisibility(View.INVISIBLE);
					//lv_filter.setVisibility(View.VISIBLE);	     
	              break;
	              
             case 6:
                    
            	 break;  
	             
			
			
			}
			*/
			
		
		}
	};
	private void set_bitmap(int nowbit){
		
		switch(nowbit){
		
		
		case 0:
			filter1.setImageBitmap(small_bitmap);
			break;
			
       case 1:
    	   filter2.setImageBitmap(small_bitmap);
			break;
			
        case 2:
        	filter3.setImageBitmap(small_bitmap);
             break;

        case 3:
        	filter4.setImageBitmap(small_bitmap);
            break;

         case 4:
        	 filter5.setImageBitmap(small_bitmap);
        	 
        	
        	   // progressBar1.setVisibility(View.INVISIBLE);
				//lv_filter.setVisibility(View.VISIBLE);	     
              break;
              
          case 5:
        	 filter6.setImageBitmap(small_bitmap);
        	 
       
              break;
              
              
             case 6:
             	 filter7.setImageBitmap(small_bitmap);
             	 
             	
             	 dismissProgressDialog();
             	   // progressBar1.setVisibility(View.INVISIBLE);
     				//lv_filter.setVisibility(View.VISIBLE);	     
                   break;
              
   
		
		
		}
		
	}
	
	
}





package pipimy.post;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import pipimy.object.PicObject;
import pipimy.others.Constant;
import pipimy.others.GetGalleryPath;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.service.R;
import pipimy.third.ImageLoaderTool;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;

@TargetApi(19) public class PicActivity extends Activity implements SurfaceHolder.Callback,
		SensorEventListener, AutoFocusCallback {

	private static String TAG = "PicActivity";

	View takePictureButton, deleteButton, doneButton;
	ImageView previewImageView;
	SurfaceView surfaceView;
	GridView gridView;

	private PhotoGridViewAdapter gridViewAdapter;

	private boolean hasAutoFocus;
	private float motionX = 0;
	private float motionY = 0;
	private float motionZ = 0;

	private SurfaceHolder surfaceHolder;
	private DisplayMetrics metrics;
	private SoundPool soundPool;
	private Camera camera;
	public SensorManager sensorManager;
	public Sensor accelerometer;

	private String fileName;
	private String dirPath;
	private Bitmap bmp;

	private int inSampleSize = 1;
	private final int MAX_PREVIEW_SIZE = 800;
	private final int MAX_PICTURE_SIZE = 480;
	private final int PICK = 3021;
	private int cur_selected = 99;
	private int soundID;
	private int picCountMax;
	private boolean isPreview = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_pic);

		takePictureButton = (View) findViewById(R.id.pic_takeLayou);
		deleteButton = (View) findViewById(R.id.pic_deleteLayou);
		doneButton = (View) findViewById(R.id.pic_doneLayou);
		previewImageView = (ImageView) findViewById(R.id.pic_previewImageView);
		surfaceView = (SurfaceView) findViewById(R.id.pic_surfaceView);
		gridView = (GridView) findViewById(R.id.pic_gridView);

		init();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (hasAutoFocus)
			sensorManager.registerListener(this, accelerometer,
					SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (hasAutoFocus)
			sensorManager.unregisterListener(this, accelerometer);
	}

	/*
	 * Actionbar and behavior
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.picture, menu);
		Drawable colorDrawable;

		colorDrawable = new ColorDrawable(getResources().getColor(
				R.color.style_red));

		Drawable bottomDrawable = getResources().getDrawable(
				R.drawable.actionbar_bottom);
		LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable,
				bottomDrawable });
		getActionBar().setBackgroundDrawable(ld);
		getActionBar().setTitle(getString(R.string.post_pic));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.pick_from_gallery:
			if (Build.VERSION.SDK_INT < 19) {
				Intent intent = new Intent();
				intent.setType("image/jpeg");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Select Picture"), PICK);
			} else {
				Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				intent.setType("image/jpeg");
				startActivityForResult(intent, PICK);
			}
			return true;
		default:
			return true;
			// return super.onOptionsItemSelected(item);
		}
	}

	@SuppressWarnings("deprecation")
	private void initCamera() {
		camera = Camera.open();
		try {

			Camera.Parameters parameters = camera.getParameters();

			/*----- get best previewSize -----*/
			List<Size> previewSizes = parameters.getSupportedPreviewSizes();
			int bestPreviewSizeIndex = 0;
			int bestPreviewSize = 0;

			for (int i = 0; i < previewSizes.size(); i++) {
				if (bestPreviewSize == 0) {

					bestPreviewSize = previewSizes.get(i).height;

				} else if (Math.abs(previewSizes.get(i).height
						- MAX_PREVIEW_SIZE) < Math.abs(bestPreviewSize
						- MAX_PREVIEW_SIZE)) {
					bestPreviewSize = previewSizes.get(i).height;
					bestPreviewSizeIndex = i;
				}
			}
			parameters.setPreviewSize(
					previewSizes.get(bestPreviewSizeIndex).width,
					previewSizes.get(bestPreviewSizeIndex).height);

			// set surfaceView size
			initSurfaceViewSize(previewSizes.get(bestPreviewSizeIndex).width,
					previewSizes.get(bestPreviewSizeIndex).height);

			/*----- get best picture size -----*/
			List<Size> pictureSizes = parameters.getSupportedPictureSizes();
			int bestPictureSizeIndex = 0;
			int bestPictureSize = 0;

			for (int i = 0; i < pictureSizes.size(); i++) {

				if (pictureSizes.get(i).width > pictureSizes.get(i).height) {
					if (bestPictureSize == 0) {

						bestPictureSize = pictureSizes.get(i).height;

					} else if (Math.abs(pictureSizes.get(i).height
							- MAX_PICTURE_SIZE) < Math.abs(bestPictureSize
							- MAX_PICTURE_SIZE)) {
						bestPictureSize = pictureSizes.get(i).height;
						bestPictureSizeIndex = i;
					}
				}
			}
			parameters.setPictureSize(
					pictureSizes.get(bestPictureSizeIndex).width,
					pictureSizes.get(bestPictureSizeIndex).height);

			parameters.setPictureFormat(PixelFormat.JPEG);
			camera.setParameters(parameters);
			camera.setPreviewDisplay(surfaceHolder);
			camera.setDisplayOrientation(90);
			camera.startPreview();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	private void initSurfaceViewSize(int w, int h) {
		int padding = 50;

		// SurfaceView && preview
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		RelativeLayout.LayoutParams surfaceViewParams = new RelativeLayout.LayoutParams(
				metrics.widthPixels - padding * 2,
				(metrics.widthPixels - padding * 2) * w / h);
		surfaceViewParams.topMargin = 3;
		surfaceViewParams.leftMargin = padding;
		surfaceView.setLayoutParams(surfaceViewParams);
		previewImageView.setLayoutParams(surfaceViewParams);

		// GridView
		View view = (View) findViewById(R.id.pic_surfaceViewLayout);
		RelativeLayout.LayoutParams gridParams = new RelativeLayout.LayoutParams(
				metrics.widthPixels, view.getHeight()
						- (metrics.widthPixels - padding * 2));
		gridParams.topMargin = metrics.widthPixels - padding * 2;

		gridView.setLayoutParams(gridParams);
		int p = (int) Util.convertDpToPixel(10, this);
		gridView.setPadding(p, p, p, p);
		gridViewAdapter = new PhotoGridViewAdapter(this, picCountMax,
				Constant.picList, metrics.widthPixels / 5, true);
		gridView.setAdapter(gridViewAdapter);
	}

	private void init() {
		// TODO Auto-generated method stub

		ImageLoader.getInstance().init(
				ImageLoaderConfiguration.createDefault(this));
		checkDeviceHasAutoFocus();
		metrics = new DisplayMetrics();
		soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 5);
		soundID = soundPool.load(this, R.raw.camera_click, 1);

		takePictureButton
				.setBackgroundResource(R.drawable.selector_style_cube_green);
		deleteButton
				.setBackgroundResource(R.drawable.selector_style_cube_green);
		doneButton.setBackgroundResource(R.drawable.selector_style_cube_green);

		if (PreferenceUtil.getBoolean(this, Constant.USER_LOGIN)) {
			picCountMax = 10;
		} else {
			picCountMax = 5;
		}

		previewImageView.setVisibility(View.GONE);

		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);

		deleteButton.setEnabled(false);

		takePictureButton.setOnClickListener(btnClickListener);
		deleteButton.setOnClickListener(btnClickListener);
		doneButton.setOnClickListener(btnClickListener);
		gridView.setOnItemClickListener(gridViewOnItemClickListener);

		if (hasAutoFocus) {
			sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
			accelerometer = sensorManager
					.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			surfaceView.setOnClickListener(autoFocusClickListener);
		}
	}

	private void checkDeviceHasAutoFocus() {
		PackageManager packageManager = getPackageManager();
		if (packageManager
				.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)) {
			hasAutoFocus = true;
		} else {
			hasAutoFocus = false;
		}
	}

	/*
	 * items click listener
	 */
	private OnClickListener btnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.pic_takeLayou: {
				doTakingPic();
				break;
			}
			case R.id.pic_deleteLayou: {
				doDeletingSelectefPic();
				break;
			}
			case R.id.pic_doneLayou: {
				if (Constant.picList.size() != 0) {
					Constant.isUploadingPics = true;
					Intent data = new Intent();
					setResult(2, data);
					finish();
				}
				break;
			}

			}
		}

	};

	private OnItemClickListener gridViewOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			// TODO Auto-generated method stub
			isPreview = true;
			cur_selected = position;
			gridViewAdapter.setCur_selected(cur_selected);
			gridViewAdapter.notifyDataSetChanged();
			deleteButton.setEnabled(true);

			if (position < Constant.picList.size()) {

				ImageLoader.getInstance().displayImage(
						"file:///"
								+ Constant.picList.get(position).getDirPath()
								+ Constant.picList.get(position).getFileName(),
						previewImageView, ImageLoaderTool.MemoryOptions);

				previewImageView.setVisibility(View.VISIBLE);
			} else {
				previewImageView.setVisibility(View.GONE);
				isPreview = false;
				camera.startPreview();
			}

		}

	};

	private OnClickListener autoFocusClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (!isPreview) {
				camera.autoFocus(PicActivity.this);
			}
		}
	};

	@Override
	public void onAutoFocus(boolean arg0, Camera arg1) {
	}

	protected void doDeletingSelectefPic() {
		Log.e(TAG, "doDeletingSelectefPic BEGIN");
		if (cur_selected < Constant.picList.size()) {

			/* delete file */
			File file = new File(Constant.picList.get(cur_selected)
					.getDirPath()
					+ Constant.picList.get(cur_selected).getFileName());
			if (file.exists()) {
				file.delete();
			}

			Constant.picList.remove(cur_selected);
			reNameFile();
			gridViewAdapter.setCur_selected(99);
			// ImageLoader.getInstance().clearMemoryCache();
			gridViewAdapter.notifyDataSetChanged();
			deleteButton.setEnabled(false);
			cur_selected = 99;
		}
	}

	protected void doTakingPic() {
		// TODO Auto-generated method stub
		if (isPreview) {
			camera.startPreview();
			previewImageView.setVisibility(View.GONE);
			isPreview = false;
		} else {
			// lock takePictureButton until picture handle complete
			takePictureButton.setClickable(false);
			camera.takePicture(null, null, jpeg);
		}
	}

	private void reNameFile() {
		// TODO Auto-generated method stub
		for (int i = 0; i < Constant.picList.size(); i++) {

			if (Constant.picList.size() > 1) {
				File to = new File(Constant.picList.get(0).getDirPath()
						+ Constant.uploadProductID + "-pic" + (i + 1) + ".jpg");
				File from = new File(Constant.picList.get(0).getDirPath()
						+ Constant.picList.get(i).getFileName());
				if (from.exists()
						&& !to.getAbsolutePath().equals(from.getAbsolutePath())) {
					from.renameTo(to);
					Constant.picList.get(i).setFileName(
							Constant.uploadProductID + "-pic" + (i + 1)
									+ ".jpg");
				}
			}
		}
	}

	private PictureCallback jpeg = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			// TODO Auto-generated method stub
			bmp = rotateBitmap(data, null);

			FileOutputStream fop;
			try {

				if (cur_selected < Constant.picList.size()) {
					fileName = Constant.uploadProductID + "-pic"
							+ (cur_selected + 1) + ".jpg";
				} else {
					fileName = Constant.uploadProductID + "-pic"
							+ (Constant.picList.size() + 1) + ".jpg";
				}
				dirPath = getFilesDir().getAbsolutePath() + "/";
				fop = new FileOutputStream(dirPath + fileName);
				soundPool.play(soundID, 1f, 1f, 0, 0, 1);
				bmp.compress(Bitmap.CompressFormat.JPEG, 100, fop);
				fop.close();
				bmp.recycle();

			} catch (Exception e) {
				e.printStackTrace();
			}

			PicObject picObject = new PicObject();
			picObject.setProductID(Constant.uploadProductID);
			picObject.setFileName(fileName);
			picObject.setDirPath(dirPath);
			// picObject.setSource(bmp);

			/*----- replace -----*/
			if (cur_selected < Constant.picList.size()) {
				Constant.picList.set(cur_selected, picObject);
			}

			/*----- over MAX -----*/
			else if (Constant.picList.size() == picCountMax) {
				Constant.picList.set(Constant.picList.size() - 1, picObject);
			}

			/*----- others -----*/
			else {
				Constant.picList.add(picObject);
			}
			cur_selected = 99;
			gridViewAdapter.setCur_selected(99);
			gridViewAdapter.notifyDataSetChanged();
			camera.startPreview();

			// release takeButton
			takePictureButton.setClickable(true);

		}

	};

	private Bitmap addWhiteBackground(Bitmap bitmap) {

		if (bitmap.getWidth() == bitmap.getHeight())
			return bitmap;

		int maxLength = (bitmap.getWidth() > bitmap.getHeight() ? bitmap
				.getWidth() : bitmap.getHeight());
		Bitmap bmpWithWhiteBack = Bitmap.createBitmap(maxLength, maxLength,
				bitmap.getConfig());
		Canvas canvas = new Canvas(bmpWithWhiteBack);
		canvas.drawColor(Color.WHITE);
		canvas.drawBitmap(bitmap, (maxLength - bitmap.getWidth()) / 2,
				(maxLength - bitmap.getHeight()) / 2, null);// 原有圖片置中，從外圍填滿白色
		return bmpWithWhiteBack;
	}

	private Bitmap resizeBitmap(Bitmap bitmap) {
		int shortSide;
		boolean longSideIsWidth;

		if (bitmap.getWidth() == bitmap.getHeight())
			return Bitmap.createScaledBitmap(bitmap, MAX_PICTURE_SIZE,
					MAX_PICTURE_SIZE, false);

		longSideIsWidth = bitmap.getWidth() > bitmap.getHeight() ? true : false;

		if (longSideIsWidth) {
			shortSide = bitmap.getHeight() * MAX_PICTURE_SIZE
					/ bitmap.getWidth();
			return Bitmap.createScaledBitmap(bitmap, MAX_PICTURE_SIZE,
					shortSide, false);
		} else {
			shortSide = bitmap.getWidth() * MAX_PICTURE_SIZE
					/ bitmap.getHeight();
			return Bitmap.createScaledBitmap(bitmap, shortSide,
					MAX_PICTURE_SIZE, false);
		}
	}

	private Bitmap rotateBitmap(byte[] data, BitmapFactory.Options options) {
		Bitmap bitmap = null;
		if (options == null) {
			bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		}
		// OOM retry
		else {
			bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,
					options);
		}

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Matrix mtx = null;

		// some phone default picture is portrait,some is landscape
		if (w > h) {// need to rotate 90
			mtx = new Matrix();
			mtx.postRotate(90);
		}

		int wh = w > h ? h : w;
		// int retX = w > h ? (w - h) / 2 : 0;
		// int retY = w > h ? 0 : (h - w) / 2;

		try {
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, wh, wh, mtx, true);
		}

		/*----- if OOM -----*/
		catch (OutOfMemoryError e) {
			// recycle and retry to get new bitmap
			bitmap.recycle();

			BitmapFactory.Options retryOptions = new BitmapFactory.Options();
			retryOptions.inPurgeable = true;
			retryOptions.inInputShareable = true;

			inSampleSize *= 2;
			retryOptions.inSampleSize = inSampleSize;

			rotateBitmap(data, retryOptions);
		}

		return bitmap;
	}

	/*
	 * Activity result callback
	 */
	/* callback pick from gallery */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;
		switch (requestCode) {
		case PICK: {

			ImageLoader.getInstance().clearMemoryCache();

			/*
			 * Uri selectedImageUri = data.getData(); String selectedImagePath =
			 * getPath(selectedImageUri);
			 * 
			 * bmp = resizeBitmap(BitmapFactory.decodeFile(selectedImagePath));
			 */
			Uri selectedImageUri = data.getData();
			//String selectedImagePath = getPath(this, selectedImageUri);
			String selectedImagePath = GetGalleryPath.getPath(this, selectedImageUri);
			bmp = resizeBitmap(BitmapFactory.decodeFile(selectedImagePath));
			bmp = addWhiteBackground(bmp);

			FileOutputStream fop;
			try {

				if (cur_selected < Constant.picList.size()) {
					fileName = Constant.uploadProductID + "-pic"
							+ (cur_selected + 1) + ".jpg";
				} else {
					fileName = Constant.uploadProductID + "-pic"
							+ (Constant.picList.size() + 1) + ".jpg";
				}
				dirPath = getFilesDir().getAbsolutePath() + "/";
				fop = new FileOutputStream(dirPath + fileName);
				soundPool.play(soundID, 1f, 1f, 0, 0, 1);
				bmp.compress(Bitmap.CompressFormat.JPEG, 100, fop);
				fop.close();
				bmp.recycle();

			} catch (FileNotFoundException e) {

				e.printStackTrace();
				System.out.println("FileNotFoundException");

			} catch (IOException e) {

				e.printStackTrace();
				System.out.println("IOException");
			}

			PicObject picObject = new PicObject();
			picObject.setProductID(Constant.uploadProductID);
			picObject.setFileName(fileName);
			picObject.setDirPath(dirPath);
			// picObject.setSource(bmp);

			/*----- replace -----*/
			if (cur_selected < Constant.picList.size()) {
				Constant.picList.set(cur_selected, picObject);
			}

			/*----- over MAX -----*/
			else if (Constant.picList.size() == picCountMax) {
				Constant.picList.set(Constant.picList.size() - 1, picObject);
			}

			/*----- others -----*/
			else {
				Constant.picList.add(picObject);
			}
			cur_selected = 99;
			gridViewAdapter.setCur_selected(99);
			gridViewAdapter.notifyDataSetChanged();

			// camera.startPreview();
			// release takeButton
			takePictureButton.setClickable(true);

			previewImageView.setVisibility(View.GONE);
			isPreview = false;
			break;
		}
		}
	}

	/*
	 * sensor implements
	 */
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (Math.abs(event.values[0] - motionX) > 1
				|| Math.abs(event.values[1] - motionY) > 1
				|| Math.abs(event.values[2] - motionZ) > 1) {
			// Log.d("Camera System", "Refocus");
			try {
				camera.autoFocus(this);
			} catch (RuntimeException e) {

			}
			motionX = event.values[0];
			motionY = event.values[1];
			motionZ = event.values[2];
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		initCamera();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		camera.stopPreview();
		camera.release();
	}

}

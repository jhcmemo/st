package pipimy.cart;

import java.io.Serializable;

public class CVSObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String storeId;
	String storeName;
	String storeAddress;
	String storePhone;
	String storeClose;
	
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}
	public String getStorePhone() {
		return storePhone;
	}
	public void setStorePhone(String storePhone) {
		this.storePhone = storePhone;
	}
	public String getStoreClose() {
		return storeClose;
	}
	public void setStoreClose(String storeClose) {
		this.storeClose = storeClose;
	}
}

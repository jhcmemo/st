package pipimy.cart;

import java.io.Serializable;

public class CreditCardObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String cardNo;
	String cardValidMonth;
	String cardValidYear;
	String cardSecureNo;
	String cardOwnerName;
	String cardOwnerTel;
	
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardValidMonth() {
		return cardValidMonth;
	}
	public void setCardValidMonth(String cardValidMonth) {
		this.cardValidMonth = cardValidMonth;
	}
	public String getCardValidYear() {
		return cardValidYear;
	}
	public void setCardValidYear(String cardValidYear) {
		this.cardValidYear = cardValidYear;
	}
	public String getCardSecureNo() {
		return cardSecureNo;
	}
	public void setCardSecureNo(String cardSecureNo) {
		this.cardSecureNo = cardSecureNo;
	}
	public String getCardOwnerName() {
		return cardOwnerName;
	}
	public void setCardOwnerName(String cardOwnerName) {
		this.cardOwnerName = cardOwnerName;
	}
	public String getCardOwnerTel() {
		return cardOwnerTel;
	}
	public void setCardOwnerTel(String cardOwnerTel) {
		this.cardOwnerTel = cardOwnerTel;
	}
}

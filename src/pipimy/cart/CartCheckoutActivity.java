package pipimy.cart;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import pipimy.chat.ChatContentActivity;
import pipimy.main.ApplicationController;
import pipimy.object.CartItemsObject;
import pipimy.order.OrderListActivity;
import pipimy.others.ApiProxy;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.DialogUtil.OnMsgInputListener;
import pipimy.others.DialogUtil.OnSearchClickListener;
import pipimy.others.IGenericDialogUtil;
import pipimy.others.IGenericDialogUtil.PositiveBtnClickListener;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CartCheckoutActivity extends Activity {

	final static String TAG = "CartCheckoutActivity";
	final static int CHECKOUT_SHOPPING_LIST = 10006;
	final static int GET_CART_LIST = 10005;

	Spinner sp_cashType;
	Spinner sp_cashTypeSubItem;
	Spinner sp_storeflowType;

	ListView listView;
	Button checkoutBtn;
	TextView txtCVStore;
	ArrayList<CartItemsObject> list;
	CommonAdapter<CartItemsObject> adapter;
	LinearLayout cvsLayout;

	int totalPrice = 0;
	boolean isPaymentTypeSelected;

	ArrayList<String> mCashType = new ArrayList<String>();
	ArrayList<String> mDeliveryType = new ArrayList<String>();
	String[] paymentItems;
	String[] bankName;
	String[] aryTotalSellerCashType;

	String tradeNo = "";
	String receiverStoreId = "";
	String cvsType;
	String buyerDecidePaymentType = "";
	String subBuyerDecidePaymentType = "";
	String buyerDecideStoreType = "";
	String subBuyerDecideStoreType = "";
	CreditCardObject userCardObj;

	ImageLoader mImageLoader;
	RequestQueue queue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checkout_result);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		queue = VolleySingleton.getInstance().getRequestQueue();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();

		Bundle params = getIntent().getExtras();
		if (params != null) {
			list = params.getParcelableArrayList("shopping_list");
			
			if(ShoppingCartUtil.getCashType(list) == 0 || ShoppingCartUtil.getDeliveryType(list) == 0) {
				toBuyError();
			} else {
				try {
					mCashType = ShoppingCartUtil.getAryCashType(list);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					toBuyError();
				}
				
				try {
					mDeliveryType = ShoppingCartUtil.getAryDeliveryType(list);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					toBuyError();
				}
			}
		}

		bankName = new String[] { "", "TAISHIN", "HUANAN", "ESUN", "FUBON",
				"BOT", "CHINATRUST", "FIRST" };
		aryTotalSellerCashType = new String[] {
				getResources().getString(R.string.seller_cash_choose0),
				getResources().getString(R.string.seller_cash_choose1),
				getResources().getString(R.string.seller_cash_choose2),
				getResources().getString(R.string.seller_cash_choose3),
				getResources().getString(R.string.seller_cash_choose4),
				getResources().getString(R.string.seller_cash_choose5), 
				getResources().getString(R.string.seller_cash_choose6),
				getResources().getString(R.string.seller_cash_choose7)
		};

		sp_cashTypeSubItem = (Spinner) findViewById(R.id.spinner_payment_choose);
		sp_cashType = (Spinner) findViewById(R.id.spinner_cash_type);
		sp_storeflowType = (Spinner) findViewById(R.id.spinner_store_types);
		listView = (ListView) findViewById(R.id.list_products);
		checkoutBtn = (Button) findViewById(R.id.btn_sure_tobuy);
		txtCVStore = (TextView) findViewById(R.id.txt_cvs_data);
		cvsLayout = (LinearLayout) findViewById(R.id.layout_choose_cvs);

		checkoutBtn.setOnClickListener(itemClickListener);
		txtCVStore.setOnClickListener(itemClickListener);
		cvsLayout.setOnClickListener(itemClickListener);

		initViews();

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initViews() {

		sp_cashType.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, mCashType));
		sp_storeflowType.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, mDeliveryType));

		// spinner listener
		sp_storeflowType.setOnItemSelectedListener(storeFlowChangeListener);
		setStoreFlowClickAbility(false, 0);
		sp_cashType.setOnItemSelectedListener(cashFlowChangeListener);
		sp_cashTypeSubItem.setOnItemSelectedListener(subCashFlowChangeListener);

		adapter = new CommonAdapter(this, list, R.layout.cell_checkout_list) {
			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				final CartItemsObject obj = (CartItemsObject) item;

				NetworkImageView product_img = (NetworkImageView) commonViewHolder
						.get(commonViewHolder, currentView,
								R.id.img_product_pic);
				TextView txtProductTitle = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_title);
				TextView txtProductBuyCount = (TextView) commonViewHolder.get(
						commonViewHolder, currentView,
						R.id.txt_product_buy_count);
				TextView txtProductPrice = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_product_price);

				int price = (Integer.parseInt(obj.getPrice()))
						* obj.getNumberToBuy();
				totalPrice = totalPrice + price;

				product_img.setImageUrl(obj.getProductPicUrl(), mImageLoader);
				txtProductTitle.setText(obj.getTitle());
				txtProductBuyCount.setText(Integer.toString(obj.getNumberToBuy()));
				txtProductPrice.setText(Integer.toString(price));
			}

		};
		listView.setFocusable(false);
		listView.setAdapter(adapter);
		Util.setListViewHeightBasedOnChildren(listView);

	}

	/*
	 * Spinner change listener
	 */
	private OnItemSelectedListener subCashFlowChangeListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			if (buyerDecidePaymentType.equals("2")) {
				if (position == 0)
					subBuyerDecidePaymentType = "";
				else if (position == 1)
					subBuyerDecidePaymentType = "CVS";
				else if (position == 2)
					subBuyerDecidePaymentType = "IBON";

			} else if (buyerDecidePaymentType.equals("4")) {
				subBuyerDecidePaymentType = bankName[position];
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
		}
	};

	private OnItemSelectedListener cashFlowChangeListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			cashTypeChoose(mCashType.get(position));

			if (mCashType.get(position).equals(aryTotalSellerCashType[1])) {
				buyerDecidePaymentType = "1";
				setStoreFlowClickAbility(true, 0, mCashType.get(position));
				setCreditCardInfo();
			} else if (mCashType.get(position).equals(aryTotalSellerCashType[2])) {
				setStoreFlowClickAbility(true, 0, mCashType.get(position));
				//showChooseCvsOrIbon();
				subBuyerDecidePaymentType = "CVS";
				buyerDecidePaymentType = "2";
			} else if (mCashType.get(position).equals(aryTotalSellerCashType[3])) {
				setStoreFlowClickAbility(true, 0, mCashType.get(position));
				showBankNameChoice();
				buyerDecidePaymentType = "4";
			} else if (mCashType.get(position).equals(aryTotalSellerCashType[4])) {
				setStoreFlowClickAbility(true, 0);
				buyerDecidePaymentType = "8";
			} else if (mCashType.get(position).equals(aryTotalSellerCashType[5])) {
				setStoreFlowClickAbility(false, 4);
				buyerDecidePaymentType = "16";
			} else if (mCashType.get(position).equals(aryTotalSellerCashType[6])) {
				buyerDecidePaymentType = "32";
				setStoreFlowClickAbility(false, 1);
			} else if (mCashType.get(position).equals(aryTotalSellerCashType[7])) {
				buyerDecidePaymentType = "64";
				setStoreFlowClickAbility(false, 2);
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
		}
	};

	private OnItemSelectedListener storeFlowChangeListener = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			String strStoreType = mDeliveryType.get(position);

			if (strStoreType.equals(getResources().getString(
					R.string.seller_cash_choose0))) {
				buyerDecideStoreType = "";
				subBuyerDecideStoreType = "";
				cvsLayout.setVisibility(View.GONE);
				receiverStoreId = "";
			} else if (strStoreType.equals(getResources().getString(
					R.string.seller_store_choose1))) {
				buyerDecideStoreType = "1";
				subBuyerDecideStoreType = "FAMIC2C";
				cvsLayout.setVisibility(View.VISIBLE);
				cvsType = "F";
				setCVSData("F");
			} else if (strStoreType.equals(getResources().getString(
					R.string.seller_store_choose2))) {
				buyerDecideStoreType = "2";
				subBuyerDecideStoreType = "UNIMARTC2C";
				cvsLayout.setVisibility(View.VISIBLE);
				cvsType = "U";
				setCVSData("U");
			} else if (strStoreType.equals(getResources().getString(
					R.string.seller_store_choose3))) {
				buyerDecideStoreType = "4";
				cvsLayout.setVisibility(View.GONE);
				receiverStoreId = "";
			} else if (strStoreType.equals(getResources().getString(
					R.string.seller_store_choose4))) {
				buyerDecideStoreType = "16";
				cvsLayout.setVisibility(View.GONE);
				receiverStoreId = "";
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
		}

	};

	/*
	 * Btns or other views click listener
	 */
	private OnClickListener itemClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.txt_cvs_data:
			case R.id.layout_choose_cvs:
				setCVSData(cvsType);
				break;

			case R.id.btn_sure_tobuy: {
				boolean isDone = checkDone();
				Log.e(TAG, "check data =" + isDone);
				if (!isDone) {
					DialogUtil.pushPureDialog(CartCheckoutActivity.this,
							"Alert", getResources().getString(R.string.dialog_str_msg_empty), "ok");
				} else {
					prepareToCheckout();
					//getDeliveryFee();
				}

				break;
			}

			}

		}
	};

	/*
	 * check done all user settings and prepare to checkout
	 */
	String[] paymentParams;
	String[] paymentData;
	protected void prepareToCheckout() {
		// TODO Auto-generated method stub
		String msg = getResources().getString(
				R.string.dialog_str_msg_sure_to_buy);

		String buyerCart = "";
		for (int i = 0; i < list.size(); i++) {
			if (i != list.size() - 1)
				buyerCart = buyerCart + list.get(i).getProductID() + ":"
						+ list.get(i).getNumberToBuy() + ",";
			else
				buyerCart = buyerCart + list.get(i).getProductID() + ":"
						+ list.get(i).getNumberToBuy();
		}

		if(buyerDecidePaymentType.equals("1")) {
			paymentParams = new String[]{ "paymentType", "subPaymentType",
					"logisticsType", "subLogisticsType", "receiverStoreID", "cart", "CreditHolder",
					"PhoneNumber", "CardNumber", "CardValidYY", "CardValidMM", "CardCVV2"};
			paymentData = new String[]{ buyerDecidePaymentType, subBuyerDecidePaymentType, buyerDecideStoreType,
					subBuyerDecideStoreType, receiverStoreId, buyerCart, userCardObj.getCardOwnerName(), 
					userCardObj.getCardOwnerTel(), userCardObj.getCardNo(), userCardObj.getCardValidYear(), 
					userCardObj.getCardValidMonth(), userCardObj.getCardSecureNo()};
		} else {
			paymentParams = new String[]{ "paymentType", "subPaymentType",
					"logisticsType", "subLogisticsType", "receiverStoreID", "cart" };
			paymentData = new String[]{ buyerDecidePaymentType,
					subBuyerDecidePaymentType, buyerDecideStoreType,
					subBuyerDecideStoreType, receiverStoreId, buyerCart };
		}
		

		Log.e(TAG, "paymentType = " + buyerDecidePaymentType
				+ " subPaymentType = " + subBuyerDecidePaymentType
				+ " logisticsType = " + buyerDecideStoreType
				+ " subLogisticsType = " + subBuyerDecideStoreType
				+ " receiverStoreID = " + receiverStoreId + " cart = "
				+ buyerCart);

		getDeliveryFee(paymentParams, paymentData);
	}
	
	protected void payMyShoppingList(String strName, String strTel) {
		
		String pdTitle = getResources().getString(R.string.dialog_str_title_loading);
		String pdMsg = getResources().getString(R.string.dialog_str_msg_loading);
		Util.showProgressDialog(CartCheckoutActivity.this, pdTitle, pdMsg);
		
		String buyerCart = "";
		for (int i = 0; i < list.size(); i++) {
			if (i != list.size() - 1)
				buyerCart = buyerCart + list.get(i).getProductID() + ":"
						+ list.get(i).getNumberToBuy() + ",";
			else
				buyerCart = buyerCart + list.get(i).getProductID() + ":"
						+ list.get(i).getNumberToBuy();
		}

		String[] params;
		String[] data;
		if(buyerDecidePaymentType.equals("1")) {
			params = new String[]{ "paymentType", "subPaymentType",
					"logisticsType", "subLogisticsType", "receiverStoreID", "cart", "pickName", "pickPhone",
					"CreditHolder", "PhoneNumber", "CardNumber", "CardValidYY", "CardValidMM", "CardCVV2"};
			data = new String[]{ buyerDecidePaymentType, subBuyerDecidePaymentType, buyerDecideStoreType,
					subBuyerDecideStoreType, receiverStoreId, buyerCart, strName, strTel, 
					userCardObj.getCardOwnerName(), userCardObj.getCardOwnerTel(), userCardObj.getCardNo(), 
					userCardObj.getCardValidYear(), userCardObj.getCardValidMonth(), userCardObj.getCardSecureNo()};
		} else {
			params = new String[]{ "paymentType", "subPaymentType",
					"logisticsType", "subLogisticsType", "receiverStoreID", "cart",  "pickName", "pickPhone" };
			data = new String[]{ buyerDecidePaymentType,
					subBuyerDecidePaymentType, buyerDecideStoreType,
					subBuyerDecideStoreType, receiverStoreId, buyerCart,strName, strTel };
		}

		apiRequest(Constant.CODE_CHECKOUT_SHOPPING_LIST, params, data);
		
	}

	protected void getDeliveryFee(final String[] paymentParams, final String[] paymentData) {
		// TODO Auto-generated method stub
		String title = getResources().getString(R.string.dialog_str_title_loading);
		String msg = getResources().getString(R.string.dialog_str_msg_loading);
		Util.showProgressDialog(this, title, msg);
		new Thread() {
			@Override
			public void run() {
				Message msg = ApiProxy.checkPayFee(paymentParams, paymentData, CartCheckoutActivity.this);
				handler.sendMessage(msg);
			}
		}.start();
	}
	

	/*
	 * decide whether the store flow spinner could be clicked or not 
	 */
	protected void setStoreFlowClickAbility(boolean b ,int select) {
		// TODO Auto-generated method stub
		mDeliveryType = ShoppingCartUtil.getAryDeliveryType(list);
		sp_storeflowType.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, mDeliveryType));
		try {
			sp_storeflowType.setClickable(b);
			if(select >= 0 && select <=4) {
				sp_storeflowType.setSelection(select);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void setStoreFlowClickAbility(boolean b ,int select, String cashType) {
		// TODO Auto-generated method stub
		if(!cashType.equals(getResources().getString(R.string.seller_store_choose4))) {
			mDeliveryType.remove(getResources().getString(R.string.seller_store_choose4));
		}
		
		sp_storeflowType.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, mDeliveryType));
		
		sp_storeflowType.setClickable(b);
	}

	/*
	 * check user settings
	 */
	protected boolean checkDone() {
		
		if(buyerDecidePaymentType.equals("32") || buyerDecidePaymentType.equals("64")) {
			subBuyerDecidePaymentType = "";
			subBuyerDecideStoreType = "";
			buyerDecideStoreType = buyerDecidePaymentType;
		}
		
		if (buyerDecidePaymentType == null || buyerDecideStoreType == null) {
			Log.e(TAG, "payment = null or delivery = null, error");
			return false;
		}
		if (buyerDecidePaymentType.equals("2")) {
			if (!subBuyerDecidePaymentType.equals("CVS")
					&& !subBuyerDecidePaymentType.equals("IBON")) {
				Log.e(TAG, "payment = cvs, but delivery != CVS or IBON, error");
				return false;
			}
		}
		if (buyerDecidePaymentType.equals("4")) {
			if (subBuyerDecidePaymentType.equals("")
					|| subBuyerDecidePaymentType == null) {
				Log.e(TAG, "payment = ATM, but bank name = null, error");
				return false;
			}
		}
		if (buyerDecideStoreType.equals("")) {
			Log.e(TAG, "delivery empty, error");
			return false;
		}
		
		if(buyerDecideStoreType.equals("1") || buyerDecideStoreType.equals("2") || buyerDecideStoreType.equals("32")
				|| buyerDecideStoreType.equals("64")) {
			if(receiverStoreId.length() <= 1) {
				Log.e(TAG, "payment or delivery = CVS, but store id = empty, error!");
				return false;
			}
		}
		
		if(buyerDecidePaymentType.equals("2") || buyerDecidePaymentType.equals("32") || buyerDecidePaymentType.equals("64")
				|| buyerDecideStoreType.equals("1") || buyerDecideStoreType.equals("2")) {
			if(totalPrice > 19999) {
				Toast.makeText(this, "money error", Toast.LENGTH_LONG).show();
				return false;
			}
		}
		
		if (buyerDecidePaymentType.equals("1")
				|| buyerDecidePaymentType.equals("8")
				|| buyerDecidePaymentType.equals("16"))
			subBuyerDecidePaymentType = "";

		if (buyerDecideStoreType.equals("4") || buyerDecideStoreType.equals("8")) {
			subBuyerDecideStoreType = "";
			receiverStoreId = "";
		}

		return true;
	}

	protected void checkoutShoppingList(final String[] params,
			final String[] data, CheckFeeObject object) {
		
		final Dialog dialog = new Dialog(this, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_subtotal);
		dialog.show();
		
		TextView txtProductTotal = (TextView)dialog.findViewById(R.id.txt_sub_total);
		txtProductTotal.setText(object.getProductsAmount());
		TextView txtDelivery = (TextView)dialog.findViewById(R.id.txt_delivery_fee);
		txtDelivery.setText(object.getDeliveryCost());
		TextView txtTotal = (TextView)dialog.findViewById(R.id.txt_total);
		txtTotal.setText(object.getTotalAmountFee());
		
		TextView btnConfirm = (TextView) dialog.findViewById(R.id.btn_confirm);
		btnConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				String pdTitle = getResources().getString(R.string.dialog_str_title_loading);
				String pdMsg = getResources().getString(R.string.dialog_str_msg_loading);
				Util.showProgressDialog(CartCheckoutActivity.this, pdTitle, pdMsg);
				apiRequest(Constant.CODE_CHECKOUT_SHOPPING_LIST, params, data);
				*/
				inputReceiverName();
				dialog.dismiss();
			}
		});
		TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
	}
	
	private void verifyOTPCode(String[] params, String[] data) {
		String msg = getResources().getString(R.string.loading_msg_verify_otp);
		Util.showProgressDialog(this, "wait", msg);
		apiRequest(Constant.CODE_VERIFY_OTP, params, data); 
	}

	protected void apiRequest(final int code, final String[] params, final String[] data) {
		new Thread() {
			@Override
			public void run() {
				switch(code) {
				case Constant.CODE_VERIFY_OTP: {
					Util.dismissProgressDialog();
					Message msg = ApiProxy.verifyOTPCode(params, data, CartCheckoutActivity.this);
					String res = msg.obj.toString();
					Toast.makeText(CartCheckoutActivity.this, res, 1500).show();
					CartCheckoutActivity.this.finish();
					break;
				}
				case Constant.CODE_CHECKOUT_SHOPPING_LIST:{
					Message msg = ApiProxy.checkoutShoppingList(
							CartCheckoutActivity.this, params, data);
					handler.sendMessage(msg);
					break;
				} 
				case Constant.CODE_GET_CART_INDEX: {
					Message msg = ApiProxy
							.getMyShoppingCartIndex(CartCheckoutActivity.this);
					handler.sendMessage(msg);
					break;
				}
					
				}
			}
		}.start();
		
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// Util.dismissProgressDialog();
			switch (msg.arg1) {
			case Constant.CODE_CHECK_PAY: {
				Util.dismissProgressDialog();
				CheckFeeObject object = (CheckFeeObject)msg.obj;
				checkoutShoppingList(paymentParams, paymentData, object);
				break;
			}
			case Constant.CODE_RETURN_ERR:
				Util.dismissProgressDialog();
				Toast.makeText(CartCheckoutActivity.this,
						"fail, something error", 1500).show();
				break;
			case Constant.CODE_CHECKOUT_SHOPPING_LIST: {
				String res = msg.obj.toString();
				Toast.makeText(CartCheckoutActivity.this, res, 1500).show();

				// remove cart items in client
				if (res.contains("success")) {
					try {
						JSONObject jsonObject = new JSONObject(res);
						tradeNo = jsonObject.getString("tradeNo");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ApplicationController.isCartChanged = true;
					apiRequest(Constant.CODE_GET_CART_INDEX, null, null);
					if(buyerDecidePaymentType.equals("1")) {
						inputOTPCode();
					} else {
						showResDialog();
					}
				} else {
					Util.dismissProgressDialog();
				}

				//showResDialog(res);
				break;
			}
			case Constant.CODE_GET_CART_INDEX: {
				Util.dismissProgressDialog();
				break;
			}

			}

		}

	};
	
	protected void setCreditCardInfo() {
		Intent i = new Intent();
		i.setClass(this, SetCreditCardActivity.class);
		this.startActivityForResult(i, 1);
	}

	protected void setCVSData(String str) {
		// TODO Auto-generated method stub
		Intent i = new Intent();
		i.putExtra("cvs-type", str);
		i.setClass(this, SetCVSActivity.class);
		this.startActivityForResult(i, 0);
	}
	
	protected void inputReceiverName() {
		Log.e(TAG, "inputReceiverName BEGIN");
		final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		final Dialog dialog = new Dialog(this, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_receiver_name);
		dialog.show();

		final EditText name = (EditText) dialog.findViewById(R.id.edittext_name);
		final EditText tel = (EditText) dialog.findViewById(R.id.edittext_tel);

		TextView btnConfirm = (TextView) dialog.findViewById(R.id.btn_confirm);
		btnConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				dialog.dismiss();
				
				String strName = Util.getEditText(name);
				String strTel = Util.getEditText(tel);
				
				payMyShoppingList(strName, strTel);
			
			}
		});

		TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				dialog.dismiss();
			}
		});
	}

	protected void showResDialog() {
		String alertTitle = getResources().getString(R.string.dailog_title_hint);
		String alertMsg  = getResources().getString(R.string.dialog_str_msg_check_suceess);
		String positiveBtn = getResources().getString(R.string.dialog_str_btn_goto_orderlist);
		String negativeBtn = getResources().getString(R.string.dialog_str_btn_goto_shop);
		
		DialogUtil.pushGeneralDialog(this, alertTitle, alertMsg, positiveBtn, negativeBtn, new IGenericDialogUtil.IGenericBtnClickListener() {
			
			@Override
			public void PositiveMethod(DialogInterface dialog, int id) {
				goToOrderList();
			}
			
			@Override
			public void NegativeMethod(DialogInterface dialog, int id) {
				onActivityFinish();
			}
		});

	}

	protected void inputOTPCode() {
		String title = getResources().getString(R.string.dialog_str_title_opt);
		DialogUtil.optInputDialog(this, title, new OnMsgInputListener(){
			@Override
			public void onMsgInputClick(View v, String message) {
				Log.e(TAG, "verifyOTPCode tradeNo = "+tradeNo + "OTP = "+message);
				String[] params = {"tradeNo", "otpCode"};
				String[] data = {tradeNo, message};
				verifyOTPCode(params, data);
				//onActivityFinish();
			}
			});
		
	}

	protected void showBankNameChoice() {

		DialogUtil.pushSingleChoiceDialog(this, paymentItems, "ok", "cancel",
				new IGenericDialogUtil.PositiveBtnClickListener() {
					@Override
					public void PositiveMethod(DialogInterface dialog, int id) {
						if (id != 0) {
							sp_cashTypeSubItem.setSelection(id);
							subBuyerDecidePaymentType = bankName[id];
							Log.e(TAG, "BankNameChoice = " + bankName[id]);
						}

					}

				});
	}

	protected void cashTypeChoose(String payType) {

		if (payType.equals(aryTotalSellerCashType[0])
				|| payType.equals(aryTotalSellerCashType[1])
				|| payType.equals(aryTotalSellerCashType[2])
				|| payType.equals(aryTotalSellerCashType[4])
				|| payType.equals(aryTotalSellerCashType[5])
				|| payType.equals(aryTotalSellerCashType[6])
				|| payType.equals(aryTotalSellerCashType[7])) {
			sp_cashTypeSubItem.setVisibility(View.GONE);
		} else if (payType.equals(aryTotalSellerCashType[3])) {
			sp_cashTypeSubItem.setVisibility(View.VISIBLE);
			paymentItems = getResources().getStringArray(R.array.bank_names);
		} 

		if (paymentItems != null) {
			sp_cashTypeSubItem.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_dropdown_item_1line, paymentItems));
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == 1) {
			CVSObject obj = (CVSObject) data.getSerializableExtra("cvs");
			txtCVStore.setText(obj.getStoreName() + ", "
					+ obj.getStoreAddress());
			receiverStoreId = obj.getStoreId();
		} else if(resultCode == 2) {
			userCardObj = (CreditCardObject)data.getSerializableExtra("credit");
		}
	}
	
	private void toBuyError() {
		String msg = getResources().getString(R.string.dialog_str_msg_shopping_error);
		String title = getResources().getString(R.string.dailog_title_hint);
		
		DialogUtil.pushAlertDialog(this, title, msg, new IGenericDialogUtil.PositiveBtnClickListener() {
			@Override
			public void PositiveMethod(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				onActivityFinish();
			}
		});
	}
	
	private void goToOrderList() {
		Intent i = new Intent();
		i.setClass(this, OrderListActivity.class);
		this.startActivity(i);
		this.finish();
		overridePendingTransition(android.R.anim.slide_in_left,
				android.R.anim.slide_out_right);
	}

	protected void onActivityFinish() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		setResult(66, intent);
		finish();
		overridePendingTransition(android.R.anim.slide_in_left,
				android.R.anim.slide_out_right);
	}

	@Override
	public void onBackPressed() {
		onActivityFinish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onActivityFinish();
			return true;

		default:
			return true;
		}
	}

}
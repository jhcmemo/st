package pipimy.cart;

import java.util.ArrayList;

import pipimy.main.ApplicationController;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class StoreListActivity extends Activity {

	ListView listView;
	String country;
	String area;

	ArrayList<CVSObject> storeAddressInfo = new ArrayList<CVSObject>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_storeinfo_list);

		listView = (ListView) findViewById(R.id.listView1);

		Bundle params = getIntent().getExtras();
		if (params != null) {
			country = params.getString("Country");
			area = params.getString("Area");
		}

		getData();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		ApplicationController.cvsInfoList = null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initView() {
		// TODO Auto-generated method stub
		storeAddressInfo.clear();

		if(ApplicationController.cvsInfoList.size() > 0) {
			for (int i = 0; i < ApplicationController.cvsInfoList.size(); i++) {
				String result = ApplicationController.cvsInfoList.get(i).storeAddress;
				if (result.contains(country) && result.contains(area)) {
					storeAddressInfo.add(ApplicationController.cvsInfoList.get(i));
				}
			}
		}
		

		CommonAdapter<CVSObject> storeAdapter;

		storeAdapter = new CommonAdapter(this, storeAddressInfo,
				R.layout.cell_store_info_list) {

			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				CVSObject obj = (CVSObject) item;

				TextView storeName = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_store_name);
				TextView storeAddress = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_store_address);

				storeName.setText(obj.getStoreName());
				storeAddress.setText(obj.getStoreAddress());
			}

		};
		listView.setAdapter(storeAdapter);

	}

	private void getData() {
		// TODO Auto-generated method stub
		Util.showProgressDialog(this, "wait", "loading");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "MerchantID", "StoreType", "CheckMacValue" };
				String[] data = { "2000132", "F",
						"6A11CDFF2761E553F532C73AA0DB2C00" };

				String result = Http.post(Constant.GET_STORE_INFO, params,
						data, StoreListActivity.this);
				// Log.e(TAG, "GET_STORE_INFO res = "+result);

				try {
					JSONParserTool.getStoreInfoList(result,
							ApplicationController.cvsInfoList);
				} catch (Exception e) {
					e.printStackTrace();
				}
				handler.sendEmptyMessage(0);

			}
		}.start();
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Util.dismissProgressDialog();
			initView();
		}
	};

}

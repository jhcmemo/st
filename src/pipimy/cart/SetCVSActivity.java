package pipimy.cart;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import pipimy.main.ApplicationController;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.Http;
import pipimy.others.IGenericDialogUtil;
import pipimy.others.JSONParserTool;
import pipimy.others.PreferenceProxy;
import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class SetCVSActivity extends Activity {

	final static String TAG = "SetCVSActivity";

	CommonAdapter<String> adapter;
	ViewFlipper viewFlipper;
	GridView gridView_country;
	GridView gridView_area;
	ListView listView_storeInfo;
	Button btnPrevious;
	TextView txtTitle;

	String[] taiwanCountry;
	ArrayList<String> aryTaiwanCountry = new ArrayList<String>();
	ArrayList<String> areaInCountry = new ArrayList<String>();
	ArrayList<CVSObject> storeAddressInfo = new ArrayList<CVSObject>();
	ArrayList<CVSObject> storeInfoList = new ArrayList<CVSObject>();

	String strSearchCountry;
	String strSearchArea;

	boolean isMovingDBDone;
	int currentViewCount;

	String cvsType;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_cvs);
		
		cvsType = getIntent().getStringExtra("cvs-type");
		Log.e(TAG, "cvs-type = " +cvsType);

		txtTitle = (TextView) findViewById(R.id.txt_title);
		viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper1);
		gridView_country = (GridView) findViewById(R.id.gridView_country);
		gridView_area = (GridView) findViewById(R.id.gridView_area);
		listView_storeInfo = (ListView) findViewById(R.id.listView1);
		btnPrevious = (Button) findViewById(R.id.btn_previous);
		btnPrevious.setOnClickListener(itemOnClickListener);
		aryTaiwanCountry.clear();
		taiwanCountry = getResources().getStringArray(R.array.taiwan_country);

		initCountryViews();
		if(!PreferenceProxy.getIsMovingCvsDBDone(this)) {
			try {
				moveDB();
				isMovingDBDone = true;
				PreferenceProxy.setIsMovingCvsDBDone(this);
			} catch (IOException e) {
				e.printStackTrace();
				isMovingDBDone = false;
			}
		}
		

		Log.e(TAG, "isMovingDBDone = " + isMovingDBDone);

		if(cvsType.equals("F")) {
			if (ApplicationController.famiStoreInfoList.size() <= 0) {
				getData();
			} else {
				storeInfoList.clear();
				storeInfoList = ApplicationController.famiStoreInfoList;
			}
		} else {
			if (ApplicationController.uniStoreInfoList.size() <= 0) {
				getData();
			} else {
				storeInfoList.clear();
				storeInfoList = ApplicationController.uniStoreInfoList;
			}
		}
		

	}

	private void moveDB() throws IOException {
		// TODO Auto-generated method stub
		InputStream is = null;
		is = this.getAssets().open("cvs_storeinfo_db.db");

		File dbFile = new File("/sdcard/cvs_storeinfo_db.db");

		OutputStream os = null;
		os = new FileOutputStream(dbFile);

		byte[] buffer = new byte[1024];
		while (is.read(buffer) > 0) {
			os.write(buffer);
		}
		
		os.flush();
		os.close();
		is.close();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initCountryViews() {

		for (int i = 0; i < taiwanCountry.length; i++) {
			aryTaiwanCountry.add(taiwanCountry[i]);
		}

		adapter = new CommonAdapter(this, aryTaiwanCountry,
				R.layout.cell_country_gridview) {

			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				String country = item.toString();
				TextView txtCountry = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_country_name);
				txtCountry.setText(country);
			}

		};

		gridView_country.setAdapter(adapter);
		Util.setGridViewHeightBasedOnChildren(gridView_country);
		gridView_country.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				strSearchCountry = aryTaiwanCountry.get(position);
				
				if(aryTaiwanCountry.get(position).equals(getResources().getString(R.string.country_taoyuan))) {
					if(cvsType.equals("U")) {
						strSearchCountry = getResources().getString(R.string.country_taoyuan_new);
					}
				}
				
				initAreaView(aryTaiwanCountry.get(position));
				flipToNext();

			}
		});

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initAreaView(String strSearchCountry) {
		StoreInfoProxy proxy = new StoreInfoProxy(this);
		areaInCountry.clear();
		
		areaInCountry = proxy.getAreaNameInCountry(strSearchCountry, cvsType);
		/*
		if(cvsType.equals("U"))
			areaInCountry = proxy.getAreaNameInCountry(strCountry+"_U");
		else areaInCountry = proxy.getAreaNameInCountry(strCountry);*/
		
		CommonAdapter<String> areaAdapter;
		areaAdapter = new CommonAdapter(this, areaInCountry,
				R.layout.cell_country_gridview) {

			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				String area = item.toString();
				TextView txtCountry = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_country_name);
				txtCountry.setText(area);
			}

		};

		gridView_area.setAdapter(areaAdapter);
		gridView_area.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				strSearchArea = areaInCountry.get(position);
				initAddressView();
				flipToNext();
			}
		});

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initAddressView() {
		Log.e(TAG, "searchAddress BEGIN");

		storeAddressInfo.clear();

		if (storeInfoList.size() > 0) {
			for (int i = 0; i < storeInfoList.size(); i++) {
				String result = storeInfoList.get(i).storeAddress;
				if (result.contains(strSearchCountry)
						&& result.contains(strSearchArea)) {
					storeAddressInfo.add(storeInfoList.get(i));
				}
			}
		} 

		CommonAdapter<CVSObject> storeAdapter;

		storeAdapter = new CommonAdapter(this, storeAddressInfo,
				R.layout.cell_store_info_list) {

			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				CVSObject obj = (CVSObject) item;

				TextView storeName = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_store_name);
				TextView storeAddress = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_store_address);

				storeName.setText(obj.getStoreName());
				storeAddress.setText(obj.getStoreAddress());
			}

		};
		listView_storeInfo.setAdapter(storeAdapter);
		listView_storeInfo.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				showStoreInfo(storeAddressInfo.get(position));
			}
		});
	}

	protected void showStoreInfo(final CVSObject obj) {
		String msg = "Store Name: " + obj.getStoreName() + "\nStore Address: "
				+ obj.getStoreAddress();
		DialogUtil.pushGeneralDialog(this, "Alert", msg, "ok", "cancel",
				new IGenericDialogUtil.IGenericBtnClickListener() {
					@Override
					public void PositiveMethod(DialogInterface dialog, int id) {
						backToCheckout(obj);
					}

					@Override
					public void NegativeMethod(DialogInterface dialog, int id) {
					}
				});
	}

	protected void backToCheckout(CVSObject obj) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.putExtra("cvs", obj);
		setResult(1, intent);
		finish();
	}

	private OnClickListener itemOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_previous: {
				flipToPrevious();
				break;
			}
			}
		}
	};

	// change to previous choose view
	private void flipToPrevious() {
		currentViewCount--;
		if (currentViewCount == 0) {
			btnPrevious.setVisibility(View.GONE);
			txtTitle.setText(getResources().getString(
					R.string.layout_txt_choose_country));
		} else if (currentViewCount == 1) {
			btnPrevious.setVisibility(View.VISIBLE);
			txtTitle.setText(getResources().getString(
					R.string.layout_txt_choose_area));
		} else if (currentViewCount == 2) {
			btnPrevious.setVisibility(View.VISIBLE);
			txtTitle.setText(getResources().getString(
					R.string.layout_txt_choose_address));
		}

		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(
				SetCVSActivity.this, android.R.anim.slide_out_right));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
				SetCVSActivity.this, android.R.anim.slide_in_left));
		viewFlipper.showPrevious();
	}

	// change to next choose view
	private void flipToNext() {
		currentViewCount++;
		if (currentViewCount == 0) {
			btnPrevious.setVisibility(View.GONE);
			txtTitle.setText(getResources().getString(
					R.string.layout_txt_choose_country));
		} else if (currentViewCount == 1) {
			btnPrevious.setVisibility(View.VISIBLE);
			txtTitle.setText(getResources().getString(
					R.string.layout_txt_choose_area));
		} else if (currentViewCount == 2) {
			btnPrevious.setVisibility(View.VISIBLE);
			txtTitle.setText(getResources().getString(
					R.string.layout_txt_choose_address));
		}

		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(
				SetCVSActivity.this, R.anim.slide_in_right));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
				SetCVSActivity.this, R.anim.slide_out_left));
		viewFlipper.showNext();
	}

	private void getData() {
		// TODO Auto-generated method stub
		Util.showProgressDialog(this, "wait", "loading");
		new Thread() {
			@Override
			public void run() {
				String CheckMacValue = "";
				
				if(cvsType.equals("U")) CheckMacValue = "8E2FFAE8D76F05CD9142EB05F3BBBC45";
				else if(cvsType.equals("F")) CheckMacValue = "6A11CDFF2761E553F532C73AA0DB2C00";
				
				String[] params = { "MerchantID", "StoreType", "CheckMacValue" };
				String[] data = { "2000132", cvsType,
						CheckMacValue };

				String result = Http.post(Constant.GET_STORE_INFO, params,
						data, SetCVSActivity.this);
				// Log.e(TAG, "GET_STORE_INFO res = "+result);

				try {
					if(cvsType.equals("U")) {
						JSONParserTool.getStoreInfoList(result,
								ApplicationController.uniStoreInfoList);
						storeInfoList.clear();
						storeInfoList = ApplicationController.uniStoreInfoList;
					} else {
						JSONParserTool.getStoreInfoList(result,
								ApplicationController.famiStoreInfoList);
						storeInfoList.clear();
						storeInfoList = ApplicationController.famiStoreInfoList;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				handler.sendEmptyMessage(0);

			}
		}.start();
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Util.dismissProgressDialog();
			// initView();
		}
	};

}

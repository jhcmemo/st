package pipimy.cart;

import pipimy.others.DialogUtil;
import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SetCreditCardActivity extends Activity{

	Button btnCheck;
	EditText txtCardNo, txtCardNo2, txtCardNo3, txtCardNo4;
	EditText txtValidMonth, txtValidYear, txtCardSecureNo, txtUserName, txtUserTel;
	CreditCardObject obj;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_creditcard);
		
		txtCardNo = (EditText) findViewById(R.id.edittext_card_no);
		txtCardNo2 = (EditText) findViewById(R.id.edittext_card_no2);
		txtCardNo3 = (EditText) findViewById(R.id.edittext_card_no3);
		txtCardNo4 = (EditText) findViewById(R.id.edittext_card_no4);
		txtValidMonth = (EditText) findViewById(R.id.edittext_valid_month);
		txtValidYear = (EditText) findViewById(R.id.edittext_valid_year);
		txtCardSecureNo = (EditText) findViewById(R.id.edittext_secure_number);
		txtUserName = (EditText) findViewById(R.id.edittext_user_name);
		txtUserTel = (EditText) findViewById(R.id.edittext_user_tel);
		btnCheck = (Button)findViewById(R.id.btn_check);
		btnCheck.setOnClickListener(onItemClickListener);
		
		obj = new CreditCardObject();
		//getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	private OnClickListener onItemClickListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			boolean isDone = checkDone();
			if(!isDone) {
				showAlertDialog();
			} else {
				Intent intent = new Intent();
				intent.putExtra("credit", obj);
				setResult(2, intent);
				finish();
			}
			//
		}};

	protected boolean checkDone() {
		// TODO Auto-generated method stub
		obj.cardNo = Util.getEditText(txtCardNo) + Util.getEditText(txtCardNo2) 
				+ Util.getEditText(txtCardNo3) + Util.getEditText(txtCardNo4);
		obj.cardValidMonth = Util.getEditText(txtValidMonth);
		obj.cardValidYear = Util.getEditText(txtValidYear);
		obj.cardSecureNo = Util.getEditText(txtCardSecureNo);
		obj.cardOwnerName = Util.getEditText(txtUserName);
		obj.cardOwnerTel = Util.getEditText(txtUserTel);
		
		int intMonth = 0;
		try {
			intMonth = Integer.parseInt(obj.cardValidMonth);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		if(!obj.cardNo.matches("[0-9]{16}")) {
			return false;
		} else if(intMonth < 1 || intMonth > 12) {
			return false;
		} else if(!obj.cardValidYear.matches("[0-9]{4}") ) {
			return false;
		} else if(!obj.cardSecureNo.matches("[0-9]{3,4}")) {
			return false;
		} else if(obj.cardOwnerName.length() < 1) {
			return false;
		} else if(obj.cardOwnerTel.length() < 1) {
			return false;
		}
	
		return true;
	}

	protected void showAlertDialog() {
		// TODO Auto-generated method stub
		String msg = getResources().getString(R.string.dialog_str_msg_empty);
		
		DialogUtil.pushPureDialog(this, "Alert", msg, "ok");
	}
}

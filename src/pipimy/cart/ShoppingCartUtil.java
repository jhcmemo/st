package pipimy.cart;

import java.util.ArrayList;

import android.util.Log;
import android.widget.Toast;
import pipimy.main.ApplicationController;
import pipimy.object.CartItemsObject;
import pipimy.service.R;



public class ShoppingCartUtil {

	/** =============================================================== */
	/** ========================== Interface ========================== */
	/** =============================================================== */
	public static onGetCartListIndexCallback cartListIndexCallback;
	public static final String TAG = "ShoppingCartUtil";
	
	public interface onGetCartListIndexCallback{
		public void onGetCartListIndex();
	}
	
	public static void setOnGetCartListIndexCallback(onGetCartListIndexCallback getCartListIndexCallback){
		ShoppingCartUtil.cartListIndexCallback = getCartListIndexCallback;
	}
	
	/*
	 * get cashtype intersaction in the list
	 */
	public static int getCashType(ArrayList<CartItemsObject> list) {
		int cashType = 0;
		if (list.size() > 0) {
			cashType = Integer.parseInt(list.get(0).getCashType());
			for (int i = 0; i < list.size(); i++) {
				cashType = Integer.parseInt(list.get(i).getCashType())
						& cashType;
			}
		}
		
		return cashType;
	}
	
	public static int getDeliveryType(ArrayList<CartItemsObject> list) {
		int deliveryType = 0;
		if (list.size() > 0) {
			if(list.get(0).getDeliveryType() != null) {
				deliveryType = Integer.parseInt(list.get(0).getDeliveryType());
				for (int i = 0; i < list.size(); i++) {
					deliveryType = Integer.parseInt(list.get(i).getDeliveryType())
							& deliveryType;
				}
			} else {
				return 0;
			}
		} else {
			return 0;
		}
		
		return deliveryType;
	}
	
	public static final String CASH_TYPE_CREDIT_CARD = "1";
	public static final String CASH_TYPE_CVS = "2";
	public static final String CASH_TYPE_ATM = "4";
	public static final String CASH_TYPE_FACE = "16";
	
	public static final String FAMI_C2C = "32";
	public static final String UNI_C2C = "64";
	
	public static final String DELIVERY_TYPE_FAMI_C2C = "1";
	public static final String DELIVERY_TYPE_UNI_C2C = "2";
	public static final String DELIVERY_TYPE_CUSTOM = "4";
	public static final String DELIVERY_TYPE_FACE = "16";
	
	/*
	 * check any error in cash type and delivery type
	 */
	public static boolean checkCashAndDelivery(String paymentType, String subPaymentType, String deliveryType, 
			String subDeliveryType, String receiverStoreId, int price) {
		
		if (paymentType == null || deliveryType == null) {
			Log.e(TAG, "payment = null or delivery = null, error");
			return false;
		}
		
		if (paymentType.equals(CASH_TYPE_CVS)) {
			if (!subPaymentType.equals("CVS")
					&& !subPaymentType.equals("IBON")) {
				Log.e(TAG, "payment = cvs, but delivery != CVS or IBON, error");
				return false;
			}
		}
		
		if (paymentType.equals(CASH_TYPE_ATM)) {
			if (subPaymentType.equals("")
					|| subPaymentType == null) {
				Log.e(TAG, "payment = ATM, but bank name = null, error");
				return false;
			}
		}
		
		if (deliveryType.equals("")) {
			Log.e(TAG, "delivery empty, error");
			return false;
		}
		
		if(deliveryType.equals(DELIVERY_TYPE_FAMI_C2C) || deliveryType.equals(DELIVERY_TYPE_UNI_C2C) || 
				deliveryType.equals(FAMI_C2C) || deliveryType.equals(UNI_C2C)) {
			if(receiverStoreId.length() <= 1) {
				Log.e(TAG, "payment or delivery = CVS, but store id = empty, error!");
				return false;
			}
		}
		
		if(paymentType.equals(CASH_TYPE_CVS) || paymentType.equals(FAMI_C2C) || paymentType.equals(UNI_C2C)
				|| deliveryType.equals(DELIVERY_TYPE_FAMI_C2C) || deliveryType.equals(DELIVERY_TYPE_UNI_C2C)) {
			if(price > 19999) {
				return false;
			}
		}
		
		return true;
	}
	
	
	public static ArrayList<String> getAryCashType(ArrayList<CartItemsObject> list) throws Exception{
		ArrayList<String> mCashType = new ArrayList<String>();

		int cashType = 0;
		if (list.size() > 0) {
			cashType = Integer.parseInt(list.get(0).getCashType());
			for (int i = 0; i < list.size(); i++) {
				cashType = Integer.parseInt(list.get(i).getCashType())
						& cashType;
			}
		}

		char[] charCashType = Integer.toBinaryString(cashType).toCharArray();
		for(int i = 0; i < charCashType.length / 2; i++) {
			  char temp = charCashType[i];
			  charCashType[i] = charCashType[charCashType.length - 1 - i];
			  charCashType[charCashType.length - 1 - i] = temp;
			}

		mCashType.add(ApplicationController.getInstance().getResources()
				.getString(R.string.seller_cash_choose0));
		for (int index = 0; index < charCashType.length; index++) {
			switch (index) {
			case 0:
				if(charCashType[0]=='1')
					mCashType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_cash_choose1));
				break;
			case 1:
				if(charCashType[1]=='1')
					mCashType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_cash_choose2));
				break;
			case 2:
				if(charCashType[2]=='1')
					mCashType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_cash_choose3));
				break;
			case 3:
				//if(charCashType[3]=='1')
					//mCashType.add(ApplicationController.getInstance().getResources().getString(
						//R.string.seller_cash_choose4));
				break;
			case 4:
				if(charCashType[4]=='1')
					mCashType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_cash_choose5));
				break;
			case 5:
				if(charCashType[5]=='1')
					mCashType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_cash_choose6));
				break;
			case 6:
				if(charCashType[6]=='1')
					mCashType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_cash_choose7));
				break;
			}
		}

		return mCashType;
	}
	
	public static ArrayList<String> getAryDeliveryType(ArrayList<CartItemsObject> list) {
		ArrayList<String> mDeliveryType = new ArrayList<String>();
		
		mDeliveryType.add(ApplicationController.getInstance().getResources()
				.getString(R.string.seller_cash_choose0));
		
		int deliveryType = 0;
		if (list.size() > 0) {
			if(list.get(0).getDeliveryType() != null) {
				deliveryType = Integer.parseInt(list.get(0).getDeliveryType());
				for (int i = 0; i < list.size(); i++) {
					deliveryType = Integer.parseInt(list.get(i).getDeliveryType())
							& deliveryType;
				}
			} else {
				return mDeliveryType;
			}
			
		}

		char[] charDeliveryType = Integer.toBinaryString(deliveryType).toCharArray();
		for(int i = 0; i < charDeliveryType.length / 2; i++) {
			  char temp = charDeliveryType[i];
			  charDeliveryType[i] = charDeliveryType[charDeliveryType.length - 1 - i];
			  charDeliveryType[charDeliveryType.length - 1 - i] = temp;
			}
		
		for (int index = 0; index < charDeliveryType.length; index++) {
			switch (index) {
			case 0:
				if(charDeliveryType[0]=='1')
					mDeliveryType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_store_choose1));
				break;
			case 1:
				if(charDeliveryType[1]=='1')
					mDeliveryType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_store_choose2));
				break;
			case 2:
				if(charDeliveryType[2]=='1')
					mDeliveryType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_store_choose3));
				break;
			case 4:
				if(charDeliveryType[3]=='1')
					mDeliveryType.add(ApplicationController.getInstance().getResources().getString(
						R.string.seller_store_choose4));
				break;

			}
		}
		
		return mDeliveryType;
	}
}

package pipimy.cart;

import java.util.ArrayList;

import pipimy.object.CartItemsObject;
import pipimy.order.OrderObject;
import pipimy.service.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CartIndexAdapter extends BaseAdapter{

	Context mContext;
	LayoutInflater myInflater;
	ArrayList<OrderObject> mList;
	
	public CartIndexAdapter(Context context, ArrayList<OrderObject> list) {
		this.mContext = context;
		myInflater = LayoutInflater.from(context);
		this.mList = list;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = myInflater.inflate(R.layout.cell_buy_list_item, null);

			holder = new ViewHolder();
			
			holder.orderNo = (TextView)convertView.findViewById(R.id.txt_order_no);
			holder.orderSumPrice = (TextView)convertView.findViewById(R.id.txt_order_total_price);
			holder.isPayInFace = (TextView)convertView.findViewById(R.id.txt_pay_face);
			holder.isShipInFace = (TextView)convertView.findViewById(R.id.txt_shipping_face);
			holder.isReceiveInFace = (TextView)convertView.findViewById(R.id.txt_receive_face);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		/*
		holder.orderNo.setText(mList.get(position).getOrderCreateTime());
		holder.orderSumPrice.setText(mList.get(position).getSumOfThisOrder());
		if(mList.get(position).paymentIsFace.equals("1")) {
			holder.isPayInFace.setVisibility(View.VISIBLE);
			holder.isPayInFace.setTextColor(0xff00DDDD);
		} else {
			holder.isPayInFace.setVisibility(View.GONE);
		}
		*/
		return convertView;
	}
	
	public final static class ViewHolder {
		public TextView orderNo;
		public TextView orderSumPrice;
		public TextView isPayInFace;
		public TextView isShipInFace;
		public TextView isReceiveInFace;
	}

}

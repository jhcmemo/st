package pipimy.cart;

import java.util.ArrayList;

import pipimy.service.R;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class StoreInfoProxy {

	private StoreInfoSQLiteHelper dbHelper;
	private SQLiteDatabase db;
	private Context mContext;
	
	
	public StoreInfoProxy(Context context) {
		this.mContext = context;
		initDB();
	}

	private void initDB() {
		// TODO Auto-generated method stub
		dbHelper = new StoreInfoSQLiteHelper(mContext);
		db = dbHelper.getWritableDatabase();
	}
	
	public ArrayList<String> getAreaNameInCountry(String name, String type) {
		ArrayList<String> areaList = new ArrayList<String>();
		
		try {
			Cursor c = null;
			c = db.query(name, null, null, null, null, null, null, null);
			if(c.getCount() > 0) {
				Log.e("getAreaNameInCountry", "BEGIN");
				for(int i = 0; i < c.getCount(); i++) {
					c.moveToPosition(i);
					if(type.equals("U")) {

						char[] aryCharSet = mContext.getResources().getString(R.string.char_set).toCharArray();
						String temp = c.getString(c.getColumnIndex("_AREA_STORE")).replace(aryCharSet[2], aryCharSet[3]);
						temp = temp.replace(aryCharSet[0], aryCharSet[3]);
						temp = temp.replace(aryCharSet[1], aryCharSet[3]);

						/*String temp = c.getString(c.getColumnIndex("_AREA_STORE")).replace('市', '區');
						temp = temp.replace('鄉', '區');
						temp = temp.replace('鎮', '區');*/

						areaList.add(temp);		
					} else {
						areaList.add(c.getString(c.getColumnIndex("_AREA_STORE")));
					}
					
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return areaList;
	}
	
}

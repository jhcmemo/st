package pipimy.cart;

public class CheckFeeObject {

	public String freeFee;
	public String[] deliveryFees = new String[]{"", "", "", "", "", "", ""};
	public String totalAmountFee;
	public String deliveryCost;
	public String productsAmount;
	
	public String getProductsAmount() {
		return productsAmount;
	}
	public void setProductsAmount(String productsAmount) {
		this.productsAmount = productsAmount;
	}
	public String getFreeFee() {
		return freeFee;
	}
	public void setFreeFee(String freeFee) {
		this.freeFee = freeFee;
	}
	public String[] getDeliveryFees() {
		return deliveryFees;
	}
	public void setDeliveryFees(String[] deliveryFees) {
		this.deliveryFees = deliveryFees;
	}
	public String getTotalAmountFee() {
		return totalAmountFee;
	}
	public void setTotalAmountFee(String totalAmountFee) {
		this.totalAmountFee = totalAmountFee;
	}
	public String getDeliveryCost() {
		return deliveryCost;
	}
	public void setDeliveryCost(String deliveryCost) {
		this.deliveryCost = deliveryCost;
	}
	
}

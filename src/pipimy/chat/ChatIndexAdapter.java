package pipimy.chat;

import java.util.ArrayList;

import pipimy.main.ApplicationController;
import pipimy.main.MyProductActivity;
import pipimy.others.Constant;
import pipimy.others.PreferenceUtil;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

@SuppressLint("InflateParams")
public class ChatIndexAdapter extends BaseExpandableListAdapter {

	private Context context;
	private int layout;
	private ArrayList<ChatIndexObject> list_seller;
	private ArrayList<ChatIndexObject> list_buyer;
	ImageLoader mImageLoader;
	RequestQueue queue;

	public ChatIndexAdapter(Context context, int layout,
			ArrayList<ChatIndexObject> list_seller,
			ArrayList<ChatIndexObject> list_buyer) {

		this.context = context;
		this.layout = layout;
		this.list_seller = list_seller;
		this.list_buyer = list_buyer;
		
		queue = VolleySingleton.getInstance().getRequestQueue();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
	}

	@Override
	public int getGroupCount() {
		return 2;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (groupPosition == 1) {
			return list_seller.size();
		} else {
			return list_buyer.size();
		}
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		View view = LayoutInflater.from(context).inflate(
				R.layout.cell_chat_group, null);
		TextView chat_group = (TextView) view.findViewById(R.id.chat_group);
		if (groupPosition == 1) {
			chat_group.setText(context.getString(R.string.chat_seller_chat));
		} else {
			chat_group.setText(context.getString(R.string.chat_buyer_chat));
		}
		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ChatIndexObject chatObject;
		// seller
		if (groupPosition == 1) {
			chatObject = list_seller.get(childPosition);
		}
		// buyer
		else {
			chatObject = list_buyer.get(childPosition);
		}

		View view = LayoutInflater.from(context).inflate(layout, parent, false);
		NetworkImageView imageView = (NetworkImageView) view
				.findViewById(R.id.cell_chatList_imageView);
		TextView titleTextView = (TextView) view
				.findViewById(R.id.cell_chatList_title_TextView);
		TextView userToTextView = (TextView) view
				.findViewById(R.id.cell_chatList_userToTextView);
		TextView updateTimeTextView = (TextView) view
				.findViewById(R.id.cell_chatList_updateTimeTextView);
		TextView lastSentenceTextView = (TextView) view
				.findViewById(R.id.cell_chatList_lastSentenceTextView);
		TextView badgeTextView = (TextView) view
				.findViewById(R.id.cell_chatList_badgeTextView);
		ImageView handImageView = (ImageView) view
				.findViewById(R.id.cell_chatList_handImageView);

		userToTextView.setOnClickListener(userIDClickListener);

		titleTextView.setText(chatObject.getProductTitle());
		// userToTextView.setText(chatObject.getUserIDTo());
		updateTimeTextView.setText(chatObject.getUpdateTime().substring(5, 16));
		lastSentenceTextView.setText(chatObject.getLastSentence());

		String memberID = PreferenceUtil.getString(
				ApplicationController.getAppContext(), Constant.USER_ID);
		if (chatObject.getUserIDFrom().equals(memberID)) {
			// from
			userToTextView.setText(chatObject.getUserIDTo());
			userToTextView.setTag(chatObject.getUserIDTo());

			if (chatObject.getUserIDFromBadge() != 0) {
				badgeTextView.setText("" + chatObject.getUserIDFromBadge());
			} else {
				badgeTextView.setVisibility(View.INVISIBLE);
			}
			setHandImageView(true, handImageView, chatObject);
		} else {
			// to
			userToTextView.setText(chatObject.getUserIDFrom());
			userToTextView.setTag(chatObject.getUserIDFrom());

			if (chatObject.getUserIDToBadge() != 0) {
				badgeTextView.setText("" + chatObject.getUserIDToBadge());
			} else {
				badgeTextView.setVisibility(View.INVISIBLE);
			}
			setHandImageView(false, handImageView, chatObject);
		}
		
		imageView.setImageUrl(Constant.PicServerURL + chatObject.getProductId()
						+ "-pic1.jpg", mImageLoader);

		/*
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				// this will make circle, pass the width of image
				.displayer(new RoundedBitmapDisplayer(3)).cacheOnDisc(true)
				.build();

		imageLoader
				.displayImage(Constant.PicServerURL + chatObject.getProductId()
						+ "-pic1.jpg", imageView, options);
*/
		return view;
	}

	private OnClickListener userIDClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			Intent intent = new Intent(context, MyProductActivity.class);
			intent.putExtra("memberID", (String) v.getTag());
			context.startActivity(intent);
		}
	};

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	private void setHandImageView(boolean isfrom, ImageView imageView,
			ChatIndexObject chatObject) {
		int fromHand = chatObject.getUserIDFromHand();
		int toHand = chatObject.getUserIDToHand();
		// String description = "";

		if (isfrom) {
			if (toHand == 1 && fromHand == 1) {// nobody
				imageView.setVisibility(View.INVISIBLE);
			} else if (toHand == 1 && fromHand == 2) {// me
				imageView.setImageResource(R.drawable.chat_hand_right);
				imageView.setTag("me");
			} else if (toHand == 2 && fromHand == 1) {// another
				imageView.setImageResource(R.drawable.chat_hand_left);
				imageView.setTag("another");
			} else if (toHand == 2 && fromHand == 2) {// both
				imageView.setImageResource(R.drawable.chat_hand_both);
				imageView.setTag("both");

			}
		} else {
			if (fromHand == 1 && toHand == 1) {// nobody
				imageView.setVisibility(View.INVISIBLE);
			} else if (fromHand == 1 && toHand == 2) {// me
				imageView.setImageResource(R.drawable.chat_hand_right);
				imageView.setTag("me");
			} else if (fromHand == 2 && toHand == 1) {// another
				imageView.setImageResource(R.drawable.chat_hand_left);
				imageView.setTag("another");
			} else if (fromHand == 2 && toHand == 2) {// both
				imageView.setImageResource(R.drawable.chat_hand_both);
				imageView.setTag("both");
			}
		}

		imageView.setOnClickListener(new OnClickListener() {
			String description = "";

			@Override
			public void onClick(View v) {

				// GlobalVariable.getInstance().sendGoogleAnalytic("android_chat_handoutside");
				if (v.getTag().equals("me")) {
					description = context.getString(R.string.chat_hand_me);
				} else if (v.getTag().equals("another")) {
					description = context.getString(R.string.chat_hand_another);
				} else if (v.getTag().equals("both")) {
					description = context.getString(R.string.chat_hand_both);
				}

				// DialogListUtil.showhandDialog(context, description);
			}
		});
	}
}

package pipimy.chat;

public class ChatContentObject {

	private String content;
	private String isFrom;
	private String sendTime;
	private boolean isSendSuccess = true;
	private String type;
	private boolean isDate;

	public boolean isDate() {
		return isDate;
	}

	public void setIsDate(boolean isDate) {
		this.isDate = isDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIsFrom() {
		return isFrom;
	}

	public void setIsFrom(String isFrom) {
		this.isFrom = isFrom;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public boolean isSendSuccess() {
		return isSendSuccess;
	}

	public void setSendSuccess(boolean isSendSuccess) {
		this.isSendSuccess = isSendSuccess;
	}

}

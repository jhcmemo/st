package pipimy.chat;

public class ChatIndexObject {

	private String chatId;
	private String userIDFrom;
	private String userIDTo;
	private String productId;
	private String productTitle;
	private String updateTime;
	private String lastSentence;
	private int userIDFromBadge;
	private int userIDToBadge;
	private int userIDFromHand;
	private int userIDToHand;

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	public String getUserIDFrom() {
		return userIDFrom;
	}

	public void setUserIDFrom(String userIDFrom) {
		this.userIDFrom = userIDFrom;
	}

	public String getUserIDTo() {
		return userIDTo;
	}

	public void setUserIDTo(String userIDTo) {
		this.userIDTo = userIDTo;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getLastSentence() {
		return lastSentence;
	}

	public void setLastSentence(String lastSentence) {
		this.lastSentence = lastSentence;
	}

	public int getUserIDFromBadge() {
		return userIDFromBadge;
	}

	public void setUserIDFromBadge(int userIDFromBadge) {
		this.userIDFromBadge = userIDFromBadge;
	}

	public int getUserIDToBadge() {
		return userIDToBadge;
	}

	public void setUserIDToBadge(int userIDToBadge) {
		this.userIDToBadge = userIDToBadge;
	}

	public int getUserIDFromHand() {
		return userIDFromHand;
	}

	public void setUserIDFromHand(int userIDFromHand) {
		this.userIDFromHand = userIDFromHand;
	}

	public int getUserIDToHand() {
		return userIDToHand;
	}

	public void setUserIDToHand(int userIDToHand) {
		this.userIDToHand = userIDToHand;
	}

}

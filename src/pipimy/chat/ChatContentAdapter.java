package pipimy.chat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import pipimy.others.CircleNetwork_ImageView;
import pipimy.others.Constant;
import pipimy.others.VolleySingleton;
import pipimy.service.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

public class ChatContentAdapter extends BaseAdapter {
	ImageLoader mImageLoader;

	private Context context;
	private ArrayList<ChatContentObject> list;
	private String fromOrTo;
	private String productID;
	private String targetID;
	private ChatContentActivity chatActivity;

	public ChatContentAdapter(ChatContentActivity activity,
			ArrayList<ChatContentObject> list, String fromOrTo,
			String productID, String targetID) {
		mImageLoader = VolleySingleton.getInstance().getImageLoader();

		this.context = activity.getBaseContext();
		this.chatActivity = activity;
		this.list = list;
		this.fromOrTo = fromOrTo;
		this.productID = productID;
		this.targetID = targetID;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		ChatContentObject chatContentObject = list.get(position);
		//Log.e("ChatContentAdapter", "fromOrTo =" +fromOrTo);
		
		if(chatContentObject.isDate()) {
			return date(convertView, parent, chatContentObject);
		} else {
			if (fromOrTo.equals("1")) {// from in the right side
				if (chatContentObject.getIsFrom().equals("1")) {
					/* from */
					return right(convertView, parent, chatContentObject);
				} else if (chatContentObject.getIsFrom().equals("2")) {
					/* to */
					return left(convertView, parent, chatContentObject);
				} else {
					return right(convertView, parent, chatContentObject);
				}
			} else if (fromOrTo.equals("2")) {// to in the right side
				if (chatContentObject.getIsFrom().equals("1")) {
					/* from */
					return left(convertView, parent, chatContentObject);
				} else if (chatContentObject.getIsFrom().equals("2")) {
					/* to */
					return right(convertView, parent, chatContentObject);
				} else {
					return left(convertView, parent, chatContentObject);
				}
			} 
		}

		return view;
	}
	
	private View date(View convertView, ViewGroup parent,
			ChatContentObject chatContentObject) {
		View view = convertView;
		view = LayoutInflater.from(context).inflate(
				R.layout.cell_chatcontent_date, parent, false);
		
		String sendTime = chatContentObject.getSendTime().replace('-', '/');
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat weekFormat = new SimpleDateFormat("E");
		String date = "";
		try {
			Date dt =sdf.parse(sendTime);
			date = sendTime.substring(0, 10) + " " + "(" + weekFormat.format(dt) + ")";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TextView txtDate = (TextView)view.findViewById(R.id.txt_date);
		txtDate.setText(date);
		
		return view;
	}

	private View right(View convertView, ViewGroup parent,
			ChatContentObject chatContentObject) {
		View view = convertView;

		view = LayoutInflater.from(context).inflate(
				R.layout.cell_chatcontent_right, parent, false);
		TextView contentTextView = (TextView) view
				.findViewById(R.id.cell_chatContentFrom_contentTextView);
		TextView sendTimeTextView = (TextView) view
				.findViewById(R.id.cell_chatContentTo_timeTextView);

		if (!chatContentObject.isSendSuccess()) {
			contentTextView.setTextColor(0xffff0000);
		} else {
			contentTextView.setTextColor(0xffffffff);
		}

		contentTextView.setText(chatContentObject.getContent());
		try {
			sendTimeTextView.setText(chatContentObject.getSendTime().substring(11, 16));
		} catch(Exception e) {
			e.printStackTrace();
			sendTimeTextView.setText("0000-00-00 00:00");
		}
		
		return view;
	}

	private View left(View convertView, ViewGroup parent,
			ChatContentObject chatContentObject) {

		View view = convertView;

		view = LayoutInflater.from(context).inflate(
				R.layout.cell_chatcontent_left, parent, false);
		LinearLayout msgLayout = (LinearLayout) view
				.findViewById(R.id.layout_msg_content);
		ImageView linkIcon = (ImageView) view.findViewById(R.id.imageView1);
		TextView contentTextView = (TextView) view
				.findViewById(R.id.cell_chatContentFrom_contentTextView);
		TextView sendTimeTextView = (TextView) view
				.findViewById(R.id.cell_chatContentFrom_timeTextView);
		TextView targetIDTextView = (TextView) view
				.findViewById(R.id.cell_chatContentFrom_targetIDTextView);
		CircleNetwork_ImageView imageView = (CircleNetwork_ImageView) view
				.findViewById(R.id.cell_chatContentFrom_image);
		imageView.setImageUrl(Constant.STORE_IMAGE_URL + targetID + "-s.jpg",
				mImageLoader);

		targetIDTextView.setText(targetID);
		String content = "";
		if (chatContentObject.getType().equals("1")) {
			content = chatContentObject.getContent();
			contentTextView.setText(content);
			contentTextView.setVisibility(View.VISIBLE);
			linkIcon.setVisibility(View.GONE);
		} else if (chatContentObject.getType().equals("2")) {
			contentTextView.setVisibility(View.GONE);
			linkIcon.setVisibility(View.VISIBLE);
			msgLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					chatActivity.getSpecificProduct();
				}
			});
		} else {
			content = chatContentObject.getContent();
			contentTextView.setText("");
			contentTextView.setVisibility(View.VISIBLE);
			linkIcon.setVisibility(View.GONE);
		}
		sendTimeTextView.setText(chatContentObject.getSendTime().substring(11, 16));
		return view;
	}
}

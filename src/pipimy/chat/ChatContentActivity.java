package pipimy.chat;

import java.util.ArrayList;

import pipimy.main.ApplicationController;
import pipimy.nearby.ProductObject;
import pipimy.others.ApiProxy;
import pipimy.others.Constant;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.others.VolleySingleton;
import pipimy.product.activity.Product_Detail_Activity;
import pipimy.service.CommonUtilities;
import pipimy.service.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;

public class ChatContentActivity extends Activity {

	private final static String TAG = "ChatContentActivity";

	ListView listView;
	EditText sendEditText;
	Button sendButton;

	private ChatContentAdapter adapter;

	private String chatId;
	private String productId;
	private int select_group;
	private int select_child;
	private String isFrom;
	private String targetID;
	private String content;

	private Menu menu;
	private ChatIndexObject chatIndexObject;

	//ArrayList<ChatContentObject> chatList = new ArrayList<ChatContentObject>();
	ImageLoader mImageLoader;
	RequestQueue queue;
	ProductObject object = new ProductObject();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_chatcontent);
		
		queue = VolleySingleton.getInstance().getRequestQueue();
		mImageLoader = VolleySingleton.getInstance().getImageLoader();

		select_group = getIntent().getIntExtra(ChatUtil.SELECT_GROUP, 0);
		select_child = getIntent().getIntExtra(ChatUtil.SELECT_CHILD, 0);
		chatId = getIntent().getStringExtra(ChatUtil.CHAT_ID);
		productId = getIntent().getStringExtra(ChatUtil.PRODUCT_ID);
		for(ProductObject obj: ApplicationController.nearProductList) {
			if(obj.getProductID().equals(productId)) {
				//object = obj;
			}
		}

		listView = (ListView) findViewById(R.id.chatContent_listview);
		sendEditText = (EditText) findViewById(R.id.chatContent_sendEditText);
		sendButton = (Button) findViewById(R.id.chatContent_sendButton);
		sendButton.setOnClickListener(itemClickListener);

		getActionBar().setTitle(getString(R.string.login_login));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		init();
		initGCM();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (ChatMessageReceiver != null) {
			try {
				this.unregisterReceiver(ChatMessageReceiver);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	@Override
	public void onBackPressed() {
		/*
		 * Intent intent = new Intent(); intent.putExtra("chatId", chatId);
		 * intent.putExtra("isFrom", isFrom); setResult(6, intent); finish();
		 */
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		this.menu = menu;
		menu.clear();
		if (select_group == 1) {
			getMenuInflater().inflate(R.menu.chat_content_seller, menu);
		} else {
			getMenuInflater().inflate(R.menu.chat_content, menu);
		}

		mImageLoader.get(Constant.PicServerURL + productId + "-pic1.jpg",
				new ImageLoader.ImageListener() {
					@Override
					public void onErrorResponse(VolleyError err) {
						// TODO Auto-generated method stub
						Log.e(TAG, "get product icon bitmap err:"+err.getMessage());
					}

					@Override
					public void onResponse(ImageContainer response, boolean arg1) {
						Bitmap bmp = response.getBitmap();
						if(bmp != null) {
							int width = (int) Util.convertDpToPixel(48, ChatContentActivity.this);
							int height = (int) Util.convertDpToPixel(48, ChatContentActivity.this);
							int w = bmp.getWidth();
							int h = bmp.getHeight();
							Matrix matrix = new Matrix();
							float scaleWidth = ((float) width / w);
							float scaleHeight = ((float) height / h);
							matrix.postScale(scaleWidth, scaleHeight);
							Bitmap newbmp = Bitmap.createBitmap(bmp, 0, 0, w, h, matrix, true);
							Drawable drawable = new BitmapDrawable(getResources(), newbmp);
							menu.getItem(0).setIcon(drawable);
						}
					}

				});
		

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent();
			intent.putExtra("chatId", chatId);
			intent.putExtra("isFrom", isFrom);
			setResult(6, intent);
			finish();
			break;
		case R.id.action_chat_product:
			getSpecificProduct();
			break;
		case R.id.action_chat_product_re_pricing:
			rePricing();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void init() {
	
		// seller
		if (select_group == 1) {
			chatIndexObject = ApplicationController.chatIndexList_seller
					.get(select_child);
		}
		// buyer
		else {
			chatIndexObject = ApplicationController.chatIndexList_buyer
					.get(select_child);
		}

		String memberID = PreferenceUtil.getString(
				ApplicationController.getAppContext(), Constant.USER_ID);
		if (chatIndexObject.getUserIDFrom().equals(memberID)) {
			// self is from
			isFrom = "1";
			targetID = chatIndexObject.getUserIDTo();
		} else {
			// self is to
			isFrom = "2";
			targetID = chatIndexObject.getUserIDFrom();
		}
		getActionBar().setTitle(
				Html.fromHtml("<b><font color=\"#514f4e\">"
						+ targetID
						+ "</font></b>"));

		getChatContent();

	}

	private void rePricing() {
		// TODO Auto-generated method stub
		final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		final Dialog dialog = new Dialog(this, R.style.selectorDialog);
		dialog.setContentView(R.layout.dialog_re_pricing);
		dialog.show();

		final EditText price = (EditText) dialog.findViewById(R.id.edittext_price);
		final EditText stock = (EditText) dialog.findViewById(R.id.edittext_stock);

		TextView btnConfirm = (TextView) dialog.findViewById(R.id.btn_confirm);
		btnConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				
				String strPrice = Util.getEditText(price);
				String strStock = Util.getEditText(stock);
				
				if(object != null) {
					if(strPrice.length() > 0 && strStock.length() > 0) {
						if(Integer.parseInt(strStock) > Integer.parseInt(strPrice)) {
							Log.e(TAG, "stock error");
							dialog.dismiss();
							Toast.makeText(ChatContentActivity.this, "stock error", 1500).show();
						} else {
							dialog.dismiss();
							setBargainProduct(productId, targetID, strPrice, strStock);
						}
					} else {
						Log.e(TAG, "input error");
						dialog.dismiss();
						Toast.makeText(ChatContentActivity.this, "input error", 1500).show();
					}
					
				}
			
			}
		});

		TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	}

	private void goToProductDetail() {
		// TODO Auto-generated method stub
		Log.e(TAG, "goToProductDetail BEGIN");

		Intent intent = new Intent(ChatContentActivity.this,
				Product_Detail_Activity.class);

		if (object != null) {
			intent.putExtra("product_object", object);
			startActivity(intent);
		}

	}

	private OnClickListener itemClickListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			content = Util.getEditText(sendEditText);
			if (!content.equals("")) {
				sendMsg(content, "1");
			}
		}

	};

	protected void addItemToListContent(final ChatContentObject obj) {
		// TODO Auto-generated method stub
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				ApplicationController.chatContentList.add(obj);
				String date = obj.getSendTime().substring(0, 10);
				String lastChatDate = "";
				for(int i = 0; i < ApplicationController.chatContentList.size(); i++) {
					if(ApplicationController.chatContentList.get(i).isDate()) {
						if(!lastChatDate.equals(ApplicationController.chatContentList.get(i).getSendTime())) 
							lastChatDate = ApplicationController.chatContentList.get(i).getSendTime();
					}
				}
					
				if(!date.equals(lastChatDate)) {
					//date = lastChatDate;
					ChatContentObject object = new ChatContentObject();
					object.setIsDate(true);
					object.setSendTime(date);
						
					ApplicationController.chatContentList.add(ApplicationController.chatContentList.size()-1, object);
				}
				
				adapter.notifyDataSetChanged();
				listView.setSelection(ApplicationController.chatContentList
						.size());

				sendEditText.setText("");
			}
		});

	}
	
	protected void listViewIsChanged() {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				adapter.notifyDataSetChanged();
				listView.setSelection(ApplicationController.chatContentList
						.size() - 1);
			}
		});
	}
	
	protected void sendMsg(String content, String type) {
		ChatContentObject contentObject = new ChatContentObject();
		
		contentObject.setContent(content);
		contentObject.setSendTime(Util.getTime());
		contentObject.setSendSuccess(false);
		contentObject.setIsFrom(isFrom);

		addItemToListContent(contentObject);
		sendChatContent(content, Util.getTime(), type);
	}

	/*
	 * HTTP Request and Response Handler
	 */
	private void apiRequest(final int code, final String[] params, final String[] data) {
		new Thread() {
			@Override
			public void run() {
				switch(code) {
				case Constant.CODE_SET_BARGAIN: {
					Message msg = ApiProxy.bargainProduct(params, data, ChatContentActivity.this);
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_GET_CHAT_CONTENT: {
					Message msg = ApiProxy.getChatContent(params, data, ChatContentActivity.this);
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_SEND_CHAT_CONTENT: {
					Message msg = ApiProxy.sendChatContent(params, data, ChatContentActivity.this);
					handler.sendMessage(msg);
					break;
				}
				case Constant.CODE_GET_ONE_PRODUCT: {
					Message msg = ApiProxy.getOneProduct(params, data, ChatContentActivity.this);
					handler.sendMessage(msg);
					break;
				}
				}
			}
		}.start();
	}
	
	private void getChatContent() {
		String[] params = { "ChatId", "IsFrom" };
		String[] data = { chatId, isFrom };
		apiRequest(Constant.CODE_GET_CHAT_CONTENT, params, data);
	}
	
	
	private void setBargainProduct(String productID, String buyerID, String price, String stock) {
		String[] params = { "ProductID", "BuyerID", "Price", "Stock" };
		String[] data = { productID, buyerID, price, stock };
		apiRequest(Constant.CODE_SET_BARGAIN, params, data);
	}

	protected void sendChatContent(final String content, final String time, final String type) {
		String[] params = null; // = {"ChatId", "IsFrom", "Content", "SendTime", "TargetID"};
		String[] data = null; // = {chatId, isFrom, content, time, targetID};
		
		if(type.equals("1")) {
			params = new String[]{"ChatId", "IsFrom", "Content", "SendTime", "TargetID"};
			data = new String[]{chatId, isFrom, content, time, targetID};
		} else if(type.equals("2")) {
			params = new String[]{"ChatId", "IsFrom", "Content", "SendTime", "TargetID", "Type"};
			data = new String[]{chatId, isFrom, content, time, targetID, type};
		}
		
		apiRequest(Constant.CODE_SEND_CHAT_CONTENT, params, data);
	}

	public void getSpecificProduct(){
		String[] params = { "ProductID" };
		String[] data = { productId };
		Util.showProgressDialog(this, "", "");
		apiRequest(Constant.CODE_GET_ONE_PRODUCT, params, data);
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case Constant.CODE_GET_ONE_PRODUCT:
				Util.dismissProgressDialog();
				object = (ProductObject)msg.obj;
				goToProductDetail();
				break;
			case Constant.CODE_SET_BARGAIN:
				String res = msg.obj.toString();
				Toast.makeText(ChatContentActivity.this, res, 1500).show();
				if(res.contains("success") || res.contains("repeat"))
					sendMsg("re-price success", "2");
				break;
			case Constant.CODE_RETURN_ERR:
				Util.dismissProgressDialog();
				String errMsg = "Error";
				Toast.makeText(ChatContentActivity.this, errMsg, 1500).show();
				break;
			case Constant.CODE_GET_CHAT_CONTENT: {
				// chatList.clear();
				//chatList = ApplicationController.chatContentList;
				String date = "";
				for(int i = 0; i < ApplicationController.chatContentList.size(); i++) {
					if(!date.equals(ApplicationController.chatContentList.get(i).getSendTime().substring(0, 10))) {
						date = ApplicationController.chatContentList.get(i).getSendTime().substring(0, 10);
						ChatContentObject object = new ChatContentObject();
						object.setIsDate(true);
						object.setSendTime(date);
						
						ApplicationController.chatContentList.add(i, object);
					}
				}
				adapter = new ChatContentAdapter(ChatContentActivity.this,
						ApplicationController.chatContentList, isFrom, productId, targetID);
				listView.setAdapter(adapter);
				listViewIsChanged();
		
				break;
			}
			case Constant.CODE_SEND_CHAT_CONTENT: {
				ApplicationController.chatContentList.get(ApplicationController.chatContentList
						.size() - 1).setSendSuccess(true);
				listViewIsChanged();
				break;
			}
			}

		}
	};
	
	/*
	 * Init GCM and register receiver
	 */
	private void initGCM() {
		// TODO Auto-generated method stub
		this.registerReceiver(ChatMessageReceiver, new IntentFilter(
				CommonUtilities.DISPLAY_MESSAGE_CHAT));
	}

	private final BroadcastReceiver ChatMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.e(TAG, "ChatMessageReceiver BEGIN");
			if (intent.getAction().equals(CommonUtilities.DISPLAY_MESSAGE_CHAT)) {

				/*-------- Get Data --------*/
				String message = intent
						.getStringExtra(CommonUtilities.CHAT_MESSAGE);
				String GCM_chatId = intent
						.getStringExtra(CommonUtilities.CHAT_ID);
				String isFromGCM = intent
						.getStringExtra(CommonUtilities.CHAT_ISFROM);
				// String sendTime =
				// intent.getStringExtra(CommonUtilities.CHAT_SENDTIME);

				// in current chat
				if (GCM_chatId.equals(chatId)) {

					/*-------- Chat hand -------*/
					if (isFromGCM.equals("3") || isFromGCM.equals("4")) {
					}

					/*-------- Chat message -------*/
					else {
						getChatContent();
					}
				}

				// in other chat
				else {
					Toast.makeText(ChatContentActivity.this, message,
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	};

	
}

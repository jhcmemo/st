package pipimy.chat;

public class ChatUtil {

	// intent tag
	public static final String SELECT_GROUP = "select_group";
	public static final String SELECT_CHILD = "select_child";
	public static final String CHAT_ID = "chatID";
	public static final String PRODUCT_ID = "productID";
	// public static final String TARGET_ID = "targetID";

	/** =============================================================== */
	/** ========================== Interface ========================== */
	/** =============================================================== */
 
	public static onNaviRightOnClickListener naviRightOnClickCallback;
	public static onGetChatIndexCallback onChatIndexCallback;
	public static onGetLogout onlogout;

	public static onGetChatContentCallback chatContentCallback;
	public static onChatContentSendSuccessCallback chatContentSendSuccessCallback;
	public static onDeleteChatIndexCallback deleteChatIndexCallback;
	
	public static onMainCallback mainCallback;

	public static onChatHandCallback chatHandCallback;

	public interface onNaviRightOnClickListener {
		public void naviRightOnClick();
	}

	public static void setOnNaviRightOnClickListener(onNaviRightOnClickListener naviRightOnClickCallback) {
		ChatUtil.naviRightOnClickCallback = naviRightOnClickCallback;
	}

	public interface onGetChatIndexCallback {
		public void onGetChatIndex();
	}

	public static void setOnLogoutcallback(onGetLogout logout) {
		ChatUtil.onlogout = logout;
	}
	
	public static void setOnGetChatIndexCallback(onGetChatIndexCallback chatIndexCallback) {
		ChatUtil.onChatIndexCallback = chatIndexCallback;
	}

	public interface onGetChatContentCallback {
		public void onGetChatContent();
	}
	
	public interface onGetLogout {
		public void onLogoutcallback();
	}
	 

	public static void setOnGetChatContentCallback(onGetChatContentCallback chatContentCallback) {
		ChatUtil.chatContentCallback = chatContentCallback;
	}

	public interface onChatContentSendSuccessCallback {
		public void onChatContentSendSuccess();
	}

	public static void setOnChatContentSendSuccess(onChatContentSendSuccessCallback chatContentSendSuccessCallback) {
		ChatUtil.chatContentSendSuccessCallback = chatContentSendSuccessCallback;
	}

	public interface onDeleteChatIndexCallback {
		public void onDeleteChatIndex();

	}

	public static void setOnDeleteChatIndexCallback(onDeleteChatIndexCallback deleteChatIndex) {
		ChatUtil.deleteChatIndexCallback = deleteChatIndex;
	}
	
	
	public interface onMainCallback {
		public void menu_state_check();

	}

	public static void setOnMainCallback(onMainCallback main) {
		ChatUtil.mainCallback = main;
	}
	
	

	public interface onChatHandCallback {
		public void onChatHand(String isFrom);
	}
	
	
	

	public static void setOnChatHandCallback(onChatHandCallback onChatHand) {
		ChatUtil.chatHandCallback = onChatHand;
	}
}

package pipimy.setting.activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.Http;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StoreFlowSettingActivity extends Activity {

	private final static String TAG = "StoreFlowSettingActivity";
	// private final static int DONE_SETTING_STORE_FLOW = 10001;

	EditText member_cash_type;
	EditText member_store_type;
	EditText member_face_location1;
	EditText member_face_location2;
	EditText editText_delivery_free_money;
	EditText editText_delivery_fee_money1;
	EditText editText_delivery_fee_money2;
	EditText editText_delivery_fee_money3;
	EditText editText_receiverName;

	Button updateStoreFlowBtn;

	int cashTypeTotal = 0;
	int storeTypeTotal = 0;
	int deliveryFreeMoney = -999;
	int deliveryFeeMoney_1 = -999;
	int deliveryFeeMoney_2 = -999;
	int deliveryFeeMoney_3 = -999;

	String location1 = "";
	String location2 = "";
	String updTime = "";
	String strCashType = "";
	String strStoreType = "";
	String receiverName = "";
	
	boolean isCashFace;
	boolean isDeliveryFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_storeflow_setting);

		member_cash_type = (EditText) findViewById(R.id.cashType);
		member_store_type = (EditText) findViewById(R.id.storeType);
		member_face_location1 = (EditText) findViewById(R.id.faceLoc1);
		member_face_location2 = (EditText) findViewById(R.id.faceLoc2);
		editText_delivery_free_money = (EditText) findViewById(R.id.editText_delivery_free);
		editText_delivery_fee_money1 = (EditText) findViewById(R.id.editText_delivery_fee1);
		editText_delivery_fee_money2 = (EditText) findViewById(R.id.editText_delivery_fee2);
		editText_delivery_fee_money3 = (EditText) findViewById(R.id.editText_delivery_fee3);
		editText_receiverName = (EditText) findViewById(R.id.receiverName);
		
		updateStoreFlowBtn = (Button) findViewById(R.id.updateStoreFlowDone);
		member_cash_type.setOnClickListener(itemClick);
		member_store_type.setOnClickListener(itemClick);
		updateStoreFlowBtn.setOnClickListener(itemClick);
		
		
		init_text();

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(R.drawable.no_icon);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle(getString(R.string.actionbat_store_flow_title));
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			return true;

		}

	}

	private void init_text(){
		
		
		member_cash_type.setText(PreferenceUtil.getString(this, Constant.USER_CASH_FLOW));
		member_store_type.setText(PreferenceUtil.getString(this, Constant.USER_STORE_FLOW));
		member_face_location1.setText(PreferenceUtil.getString(this, Constant.USER_FACE_LOC1));
		member_face_location2.setText(PreferenceUtil.getString(this, Constant.USER_FACE_LOC2)); 
		editText_delivery_free_money.setText(PreferenceUtil.getString(this, Constant.USER_DELIVERY_FREE)); 
		editText_delivery_fee_money1.setText(PreferenceUtil.getString(this, Constant.USER_DELIVERY_FEE1)); 
		editText_delivery_fee_money2 .setText(PreferenceUtil.getString(this, Constant.USER_DELIVERY_FEE2)); 
		editText_delivery_fee_money3.setText(PreferenceUtil.getString(this, Constant.USER_DELIVERY_FEE3)); 
		editText_receiverName .setText(PreferenceUtil.getString(this, Constant.RECEIVER_NAME)); 
		
		
		
	}
	
	private OnClickListener itemClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.cashType: {
				doPickCashType();
				break;
			}
			case R.id.storeType: {
				doPickStoreType();
				break;
			}
			case R.id.updateStoreFlowDone: {
				doUpdate();
				break;
			}
			}
		}

	};

	protected void doPickCashType() {
		// TODO Auto-generated method stub
		cashTypeTotal = 0;
		final CharSequence[] items = getResources().getStringArray(
				R.array.cash_typs);
		final int[] cashTypeNum = { 1, 2, 4, 16, 32, 64 };
		final boolean[] isItemChecked = new boolean[cashTypeNum.length];
		
		if(isDeliveryFace) isItemChecked[3] = true;
	

		AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setTitle(getResources().getString(R.string.cash_type));
		b.setMultiChoiceItems(items, isItemChecked,
				new DialogInterface.OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						
						if(isDeliveryFace) {
							 ((AlertDialog)dialog).getListView().setItemChecked(3, true);
						}
						
						isItemChecked[which] = isChecked;
						
					}
				});
		b.setPositiveButton("ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				if(isItemChecked[3]) isCashFace = true;
				
				String str = "";
				for (int i = 0; i < isItemChecked.length; i++) {
					if (isItemChecked[i]) {
						cashTypeTotal = cashTypeTotal + cashTypeNum[i];
						str = str + items[i] + ",";
						// save preference by index
						//strCashType = strCashType + i + ",";
						strCashType = strCashType + items[i] + ",";
					}
				}
				Log.e(TAG, "cashTypeTotal = " + cashTypeTotal);
				//PreferenceUtil.setString(StoreFlowSettingActivity.this, "cash_type", str);
				member_cash_type.setText(str);
			}
		});
		b.show();

	}

	protected void doPickStoreType() {
		// TODO Auto-generated method stub
		storeTypeTotal = 0;
		final CharSequence[] items = getResources().getStringArray(
				R.array.store_typs);
		final int[] storeTypeNum = { 1, 2, 4, 16 };
		final boolean[] isItemChecked = new boolean[storeTypeNum.length];
		if(isCashFace) isItemChecked[3] = true;

		AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setTitle(getResources().getString(R.string.store_type));
		b.setMultiChoiceItems(items, isItemChecked,
				new DialogInterface.OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						if(isCashFace) {
							 ((AlertDialog)dialog).getListView().setItemChecked(3, true);
						}
						isItemChecked[which] = isChecked;
					}
				});
		b.setPositiveButton("ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				if(isItemChecked[3]) isDeliveryFace = true;
				
				String str = "";
				for (int i = 0; i < isItemChecked.length; i++) {
					if (isItemChecked[i]) {
						storeTypeTotal = storeTypeTotal + storeTypeNum[i];
						str = str + items[i] + ",";
						//strStoreType = strStoreType + i + ",";
						strStoreType =  strStoreType + items[i] + ",";
					}
				}
				Log.e(TAG, "storeTypeTotal = " + storeTypeTotal);
				//PreferenceUtil.setString(StoreFlowSettingActivity.this, "store_type", str);

				member_store_type.setText(str);
			}
		});
		b.show();
	}

	/*
	 * Update user store flow settings
	 */
	protected void doUpdate() {
		// TODO Auto-generated method stub
		Log.e(TAG, "doUpdate BEGIN");
		if (checkDone()) {
			new Thread() {
				@Override
				public void run() {

					String[] params = { "StoreCash", "StoreDelivery",
							"StoreFaceLoc1", "StoreFaceLoc2", "UpdateTime",
							"StoreDeliveryFree", "StoreDeliveryFee1",
							"StoreDeliveryFee2", "StoreDeliveryFee4",
							"StoreDeliveryFee8", "StoreDeliveryFee16",
							"StoreDeliveryFee32", "StoreDeliveryFee64", "receiverName" };
					String[] data = { Integer.toString(cashTypeTotal),
							Integer.toString(storeTypeTotal), location1,
							location2, updTime,
							Integer.toString(deliveryFreeMoney),
							Integer.toString(deliveryFeeMoney_1),
							Integer.toString(deliveryFeeMoney_2),
							Integer.toString(deliveryFeeMoney_3), "0", "0", "0", "0",
							receiverName };

					String result = Http.post(Constant.UPDATE_STORE_FLOW,
							params, data, StoreFlowSettingActivity.this);
					Log.e(TAG, "doUpdate res = " + result);

					Message msg = new Message();
					msg.obj = result;

					handler.sendMessage(msg);
				}

			}.start();
		}
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			
			if (msg.obj.toString().contains("success")) {
				
				Toast.makeText(StoreFlowSettingActivity.this, getString(R.string.toast_success),1500).show();
				cacheStoreFlowSettings();
			}

		}
	};

	protected void cacheStoreFlowSettings() {
		// TODO Auto-generated method stub
		Log.e(TAG, "cacheStoreFlowSettings");

		PreferenceUtil.setString(this, Constant.USER_CASH_FLOW, strCashType);
		PreferenceUtil.setString(this, Constant.USER_STORE_FLOW, strStoreType);
		PreferenceUtil.setString(this, Constant.USER_FACE_LOC1, location1);
		PreferenceUtil.setString(this, Constant.USER_FACE_LOC2, location2);
		PreferenceUtil.setString(this, Constant.USER_DELIVERY_FREE,
				Integer.toString(deliveryFreeMoney));
		PreferenceUtil.setString(this, Constant.USER_DELIVERY_FEE1,
				Integer.toString(deliveryFeeMoney_1));
		PreferenceUtil.setString(this, Constant.USER_DELIVERY_FEE2,
				Integer.toString(deliveryFeeMoney_2));
		
		PreferenceUtil.setString(this, Constant.USER_DELIVERY_FEE3,
				Integer.toString(deliveryFeeMoney_3));

		PreferenceUtil.setString(this, Constant.RECEIVER_NAME, receiverName);

		this.finish();
	}

	private boolean checkDone() {
		// TODO Auto-generated method stub

		location1 = Util.getEditText(member_face_location1);
		location2 = Util.getEditText(member_face_location2);
		receiverName = Util.getEditText(editText_receiverName);
		updTime = getTime();

		try {
			deliveryFreeMoney = Integer.parseInt(Util
					.getEditText(editText_delivery_free_money));
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			deliveryFeeMoney_1 = Integer.parseInt(Util
					.getEditText(editText_delivery_fee_money1));
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			deliveryFeeMoney_2 = Integer.parseInt(Util
					.getEditText(editText_delivery_fee_money2));
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			deliveryFeeMoney_3 = Integer.parseInt(Util
					.getEditText(editText_delivery_fee_money3));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (cashTypeTotal == 0) {
			DialogUtil.pushPureDialog(this, getString(R.string.dailog_title_hint),getString(R.string.must_setting_cash_flow),
					getString(R.string.Confirm));
		} else if (storeTypeTotal == 0) {
			DialogUtil.pushPureDialog(this, getString(R.string.dailog_title_hint), getString(R.string.must_setting_store_flow),
					getString(R.string.Confirm));
		} else if (deliveryFreeMoney < 0) {
			//免運
			DialogUtil.pushPureDialog(this, getString(R.string.dailog_title_hint),
					getString(R.string.must_setting_dv0), getString(R.string.Confirm));
		} else if (deliveryFeeMoney_1 < 0) {
			//全家
			DialogUtil.pushPureDialog(this, getString(R.string.dailog_title_hint),
					getString(R.string.must_setting_dv1), getString(R.string.Confirm));
		} else if (deliveryFeeMoney_2 < 0) {
			
			//自寄
			DialogUtil.pushPureDialog(this, getString(R.string.dailog_title_hint),
					getString(R.string.must_setting_dv2), getString(R.string.Confirm));
		} else if (deliveryFeeMoney_3 < 0) {
			//7-11
			DialogUtil.pushPureDialog(this, getString(R.string.dailog_title_hint),
					getString(R.string.must_setting_dv3), getString(R.string.Confirm));
		} else if (receiverName.trim().equals("")) {
			DialogUtil.pushPureDialog(this, getString(R.string.dailog_title_hint), "must set receiver name",
					getString(R.string.Confirm));
		}

		else {
			return true;
		}

		return false;
	}

	protected String getTime() {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dt = new Date();
		String dts = sdf.format(dt);
		return dts;
	}
}

package pipimy.setting.activity;

import pipimy.main.ApplicationController;
import pipimy.order.OrderObject;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyCommentListActivity extends Activity {

	private static final String TAG = "MyCommentListActivity";
	ListView commentList;
	CommonAdapter<RatingObject> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mycomment_list);

		commentList = (ListView) findViewById(R.id.listView1);

		getData();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initViews() {
		adapter = new CommonAdapter(this,
				ApplicationController.myBoughtRatingList,
				R.layout.cell_rating_list) {
			@Override
			public void setViewData(CommonViewHolder commonViewHolder,
					View currentView, Object item) {
				RatingObject obj = new RatingObject();
				
				TextView txt_order_id = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_order_id);
				TextView txt_seller_name = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_seller_name);
				TextView txt_rating_score = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_rating_score);
				TextView txt_comment = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_comment);
				TextView txt_rating_time = (TextView) commonViewHolder.get(
						commonViewHolder, currentView, R.id.txt_rating_time);
				
				txt_order_id.setText(obj.getRatingOrderId());
				txt_seller_name.setText(obj.getSellerId());
				txt_rating_score.setText(obj.getRatingScore());
				txt_comment.setText(obj.getRatingComment());
				txt_rating_time.setText(obj.getRatingTime());
			}

		};
		
		commentList.setAdapter(adapter);
	}

	private void getData() {
		Util.showProgressDialog(this, "wait", "loading");

		new Thread() {
			@Override
			public void run() {
				String[] params = new String[] { "" };
				String[] data = new String[] { "" };
				String result = Http.get(Constant.GET_BUYER_RATING, params,
						data, MyCommentListActivity.this);
				Log.e(TAG, "GET_BUYER_RATING res = " + result);

				try {
					JSONParserTool.getBoughtRatingHistoryList(result,
							ApplicationController.myBoughtRatingList);
				} catch (Exception e) {
					e.printStackTrace();
				}

				Message msg = new Message();
				msg.arg1 = Constant.CODE_GET_BUYER_RATING;
				handler.sendMessage(msg);
			}
		}.start();
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.arg1) {
			case Constant.CODE_GET_BUYER_RATING:
				Util.dismissProgressDialog();
				initViews();
				break;
			}
			
		}
	};

}

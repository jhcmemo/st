package pipimy.setting.activity;

import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import com.android.volley.toolbox.NetworkImageView;

import pipimy.main.MyProductActivity;
import pipimy.object.SomeoneRatingObject;
import pipimy.object.TrackObject;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.DialogListUtil.OnSomeoneClickListener;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.PreferenceUtil;
import pipimy.service.R;




import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

 public class SomeoneRatingActivity extends Activity {

	private String otherMemberID = "";
	//private ArrayList<SomeoneRatingObject> list;
    TextView someoneRating_no_data_view;
    TextView countTextView ;
    TextView averageTextView;
    TextView dealTimesTextView;
    RatingBar avgRatingBar;
    LinearLayout lv_someoneRating_detait_rating;
    ProgressDialog progressDialog;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		displayProgressDialog("Loading...","Data Loading...please wait");

		setContentView(R.layout.activity_someonerating);
		
		someoneRating_no_data_view=(TextView) findViewById(R.id.someoneRating_no_data_view);
	
		 countTextView = (TextView) findViewById(R.id.SomeoneRating_countTextView);
		 averageTextView = (TextView) findViewById(R.id.SomeoneRating_averageTextView);
	     dealTimesTextView = (TextView) findViewById(R.id.SomeoneRating_dealTimesTextView);
		 avgRatingBar = (RatingBar) findViewById(R.id.rating_ratingBar);
		 lv_someoneRating_detait_rating=(LinearLayout) findViewById(R.id.lv_someoneRating_detait_rating);
		init(getIntent().getStringExtra("memberID"));
	}


	/** =============================================================== */
	/** =========================== Actionbar ========================= */
	/** =============================================================== */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();

		//Drawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.style_red));
	//	Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
		//LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
		//getActionBar().setBackgroundDrawable(ld);
//		getActionBar().setTitle(getString(R.string.setting_myrating));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	

	/** =============================================================== */
	/** =========================== initial =========================== */
	/** =============================================================== */
	private void init(String memberID) {
		lv_someoneRating_detait_rating.setVisibility(View.GONE);

		
		if (memberID != null) {
			//HTTP_getSomeoneRating(memberID);
			GetSomeoneRating(memberID,false);
			getActionBar().setTitle(memberID);
		} else {
			
			//HTTP_getSomeoneRating(memberID);
			
			//GetSomeoneRating();
			getActionBar().setTitle(memberID);
			
			
		}
		
	}
	
	private void GetSomeoneRating(final String memberID,final boolean if_otherMember){
		new Thread() {
			@Override
			public void run() {
				 
				String[] params = { "MemberID" };
				String[] data = { memberID };
				String result = Http.post(Constant.GET_SOMEONE_RATING, params,data, SomeoneRatingActivity.this); 
				   Bundle b =new Bundle();
		            Message m=new  Message();
				try {
				//	Log.d("GetSomeoneRating","取得 : "+result);
					 
					JSONParserTool.getSomeoneRating(result, Constant.someoneRatingObject, Constant.someoneRatingList);
					//Get_rating_success.sendMessage(Get_rating_success.obtainMessage());
					
					/* ----- Get my track list ----- */	
					if(if_otherMember){
						b.putString("memberID",memberID);
						m.setData(b);
						handler_showDialog.sendMessage(m);

					       
					}else{
					  
							if( Constant.someoneRatingList.size() == 0 ){
								
								b.putBoolean("Have_rating", false);
								m.setData(b);
						        handler_setlistView.sendMessage(m);

							}
						else{

							    b.putBoolean("Have_rating", true);
							    m.setData(b);
								handler_setlistView.sendMessage(m);
						
							}
					}
					
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}

		}.start();
		
		
		
		
	}
	
	

	private void initRatingView() {
		// description
		
		countTextView.setText(getString(R.string.someonerating_times) +"  "+ Constant.someoneRatingObject.getCount());
		averageTextView.setText(getString(R.string.someonerating_average)
				+"  "+ String.format("%.1f", Constant.someoneRatingObject.getAverage()));
		dealTimesTextView.setText(getString(R.string.someonerating_deal_times)+"  "
				+ Constant.someoneRatingObject.getDealTimes());
		
		avgRatingBar.setRating((float)Constant.someoneRatingObject.getAverage());
		// list
		LinearLayout parentView = (LinearLayout) findViewById(R.id.SomeoneRating_Des_Layout);

		parentView.removeAllViews();
		for (int i = 0; i < Constant.someoneRatingList.size(); i++) {
			View childView = LayoutInflater.from(this).inflate(R.layout.view_cell_productrating, null);
			TextView userIDFromTextView = (TextView) childView.findViewById(R.id.ProductRating_cell_userIDFromTextView);
			RatingBar ratingBar = (RatingBar) childView.findViewById(R.id.ProductRating_cell_ratingBar);
			TextView commentTextView = (TextView) childView.findViewById(R.id.ProductRating_cell_commentTextView);
			TextView ratingTime = (TextView) childView.findViewById(R.id.ProductRating_cell_RatingTimeTextView);

			userIDFromTextView.setText("" + Constant.someoneRatingList.get(i).getUserIdFrom());
			userIDFromTextView.setTag(userIDFromTextView.getText().toString());
			userIDFromTextView.setOnClickListener(onIdClickListener);

			ratingBar.setRating(Constant.someoneRatingList.get(i).getScore());
			commentTextView.setText("" + Constant.someoneRatingList.get(i).getComment());
			ratingTime.setText("" + Constant.someoneRatingList.get(i).getRatingTime().substring(5, 16).replace("-", "/"));

			parentView.addView(childView);
		}
	}  

	

	/** =============================================================== */
	/** ========================= UI Callback ========================= */
	/** =============================================================== */

	private OnClickListener onIdClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			otherMemberID= v.getTag().toString();
			Log.d("now_memberID",otherMemberID);
			GetSomeoneRating(otherMemberID,true);
			
			
		}
	};

	private OnSomeoneClickListener someoneClickListener = new OnSomeoneClickListener() {

		@Override
		public void onSomeClick(int position) {
			/*----- someone rating -----*/
			if (position == 0) {
				
				GetSomeoneRating(otherMemberID,false);
				getActionBar().setTitle(otherMemberID);
				otherMemberID = "";
			}
			/*----- someone product -----*/
			else if (position == 1) {
				//setResult(4);
				Intent it =new Intent();
				it.putExtra("memberID",otherMemberID);
				it.setClass(SomeoneRatingActivity.this, MyProductActivity.class);
				startActivity(it);				
				finish();
			}
		}
	};
	/*public interface OnSomeoneClickListener {
		public void onSomeClick(int position);
	}*/
	/** =============================================================== */
	/** ======================== HTTP Command ========================= */
	/** =============================================================== */

	

	/** ======================================================================= */
	/** ======================== HTTP POST callback =========================== */
	/** ======================================================================= */


	private Handler handler_showDialog = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			
			   String memberID =msg.getData().getString("memberID");
			   DialogListUtil.showSomeoneDialog(SomeoneRatingActivity.this, Constant.someoneRatingObject,someoneClickListener,memberID);

			
			
			
		}
	};

	private Handler handler_setlistView = new Handler() {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public void handleMessage(Message msg) {

			boolean have_rating= msg.getData().getBoolean("Have_rating");
			
			if(have_rating){
				someoneRating_no_data_view.setVisibility(View.GONE);
				lv_someoneRating_detait_rating.setVisibility(View.VISIBLE);
				initRatingView();
				dismissProgressDialog();
			}
			else{
				
				someoneRating_no_data_view.setVisibility(View.VISIBLE);
				lv_someoneRating_detait_rating.setVisibility(View.GONE);
				dismissProgressDialog();

			}
			
			
			
		}
	};
	
	/** ProgressDialog methods **/
	private void displayProgressDialog(String title, String message) {
		if (progressDialog == null || !progressDialog.isShowing()) {
			progressDialog = ProgressDialog.show(this, title, message);
		}
	}

	private void dismissProgressDialog() {
		if (progressDialog != null) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}


}

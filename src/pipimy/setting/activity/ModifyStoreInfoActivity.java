package pipimy.setting.activity;

import java.util.HashMap;

import org.json.JSONException;

import pipimy.main.LunchActivity;
import pipimy.main.MainActivity;
import pipimy.main.MyProductActivity;
import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.PreferenceUtil;
import pipimy.others.DialogListUtil.OnTypeClickListener;
import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class ModifyStoreInfoActivity extends Activity {
	HashMap<String, String> storeInfo = new HashMap<String, String>();
	final static int GET_STORE_INFO = 10001;
	final int TOAST_SUCCESS = 1;
	final int TOAST_FAILED = 2;

	TextView memberID;
	//TextView city;
	TextView StoreType;
	TextView tv_update_info;
	TextView avg_count;
	TextView rating_count;

	EditText StoreName;
	EditText detail_info_edit;

	String memeber_id = "";
	String[] mTypes;

	//RatingBar avg_ratingBar;
	int mStoreType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_storeinfo);
		
		Util.showProgressDialog(this, "", getString(R.string.loading));

		memberID = (TextView) findViewById(R.id.memberID);
		//city = (TextView) findViewById(R.id.city);
		avg_count = (TextView) findViewById(R.id.avg_count);
		rating_count = (TextView) findViewById(R.id.rating_count);
		StoreType = (TextView) findViewById(R.id.StoreType);
		tv_update_info = (TextView) findViewById(R.id.tv_update_info);
		StoreName = (EditText) findViewById(R.id.StoreName);
		detail_info_edit = (EditText) findViewById(R.id.detail_info_edit);

	//	avg_ratingBar = (RatingBar) findViewById(R.id.avg_ratingBar);

		memeber_id = PreferenceUtil.getString(ModifyStoreInfoActivity.this,
				Constant.USER_ID);
		mTypes = getResources().getStringArray(R.array.types);

		StoreType.setOnClickListener(typeOnClickListener);
		tv_update_info.setOnClickListener(UpdateStoreInfo_Click);
		initStore();
		//Handler handler = new Handler();
		//handler.postDelayed(launchRunnable, 550);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle(getString(R.string.store_intro));

		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case android.R.id.home:
			//finish();
			finish();
			return true;
		
		}
		return true;

	}
	private void initStore() {

		getStore();

	}

	private void initStoreInfo() {

		memberID.setText(memeber_id);

		/*if (storeInfo.get("storeCity").equals("null")) {

		} else {
			city.setText(storeInfo.get("storeCity"));
		}*/

		if (storeInfo.get("storeName").equals("null")) {

		} else {
			StoreName.setText(storeInfo.get("storeName"));
		}

		if (storeInfo.get("storeType").equals("null")) {

		} else {
			StoreType.setText(mTypes[Integer.valueOf(storeInfo.get("storeType"))]);
			mStoreType=Integer.valueOf(storeInfo.get("storeType"));
		}

		if (storeInfo.get("storeIntro").equals("null")) {

		} else {
			
			detail_info_edit.setText(storeInfo.get("storeIntro"));
		}
		//avg_count.setText(storeInfo.get("average") + ".0");
		//rating_count.setText("(" + storeInfo.get("count") + ")");
	//	avg_ratingBar.setRating(Float.valueOf(storeInfo.get("average")));

	}

	private void getStore() {
		// TODO Auto-generated method stub

		new Thread() {
			@Override
			public void run() {
				String[] params = { "MemberID" };
				String[] data = { memeber_id };
				String result = Http.post(Constant.GET_STORE, params, data,
						ModifyStoreInfoActivity.this);
				try {
					storeInfo = JSONParserTool.getStore(result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Message msg = new Message();
				msg.arg1 = GET_STORE_INFO;
				handler.sendMessage(msg);
			}

		}.start();
	}
	
	private void Upload_Stor_Info() {
		new Thread() {

			@Override
			public void run() {
				String[] params = { "StoreName", "StoreIntro", "StoreType" };
				String[] data = { StoreName.getText().toString().trim(),
						detail_info_edit.getText().toString(),
						String.valueOf(mStoreType)};
				String result = Http.post(Constant.UPDATE_STORE, params, data,
						ModifyStoreInfoActivity.this);
				//Log.d("modifystore","data[0] :"+data[0]);
				//Log.d("modifystore","data[1] :"+data[1]);
				//Log.d("modifystore","data[2] :"+data[2]);
			    //Log.d("modifystore","result :"+result);
				
			   if(result.contains("success")){
				   
				   
					Message msg = new Message();
					msg.arg1 =TOAST_SUCCESS;
					handler_refresh_store_info.sendMessage(msg);
			   }
			   else{
				   
				   
					Message msg = new Message();
					msg.arg1 = TOAST_FAILED;
					handler_refresh_store_info.sendMessage(msg);
			   }
                   

			}

		}.start();

	}
private Runnable launchRunnable = new Runnable() {
		
		@Override
		public void run() {
			Util.dismissProgressDialog();

		}
	};
	

	private OnClickListener typeOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			DialogListUtil.showTypeDialog(ModifyStoreInfoActivity.this, true,
					new OnTypeClickListener() {

						@Override
						public void onTypeClick(int position) {
							mStoreType = position;
							StoreType.setText(mTypes[position]);
						}
					});

		}
	};

	private OnClickListener UpdateStoreInfo_Click = new OnClickListener() {

		@Override
		public void onClick(View v) {

			Upload_Stor_Info();
			
		}
	};

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case GET_STORE_INFO:
				initStoreInfo();
				Util.dismissProgressDialog();

				break;

			}
		}
	};
	Handler handler_refresh_store_info = new Handler() {
		@Override
		public void handleMessage(Message msg) {
		
			switch (msg.arg1) {
			case TOAST_SUCCESS:
				Toast.makeText(ModifyStoreInfoActivity.this, getString(R.string.toast_success), 1500).show();
				
				break;
				
			case TOAST_FAILED:
				Toast.makeText(ModifyStoreInfoActivity.this, getString(R.string.toast_failed_txt), 1500).show();

				
				break;
				

			}
			
			
			
			
		}
	};

}

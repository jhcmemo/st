package pipimy.setting.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import pipimy.main.MainActivity;
import pipimy.object.PicObject;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.DialogUtil.OnTypeClickListener;
import pipimy.others.BitmapUtils;
import pipimy.others.Http;
import pipimy.others.MediaUtils;
import pipimy.others.PreferenceUtil;
import pipimy.others.TransferProgressBar;
import pipimy.others.VolleySingleton;
import pipimy.others.TransferProgressBar.AmazonUpLoadCompleteListener;
import pipimy.others.Util;
import pipimy.post.Filter_Activity;
import pipimy.post.PicActivity;
import pipimy.post.Postpic_select;
import pipimy.post.VideoActivity;
import pipimy.service.R;
import pipimy.third.AmazonUtil;
import pipimy.third.TransferController;
import pipimy.third.TransferModel;
import android.text.Editable;
import android.text.TextWatcher;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class PostActivity extends Activity {

	private final static String TAG = "PostFragment";

	private final static int DONE_TEXT = 20001;

	private View doneView;
	private View videoLayout;
	private View picLayout;
	private Bitmap smll_bitmap;
	private PopupWindow mPopupWindow;
	String new_old="";

	
	public static TransferProgressBar videoProgressBar;
	private TransferProgressBar picProgressBar,pic1_picProgressBar,pic2_picProgressBar,pic3_picProgressBar,
	pic4_picProgressBar,pic5_picProgressBar,pic6_picProgressBar,pic7_picProgressBar,
	pic8_picProgressBar,pic9_picProgressBar,pic10_picProgressBar;
	private ProgressBar mPictureIsUploading,pic1_uploading,pic2_uploading,pic3_uploading,
	pic4_uploading,pic5_uploading,pic6_uploading,pic7_uploading,pic8_uploading,pic9_uploading,pic10_uploading;
	private ProgressBar mVideoIsUploading;
	private EditText titleEditText;
	private EditText priceEditText;
	private EditText typeEditText;
	private EditText brandEditText;
	private EditText newoldEditText;
	private EditText snoEditText;

	

	private EditText stockCountEditText;
	private EditText descriptionEditText;

	private ImageView checkVideoImageView;
	private ImageView checkPicImageView;
	private ImageView checkNewOldImageView;

	private ImageView checkTitleImageView;
	private ImageView checkPriceImageView;
	private ImageView checkTypeImageView;
	private ImageView checkStockImageView;

	
	//private ImageView checkSnoImageView;

	

	private Handler handler;
	private int mScreenWidth;  
	    // 屏幕的height  
	    private int mScreenHeight;  
    // PopupWindow的width  
    private int mPopupWindowWidth;  
	    // PopupWindow的height  
    private int mPopupWindowHeight;  
    private RadioGroup group_temo;
    private RadioButton checkRadioButton;
    int now_pic_object;
    Bitmap nowpic_bitmap;
	private boolean isVideoDoneInvoke;
	private boolean isPicDoneInvoke;
	private boolean isVideoUpLoaded;
	private boolean isPicUpLoaded;
	private boolean isNeedToSetStoreFlow;

	private String product_title;
	private String product_price;
	private String product_des;

	private String product_type;
	private String product_Count;
	private String types[];
	public static final int REQUEST_CODE_SELECT_PHOTO = 3;
	public static final int REQUEST_CODE_CAMERA = 5;

	private static final String PREFS_NAME = "PhotoProcessingPrefsFiles";
	private static final String PREFS_KEY_CAMERA_FILE_COUNT = "PrefsKeyCameraFileCount";

	private static final String SAVE_STATE_PATH = "com.lightbox.android.photoprocessing.PhotoProcessing.mOriginalPhotoPath";
	private static final String SAVE_CURRENT_FILTER = "com.lightbox.android.photoprocessing.PhotoProcessing.mCurrentFilter";
	private static final String SAVE_EDIT_ACTIONS = "com.lightbox.android.photoprocessing.PhotoProcessing.mEditActions";
	private static final String SAVE_CAMERA_FILE_PATH = "com.lightbox.android.photoprocessing.PhotoProcessing.mCurrentCameraFilePath";
	
	
	private String mOriginalPhotoPath = null;
	private Bitmap mBitmap = null;
	private ImageView mImageView = null;
	private ListView mFilterListView = null;
	private boolean mIsFilterListShowing = false;
	private boolean mIsEditListShowing = false;
	
	private int mCurrentFilter = 0;
	private int mCurrentEditAction = 0;
	private int inSampleSize = 1;

	private ArrayList<Integer> mEditActions = new ArrayList<Integer>();
	
	//private ArrayList<Object> pic_TransferProgressBar = new ArrayList<Object>();
	private int now_radio = 1;

	
	ImageLoader mImageLoader;
	RequestQueue queue;

	ImageView upload_pic2,upload_pic3,upload_pic4,upload_pic5,upload_pic6,
	upload_pic7,upload_pic8,upload_pic9,upload_pic10;
	
	ImageView iv_finish,iv_finish2,iv_finish3,iv_finish4,iv_finish5,iv_finish6,
	iv_finish7,iv_finish8,iv_finish9,iv_finish10;
	
	ImageView upload_pic1;
	
	LinearLayout lv_upload_pic2,lv_upload_pic3,lv_upload_pic4,lv_upload_pic5,
	lv_upload_pic6,lv_upload_pic7,lv_upload_pic8,lv_upload_pic9,lv_upload_pic10;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post);
		
	

		types = getResources().getStringArray(R.array.types);
		doneView = (View) findViewById(R.id.UpLoadProduct_doneLayout);

		videoLayout = (View) findViewById(R.id.videoLayout);
		picLayout = (View) findViewById(R.id.picLayout);

		videoProgressBar = (TransferProgressBar) findViewById(R.id.UpLoadProduct_videoProgressBar);
		picProgressBar = (TransferProgressBar) findViewById(R.id.UpLoadProduct_picProgressBar);
		
		/**upload_pic_progress**/
		pic1_picProgressBar= (TransferProgressBar) findViewById(R.id.pic1_picProgressBar);
	    pic2_picProgressBar= (TransferProgressBar) findViewById(R.id.pic2_picProgressBar);
	    pic3_picProgressBar= (TransferProgressBar) findViewById(R.id.pic3_picProgressBar);
	    pic4_picProgressBar= (TransferProgressBar) findViewById(R.id.pic4_picProgressBar);
	    pic5_picProgressBar= (TransferProgressBar) findViewById(R.id.pic5_picProgressBar);
	    pic6_picProgressBar= (TransferProgressBar) findViewById(R.id.pic6_picProgressBar);
	    pic7_picProgressBar= (TransferProgressBar) findViewById(R.id.pic7_picProgressBar);
	    pic8_picProgressBar= (TransferProgressBar) findViewById(R.id.pic8_picProgressBar);
	    pic9_picProgressBar= (TransferProgressBar) findViewById(R.id.pic9_picProgressBar);
	    pic10_picProgressBar= (TransferProgressBar) findViewById(R.id.pic10_picProgressBar);

	    	    
		pic1_uploading=(ProgressBar) findViewById(R.id.pic1_uploading);
		pic2_uploading=(ProgressBar) findViewById(R.id.pic2_uploading);
		pic3_uploading=(ProgressBar) findViewById(R.id.pic3_uploading);
		pic4_uploading=(ProgressBar) findViewById(R.id.pic4_uploading);
		pic5_uploading=(ProgressBar) findViewById(R.id.pic5_uploading);
		pic6_uploading=(ProgressBar) findViewById(R.id.pic6_uploading);
		pic7_uploading=(ProgressBar) findViewById(R.id.pic7_uploading);
		pic8_uploading=(ProgressBar) findViewById(R.id.pic8_uploading);
		pic9_uploading=(ProgressBar) findViewById(R.id.pic9_uploading);
		pic10_uploading=(ProgressBar) findViewById(R.id.pic10_uploading);

		iv_finish=(ImageView) findViewById(R.id.iv_finish);
		iv_finish2=(ImageView) findViewById(R.id.iv_finish2);
		iv_finish3=(ImageView) findViewById(R.id.iv_finish3);
		iv_finish4=(ImageView) findViewById(R.id.iv_finish4);
		iv_finish5=(ImageView) findViewById(R.id.iv_finish5);
		iv_finish6=(ImageView) findViewById(R.id.iv_finish6);
		iv_finish7=(ImageView) findViewById(R.id.iv_finish7);
		iv_finish8=(ImageView) findViewById(R.id.iv_finish8);
		iv_finish9=(ImageView) findViewById(R.id.iv_finish9);
		iv_finish10=(ImageView) findViewById(R.id.iv_finish10);

				
		mVideoIsUploading = (ProgressBar) findViewById(R.id.videoIsUploading);
		mPictureIsUploading = (ProgressBar) findViewById(R.id.pictureIsUploading);

		// picCount = (TextView)
		// rootView.findViewById(R.id.UpLoadProduct_picCount);
		brandEditText = (EditText) findViewById(R.id.UpLoadProduct_brandEditText);
		titleEditText = (EditText) findViewById(R.id.UpLoadProduct_titleEditText);
		priceEditText = (EditText) findViewById(R.id.UpLoadProduct_priceEditText);
		typeEditText = (EditText) findViewById(R.id.UpLoadProduct_typeEditText);
		newoldEditText = (EditText) findViewById(R.id.UpLoadProduct_newoldEditText);
		snoEditText= (EditText) findViewById(R.id.UpLoadProduct_snoEditText);
		// dealTypeEditText = (EditText) view
		// .findViewById(R.id.UpLoadProduct_dealTypeEditText);
		stockCountEditText = (EditText) findViewById(R.id.UpLoadProduct_stockCountEditText);
		descriptionEditText = (EditText) findViewById(R.id.UpLoadProduct_descriptionEditText);

		checkVideoImageView = (ImageView) findViewById(R.id.UpLoadProduct_videoCheckImageView);
		checkPicImageView = (ImageView) findViewById(R.id.UpLoadProduct_picCheckImageView);

		checkTitleImageView = (ImageView) findViewById(R.id.UpLoadProduct_titleCheckImageView);
		checkPriceImageView = (ImageView) findViewById(R.id.UpLoadProduct_priceCheckImageView);
		checkTypeImageView = (ImageView) findViewById(R.id.UpLoadProduct_typeCheckImageView);
		checkNewOldImageView = (ImageView) findViewById(R.id.UpLoadProduct_newoldCheckImageView);
		checkStockImageView= (ImageView) findViewById(R.id.UpLoadProduct_stockCheckImageView);
		
		//checkSnoImageView= (ImageView) findViewById(R.id.UpLoadProduct_snoCheckImageView);
		/*
		 * item click listener
		 */
		videoLayout.setOnClickListener(itemClickListener);
		picLayout.setOnClickListener(itemClickListener);
		doneView.setOnClickListener(itemClickListener);
		typeEditText.setOnClickListener(itemClickListener);
		newoldEditText.setOnClickListener(itemClickListener);
	//	snoEditText.setOnClickListener(itemClickListener);
		
		
		titleEditText.addTextChangedListener(titleWatcher);
		priceEditText.addTextChangedListener(priceWatcher);
		stockCountEditText.addTextChangedListener(stockWatcher);
		//snoEditText.addTextChangedListener(snoWatcher);
		
		/**upload pic view*/
		upload_pic1=(ImageView) findViewById(R.id.upload_pic1);
		//upload_pic1.setDefaultImageResId(R.drawable.setting_post);
		upload_pic2=(ImageView) findViewById(R.id.upload_pic2);
		upload_pic3=(ImageView) findViewById(R.id.upload_pic3);
		upload_pic4=(ImageView) findViewById(R.id.upload_pic4);
		upload_pic5=(ImageView) findViewById(R.id.upload_pic5);
		upload_pic6=(ImageView) findViewById(R.id.upload_pic6);
		upload_pic7=(ImageView) findViewById(R.id.upload_pic7);
		upload_pic8=(ImageView) findViewById(R.id.upload_pic8);
		upload_pic9=(ImageView) findViewById(R.id.upload_pic9);
		upload_pic10=(ImageView) findViewById(R.id.upload_pic10);
		
		lv_upload_pic2=(LinearLayout) findViewById(R.id.lv_upload_pic2);
		lv_upload_pic3=(LinearLayout) findViewById(R.id.lv_upload_pic3);
		lv_upload_pic4=(LinearLayout) findViewById(R.id.lv_upload_pic4);
		lv_upload_pic5=(LinearLayout) findViewById(R.id.lv_upload_pic5);
		lv_upload_pic6=(LinearLayout) findViewById(R.id.lv_upload_pic6);
		lv_upload_pic7=(LinearLayout) findViewById(R.id.lv_upload_pic7);
		lv_upload_pic8=(LinearLayout) findViewById(R.id.lv_upload_pic8);
		lv_upload_pic9=(LinearLayout) findViewById(R.id.lv_upload_pic9);
		lv_upload_pic10=(LinearLayout) findViewById(R.id.lv_upload_pic10);

		
		
		upload_pic1.setOnClickListener(fileter_click);
		upload_pic2.setOnClickListener(fileter_click);
		upload_pic3.setOnClickListener(fileter_click);
		upload_pic4.setOnClickListener(fileter_click);
		upload_pic5.setOnClickListener(fileter_click);
		upload_pic6.setOnClickListener(fileter_click);
		upload_pic7.setOnClickListener(fileter_click);
		upload_pic8.setOnClickListener(fileter_click);
		upload_pic9.setOnClickListener(fileter_click);
		upload_pic10.setOnClickListener(fileter_click);

		
		mImageLoader = VolleySingleton.getInstance().getImageLoader();
		queue = VolleySingleton.getInstance().getRequestQueue();

		checkStoreFlow();

	}

	@Override
	public void onResume() {
		super.onResume();
		// Log.e("onResume","Post Fragment");
		if (handler == null) {
			handler = new Handler();
		}

		if (Constant.isUploadingVideo) {
			Log.e(TAG, "isUploadingVideo");
			
			mVideoIsUploading.setVisibility(View.VISIBLE);
			handler.post(upLoadVideoRunnable);
		}

		if (Constant.isUploadingPics) {
			
			mPictureIsUploading.setVisibility(View.VISIBLE);
			handler.post(upLoadPicRunnable);
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		// Log.e("onPause","Post Fragment");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(smll_bitmap!=null){
			smll_bitmap.recycle();
		}
		System.gc();
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		getActionBar().setTitle(getString(R.string.post_product));

		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case android.R.id.home:
			//finish();
			finish();
			return true;
		
		}
		return true;

	}
	
	
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(SAVE_STATE_PATH, mOriginalPhotoPath);
		outState.putInt(SAVE_CURRENT_FILTER, mCurrentFilter);
		outState.putIntegerArrayList(SAVE_EDIT_ACTIONS, mEditActions);
		outState.putString(SAVE_CAMERA_FILE_PATH, mOriginalPhotoPath);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		mOriginalPhotoPath = savedInstanceState.getString(SAVE_STATE_PATH);
		mCurrentFilter = savedInstanceState.getInt(SAVE_CURRENT_FILTER);
		mEditActions = savedInstanceState.getIntegerArrayList(SAVE_EDIT_ACTIONS);
		if (mEditActions == null) {
			mEditActions = new ArrayList<Integer>();
		}
		String currentCameraFilePath = savedInstanceState.getString(SAVE_CAMERA_FILE_PATH);
		if (currentCameraFilePath != null) {
			mOriginalPhotoPath = currentCameraFilePath;
		}
		if (mOriginalPhotoPath != null) {
			loadFromCache();
			mImageView.setImageBitmap(mBitmap);
		}
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
if(mPopupWindow==null){
	finish();
	
}
else{
		 if ( mPopupWindow.isShowing()) {  
	           mPopupWindow.dismiss();  
	          }
		 else{
			 finish();
		 }
} 
		 
	}
	
	
	
	
	/*
	 * EditText text chang watched
	 */
	private TextWatcher countWatcher = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			/*
			 * if (s.length() < 1 || Integer.parseInt(s.toString()) < 1) {
			 * checkStockCountImageView
			 * .setBackgroundResource(R.drawable.left_arrow_red); } else {
			 * checkStockCountImageView
			 * .setBackgroundResource(R.drawable.check48_48); }
			 */
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

	};

	private TextWatcher priceWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (s.length() < 1) {
				checkPriceImageView
				.setImageResource(R.drawable.left_arrow_red);
			} else {
				checkPriceImageView
				.setImageResource(R.drawable.check48_48);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

	};
	
	private TextWatcher stockWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (s.length() < 1) {
				checkStockImageView
				.setImageResource(R.drawable.left_arrow_red);
			} else {
				checkStockImageView
				.setImageResource(R.drawable.check48_48);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

	};
	/*private TextWatcher snoWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (s.length() < 1) {
				checkSnoImageView
						.setBackgroundResource(R.drawable.left_arrow_red);
			} else {
				checkSnoImageView
						.setBackgroundResource(R.drawable.check48_48);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

	};*/

	private TextWatcher titleWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (s.length() < 1) {
				checkTitleImageView
				.setImageResource(R.drawable.left_arrow_red);
			} else {
				checkTitleImageView
				.setImageResource(R.drawable.check48_48);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
		}

	};

	
	/**filter click*/
	
 private OnClickListener fileter_click=new OnClickListener() {
	
	@Override
	public void onClick(View v) {
    
	  
		switch(v.getId()){
		
		case  R.id.upload_pic1 :
			
			now_pic_object=0;
			
			break;
		
		
        case  R.id.upload_pic2 :
			now_pic_object=1;

			
        	break;
        	
        	
        case  R.id.upload_pic3 :
			now_pic_object=2;

			
        	break;
        	
        	
        case  R.id.upload_pic4 :
			now_pic_object=3;

			
        	break;
        	
        	
        case  R.id.upload_pic5 :
			now_pic_object=4;

			
        	break;
        	
        	
        case  R.id.upload_pic6 :
			now_pic_object=5;

			
        	break;
        	
        case  R.id.upload_pic7 :
			now_pic_object=6;

			
        	break;
        	
        case  R.id.upload_pic8 :
			now_pic_object=7;

			
        	break;
        case  R.id.upload_pic9 :
			now_pic_object=8;
			break;
        case  R.id.upload_pic10 :
			now_pic_object=9;

			
        	break;
			
        	
		}
		
		
		  
		   getPopupWindow_Select_Pic();  
		  mPopupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);		
	}
};



	
	
	/*
	 * Items click listener
	 */
	private OnClickListener itemClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.videoLayout: {
				if (checkLogin()) {
					Constant.isUploadingPics=false;
					
					Intent intent = new Intent(PostActivity.this,
							VideoActivity.class);
					startActivityForResult(intent, 0);
				}
				break;
			}
			case R.id.picLayout: {
				if (checkLogin()) {
					Intent intent = new Intent(PostActivity.this,
							PicActivity.class);
					intent.putExtra("isSell", true);
					startActivityForResult(intent, 0);

				}
				break;
			}
			case R.id.UpLoadProduct_doneLayout: {
				if (checkLogin()) {
					doneTextInvoke();

				}
				break;
			}
			case R.id.UpLoadProduct_typeEditText: {
				doPickProductType();
				break;
				
				
			   }
			case R.id.UpLoadProduct_newoldEditText: {
				
				getPopupWindow_New_Old()	;	
				mPopupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);	
				
				
				
				//doPickNewOld();
				break;
				}
      

			}

		}
	};
	
	private OnClickListener pop_click=new OnClickListener() {
		
		@Override
		public void onClick(View v) {

			switch(v.getId()){
			
			case R.id.lv_select:

				 if(mPopupWindow.isShowing()){
						
			            mPopupWindow.dismiss();  

					  }
			
				break;
			case R.id.tv_take_picture:
				
				 if(mPopupWindow.isShowing()){
					 mPopupWindow.dismiss();
				 }
					File saveDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Lightbox/");
					saveDir.mkdir();
					String format = String.format("%%0%dd", 3);
					File saveFile;
					do {
						int count = getCameraFileCount();
						String filename = "Lightbox_" + String.format(format, count) +"_000.jpeg";
						saveFile = new File(saveDir, filename);
						incrementCameraFileCount();
					} while (saveFile.exists());
					
					Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE );
					mOriginalPhotoPath = saveFile.getAbsolutePath();
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(saveFile));
					startActivityForResult(intent, REQUEST_CODE_CAMERA);
				
				
				break;
			
			 
			case R.id.tv_select_picture:
				 if(mPopupWindow.isShowing()){
					 mPopupWindow.dismiss();
				 }
				Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(i, REQUEST_CODE_SELECT_PHOTO);
				
				
				break;
				
			case R.id.btn_radio_confirm:
				if(mPopupWindow.isShowing()){
					 mPopupWindow.dismiss();
				 }
				new_old=checkRadioButton.getText().toString();
				newoldEditText.setText(new_old);
				checkNewOldImageView.setImageResource(R.drawable.check48_48);
				
				switch(group_temo.getCheckedRadioButtonId()){
				
				case R.id.radio0:
					
					now_radio=1;
					break;
                case R.id.radio1:
					now_radio=2;

					break;
					
               case R.id.radio2:
					now_radio=3;

	                 break;
	
               case R.id.radio3:
					now_radio=4;

	                  break;
				
				}
				
				
				
				break;
				
			}
			
			
			
		}
	};
	
	private void getPopupWindow_Select_Pic() {  
	      // if (null != mPopupWindow) {  
	    	   // mPopupWindow=null;
	         //   mPopupWindow.dismiss(); 
	            
	         //   return;  
	       // } else {  
	            initPopuptWindow(false);  
	        //}  
	   }  
	
	private void getPopupWindow_New_Old() {  
	      // if (null != mPopupWindow) {  
	    	   // mPopupWindow=null;
	       //     mPopupWindow.dismiss();  
	       //     return;  
	      //  } else {  
	            initPopuptWindow(true);  
	       // }  
	   } 
	
	 private void initPopuptWindow(boolean pop_newold) {  
		 
		 if(!pop_newold){
			 Log.d("pop","pop pic");
		        LayoutInflater layoutInflater = LayoutInflater.from(this);  
		        View popupWindow = layoutInflater.inflate(R.layout.activity_postpic_select, null);  
		        LinearLayout lv_select=(LinearLayout) popupWindow.findViewById(R.id.lv_select);
		        lv_select.setOnClickListener(pop_click);
		        
		        TextView tv_take_picture =(TextView) popupWindow.findViewById(R.id.tv_take_picture);
		        TextView tv_select_picture =(TextView) popupWindow.findViewById(R.id.tv_select_picture);
 
		        tv_take_picture.setOnClickListener(pop_click);
		        tv_select_picture.setOnClickListener(pop_click);
		        // popupWindow.getBackground().setAlpha(150);//
		        mPopupWindow = new PopupWindow(popupWindow, getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight());  	  
		        // 获取屏幕和PopupWindow的width和height  
		 }   else{
			 Log.d("pop","pop radio");

			  LayoutInflater layoutInflater = LayoutInflater.from(this);  
		        View popupWindow = layoutInflater.inflate(R.layout.activity_new_old, null);  
		        LinearLayout lv_select=(LinearLayout) popupWindow.findViewById(R.id.lv_select);
		        lv_select.setOnClickListener(pop_click);
		        
		        group_temo=(RadioGroup) popupWindow.findViewById(R.id.rad_G);
		       // RadioButton radio_all_new =(RadioButton)  popupWindow.findViewById(R.id.radio0);
		      ////  RadioButton radio_second_never_use =(RadioButton)  popupWindow.findViewById(R.id.radio1);
		      //  RadioButton radio_second_use =(RadioButton)  popupWindow.findViewById(R.id.radio2);
		      //  RadioButton radio_second_else =(RadioButton)  popupWindow.findViewById(R.id.radio3);
        		group_temo.check(R.id.radio0);
        		checkRadioButton = (RadioButton) group_temo.findViewById(group_temo.getCheckedRadioButtonId());

        		group_temo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

		        	@Override
		        	public void onCheckedChanged(RadioGroup group, int checkedId) {
		        		checkRadioButton = (RadioButton) group_temo.findViewById(group_temo.getCheckedRadioButtonId());

		        	//點擊事件獲取的選擇對象
		        		//Toast.makeText(getApplicationContext(), "您選擇的是"+checkRadioButton.getText(), Toast.LENGTH_LONG).show();
		        	}
		        	});

		        
                 Button btn_radio_confirm=(Button) popupWindow.findViewById(R.id.btn_radio_confirm);
                 btn_radio_confirm.setOnClickListener(pop_click);
		        // popupWindow.getBackground().setAlpha(150);//
		        mPopupWindow = new PopupWindow(popupWindow, getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight());  	  
			 
		 }
		        
		        
		     
		    }  

	 private int getCameraFileCount() {
			SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
			return prefs.getInt(PREFS_KEY_CAMERA_FILE_COUNT, 0);
		}
		@SuppressLint("NewApi") private void incrementCameraFileCount() {
			SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
			int count = prefs.getInt(PREFS_KEY_CAMERA_FILE_COUNT, 0) + 1;
			Editor editor = prefs.edit();
			editor.putInt(PREFS_KEY_CAMERA_FILE_COUNT, count);
			editor.apply();
		}
		
		private void loadFromCache() {
			DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

			if (mBitmap != null) {
				mBitmap.recycle();
			}
			
			File cacheFile = new File(getCacheDir(), "cached.jpg");
			mBitmap = BitmapUtils.getSampledBitmap(cacheFile.getAbsolutePath(), displayMetrics.widthPixels, displayMetrics.heightPixels,5);
			
		}
		
		private void loadPhoto(String path) {
			DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
			
			if (mBitmap != null) {
				mBitmap.recycle();
			}
			
			mBitmap = BitmapUtils.getSampledBitmap(path, displayMetrics.widthPixels, displayMetrics.heightPixels,5);
		
			
		}
		private void saveToCache(Bitmap bitmap) {
			if (bitmap == null || bitmap.isRecycled()) {
				return;
			}
			
			File cacheFile = new File(getCacheDir(), "cached.jpg");
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(cacheFile);
			} catch (FileNotFoundException e) {
				// do nothing
			} finally {
				if (fos != null) {
					bitmap.compress(CompressFormat.JPEG, 100, fos);
					try {
						fos.flush();
						fos.close();
					} catch (IOException e) {
						// Do nothing
					}
				}
			}
		}
		
	protected boolean checkLogin() {
		// TODO Auto-generated method stub
		boolean isLogin = PreferenceUtil.getBoolean(PostActivity.this,
				Constant.USER_LOGIN);
		if (!isLogin) {
			DialogUtil.pushPureDialog(PostActivity.this, "Alert",
					"Login please", "ok");
		}

		return isLogin;
	}

	private void checkStoreFlow() {
		// TODO Auto-generated method stub
		if (PreferenceUtil.getString(this, Constant.USER_CASH_FLOW).equals("")) {
			getStoreFlow();
		}
	}

	private void getStoreFlow() {
		// TODO Auto-generated method stub
		new Thread() {
			@Override
			public void run() {
				String[] params = { "" };
				String[] data = { "" };
				String result = Http.get(Constant.GET_USER_STORE_FLOW, params,
						data, PostActivity.this);
				Log.e(TAG, "checkStoreFlow result = " + result);

				try {
					JSONObject jsonObject = new JSONObject(result);
					if (jsonObject.getString("storeCash").equals("null")) {
						isNeedToSetStoreFlow = true;
					} else {
						PreferenceUtil.setString(PostActivity.this,
								Constant.USER_CASH_FLOW,
								jsonObject.getString("storeCash"));
					}
					
					if(jsonObject.getString("storeDelivery").equals("null")) {
						isNeedToSetStoreFlow = true;
					} else {
						PreferenceUtil.setString(PostActivity.this,
								Constant.USER_STORE_FLOW,
								jsonObject.getString("storeDelivery"));
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}

	/** =============================================================== */
	/** ================= Amazon upLoad even Callback ================= */
	/** =============================================================== */

	/*----- runnable for upLoad progress refresh -----*/
	private Runnable upLoadVideoRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			// run until upload complete
			if (!isVideoUpLoaded ) {			
				syncModels();
				handler.postDelayed(upLoadVideoRunnable, 500);
			}
			
			// video complete
			if (isVideoUpLoaded) {
				videoProgressBar.setProgress(videoProgressBar.getMax());
				mVideoIsUploading.setVisibility(View.INVISIBLE);
				Constant.isUploadingVideo = false;
				checkVideoImageView
						.setImageResource(R.drawable.check48_48);
			} else {
				checkVideoImageView
				.setImageResource(R.drawable.left_arrow_red);
			}

		}

	};
	
	
	
	private Runnable upLoadPicRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			// run until upload complete
			if (!isPicUpLoaded) {	
				syncModels();
				handler.postDelayed(upLoadPicRunnable, 500);
				
			}
			
			// pic complete
			/**filter edit*/
			if (isPicUpLoaded) {		
				//pic1_picProgressBar.setProgress(picProgressBar.getMax());
				Constant.isUploadingPics = false;
								
				PicObject object = Constant.picList.get(0);	
				
			
				
			//   nowpic_bitmap=getSmallBitmap(object.getDirPath() + object.getFileName());
			//	Constant.PicServerURL + productId + "-pic" + (position + 1) + ".jpg"
			/*   if(nowpic_bitmap!=null){
				   nowpic_bitmap.recycle();
			   }				
				upload_pic1.setImageBitmap(null);
				upload_pic1.setImageBitmap(nowpic_bitmap);*/
				
			//	upload_pic1.setImageURI(null);			
				//upload_pic1.setImageURI(Uri.parse(object.getDirPath() + object.getFileName()));			
				//String url=Constant.PicServerURL+Constant.uploadProductID+"-pic"+(now_pic_object+1)+".jpg";
			//	Log.d("url","url :"+url);
				//VolleySingleton.removeCache(url);
				//VolleySingleton.p
				String fileName = Constant.uploadProductID + "-pic"
        				+ (now_pic_object + 1) + ".jpg";
        		String dirPath = getFilesDir().getAbsolutePath() + "/";
         		
         		Log.d("filter","set test :"+ dirPath+fileName);
         		Log.d("filter","now_pic_object :"+ now_pic_object);
        		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        	

        		
         		smll_bitmap = BitmapUtils.getSampledBitmap(dirPath+fileName, displayMetrics.widthPixels, displayMetrics.heightPixels,10);
         		float scaleWidth = 70 / (float)smll_bitmap.getWidth();
        		float scaleHeight = 70 / (float)smll_bitmap.getHeight();
        		//Log.i("Alex","SettingManager====> set scale value : "+scaleWidth + ":" + scaleHeight);
        		
        		
        		Matrix matrix = new Matrix();
        		matrix.postScale(scaleWidth, scaleHeight);
         		
         		
         		smll_bitmap=Bitmap.createBitmap(smll_bitmap, 0, 0, smll_bitmap.getWidth(),smll_bitmap.getHeight(), matrix, true);

         		
	switch(now_pic_object){
				
				case 0:
					//pic_done[0]=1;
					pic1_uploading.setVisibility(View.GONE);				
					iv_finish.setVisibility(View.VISIBLE);	
	         		upload_pic1.setImageBitmap(smll_bitmap);
	         		lv_upload_pic2.setVisibility(0);

					break;
                 case 1:
                		pic2_uploading.setVisibility(View.GONE);				
        				iv_finish2.setVisibility(View.VISIBLE);	
                 		upload_pic2.setImageBitmap(smll_bitmap);
                 		lv_upload_pic3.setVisibility(0);
                
					break;
                 case 2:
                	 	pic3_uploading.setVisibility(View.GONE);				
        				iv_finish3.setVisibility(View.VISIBLE);		
        				upload_pic3.setImageBitmap(smll_bitmap);
                 		lv_upload_pic4.setVisibility(0);
						break;
						
                 case 3:
                		pic4_uploading.setVisibility(View.GONE);				
        				iv_finish4.setVisibility(View.VISIBLE);	
        				upload_pic4.setImageBitmap(smll_bitmap);
                 		lv_upload_pic5.setVisibility(0);
						break;
                 case 4:
                		pic5_uploading.setVisibility(View.GONE);				
        				iv_finish5.setVisibility(View.VISIBLE);
        				upload_pic5.setImageBitmap(smll_bitmap);
                 		lv_upload_pic6.setVisibility(0);
						break;
                 case 5:
                		pic6_uploading.setVisibility(View.GONE);				
        				iv_finish6.setVisibility(View.VISIBLE);	
        				upload_pic6.setImageBitmap(smll_bitmap);
                 		lv_upload_pic7.setVisibility(0);
						break;
                 case 6:
                		pic7_uploading.setVisibility(View.GONE);				
        				iv_finish7.setVisibility(View.VISIBLE);	
        				upload_pic7.setImageBitmap(smll_bitmap);
                 		lv_upload_pic8.setVisibility(0);
						break;
                 case 7:
                  		pic8_uploading.setVisibility(View.GONE);				
        				iv_finish8.setVisibility(View.VISIBLE);	
        				upload_pic8.setImageBitmap(smll_bitmap);
                 		lv_upload_pic9.setVisibility(0);
						break;
                 case 8:
                		pic9_uploading.setVisibility(View.GONE);				
        				iv_finish9.setVisibility(View.VISIBLE);	
        				upload_pic9.setImageBitmap(smll_bitmap);
                 		lv_upload_pic10.setVisibility(0);
						break;
                 case 9:
                		pic10_uploading.setVisibility(View.GONE);				
        				iv_finish10.setVisibility(View.VISIBLE);
        				upload_pic10.setImageBitmap(smll_bitmap);
                 		//lv_upload_pic6.setVisibility(0);
						break;
               
				}
         		
         		
				//original pic
			/*	picProgressBar.setProgress(picProgressBar.getMax());
				mPictureIsUploading.setVisibility(View.INVISIBLE);
				Constant.isUploadingPics = false;
				checkPicImageView.setBackgroundResource(R.drawable.check48_48);
*/
				
			} else {
				checkPicImageView
				.setImageResource(R.drawable.left_arrow_red);
			}

		}

	};
	
	

	
	
	private void check_now_progress(){
		
		switch(now_pic_object){
		
		case 0:
			iv_finish.setVisibility(8);

			break;
         case 1:
 			iv_finish2.setVisibility(8);
      
			break;
         case 2:
  			iv_finish3.setVisibility(8);

				break;
				
         case 3:
  			iv_finish4.setVisibility(8);

				break;
         case 4:
  			iv_finish5.setVisibility(8);

				break;
         case 5:
  			iv_finish6.setVisibility(8);

				break;
         case 6:
  			iv_finish7.setVisibility(8);

				break;
         case 7:
  			iv_finish8.setVisibility(8);

				break;
         case 8:
  			iv_finish9.setVisibility(8);

				break;
         case 9:
  			iv_finish10.setVisibility(8);

				break;
       
		}
		
		
		
	}
	
	
	

	/*----- makes sure that we are up to date on the transfers -----*/
	private void syncModels() {
		TransferModel[] models = TransferModel.getAllTransfers();
		for (int i = 0; i < models.length; i++) {

			if (Constant.uploadProductID != null
					&& models[i].getStatus() != TransferModel.Status.CANCELED) {

				if (models[i].getFileName().contains(".mp4")) {
					// progress for video upload
					// Log.e(TAG, "syncModels refresh transfer progress");
					videoProgressBar.initTransferProgressBar(PostActivity.this,
							models[i], videoCompleteListener);
					videoProgressBar.refresh();

				} else if (models[i].getFileName().contains(".jpg")
						&& Constant.picList != null) {
					
					/**orignal pic*/
					// progress for pic upload
				/*	picProgressBar.initTransferProgressBar(PostActivity.this,
							models[i], picCompleteListener,
							Constant.picList.size());
					*/				
					/**filter pic*/
					
					switch(now_pic_object){
					
					case 0:
						
						pic1_picProgressBar.initTransferProgressBar(PostActivity.this,
								models[i], picCompleteListener,
								Constant.picList.size());					
						break;
                     case 1:
						
                    	 pic2_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
						break;
                     case 2:
                    	 pic3_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());						
 						break;
 						
                     case 3:
                    	 pic4_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
 						break;
                     case 4:
                    	 pic5_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
 						break;
                     case 5:
                    	 pic6_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
 						break;
                     case 6:
                    	 pic7_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
 						break;
                     case 7:
                    	 pic8_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
 						break;
                     case 8:
                    	 pic9_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
 						break;
                     case 9:
                    	 pic10_picProgressBar.initTransferProgressBar(PostActivity.this,
 								models[i], picCompleteListener,
 								Constant.picList.size());
 						break;
                   
					
					  }
					
					
				
					
					
					
					
					
					
					
					
				}
			}
		}
	}

	/*
	 * Product information done
	 */
	
	private void doPickNewOld(){
		
			

	}
	
	protected void doPickProductType() {
		// TODO Auto-generated method stub
		DialogUtil.typeSearchDialog(PostActivity.this, true,
				new OnTypeClickListener() {

					@Override
					public void onTypeClick(int position) {
						// TODO Auto-generated method stub
						typeEditText.setText(types[position]);
						checkTypeImageView
								.setImageResource(R.drawable.check48_48);
						product_type = "" + position;
					}
				});
	}

	protected void doneTextInvoke() {
		// TODO Auto-generated method stub
		Log.e(TAG, " BEGIN");
		if (checkDone()) {
			Util.showProgressDialog(PostActivity.this, getString(R.string.post_wait),  getString(R.string.post_plz_wait));

			// Util.showProgressDialog(getActivity(), "",
			// getString(R.string.loading));
			new Thread() {
				@Override
				public void run() {

					String[] params = { "ProductID", "Title", "Type", "Price",
							"CC", "Lat", "Lng", "Des", "PostTime", "Stock","Brand","NewOld","SNO"};
					String[] data = {
							Constant.uploadProductID,
							product_title,
							product_type,
							product_price,
							"TW",
							PreferenceUtil.getString(PostActivity.this,
									Constant.USER_LAT),
							PreferenceUtil.getString(PostActivity.this,
									Constant.USER_LNG), product_des, getTime(),
							product_Count ,brandEditText.getText().toString(),String.valueOf(now_radio).trim(),snoEditText.getText().toString().trim()};

					for (int i = 0; i < data.length; i++) {
						Log.d("post", "doneTextInvoke" + params[i] + "= "
								+ data[i]);
					}

					String result = Http.post(Constant.DONE_TEXT_M, params,
							data, PostActivity.this);
					Log.d(TAG, "doneTextInvoke res = " + result);
					Message msg = new Message();
					msg.arg1 = DONE_TEXT;
					msg.obj = result;
					apiCallbackHandler.sendMessage(msg);
					
				}

			}.start();
		}
	}

	protected void doneVideoInvoke() {
		// TODO Auto-generated method stub
		Log.e(TAG, "doneVideoInvoke BEGIN");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "ProductID" };
				String[] data = { Constant.uploadProductID };
				String result = Http.post(Constant.DONE_VIDEO_URL_M, params,
						data, PostActivity.this);
				Log.d("video","done video result:"+result);
				try {
					JSONObject jsonObject = new JSONObject(result);
					if (jsonObject.getString("result").equals("success"))
						isVideoDoneInvoke = true;

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}.start();
	}

	protected void donePicInvoke() {
		// TODO Auto-generated method stub
	//	Log.e(TAG, "donePicInvoke BEGIN");
		new Thread() {
			@Override
			public void run() {
				String[] params = { "ProductID", "PicNumber" };
				String[] data = { Constant.uploadProductID,
						String.valueOf(now_pic_object+1) };
				
				Log.d("url","PicNumber: "+(now_pic_object+1));

				String result = Http.post(Constant.DONE_PIC_URL_M, params,
						data, PostActivity.this);
				Log.d("url","pic done result :"+result);
				try {
					JSONObject jsonObject = new JSONObject(result);
					if (jsonObject.getString("result").equals("success"))
						isPicDoneInvoke = true;
					Log.d("url","donePicInvoke !");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}.start();
	}
	
	protected void getAnotherProductID() {
		// TODO Auto-generated method stub
		new Thread() {
			@Override
			public void run() {
				String[] params = { "" };
				String[] data = { "" };
				String result = Http.get(Constant.GET_PRODUCTID_URL, params,
						data, PostActivity.this);
				try {
					JSONObject jsonObject = new JSONObject(result);
					String myProductID = jsonObject.getString("result");
					PreferenceUtil.setString(PostActivity.this,
							Constant.USER_PRODUCTID, myProductID);
					Constant.uploadProductID = myProductID;
Log.d("post","post finsih get product id:"+Constant.uploadProductID);
					finish();
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}.start();
	}

	Handler apiCallbackHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case DONE_TEXT: {
				Util.dismissProgressDialog();
				String str = (String) msg.obj;
				if(str.contains("success")) {
					Toast.makeText(PostActivity.this, getString(R.string.post_success), Toast.LENGTH_LONG).show();

					getAnotherProductID();
					
				}else if(str.contains("video")){
					Toast.makeText(PostActivity.this, getString(R.string.post_error_video), Toast.LENGTH_LONG).show();
 
				
				  }
				else if(str.contains("pic")){
					Toast.makeText(PostActivity.this, getString(R.string.post_error_pic), Toast.LENGTH_LONG).show();
 
				
				  }
				break;
			}
			}
		}
	};

	private boolean checkDone() {
		// TODO Auto-generated method stub
		product_title = Util.getEditText(titleEditText).trim();
		product_price = Util.getEditText(priceEditText);
		product_des = Util.getEditText(descriptionEditText);
		product_Count = Util.getEditText(stockCountEditText);

		if(isNeedToSetStoreFlow) {
			DialogUtil.pushPureDialog(PostActivity.this,getString(R.string.dailog_title_hint),
					getString(R.string.must_setting_flows), getString(R.string.Confirm));
			
		} 
		
		else if (!isVideoDoneInvoke) {
			DialogUtil.pushPureDialog(PostActivity.this, getString(R.string.dailog_title_hint),
					getString(R.string.post_video_not_done), getString(R.string.Confirm));
		} else if (!isPicDoneInvoke) {
			DialogUtil.pushPureDialog(PostActivity.this, getString(R.string.dailog_title_hint),
					getString(R.string.post_pic_not_done), getString(R.string.Confirm));
		} else if (product_title.equals("")) {
			DialogUtil.pushPureDialog(PostActivity.this, getString(R.string.dailog_title_hint),
					getString(R.string.post_title_empty), getString(R.string.Confirm));
		} else if (product_price.equals("")) {
			DialogUtil.pushPureDialog(PostActivity.this, getString(R.string.dailog_title_hint),
					getString(R.string.post_price_empty), getString(R.string.Confirm));
		} else if (product_type.equals("")) {
			DialogUtil.pushPureDialog(PostActivity.this, getString(R.string.dailog_title_hint),
					getString(R.string.post_type_empty), getString(R.string.Confirm));
		} 
		else if (new_old.equals("")) {
			DialogUtil.pushPureDialog(PostActivity.this,getString(R.string.dailog_title_hint),
					getString(R.string.post_newold_empty), getString(R.string.Confirm));
		} 
		
		else if (product_Count.equals("")
				|| Integer.parseInt(product_Count) < 1) {
			DialogUtil.pushPureDialog(PostActivity.this, getString(R.string.dailog_title_hint),
					getString(R.string.post_stock_empty), getString(R.string.Confirm));
		} else {
			return true;
		}

		return false;
	}

	

	protected String getTime() {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dt = new Date();
		String dts = sdf.format(dt);
		return dts;
	}

	/*
	 * Amazon upload callback
	 */
	private AmazonUpLoadCompleteListener videoCompleteListener = new AmazonUpLoadCompleteListener() {

		@Override
		public void onAmazonUpLoadComplete() {
			if (!isVideoUpLoaded) {
				isVideoUpLoaded = true;
				doneVideoInvoke();
			}
		}
	};

	private AmazonUpLoadCompleteListener picCompleteListener = new AmazonUpLoadCompleteListener() {

		@Override
		public void onAmazonUpLoadComplete() {
			if (!isPicUpLoaded) {
				isPicUpLoaded = true;
				donePicInvoke();
				
			}
		}
	};

	/** =============================================================== */
	/** ================== Activities call back result ================ */
	/** =============================================================== */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Intent it ;

		if (resultCode == 1) {
			uploadVideoToAmazon();
		} else if (resultCode == 2) {
			uploadPicToAmazon();
			
			
			
		}
		
		
		if (requestCode == REQUEST_CODE_SELECT_PHOTO && resultCode == RESULT_OK) {
			check_now_progress();
			/*if(nowpic_bitmap!=null){
			nowpic_bitmap.recycle();
			}*/
			if (mEditActions != null) {
				mEditActions.clear();
			}
			Uri photoUri = data.getData();
			
			 it =new Intent(PostActivity.this,Filter_Activity.class);
			 it.putExtra("PicUri", photoUri.toString());
			 it.putExtra("state", 0);
			 it.putExtra("now_pic_object", now_pic_object);
			 startActivityForResult(it, 0);
			
			 if(mPopupWindow.isShowing()){
				 mPopupWindow.dismiss();
			 }
			//mImageView.setImageBitmap(null);
		//	mOriginalPhotoPath = MediaUtils.getPath(this, photoUri);
		//	loadPhoto(mOriginalPhotoPath);
			//mImageView.setImageBitmap(mBitmap);
			//saveToCache(mBitmap);
		} else if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
			check_now_progress();

			 it =new Intent(PostActivity.this,Filter_Activity.class);
			 it.putExtra("PicUri", mOriginalPhotoPath);	
			 it.putExtra("state", 1);
			 it.putExtra("now_pic_object", now_pic_object);

			 startActivityForResult(it, 0);
			
			
			
			//mImageView.setImageBitmap(null);
			//loadPhoto(mOriginalPhotoPath);
			//mImageView.setImageBitmap(mBitmap);
			//saveToCache(mBitmap);
		}
		
		
		
		
		

	}

	private void uploadPicToAmazon() {
		// TODO Auto-generated method stub
		Log.e(TAG, "uploadPicToAmazon BEGIN");

		switch(now_pic_object){
		case 0:
			pic1_uploading.setVisibility(View.VISIBLE);
			pic2_uploading.setVisibility(View.GONE);
			pic3_uploading.setVisibility(View.GONE);
			pic4_uploading.setVisibility(View.GONE);
			pic5_uploading.setVisibility(View.GONE);
			pic6_uploading.setVisibility(View.GONE);
			pic7_uploading.setVisibility(View.GONE);
			pic8_uploading.setVisibility(View.GONE);
			pic9_uploading.setVisibility(View.GONE);
			pic10_uploading.setVisibility(View.GONE);

			
			break;
         case 1:		
        	pic1_uploading.setVisibility(View.GONE);
 			pic2_uploading.setVisibility(View.VISIBLE);
 			pic3_uploading.setVisibility(View.GONE);
 			pic4_uploading.setVisibility(View.GONE);
 			pic5_uploading.setVisibility(View.GONE);
 			pic6_uploading.setVisibility(View.GONE);
 			pic7_uploading.setVisibility(View.GONE);
 			pic8_uploading.setVisibility(View.GONE);
 			pic9_uploading.setVisibility(View.GONE);
 			pic10_uploading.setVisibility(View.GONE);
			break;
         case 2:
        	pic1_uploading.setVisibility(View.GONE);
  			pic2_uploading.setVisibility(View.GONE);
  			pic3_uploading.setVisibility(View.VISIBLE);
  			pic4_uploading.setVisibility(View.GONE);
  			pic5_uploading.setVisibility(View.GONE);
  			pic6_uploading.setVisibility(View.GONE);
  			pic7_uploading.setVisibility(View.GONE);
  			pic8_uploading.setVisibility(View.GONE);
  			pic9_uploading.setVisibility(View.GONE);
  			pic10_uploading.setVisibility(View.GONE);	
				break;
				
         case 3:
        	pic1_uploading.setVisibility(View.GONE);
   			pic2_uploading.setVisibility(View.GONE);
   			pic3_uploading.setVisibility(View.GONE);
   			pic4_uploading.setVisibility(View.VISIBLE);
   			pic5_uploading.setVisibility(View.GONE);
   			pic6_uploading.setVisibility(View.GONE);
   			pic7_uploading.setVisibility(View.GONE);
   			pic8_uploading.setVisibility(View.GONE);
   			pic9_uploading.setVisibility(View.GONE);
   			pic10_uploading.setVisibility(View.GONE);
				break;
         case 4:
        	    pic1_uploading.setVisibility(View.GONE);
    			pic2_uploading.setVisibility(View.GONE);
    			pic3_uploading.setVisibility(View.GONE);
    			pic4_uploading.setVisibility(View.GONE);
    			pic5_uploading.setVisibility(View.VISIBLE);
    			pic6_uploading.setVisibility(View.GONE);
    			pic7_uploading.setVisibility(View.GONE);
    			pic8_uploading.setVisibility(View.GONE);
    			pic9_uploading.setVisibility(View.GONE);
    			pic10_uploading.setVisibility(View.GONE);
        	 
        
				break;
         case 5:
        	pic1_uploading.setVisibility(View.GONE);
 			pic2_uploading.setVisibility(View.GONE);
 			pic3_uploading.setVisibility(View.GONE);
 			pic4_uploading.setVisibility(View.GONE);
 			pic5_uploading.setVisibility(View.GONE);
 			pic6_uploading.setVisibility(View.VISIBLE);
 			pic7_uploading.setVisibility(View.GONE);
 			pic8_uploading.setVisibility(View.GONE);
 			pic9_uploading.setVisibility(View.GONE);
 			pic10_uploading.setVisibility(View.GONE);
        	 break;
         case 6:
        	pic1_uploading.setVisibility(View.GONE);
  			pic2_uploading.setVisibility(View.GONE);
  			pic3_uploading.setVisibility(View.GONE);
  			pic4_uploading.setVisibility(View.GONE);
  			pic5_uploading.setVisibility(View.GONE);
  			pic6_uploading.setVisibility(View.GONE);
  			pic7_uploading.setVisibility(View.VISIBLE);
  			pic8_uploading.setVisibility(View.GONE);
  			pic9_uploading.setVisibility(View.GONE);
  			pic10_uploading.setVisibility(View.GONE);
				break;
         case 7:
        	pic1_uploading.setVisibility(View.GONE);
   			pic2_uploading.setVisibility(View.GONE);
   			pic3_uploading.setVisibility(View.GONE);
   			pic4_uploading.setVisibility(View.GONE);
   			pic5_uploading.setVisibility(View.GONE);
   			pic6_uploading.setVisibility(View.GONE);
   			pic7_uploading.setVisibility(View.GONE);
   			pic8_uploading.setVisibility(View.VISIBLE);
   			pic9_uploading.setVisibility(View.GONE);
   			pic10_uploading.setVisibility(View.GONE);
				break;
         case 8:
        	    pic1_uploading.setVisibility(View.GONE);
    			pic2_uploading.setVisibility(View.GONE);
    			pic3_uploading.setVisibility(View.GONE);
    			pic4_uploading.setVisibility(View.GONE);
    			pic5_uploading.setVisibility(View.GONE);
    			pic6_uploading.setVisibility(View.GONE);
    			pic7_uploading.setVisibility(View.GONE);
    			pic8_uploading.setVisibility(View.GONE);
    			pic9_uploading.setVisibility(View.VISIBLE);
    			pic10_uploading.setVisibility(View.GONE);
				break;
         case 9:
        	pic1_uploading.setVisibility(View.GONE);
 			pic2_uploading.setVisibility(View.GONE);
 			pic3_uploading.setVisibility(View.GONE);
 			pic4_uploading.setVisibility(View.GONE);
 			pic5_uploading.setVisibility(View.GONE);
 			pic6_uploading.setVisibility(View.GONE);
 			pic7_uploading.setVisibility(View.GONE);
 			pic8_uploading.setVisibility(View.GONE);
 			pic9_uploading.setVisibility(View.GONE);
 			pic10_uploading.setVisibility(View.VISIBLE);
				break;
		}
		
		
		
		
		
		AmazonUtil.isVideo = false;
		AmazonUtil.isStore = false;
		AmazonUtil.isBackGround = false;

		isPicUpLoaded = false;

		// Clear pic
		TransferModel[] models = TransferModel.getAllTransfers();
		for (int i = 0; i < models.length; i++) {
			if (models[i].getFileName().contains(".jpg")) {
				models[i].abort();
			}
		}

		/** upload pic  orinigal*/
	/*	for (int i = 0; i < Constant.picList.size(); i++) {
			PicObject object = Constant.picList.get(i);	
			File tmpFile = new File(object.getDirPath() + object.getFileName());
			Uri uri = Uri.fromFile(tmpFile);
			TransferController.upload(this, uri);
		}
	*/
		
		/** my edit*/
	
			PicObject object = Constant.picList.get(0);	
			Log.d("url","upload path :"+object.getDirPath() + object.getFileName());
			File tmpFile = new File(object.getDirPath() + object.getFileName());
			Uri uri = Uri.fromFile(tmpFile);
			TransferController.upload(this, uri);
			
		
		
	}

	private void uploadVideoToAmazon() {
		// TODO Auto-generated method stub
		Log.e(TAG, "uploadVideoToAmazon BEGIN");

		AmazonUtil.isVideo = true;
		AmazonUtil.isStore = false;
		AmazonUtil.isBackGround = false;

		isVideoUpLoaded = false;

		// Clear video
		TransferModel[] models = TransferModel.getAllTransfers();
		for (int i = 0; i < models.length; i++) {
			if (models[i].getFileName().contains(".mp4")) {
				models[i].abort();
			}
		}

		// upload video
		File tmpFile = new File(Constant.uploadVideoPath);
		if (tmpFile.exists()) {
			Log.d("video","uplaod video path:"+Constant.uploadVideoPath);
			Uri uri = Uri.fromFile(tmpFile);
			TransferController.upload(this, uri);
		}
	}
	
	public  Bitmap getSmallBitmap(String filePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
      BitmapFactory.decodeFile(filePath, options);
      options.inSampleSize = 20;
      options.inJustDecodeBounds = false;

    return BitmapFactory.decodeFile(filePath, options).copy(Bitmap.Config.RGB_565, true);
    
	  }
	

}

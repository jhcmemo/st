package pipimy.setting.activity;

import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import com.android.volley.toolbox.NetworkImageView;

import pipimy.main.MyProductActivity;
import pipimy.object.MyDealObject;
import pipimy.object.TrackObject;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogListUtil;
import pipimy.others.DialogListUtil.OnRatingClickListener;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.MyDealAdapter;
import pipimy.others.PreferenceUtil;
import pipimy.others.Util;
import pipimy.service.R;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

public class MyDealActivity extends Activity {

	private ExpandableListView expandableListView;
	private MyDealAdapter adapter;

	private String otherMemberID = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mydeal);
		expandableListView = (ExpandableListView) findViewById(R.id.MyDeal_listView);
		init();
	}
	@Override
	public void onBackPressed() {
		backEven();
	}
	

	/** =============================================================== */
	/** =========================== Actionbar ========================= */
	/** =============================================================== */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();

	//	Drawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.style_red));
	//	Drawable bottomDrawable = getResources().getDrawable(R.drawable.actionbar_bottom);
	//	LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
	//	getActionBar().setBackgroundDrawable(ld);
		getActionBar().setTitle(getString(R.string.setting_mydeal));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		return super.onCreateOptionsMenu(menu);
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	/** =============================================================== */
	/** =========================== initial =========================== */
	/** =============================================================== */


	private void init() {
			Get_MyDealList();
	}

	private void Get_MyDealList(){
		new Thread() {
			@Override
			public void run() {
				 
				String[] params = { "MemberID" };
				String[] data = { PreferenceUtil.getString(MyDealActivity.this, Constant.USER_ID) };
				String result = Http.post(Constant.GET_MY_DEAL, params,data, MyDealActivity.this); 
				
				try {
					Log.d("deal","?? : "+result);
					
					JSONParserTool.getMyDeal(result, Constant.myDealList_seller,Constant.myDealList_buyer,PreferenceUtil.getString(MyDealActivity.this, Constant.USER_ID));
		
					         // //  Bundle b =new Bundle();
					         //   Message m=new  Message();
								
							    handler_setlistView.sendMessage(handler_setlistView.obtainMessage());
						
					
					
			} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}

		}.start();
		
		
		
	}
	
	private void Product_Rating(final String productId, final int rating, final String comment){
		
		
		new Thread() {
			@Override
			public void run() {
				 
				String[] params = { "ProductID" ,"Score","Comment","RatingTime"};
				
				String[] data = { productId,"" + rating,comment,Util.getTime()};
				String result = Http.post(Constant.RATING_PRODUCT, params,data, MyDealActivity.this); 
				
				if (result.contains("success")) {
					Log.d("deal","success");

					handler_ShowToast.sendMessage(handler_ShowToast.obtainMessage());				
					 Get_MyDealList();

				}
				else{
					Log.d("deal","failed");

				}
			
			}

		}.start();
		
		
	}
	

	private Handler handler_ShowToast = new Handler() {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public void handleMessage(Message msg) {

		Toast.makeText(MyDealActivity.this, getString(R.string.mydeal_rating_success), Toast.LENGTH_SHORT).show();
	
		}
	};
	
	
	private Handler handler_setlistView = new Handler() {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public void handleMessage(Message msg) {

		
		boolean isSeller = (getIntent().getAction().equals(Constant.ACTION_TRANSACTION_AS_SELLER)) ? true : false;

			if(isSeller){
				
				adapter = new MyDealAdapter(MyDealActivity.this, R.layout.setting_mydeal, Constant.myDealList_seller,
						 idClickListener, ratingClickListener, isSeller,PreferenceUtil.getString(MyDealActivity.this, Constant.USER_ID));
				
				expandableListView.setAdapter(adapter);

			}
			else{

				adapter = new MyDealAdapter(MyDealActivity.this, R.layout.setting_mydeal, Constant.myDealList_buyer,
						 idClickListener, ratingClickListener, isSeller,PreferenceUtil.getString(MyDealActivity.this, Constant.USER_ID));		
				expandableListView.setAdapter(adapter);		
			}
		
			int groupCount = expandableListView.getCount();
			for (int i = 0; i < groupCount; i++) {
				expandableListView.expandGroup(i);
			}
		
			
			
			
			
		}
	};



	/** =============================================================== */
	/** ========================= UI Callback ========================= */
	/** =============================================================== */

	private OnClickListener idClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Log.d("idclick","??");
			otherMemberID = v.getTag().toString();
//			HTTP_getSomeoneRating(otherMemberID);
			Intent intent = new Intent();
			intent.putExtra("memberID", otherMemberID);
			intent.setClass(MyDealActivity.this, MyProductActivity.class);
			startActivity(intent);
		}
	};

	private OnClickListener ratingClickListener = new OnClickListener() { 

		String productID;
		int score;
		String comment;

		@Override
		public void onClick(View v) {

			String tag = v.getTag().toString();
			productID = tag.split(",")[0];
			Log.d("rating","productID  :"+productID );
			score = Integer.valueOf(tag.split(",")[1]);
			
			if(tag.split(",").length == 3){
				comment = tag.split(",")[2];
			} else {
				comment = "";
			}
			

			if (comment.equals("null")) {
				comment = getString(R.string.mydeal_rating_hint);
			} 
 
			DialogListUtil.showRatingDialog(MyDealActivity.this, score, comment, new OnRatingClickListener() {
				@Override
				public void onRatingClick(int rating, String comm) {
				
					Product_Rating(productID, rating, comm);
					//HTTP_ratingProduct(productID, rating, comm);
				
				}
			});
		}
	};

/*	private OnSomeoneClickListener someoneClickListener = new OnSomeoneClickListener() {

		Intent intent = new Intent();

		@Override
		public void onSomeClick(int position) {
			//
			if (position == 0) {

				intent.setClass(MyDealActivity.this, SomeoneRatingActivity.class);
				intent.putExtra("memberID", otherMemberID);
				startActivityForResult(intent, 0);
			}
			//
			else if (position == 1) {
				setResult(4);
				finish();
			}
			//
			else if (position == 2) {
				setResult(7);
				finish();
			}
		}
	};*/

	private OnChildClickListener childClickListener = new OnChildClickListener() {
		String productID;

		@Override
		public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
			productID = v.getTag().toString();

			//Intent intent = new Intent(MyDealActivity.this, ProductDetailActivity.class);
			//intent.setAction(GlobalVariable.ACTION_DEAL);
			//intent.putExtra("productID", productID);
			//if (groupPosition == 0) {
			//	intent.putExtra("isBuyer", 1);
		//	} else {
			//	intent.putExtra("isBuyer", 2);
		//	}
		//	startActivityForResult(intent, 0);
			return false;
		}
	};



	private void backEven() {
		if (getFragmentManager().getBackStackEntryCount() > 0) {
		//	FragmentManagerTool.popBackStackFragment(getSupportFragmentManager());
		} else {
			finish();
		}
	}

	/** =============================================================== */
	/** ======================== HTTP Command ========================= */
	/** =============================================================== */

	private void HTTP_getMyDeal() {
		//startHttpGet(GlobalVariable.GET_MY_DEAL, true, null, getString(R.string.loading), null, null);
	}

	/*private void HTTP_ratingProduct(String productId, int rating, String comment) {

		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("ProductID", productId));
		params.add(new BasicNameValuePair("Score", "" + rating));
		params.add(new BasicNameValuePair("Comment", comment));
		params.add(new BasicNameValuePair("RatingTime", Util.getTime()));
		startHttpPost(GlobalVariable.RATING_PRODUCT, true, null, null, params, null, null);
	}*/

	private void HTTP_getSomeoneRating(String memberID) {
		//GlobalVariable.someoneRatingObject.setMemberID(memberID);

		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		//params.add(new BasicNameValuePair("MemberID", memberID));
		//startHttpPost(GlobalVariable.GET_SOMEONE_RATING, true, null, getString(R.string.loading), params, null, null);

	}

	/** =============================================================== */
	/** ====================== HTTP GET Callback ====================== */
	/** =============================================================== */

	/*@Override
	public void didFinishWithGetRequest(String requestString, String resultString, Header[] respondHeaders) {
		super.didFinishWithGetRequest(requestString, resultString, respondHeaders);

		try {
			JSONParserTool.getMyDeal(resultString, GlobalVariable.myDealList_seller, GlobalVariable.myDealList_buyer);
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}*/


	/** =============================================================== */
	/** ===================== HTTP POST Callback ====================== */
	/** =============================================================== */

	//@Override
	//public void didFinishWithPostRequest(String requestString, String resultString, Header[] respondHeaders) {
	//	super.didFinishWithPostRequest(requestString, resultString, respondHeaders);
	//	if (requestString.equals(GlobalVariable.RATING_PRODUCT)) {
		//	if (resultString.contains("success")) {
		//		Toast.makeText(this, getString(R.string.mydeal_rating_success), Toast.LENGTH_SHORT).show();
		//		HTTP_getMyDeal();
		//	}
		//}

		/*----- get product rating -----*/
//		else if (requestString.equals(GlobalVariable.GET_SOMEONE_RATING)) {
//			try {
//				JSONParserTool.getSomeoneRating(resultString, GlobalVariable.someoneRatingObject,
//						GlobalVariable.someoneRatingList);
//
//				DialogListUtil.showSomeomeDialog(this, GlobalVariable.someoneRatingObject, someoneClickListener);
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//		}
	//}


	
	
	
	/*----- for jump to someone product -----*/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		/*----- get someone product Sell-----*/
		if (resultCode == 4) {
			setResult(4);
			finish();
		}

		/*----- get someone product Buy-----*/
		else if (resultCode == 7) {
			setResult(7);
			finish();
		}

	}
}

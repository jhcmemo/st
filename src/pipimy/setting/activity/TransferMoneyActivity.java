package pipimy.setting.activity;

import pipimy.others.Util;
import pipimy.service.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/*
 * 
 * You can transfer money from your allpay account
 * 
 */
public class TransferMoneyActivity extends Activity {

	String url = "https://payment.allpay.com.tw/BaseMember/AllPayWithdrawBalance";
	WebView webView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_transfer_money);
		
		initWeb();
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void initWeb() {
		Util.showProgressDialog(this, "", "");
		
		WebView webView = (WebView) findViewById(R.id.webView1);     
        WebSettings websettings = webView.getSettings();  
        websettings.setSupportZoom(true);  
        websettings.setBuiltInZoomControls(true);  
        websettings.setJavaScriptEnabled(true);
         
        webView.setWebViewClient(new WebViewClient(){
        	 @Override
        	public void onPageFinished(WebView view, String url) {
                Util.dismissProgressDialog();
            }
        });
        webView.loadUrl(url);  
		
	}
}

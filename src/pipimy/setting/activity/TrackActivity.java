package pipimy.setting.activity;

import java.util.ArrayList;

import org.json.JSONException;

import pipimy.main.MyProductActivity;
import pipimy.object.TrackObject;
import pipimy.others.CircleNetwork_ImageView;
import pipimy.others.CommonAdapter;
import pipimy.others.CommonViewHolder;
import pipimy.others.Constant;
import pipimy.others.DialogUtil;
import pipimy.others.DialogUtil.OnMsgInputListener;
import pipimy.others.Http;
import pipimy.others.JSONParserTool;
import pipimy.others.PreferenceUtil;
import pipimy.others.VolleySingleton;
import pipimy.others.DialogUtil.OnSearchClickListener;
import pipimy.service.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class TrackActivity extends Activity {

	private ListView mListView;
	private ArrayList<TrackObject> list;
	private TextView noTrace;
	ImageLoader mImageLoader;

	// added by Henry for testing push msg
	Button btnSendMsg;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_track);
		mListView = (ListView) findViewById(R.id.TrackActivity_listView);
		noTrace = (TextView) findViewById(R.id.no_trace_member);
		btnSendMsg = (Button) findViewById(R.id.btn_push_msg);
		btnSendMsg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//sendMsgToFans("test push msg");
				inputMessage();
			}
		});

		init();
	}

	private void init() {

		mImageLoader = VolleySingleton.getInstance().getImageLoader();

		Intent intent = getIntent();
		if (intent.getAction().equals(Constant.ACTION_TRACE_LIST)) {
			getActionBar().setTitle(getString(R.string.following));
			// HTTP_getTrackList(true);

		} else if (intent.getAction().equals(Constant.ACTION_FANS_LIST)) {
			getActionBar().setTitle(getString(R.string.followers));
			// HTTP_getFansList(true);
			Get_FansList();

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.track, menu);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setBackgroundDrawable(
		// getResources().getDrawable(R.color.style_red));

		if (getIntent().getAction().equals(Constant.ACTION_TRACE_LIST)) {
			menu.getItem(0).setVisible(true);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// case R.id.action_edit_trace:
		// GlobalVariable.editTrace *= -1;
		// adapter.notifyDataSetInvalidated();
		// return true;
		case android.R.id.home:
			// GlobalVariable.editTrace = -1;
			// adapter.notifyDataSetInvalidated();
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	protected void inputMessage() {
		String title = getResources().getString(R.string.dialog_str_title_push_msg);
		final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		DialogUtil.optInputDialog(this, title, new OnMsgInputListener(){

			@Override
			public void onMsgInputClick(View v, String message) {
				// TODO Auto-generated method stub
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				sendMsgToFans(message);
			}
			});
		
	}

	/** =============================================================== */
	/** ======================= HTTP GET Callback ===================== */
	/** =============================================================== */
	protected void sendMsgToFans(final String msg) {
		new Thread() {
			@Override
			public void run() {
				String[] params = new String[] { "Message" };
				String[] data = new String[] { msg };
				String result = Http.post(Constant.POST_MSG_TO_FANS, params,
						data, TrackActivity.this);
				Log.e("sendMsgToFans", "res = " + result);

				Message msg = new Message();
				msg.arg1 = Constant.CODE_PUSH_MSG;
				msg.obj = result;
				handler_setlistView.sendMessage(msg);
			}
		}.start();
	}

	private void Get_FansList() {

		new Thread() {
			@Override
			public void run() {

				String[] params = { "MemberID" };
				String[] data = { PreferenceUtil.getString(TrackActivity.this,
						Constant.USER_ID) };
				String result = Http.post(Constant.GET_FANS_LIST, params, data,
						TrackActivity.this);

				try {

					list = JSONParserTool.getFansList(result);
					// Get_rating_success.sendMessage(Get_rating_success.obtainMessage());

					/* ----- Get my track list ----- */
					Bundle b = new Bundle();
					Message m = new Message();
					if (list.size() == 0) {

						b.putBoolean("Have_track", false);
						m.setData(b);
						handler_setlistView.sendMessage(m);

					} else {

						b.putBoolean("Have_track", true);
						m.setData(b);
						handler_setlistView.sendMessage(m);

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}.start();

	}

	/** =============================================================== */
	/** ======================= HTTP POST Callback ===================== */
	/** =============================================================== */

	/*
	 * if (requestString.equals(Constant.POST_DELETE_TRACE)) {
	 * adapter.notifyDataSetChanged(); if( this.list.size() == 0){ TextView
	 * noTrace = (TextView)findViewById(R.id.no_trace_member);
	 * noTrace.setVisibility(View.VISIBLE); } }
	 */

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*----- login -----*/
		if (resultCode == 3) {
			setResult(3);
			finish();
		}

		/*----- get someone product Sell-----*/
		else if (resultCode == 4) {
			setResult(4);
			finish();
		}

		/*----- get someone product Buy-----*/
		else if (resultCode == 7) {
			setResult(7);
			finish();
		}

		/*----- chat start -----*/
		else if (resultCode == 5) {
			setResult(5);
			finish();
		}
	}

	/*
	 * public void refreshList(){ adapter.notifyDataSetChanged(); }
	 */

	@Override
	public void onDestroy() {
		super.onDestroy();

		System.gc();

	}

	private Handler handler_setlistView = new Handler() {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public void handleMessage(Message msg) {

			if (msg.arg1 == Constant.CODE_PUSH_MSG) {
				String str = msg.obj.toString();
				Toast.makeText(TrackActivity.this, str, 1500).show();
				
			} else {
				boolean have_track = msg.getData().getBoolean("Have_track");

				if (have_track) {
					
					noTrace.setVisibility(View.GONE);
					btnSendMsg.setVisibility(View.VISIBLE);

					mListView.setAdapter(new CommonAdapter(TrackActivity.this,
							list, R.layout.setting_tracklist) {
						@Override
						public void setViewData(
								CommonViewHolder commonViewHolder,
								View currentView, Object item) {

							final TrackObject fans_list = (TrackObject) item;

							TextView track_memberID = (TextView) commonViewHolder
									.get(commonViewHolder, currentView,
											R.id.track_memberID);

							TextView trace_time = (TextView) commonViewHolder
									.get(commonViewHolder, currentView,
											R.id.trace_time);

							CircleNetwork_ImageView track_imageView = (CircleNetwork_ImageView) commonViewHolder
									.get(commonViewHolder, currentView,
											R.id.track_imageView);

							track_imageView.setImageUrl(
									Constant.STORE_IMAGE_URL
											+ fans_list.getMemberID()
											+ ".jpg", mImageLoader);

							track_memberID.setText(fans_list.getMemberID());
							trace_time.setText(fans_list.getTime());

							mListView.setTag(fans_list);

						}
					});
					mListView.setOnItemClickListener(item_click);

				}

				else {
					btnSendMsg.setVisibility(View.GONE);
					noTrace.setVisibility(View.VISIBLE);

				}
			}

		}
	};

	private OnItemClickListener item_click = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {

			Intent intent = new Intent(TrackActivity.this,
					MyProductActivity.class);
			intent.putExtra("memberID", list.get(arg2).getMemberID());
			startActivity(intent);

		}
	};

}

package pipimy.setting.activity;

public class RatingObject {

	String ratingOrderId;
	String ratingTradeNo;
	String sellerId;
	String ratingScore;
	String ratingComment;
	String ratingTime;
	
	public String getRatingOrderId() {
		return ratingOrderId;
	}
	
	public void setRatingOrderId(String ratingOrderId) {
		this.ratingOrderId = ratingOrderId;
	}
	
	public String getRatingTradeNo() {
		return ratingTradeNo;
	}
	
	public void setRatingTradeNo(String ratingTradeNo) {
		this.ratingTradeNo = ratingTradeNo;
	}
	
	public String getSellerId() {
		return sellerId;
	}
	
	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}
	
	public String getRatingScore() {
		return ratingScore;
	}
	
	public void setRatingScore(String ratingScore) {
		this.ratingScore = ratingScore;
	}
	
	public String getRatingComment() {
		return ratingComment;
	}
	
	public void setRatingComment(String ratingComment) {
		this.ratingComment = ratingComment;
	}
	
	public String getRatingTime() {
		return ratingTime;
	}
	
	public void setRatingTime(String ratingTime) {
		this.ratingTime = ratingTime;
	}
	
	
}
